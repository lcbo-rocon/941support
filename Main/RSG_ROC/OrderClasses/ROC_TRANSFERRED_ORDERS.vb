Imports RSG_ROC.GLClasses
Imports RSG_ROC.DataLayer

Namespace OrderClasses
    Public Class ROC_TRANSFERRED_ORDERS
        Inherits REC_LCBO_TRANSFERRED_ORDERS

        Private lstJEReturnPayemnts As ArrayList
        Public Property JE_Return_Payments() As ArrayList
            Get
                Return lstJEReturnPayemnts
            End Get
            Set(ByVal value As ArrayList)
                lstJEReturnPayemnts = value
            End Set
        End Property

        Private lstJENewOrderPayemnts As ArrayList
        Public Property JE_New_Order_Payments() As ArrayList
            Get
                Return lstJENewOrderPayemnts
            End Get
            Set(ByVal value As ArrayList)
                lstJENewOrderPayemnts = value
            End Set
        End Property

        Sub New()
            MyBase.New()
            JE_Return_Payments = New ArrayList
            JE_New_Order_Payments = New ArrayList
        End Sub

        Overloads Sub Insert(ByVal oraCmd As OracleClient.OracleCommand)
            MyBase.Insert(oraCmd)
            For Each je As ROC_JOURNAL_ENTRY In JE_Return_Payments
                je.Insert(oraCmd)
            Next
            For Each je As ROC_JOURNAL_ENTRY In JE_New_Order_Payments
                je.Insert(oraCmd)
            Next
        End Sub

        Public ReadOnly Property Orig_JE_Payments() As DataTable
            Get
                Dim tbl As New DataTable
                Dim dr As DataRow = Nothing

                tbl.Columns.Add("TRANS_NO")
                tbl.Columns.Add("ORIG_ORDER_NO")
                tbl.Columns.Add("ORIG_CUST_NO")
                tbl.Columns.Add("TENDER_GL")
                tbl.Columns.Add("TENDER_AMOUNT")
                tbl.Columns.Add("COMMENTS")


                For Each JE As ROC_JOURNAL_ENTRY In JE_Return_Payments
                    For Each GL As REC_ON_GLTRAN In JE.GL_Records
                        If Not (GL.gl_cd = "14255" Or GL.gl_cd = "14240") Then
                            dr = tbl.NewRow
                            dr("TRANS_NO") = GL.gl_trans_no
                            dr("ORIG_ORDER_NO") = GL.gl_memo
                            dr("ORIG_CUST_NO") = JE.JE_Record.je_cust_no
                            dr("TENDER_GL") = GL.gl_cd
                            dr("TENDER_AMOUNT") = GL.gl_dr_amount - GL.gl_cr_amount
                            dr("COMMENTS") = JE.JE_Record.je_comment
                            tbl.Rows.Add(dr)
                        End If
                    Next
                Next

                Return tbl

            End Get
        End Property

        Public ReadOnly Property New_Order_JE_Payments() As DataTable
            Get
                Dim tbl As New DataTable
                Dim DR As DataRow = Nothing
                tbl.Columns.Add("TRANS_NO")
                tbl.Columns.Add("ORDER_NO")
                tbl.Columns.Add("CUST_NO")
                tbl.Columns.Add("TENDER_GL")
                tbl.Columns.Add("TENDER_AMOUNT")
                tbl.Columns.Add("COMMENTS")


                For Each JE As ROC_JOURNAL_ENTRY In JE_New_Order_Payments
                    For Each GL As REC_ON_GLTRAN In JE.GL_Records
                        If Not (GL.gl_cd = "14255" Or GL.gl_cd = "14240") Then
                            DR = tbl.NewRow
                            DR("TRANS_NO") = GL.gl_trans_no
                            DR("ORDER_NO") = GL.gl_memo
                            DR("CUST_NO") = JE.JE_Record.je_cust_no
                            DR("TENDER_GL") = GL.gl_cd
                            DR("TENDER_AMOUNT") = GL.gl_dr_amount - GL.gl_cr_amount
                            DR("COMMENTS") = JE.JE_Record.je_comment
                            tbl.Rows.Add(DR)
                        End If
                    Next
                Next

                Return tbl
            End Get
        End Property
        Public Sub AddJEReturnPayRecord(ByVal tender_cd As Integer, _
                                                 ByVal Pay_Amt As Decimal, _
                                                 ByVal sComments As String, _
                                                 ByVal sPostDt As String, _
                                                 ByVal sLinkDt As String, _
                                                 ByVal sLinkTransNo As String, _
                                                 ByVal sEmpno As String, _
                                                 ByRef cmdOracle As OracleClient.OracleCommand)

            Dim JE As ROC_JOURNAL_ENTRY = Nothing

            Try
                JE = DALRoc.CreateJEPayRecord(Me.orig_order_no, Me.orig_cu_no, tender_cd, Pay_Amt, _
                                                   sComments, sPostDt, sLinkDt, _
                                                   sLinkTransNo, sEmpno)

                If JE Is Nothing Then
                    Throw New Exception("Payment entry for order: " & Me.cr_order_no & "," & Me.cr_cu_no & " could not be created!")
                End If

                JE.SetTransNo(DALRoc.getNewGLTransNo(cmdOracle))
                JE.AR_SubLedger.ar_rec_no = DALRoc.getNextARRecNo(Me.cr_order_no, cmdOracle)
                JE.AR_SubLedger.ar_trans_type = "D"

                JE_Return_Payments.Add(JE)

            Catch ex As Exception
                Throw New Exception("ROC_TRANSFERRED_ORDERS.AddJEReturnPayRecord(" & Me.cr_order_no & "," & Me.cr_cu_no & "," & _
                                                         tender_cd & "," & Pay_Amt & "," & sComments & "," & sPostDt & "," & _
                                                          sLinkDt & "," & sLinkTransNo & "," & sEmpno & ")" & vbNewLine & _
                                                      "Err Msg: " & ex.Message.ToString)
            End Try


        End Sub

        Public Sub AddJENewOrderPayRecord(ByVal tender_cd As Integer, _
                                                        ByVal Pay_Amt As Decimal, _
                                                        ByVal sComments As String, _
                                                        ByVal sPostDt As String, _
                                                        ByVal sLinkDt As String, _
                                                        ByVal sLinkTransNo As String, _
                                                        ByVal sEmpno As String, _
                                                        ByRef cmdOracle As OracleClient.OracleCommand)

            Dim JE As ROC_JOURNAL_ENTRY = Nothing

            Try
                JE = DALRoc.CreateJEPayRecord(Me.order_no, Me.cu_no, tender_cd, Pay_Amt, _
                                                   sComments, sPostDt, sLinkDt, _
                                                   sLinkTransNo, sEmpno)


                If JE Is Nothing Then
                    Throw New Exception("Payment entry for order: " & Me.order_no & "," & Me.cu_no & " could not be created!")
                End If

                JE.SetTransNo(DALRoc.getNewGLTransNo(cmdOracle))
                JE.AR_SubLedger.ar_rec_no = DALRoc.getNextARRecNo(Me.order_no, cmdOracle)
                JE.AR_SubLedger.ar_trans_type = "C"

                JE_New_Order_Payments.Add(JE)

            Catch ex As Exception
                Throw New Exception("ROC_TRANSFERRED_ORDERS.AddJENewOrderPayRecord(" & Me.order_no & "," & Me.cu_no & "," & _
                                                         tender_cd & "," & Pay_Amt & "," & sComments & "," & sPostDt & "," & _
                                                          sLinkDt & "," & sLinkTransNo & "," & sEmpno & ")" & vbNewLine & _
                                                      "Err Msg: " & ex.Message.ToString)
            End Try


        End Sub

    End Class
End Namespace