Imports RSG_ROC.GLClasses
Imports RSG_ROC.DataLayer

Namespace OrderClasses
    Public Class ROC_RETURN
        Private objInvoice As ROC_ON_INV_CO
        Public Property Invoice() As ROC_ON_INV_CO
            Get
                Return objInvoice
            End Get
            Set(ByVal value As ROC_ON_INV_CO)
                objInvoice = value
            End Set
        End Property

        Private objCredit As ROC_CRHDR
        Public Property Credit() As ROC_CRHDR
            Get
                Return objCredit
            End Get
            Set(ByVal value As ROC_CRHDR)
                objCredit = value
            End Set
        End Property

        Private objDeliveryReturn As ROC_DELIVERY_RETURN
        Public Property Delivery_Return() As ROC_DELIVERY_RETURN
            Get
                Return objDeliveryReturn
            End Get
            Set(ByVal value As ROC_DELIVERY_RETURN)
                objDeliveryReturn = value
            End Set
        End Property

        Private sLinkTransNo As String
        Private objARRecord As REC_ON_ARTRAN
        Private objGLList As ArrayList

        Public ReadOnly Property inv_no() As Integer
            Get
                Return Invoice.Header.inv_no
            End Get
        End Property

        Public ReadOnly Property order_no() As String
            Get
                Return Invoice.Header.co_odno
            End Get
        End Property

        Public ReadOnly Property order_status_type() As String
            Get
                Return Invoice.Header.co_inv_type
            End Get
        End Property

        Public ReadOnly Property inv_dt() As Date
            Get
                Return DateTime.Parse(Invoice.Header.inv_dt)
            End Get

        End Property

        Public ReadOnly Property customer_no() As String
            Get
                Return Invoice.on_invoice.inv_cu_no
            End Get
        End Property

        Public ReadOnly Property emp_no() As String
            Get
                Return Invoice.Header.em_no
            End Get
        End Property

        Public ReadOnly Property invoice_amt() As Decimal
            Get
                Return Invoice.Header.inv_amt
            End Get
        End Property

        Public ReadOnly Property invoice_trans_no() As String
            Get
                Return Invoice.on_invoice.inv_trans_no
            End Get
        End Property

        Public ReadOnly Property delivery_charge() As Decimal
            Get
                Return Invoice.Header.delivery_chg
            End Get
        End Property

        Public ReadOnly Property delivery_tax1() As Decimal
            Get
                Return Invoice.Header.delivery_tax1
            End Get
        End Property

        Public ReadOnly Property delivery_tax2() As Decimal
            Get
                Return Invoice.Header.delivery_tax2
            End Get
        End Property

        Public ReadOnly Property Payments() As DataTable
            Get
                Dim tbl As New DataTable
                Dim dr As DataRow = Nothing

                With tbl.Columns
                    .Add("INV_NO")
                    .Add("REF_SEQ")
                    .Add("REF_NO")
                    .Add("EMP_NO")
                    .Add("TENDER_CD")
                    .Add("TENDER_TYPE")
                    .Add("PAY_AMT")
                    .Add("CREDIT_CARD_NO")
                    .Add("CREDIT_AUTH")
                    .Add("PAY_COMMENT")
                    .Add("ACC_POST_DT")
                    .Add("LINK_REF_SEQ")
                    .Add("UPLOAD_DT")
                    .Add("PAY_TRANS_NO")
                    .Add("PAY_DT")
                End With

                For Each pymt As REC_ON_INV_PAY In Me.Invoice.paymentList
                    dr = tbl.NewRow
                    dr("INV_NO") = pymt.inv_no
                    dr("REF_SEQ") = pymt.ref_seq
                    dr("REF_NO") = pymt.ref_no
                    dr("EMP_NO") = pymt.emp_no
                    dr("TENDER_CD") = pymt.tender_cd
                    dr("TENDER_TYPE") = pymt.tender_type
                    dr("PAY_AMT") = pymt.pay_amt
                    dr("CREDIT_CARD_NO") = pymt.credit_card_no
                    dr("CREDIT_AUTH") = pymt.credit_auth
                    dr("PAY_COMMENT") = pymt.pay_comment
                    dr("ACC_POST_DT") = pymt.acc_post_dt
                    dr("LINK_REF_SEQ") = pymt.link_ref_seq
                    dr("UPLOAD_DT") = pymt.upload_dt
                    dr("PAY_TRANS_NO") = pymt.pay_trans_no
                    dr("PAY_DT") = pymt.pay_dt
                    tbl.Rows.Add(dr)
                Next

                Return tbl
            End Get
        End Property

        Public ReadOnly Property OrderedItems() As DataTable
            Get
                Dim tbl As New DataTable
                Dim dr As DataRow = Nothing

                With tbl.Columns
                    .Add("inv_no")
                    .Add("co_odno")
                    .Add("co_inv_type")
                    .Add("cod_line")
                    .Add("Price")
                    .Add("extd_price")
                    .Add("ttl_cases")
                    .Add("ttl_units")
                    .Add("item")
                    .Add("packaging_code")
                    .Add("deposit_price")
                    .Add("qty")
                    .Add("item_category")
                    .Add("orig_price")
                    .Add("price_chg_comment")
                    .Add("retail_price")
                    .Add("orig_retail_price")
                End With

                For Each itm As ON_INV_CODTL_ITEM In Invoice.ItemList
                    dr = tbl.NewRow
                    dr("inv_no") = itm.ON_INV_CODTL.inv_no
                    dr("co_odno") = itm.ON_INV_CODTL.co_odno
                    dr("co_inv_type") = itm.ON_INV_CODTL.co_inv_type
                    dr("cod_line") = itm.ON_INV_CODTL.cod_line
                    dr("Price") = itm.ON_INV_CODTL.Price
                    dr("extd_price") = itm.ON_INV_CODTL.extd_price
                    dr("ttl_cases") = itm.ON_INV_CODTL.ttl_cases
                    dr("ttl_units") = itm.ON_INV_CODTL.ttl_units
                    dr("item") = itm.ON_INV_CODTL.item
                    dr("packaging_code") = itm.ON_INV_CODTL.packaging_code
                    dr("deposit_price") = itm.ON_INV_CODTL.deposit_price
                    dr("qty") = itm.ON_INV_CODTL.qty
                    dr("item_category") = itm.ON_INV_CODTL.item_category
                    dr("orig_price") = itm.ON_INV_CODTL.orig_price
                    dr("price_chg_comment") = itm.ON_INV_CODTL.price_chg_comment
                    dr("retail_price") = itm.ON_INV_CODTL.retail_price
                    dr("orig_retail_price") = itm.ON_INV_CODTL.orig_retail_price
                    tbl.Rows.Add(dr)
                Next

                Return tbl
            End Get
        End Property

        Public ReadOnly Property Discount_Total() As Decimal
            Get
                Dim dDisc As Decimal = 0
                For Each itm As ON_INV_CODTL_ITEM In Invoice.ItemList
                    dDisc = dDisc + itm.discount
                Next
                Return dDisc
            End Get
        End Property

        Public ReadOnly Property Markup_Total() As Decimal
            Get
                Dim dLMK As Decimal = 0
                For Each itm As ON_INV_CODTL_ITEM In Invoice.ItemList
                    dLMK = dLMK + itm.markup
                Next
                Return dLMK
            End Get
        End Property

        Public ReadOnly Property Levy_Total() As Decimal
            Get
                Dim dLevy As Decimal = 0
                For Each itm As ON_INV_CODTL_ITEM In Invoice.ItemList
                    dLevy = dLevy + itm.levy
                Next
                Return dLevy
            End Get
        End Property

        Public ReadOnly Property HST_Total() As Decimal
            Get
                Dim dHST As Decimal = 0
                For Each itm As ON_INV_CODTL_ITEM In Invoice.ItemList
                    dHST = dHST + itm.hst
                Next
                Return dHST
            End Get
        End Property

        Public ReadOnly Property HST_Rounding_Total() As Decimal
            Get
                Dim dHST_Rounding As Decimal = 0
                For Each itm As ON_INV_CODTL_ITEM In Invoice.ItemList
                    dHST_Rounding = dHST_Rounding + itm.hst_rounding
                Next
                Return dHST_Rounding
            End Get
        End Property

        Public ReadOnly Property Extended_Amount_Total() As Decimal
            Get
                Dim dExtended_Amount As Decimal = 0
                For Each itm As ON_INV_CODTL_ITEM In Invoice.ItemList
                    dExtended_Amount = dExtended_Amount + itm.ON_INV_CODTL.extd_price
                Next
                Return dExtended_Amount
            End Get
        End Property

        Public ReadOnly Property Link_Order_No() As String
            Get
                Return Invoice.Header.link_co_odno
            End Get
        End Property
        Public ReadOnly Property Order_Type() As String
            Get
                Return Credit.Header.order_type
            End Get
        End Property
        Sub New()
            Invoice = New ROC_ON_INV_CO
            Credit = New ROC_CRHDR
            Delivery_Return = New ROC_DELIVERY_RETURN

            objARRecord = New REC_ON_ARTRAN
            objGLList = New ArrayList

        End Sub

        Sub New(ByVal sOrderNum As String)
            Me.New()

            Dim objOrder As New ROC_ORDER
            objOrder.CopyOrderInfo(sOrderNum, "A")
            Dim origTransNo As String = objOrder.invoice_trans_no

            Me.Delivery_Return.SetInfo(objOrder)
            Me.Invoice = objOrder.Invoice

            If Delivery_Return.total_delivery_charge <> 0D Then
                Me.Invoice.Header.inv_amt = (Me.Invoice.Header.inv_amt - Delivery_Return.total_delivery_charge)
            End If

            'Need to convert invoice qty to units as the return modules does not calculate cases properly
            For Each itm As ON_INV_CODTL_ITEM In Me.Invoice.ItemList
                itm.ON_INV_CODTL.ttl_cases = 0
                itm.ON_INV_CODTL.ttl_units = itm.ON_INV_CODTL.qty
            Next

            'return with credit note payment
            Me.Invoice.paymentList.Clear()
            Dim pymt As REC_ON_INV_PAY = DALRoc.getCreditPaymentRecord(Invoice.Header.inv_amt)
            pymt.acc_post_dt = Now.ToString("yyyy-MM-dd HH:mm:ss")
            pymt.upload_dt = pymt.acc_post_dt
            Me.Invoice.paymentList.Add(pymt)

            Me.setCustomerNo(objOrder.PickedOrder.Header.cu_no)
            Me.SetOrderStatusType("R")
            Me.Credit.Header.order_type = objOrder.PickedOrder.Header.order_type
            Me.Invoice.Header.link_co_odno = objOrder.Invoice.Header.co_odno
            Me.Invoice.Header.acc_post_dt = Now.ToString("yyyy-MM-dd HH:mm:ss")
            Me.Invoice.Header.upload1_dt = ""

            With Me.Credit.Header
                .cr_status = "I"
                .cu_no = objOrder.PickedOrder.Header.cu_no
                .orig_odno = objOrder.Invoice.Header.co_odno
            End With

            For Each itm As ON_INV_CODTL_ITEM In objOrder.Invoice.ItemList
                Dim objReturnItem As New REC_CRDTL
                With objReturnItem
                    .cr_line = itm.ON_INV_CODTL.cod_line
                    .item = itm.ON_INV_CODTL.item
                    .packaging_code = itm.ON_INV_CODTL.packaging_code
                    .expected_qty = itm.ON_INV_CODTL.qty
                    .received_qty = itm.ON_INV_CODTL.qty
                    .confirm_qty = itm.ON_INV_CODTL.qty
                End With
                Me.Credit.AddItem(objReturnItem)
            Next

            'set argl tables
            Dim drConfig As DataRow = DALRoc.getONConfigGL
            Dim sARGL As String = drConfig("AR_GL")
            Dim sDeliveryChargeGL As String = drConfig("DELIVERY_CHG_GL")
            Dim sDeliveryTax1GL As String = drConfig("DELIVERY_TAX1_GL")
            Dim sDeliveryTax2GL As String = drConfig("DELIVERY_TAX2_GL")

            Dim drARData As DataRow = DALRoc.getARTRAN(origTransNo)
            Me.objARRecord.SetInfo(drARData)
            '  Me.objARRecord.ar_link_trans_no = objARRecord.ar_trans_no

            Me.objARRecord.ar_trans_no = DALRoc.getNewGLTransNo(DALRoc.getOraCmd)
            Me.objARRecord.ar_date = Now.ToString("yyyy-MM-dd")
            Me.Invoice.on_invoice.inv_trans_no = objARRecord.ar_trans_no
            Me.objARRecord.ar_rec_no = DALRoc.getNextARRecNo(objOrder.order_no, DALRoc.getOraCmd)

            'this step only if delivery charge needs to write as a journal entry 
            objARRecord.ar_cr_amount = Invoice.Header.inv_amt 'delivery charge deducted above already
            objARRecord.ar_dr_amount = 0

            Dim dtGLData As DataTable = DALRoc.getGLTRAN(origTransNo)

            Dim objTmpGL As REC_ON_GLTRAN = Nothing
            Dim bValid As Boolean = True
            Dim tmpGLVal As Decimal = 0
            For Each dr As DataRow In dtGLData.Rows

                If Not (dr("GL_CD").ToString.Trim = sDeliveryChargeGL.Trim Or _
                    dr("GL_CD").ToString.Trim = sDeliveryTax1GL.Trim Or _
                    dr("GL_CD").ToString.Trim = sDeliveryTax2GL.Trim) Then
                    objTmpGL = New REC_ON_GLTRAN
                    objTmpGL.SetInfo(dr)
                    objTmpGL.gl_date = objARRecord.ar_date
                    objTmpGL.gl_link_date = objARRecord.ar_link_date
                    objTmpGL.gl_trans_no = objARRecord.ar_trans_no
                    'bValid = (objTmpGL.gl_cd.Trim <> sDeliveryChargeGL.Trim)
                    'bValid = bValid AndAlso (objTmpGL.gl_cd.Trim <> sDeliveryTax2GL.Trim)
                    'swap the gl values
                    If dr("GL_CD") = sARGL.Trim Then
                        objTmpGL.gl_dr_amount = objARRecord.ar_dr_amount
                        objTmpGL.gl_cr_amount = objARRecord.ar_dr_amount
                    Else
                        tmpGLVal = objTmpGL.gl_dr_amount
                        objTmpGL.gl_dr_amount = objTmpGL.gl_cr_amount
                        objTmpGL.gl_cr_amount = tmpGLVal
                    End If

                    objGLList.Add(objTmpGL)
                End If

            Next

        End Sub

        Public Sub SetInvNo(ByVal iInv_no As Integer)
            Me.Invoice.setInvNo(iInv_no)
        End Sub

        Public Sub SetOrderNo(ByVal sCo_Odno As String)
            Me.Invoice.setOrderNo(sCo_Odno)
            Me.Credit.setOrderNo(sCo_Odno)
            Me.objARRecord.ar_ref_no = sCo_Odno
            Me.Delivery_Return.setRefNo(sCo_Odno)

            For Each GLrec As REC_ON_GLTRAN In Me.objGLList
                GLrec.gl_memo = sCo_Odno
            Next
        End Sub
        Public Sub SetOrderStatusType(ByVal sOrderStatusType As String)
            Me.Invoice.setOrderStatusType(sOrderStatusType)
        End Sub

        Public Sub SetInvDt(ByVal inv_dt As DateTime)
            Me.Invoice.setInvDt(inv_dt)
            'Me.Delivery_Return.setTransDate(inv_dt)
            Me.Credit.Header.order_dt_tm = inv_dt

            'TODO: determine should the order_dt_tm date on cohdr should be changed as well
        End Sub

        Public Sub setCustomerNo(ByVal sCu_No As String)
            Me.Invoice.on_invoice.inv_cu_no = sCu_No
            Me.Credit.Header.cu_no = sCu_No
            Me.objARRecord.ar_cust_no = sCu_No
            'Me.Delivery_Return.setCustNo(sCu_No)
        End Sub

        Public Sub setEmpNo(ByVal sEmpNo As String)
            Me.Invoice.setEmpNo(sEmpNo)
            Me.Credit.Header.em_no = sEmpNo
            Me.objARRecord.ar_user = Right(sEmpNo.Trim, 3)
            For Each glrec As REC_ON_GLTRAN In objGLList
                glrec.gl_user = Me.objARRecord.ar_user
            Next

            'Me.Delivery_Return.setGLUser(sEmpNo)
        End Sub

        Public Sub Insert(ByRef oraCmd As OracleClient.OracleCommand, Optional ByVal postReturn As Boolean = True)
            Dim inv_no As Integer = DALRoc.getNewInvNo(oraCmd)
            Dim co_odno As String = DALRoc.getNewReturnOrderNo(oraCmd)

            SetOrderNo(co_odno)
            SetInvNo(inv_no)

            Me.Invoice.Insert(oraCmd)
            Me.Credit.Insert(oraCmd)

            'Post to AR/GL as well
            If postReturn = True Then
                Me.objARRecord.Insert(oraCmd)

                For Each glrec As REC_ON_GLTRAN In objGLList
                    glrec.Insert(oraCmd)
                Next

                Dim iRecNo As Integer = Convert.ToInt16(objARRecord.ar_rec_no) + 1
                Dim ar_balance As Decimal = objARRecord.ar_balance + (Delivery_Return.AR.ar_dr_amount - Delivery_Return.AR.ar_cr_amount)

                If Me.Delivery_Return.total_delivery_charge <> 0 Then
                    Me.Delivery_Return.Insert(oraCmd, Me.inv_dt, iRecNo, objARRecord.ar_user, ar_balance)
                End If

            End If


            'If Me.Invoice.Header.delivery_chg <> 0D Then
            '    Me.Delivery_Return.AR.ar_rec_no = DALRoc.getNextARRecNo(objARRecord.ar_link_trans_no)
            '    Me.Delivery_Return.Insert(oraCmd, Now)
            'End If

        End Sub
    End Class
End Namespace