Imports RSG_ROC.DataLayer
Imports RSG_ROC.GLClasses

Namespace OrderClasses
    Public Class ROC_ORDER

        Private objInvoice As ROC_ON_INV_CO
        Public Property Invoice() As ROC_ON_INV_CO
            Get
                Return objInvoice
            End Get
            Set(ByVal value As ROC_ON_INV_CO)
                objInvoice = value
            End Set
        End Property

        Private objShipping As REC_COCUS
        Public Property ShippingInfo() As REC_COCUS
            Get
                Return objShipping
            End Get
            Set(ByVal value As REC_COCUS)
                objShipping = value
            End Set
        End Property

        Private objPickedOrder As ROC_COHDR
        Public Property PickedOrder() As ROC_COHDR
            Get
                Return objPickedOrder
            End Get
            Set(ByVal value As ROC_COHDR)
                objPickedOrder = value
            End Set
        End Property

        Public ReadOnly Property inv_no() As Integer
            Get
                Return Invoice.Header.inv_no
            End Get
        End Property

        Public ReadOnly Property order_no() As String
            Get
                Return Invoice.Header.co_odno
            End Get
        End Property

        Public ReadOnly Property order_status_type() As String
            Get
                Return Invoice.Header.co_inv_type
            End Get
        End Property

        Public ReadOnly Property inv_dt() As Date
            Get
                Return DateTime.Parse(Invoice.Header.inv_dt)
            End Get
           
        End Property

        Public ReadOnly Property customer_no() As String
            Get
                Return Invoice.on_invoice.inv_cu_no
            End Get
        End Property

        Public ReadOnly Property emp_no() As String
            Get
                Return Invoice.Header.em_no
            End Get
        End Property

        Public ReadOnly Property invoice_amt() As Decimal
            Get
                Return Invoice.Header.inv_amt
            End Get
        End Property

        Public ReadOnly Property invoice_trans_no() As String
            Get
                Return Invoice.on_invoice.inv_trans_no
            End Get
        End Property

        Public ReadOnly Property delivery_charge() As Decimal
            Get
                ' Return Invoice.Header.delivery_chg
                Return 0D
            End Get
        End Property

        Public ReadOnly Property delivery_tax1() As Decimal
            Get
                'Return Invoice.Header.delivery_tax1
                Return 0D
            End Get
        End Property

        Public ReadOnly Property delivery_tax2() As Decimal
            Get
                'Return Invoice.Header.delivery_tax2
                Return 0D
            End Get
        End Property

        Public ReadOnly Property Payments() As DataTable
            Get
                Dim tbl As New DataTable
                Dim dr As DataRow = Nothing

                With tbl.Columns
                    .Add("INV_NO")
                    .Add("REF_SEQ")
                    .Add("REF_NO")
                    .Add("EMP_NO")
                    .Add("TENDER_CD")
                    .Add("TENDER_TYPE")
                    .Add("PAY_AMT")
                    .Add("CREDIT_CARD_NO")
                    .Add("CREDIT_AUTH")
                    .Add("PAY_COMMENT")
                    .Add("ACC_POST_DT")
                    .Add("LINK_REF_SEQ")
                    .Add("UPLOAD_DT")
                    .Add("PAY_TRANS_NO")
                    .Add("PAY_DT")
                End With

                For Each pymt As REC_ON_INV_PAY In Me.Invoice.paymentList
                    dr = tbl.NewRow
                    dr("INV_NO") = pymt.inv_no
                    dr("REF_SEQ") = pymt.ref_seq
                    dr("REF_NO") = pymt.ref_no
                    dr("EMP_NO") = pymt.emp_no
                    dr("TENDER_CD") = pymt.tender_cd
                    dr("TENDER_TYPE") = pymt.tender_type
                    dr("PAY_AMT") = pymt.pay_amt
                    dr("CREDIT_CARD_NO") = pymt.credit_card_no
                    dr("CREDIT_AUTH") = pymt.credit_auth
                    dr("PAY_COMMENT") = pymt.pay_comment
                    dr("ACC_POST_DT") = pymt.acc_post_dt
                    dr("LINK_REF_SEQ") = pymt.link_ref_seq
                    dr("UPLOAD_DT") = pymt.upload_dt
                    dr("PAY_TRANS_NO") = pymt.pay_trans_no
                    dr("PAY_DT") = pymt.pay_dt
                    tbl.Rows.Add(dr)
                Next

                Return tbl
            End Get
        End Property

        Public ReadOnly Property OrderedItems() As DataTable
            Get
                Dim tbl As New DataTable
                Dim dr As DataRow = Nothing

                With tbl.Columns
                    .Add("inv_no")
                    .Add("co_odno")
                    .Add("co_inv_type")
                    .Add("cod_line")
                    .Add("Price")
                    .Add("extd_price")
                    .Add("ttl_cases")
                    .Add("ttl_units")
                    .Add("item")
                    .Add("packaging_code")
                    .Add("deposit_price")
                    .Add("qty")
                    .Add("item_category")
                    .Add("orig_price")
                    .Add("price_chg_comment")
                    .Add("retail_price")
                    .Add("orig_retail_price")
                End With

                For Each itm As ON_INV_CODTL_ITEM In Invoice.ItemList
                    dr = tbl.NewRow
                    dr("inv_no") = itm.ON_INV_CODTL.inv_no
                    dr("co_odno") = itm.ON_INV_CODTL.co_odno
                    dr("co_inv_type") = itm.ON_INV_CODTL.co_inv_type
                    dr("cod_line") = itm.ON_INV_CODTL.cod_line
                    dr("Price") = itm.ON_INV_CODTL.Price
                    dr("extd_price") = itm.ON_INV_CODTL.extd_price
                    dr("ttl_cases") = itm.ON_INV_CODTL.ttl_cases
                    dr("ttl_units") = itm.ON_INV_CODTL.ttl_units
                    dr("item") = itm.ON_INV_CODTL.item
                    dr("packaging_code") = itm.ON_INV_CODTL.packaging_code
                    dr("deposit_price") = itm.ON_INV_CODTL.deposit_price
                    dr("qty") = itm.ON_INV_CODTL.qty
                    dr("item_category") = itm.ON_INV_CODTL.item_category
                    dr("orig_price") = itm.ON_INV_CODTL.orig_price
                    dr("price_chg_comment") = itm.ON_INV_CODTL.price_chg_comment
                    dr("retail_price") = itm.ON_INV_CODTL.retail_price
                    dr("orig_retail_price") = itm.ON_INV_CODTL.orig_retail_price
                    tbl.Rows.Add(dr)
                Next

                Return tbl
            End Get
        End Property

        Public ReadOnly Property Discount_Total() As Decimal
            Get
                Dim dDisc As Decimal = 0
                For Each itm As ON_INV_CODTL_ITEM In Invoice.ItemList
                    dDisc = dDisc + itm.discount
                Next
                Return dDisc
            End Get
        End Property

        Public ReadOnly Property Markup_Total() As Decimal
            Get
                Dim dLMK As Decimal = 0
                For Each itm As ON_INV_CODTL_ITEM In Invoice.ItemList
                    dLMK = dLMK + itm.markup
                Next
                Return dLMK
            End Get
        End Property

        Public ReadOnly Property Levy_Total() As Decimal
            Get
                Dim dLevy As Decimal = 0
                For Each itm As ON_INV_CODTL_ITEM In Invoice.ItemList
                    dLevy = dLevy + itm.levy
                Next
                Return dLevy
            End Get
        End Property

        Public ReadOnly Property HST_Total() As Decimal
            Get
                Dim dHST As Decimal = 0
                For Each itm As ON_INV_CODTL_ITEM In Invoice.ItemList
                    dHST = dHST + itm.hst
                Next
                Return dHST
            End Get
        End Property

        Public ReadOnly Property HST_Rounding_Total() As Decimal
            Get
                Dim dHST_Rounding As Decimal = 0
                For Each itm As ON_INV_CODTL_ITEM In Invoice.ItemList
                    dHST_Rounding = dHST_Rounding + itm.hst_rounding
                Next
                Return dHST_Rounding
            End Get
        End Property

        Public ReadOnly Property Extended_Amount_Total() As Decimal
            Get
                Dim dExtended_Amount As Decimal = 0
                For Each itm As ON_INV_CODTL_ITEM In Invoice.ItemList
                    dExtended_Amount = dExtended_Amount + itm.ON_INV_CODTL.extd_price
                Next
                Return dExtended_Amount
            End Get
        End Property

        Public ReadOnly Property Link_Order_No() As String
            Get
                Return Invoice.Header.link_co_odno
            End Get
        End Property
        Public ReadOnly Property Order_Type() As String
            Get
                Return PickedOrder.Header.order_type
            End Get
        End Property
        Sub New()
            Invoice = New ROC_ON_INV_CO
            PickedOrder = New ROC_COHDR
            ShippingInfo = New REC_COCUS
        End Sub
        
        Sub New(ByVal sOrderNum As String, ByVal sCoInvType As String)
            Me.New()
            CopyOrderInfo(sOrderNum, sCoInvType)

        End Sub
        Sub CopyOrderInfo(ByVal sOrderNum As String, ByVal sCoInvType As String)
            Dim drInvoice As DataRow = DALRoc.getOrderHeader(sOrderNum, sCoInvType)
            Dim dtInvItems As DataTable = DALRoc.getOrderItems(sOrderNum, sCoInvType)
            Dim drShipping As DataRow = DALRoc.getCOCUSInfo(sOrderNum)
            If drShipping Is Nothing Then
                drShipping = DALRoc.getCOCUSInfo(sOrderNum, drInvoice("inv_cu_no"))
            End If
            Dim dtPayments As DataTable = Nothing
            Dim drPickedOrder As DataRow = DALRoc.getCOHDRInfo(sOrderNum)
            Dim dtPickedItems As DataTable = DALRoc.getPickedItems(sOrderNum)

            If Not drInvoice Is Nothing Then
                dtPayments = DALRoc.getPaymentList(drInvoice("INV_NO"))
                Invoice.setInfo(drInvoice, dtInvItems, dtPayments)
                PickedOrder.SetInfo(drPickedOrder, dtPickedItems)
                ShippingInfo.SetInfo(drShipping)
            End If

        End Sub

        Public Sub SetInvNo(ByVal iInv_no As Integer)
            Me.Invoice.setInvNo(iInv_no)
        End Sub

        Public Sub SetOrderNo(ByVal sCo_Odno As String)
            Me.Invoice.setOrderNo(sCo_Odno)
            Me.PickedOrder.SetOrderNo(sCo_Odno)
            Me.ShippingInfo.co_odno = sCo_Odno
        End Sub

        Public Sub SetOrderStatusType(ByVal sOrderStatusType As String)
            Me.Invoice.setOrderStatusType(sOrderStatusType)
        End Sub

        Public Sub SetInvDt(ByVal dInv_dt As DateTime)
            Me.Invoice.setInvDt(dInv_dt)
            Me.Invoice.on_invoice.inv_dt = dInv_dt
        End Sub

        Public Sub setCustomerNo(ByVal sCu_No As String)
            Me.Invoice.on_invoice.inv_cu_no = sCu_No
            Me.PickedOrder.Header.cu_no = sCu_No
            Me.PickedOrder.Header.ship_to = sCu_No

            'Need to change COCUS information when customer number is changed.
            Dim drCustShipping As DataRow = DALRoc.getCOCUSInfo(PickedOrder.Header.co_odno, sCu_No)
            Me.ShippingInfo.SetInfo(drCustShipping)

        End Sub

        Public Sub setEmpNo(ByVal sEmpNo As String)
            Me.Invoice.setEmpNo(sEmpNo)
        End Sub

        Public Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)
            Dim inv_no As Integer = DALRoc.getNewInvNo(oraCmd)
            Dim co_odno As String = DALRoc.getNewOrderNo(oraCmd)
            Invoice.setInvNo(inv_no)
            Invoice.setOrderNo(co_odno)
            PickedOrder.SetOrderNo(co_odno)
            ShippingInfo.co_odno = co_odno

            Invoice.Insert(oraCmd)
            PickedOrder.Insert(oraCmd)
            ShippingInfo.Insert(oraCmd)

        End Sub
    End Class
End Namespace