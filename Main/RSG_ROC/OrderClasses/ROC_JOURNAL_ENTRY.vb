Imports RSG_ROC.GLClasses
Imports RSG_ROC.OrderClasses
Imports RSG_ROC.DataLayer

Namespace GLClasses
    Public Class ROC_JOURNAL_ENTRY


        Private objJERecord As REC_ON_JETRAN
        Public Property JE_Record() As REC_ON_JETRAN
            Get
                Return objJERecord
            End Get
            Set(ByVal value As REC_ON_JETRAN)
                objJERecord = value
            End Set
        End Property

        Private objARRecord As REC_ON_ARTRAN
        Public Property AR_SubLedger() As REC_ON_ARTRAN
            Get
                Return objARRecord
            End Get
            Set(ByVal value As REC_ON_ARTRAN)
                objARRecord = value
            End Set
        End Property

        Private lstGLEntries As ArrayList
        Public Property GL_Records() As ArrayList
            Get
                Return lstGLEntries
            End Get
            Set(ByVal value As ArrayList)
                lstGLEntries = value
            End Set
        End Property

        Sub New()
            JE_Record = New REC_ON_JETRAN
            AR_SubLedger = New REC_ON_ARTRAN
            GL_Records = New ArrayList
        End Sub

        Public Sub SetTransNo(ByVal sTransNo As String)
            JE_Record.je_trans_no = sTransNo.Trim
            AR_SubLedger.ar_trans_no = sTransNo.Trim

            For Each GLRec As REC_ON_GLTRAN In GL_Records
                GLRec.gl_trans_no = sTransNo
            Next
        End Sub

        Public Sub SetCustomer(ByVal sCU_NO As String)
            JE_Record.je_cust_no = sCU_NO
            AR_SubLedger.ar_cust_no = sCU_NO
        End Sub

        Public Sub SetEmpNo(ByVal sEmp_No As String)
            AR_SubLedger.ar_user = Right(sEmp_No.Trim, 3)
        End Sub

        Public Sub setOrderNum(ByVal sOrderNum As String)
            AR_SubLedger.ar_ref_no = sOrderNum.Trim
            For Each GLRec As REC_ON_GLTRAN In GL_Records
                GLRec.gl_memo = sOrderNum.Trim
            Next
        End Sub

        Public Sub setPostDt(ByVal sPostDate As String)
            Dim dtm As DateTime = DateTime.Parse(sPostDate)
            JE_Record.je_post_dt = dtm.ToString("yyyy-MM-dd hh:mm:ss")
            AR_SubLedger.ar_date = dtm.ToString("yyyy-MM-dd")
            For Each GLRec As REC_ON_GLTRAN In GL_Records
                GLRec.gl_date = dtm.ToString("yyyy-MM-dd")
            Next
        End Sub

        Public Sub setLinkDt(ByVal sLinkDate As String)
            AR_SubLedger.ar_link_date = sLinkDate
            For Each GLRec As REC_ON_GLTRAN In GL_Records
                GLRec.gl_link_date = sLinkDate
            Next
        End Sub

        Public Function validate() As Boolean
            Dim bValid As Boolean = True
            Dim dAR_Amount As Decimal = 0
            Dim dGL_DR_Amount As Decimal = 0
            Dim dGL_CR_Amount As Decimal = 0

            bValid = bValid AndAlso (JE_Record.je_trans_no = AR_SubLedger.ar_trans_no)

            For Each glrec As REC_ON_GLTRAN In GL_Records
                bValid = bValid AndAlso glrec.gl_trans_no = AR_SubLedger.ar_trans_no
                If glrec.gl_cd.Trim = "14255" Or glrec.gl_cd.Trim = "14240" Then
                    dAR_Amount = glrec.gl_dr_amount - glrec.gl_cr_amount
                End If
                dGL_DR_Amount = dGL_DR_Amount + glrec.gl_dr_amount
                dGL_CR_Amount = dGL_CR_Amount + glrec.gl_cr_amount
            Next

            bValid = bValid AndAlso ((AR_SubLedger.ar_dr_amount - AR_SubLedger.ar_cr_amount) = dAR_Amount)
            bValid = bValid AndAlso (dGL_DR_Amount = dGL_CR_Amount)

            Return bValid
        End Function

        Public Sub Insert(ByRef oracmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()

            If bValid = False Then
                Throw New Exception("ROC_JOURNAL_ENTRY.INSERT - FAILED VALIDATION FOR JE: " & JE_Record.je_trans_no)
            End If

            'SET AR REC NO here
            JE_Record.Insert(oracmd)
            AR_SubLedger.Insert(oracmd)

            For Each GLRec As REC_ON_GLTRAN In GL_Records
                GLRec.Insert(oracmd)
            Next

        End Sub
    End Class
End Namespace





