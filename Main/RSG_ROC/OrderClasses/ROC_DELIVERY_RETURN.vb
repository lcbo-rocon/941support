Imports RSG_ROC.GLClasses
Imports RSG_ROC.DataLayer

Namespace OrderClasses
    Public Class ROC_DELIVERY_RETURN

        Private dDeliveryCharge As Decimal
        Public Property delivery_charge() As Decimal
            Get
                Return dDeliveryCharge
            End Get
            Set(ByVal value As Decimal)
                dDeliveryCharge = value
            End Set
        End Property

        Private dDeliveryTax1 As Decimal
        Public Property delivery_tax1() As Decimal
            Get
                Return dDeliveryTax1
            End Get
            Set(ByVal value As Decimal)
                dDeliveryTax1 = value
            End Set
        End Property

        Private dDeliveryTax2 As Decimal
        Public Property delivery_tax2() As Decimal
            Get
                Return dDeliveryTax2
            End Get
            Set(ByVal value As Decimal)
                dDeliveryTax2 = value
            End Set
        End Property

        Public ReadOnly Property total_delivery_charge() As Decimal
            Get
                Return delivery_charge + delivery_tax1 + delivery_tax2
            End Get

        End Property

        Private objON_JETRAN As REC_ON_JETRAN
        Public Property JE() As REC_ON_JETRAN
            Get
                Return objON_JETRAN
            End Get
            Set(ByVal value As REC_ON_JETRAN)
                objON_JETRAN = value
            End Set
        End Property

        Private objON_ARTRAN As REC_ON_ARTRAN
        Public Property AR() As REC_ON_ARTRAN
            Get
                Return objON_ARTRAN
            End Get
            Set(ByVal value As REC_ON_ARTRAN)
                objON_ARTRAN = value
            End Set
        End Property

        Private lstON_GLTRAN As ArrayList
        Public Property GLTranList() As ArrayList
            Get
                Return lstON_GLTRAN
            End Get
            Set(ByVal value As ArrayList)
                lstON_GLTRAN = value
            End Set
        End Property

        Private sARGL As String
        Private sDeliveryChargeGL As String
        Private sDeliveryTax1GL As String
        Private sDeliveryTax2GL As String 'HST

        Sub New()
            JE = New REC_ON_JETRAN
            AR = New REC_ON_ARTRAN
            GLTranList = New ArrayList

            Dim drGLs As DataRow = DALRoc.getONConfigGL

            delivery_charge = 0D
            delivery_tax1 = 0D
            delivery_tax2 = 0D

            sARGL = drGLs("AR_GL")
            sDeliveryChargeGL = drGLs("DELIVERY_CHG_GL")
            sDeliveryTax1GL = drGLs("DELIVERY_TAX1_GL")
            sDeliveryTax2GL = drGLs("DELIVERY_TAX2_GL")

            'Build GL records
            Dim objGL As New REC_ON_GLTRAN
            objGL.gl_cd = sARGL
            GLTranList.Add(objGL)

            objGL = New REC_ON_GLTRAN
            objGL.gl_cd = sDeliveryChargeGL
            GLTranList.Add(objGL)

            objGL = New REC_ON_GLTRAN
            objGL.gl_cd = sDeliveryTax2GL
            GLTranList.Add(objGL)

            'Uncomment if there is deliverytax1 is used. Currently not in use
            'objGL = New REC_ON_GLTRAN
            'objGL.gl_cd = sDeliveryTax1GL
            'GLTranList.Add(objGL)



        End Sub

        Sub New(ByVal objOrder As ROC_ORDER)
            Me.New()
            SetInfo(objOrder)
        End Sub

        Sub SetInfo(ByVal objOrder As ROC_ORDER)
            Dim drAR As DataRow = DALRoc.getARTRAN(objOrder.Invoice.on_invoice.inv_trans_no)
            With AR
                .ar_link_trans_no = drAR("AR_LINK_TRANS_NO")
                .ar_link_date = drAR("AR_LINK_DATE")
                .ar_trans_type = "I"
            End With

            For Each gl As REC_ON_GLTRAN In GLTranList
                gl.gl_link_date = drAR("AR_LINK_DATE")
            Next

            setRefNo(objOrder.Invoice.Header.co_odno)
            setCustNo(objOrder.PickedOrder.Header.cu_no)
            setDeliveryChargeAmount(objOrder.Invoice.Header.delivery_chg, _
                                    objOrder.Invoice.Header.delivery_tax1, _
                                    objOrder.Invoice.Header.delivery_tax2)
        End Sub
        Sub setTranNo(ByVal sTransNo As String)
            JE.je_trans_no = sTransNo
            AR.ar_trans_no = sTransNo

            For Each gl As REC_ON_GLTRAN In GLTranList
                gl.gl_trans_no = sTransNo
            Next

        End Sub

        Sub setRefNo(ByVal sRefNo As String)
            JE.je_comment = "Delivery Charge return for Order: " & sRefNo.Trim

            AR.ar_ref_no = sRefNo

            For Each gl As REC_ON_GLTRAN In GLTranList
                gl.gl_memo = sRefNo
            Next
        End Sub

        Sub setCustNo(ByVal sCustNo As String)
            AR.ar_cust_no = sCustNo
            JE.je_cust_no = sCustNo
        End Sub

        Sub setTransDate(ByVal dtmTransDate As DateTime)
            JE.je_post_dt = dtmTransDate
            AR.ar_date = dtmTransDate.ToString("yyyy-MM-dd")
            For Each gl As REC_ON_GLTRAN In GLTranList
                gl.gl_date = AR.ar_date
            Next
        End Sub

        Sub setGLUser(ByVal sUser As String)
            AR.ar_user = Right(sUser.Trim, 3)
            For Each gl As REC_ON_GLTRAN In GLTranList
                gl.gl_user = Right(sUser.Trim, 3)
            Next
        End Sub


        Sub setDeliveryChargeAmount(ByVal dDeliveryAmt As Decimal, ByVal dDeliveryTax1 As Decimal, ByVal dDeliveryTax2 As Decimal)
            AR.ar_cr_amount = Decimal.Round(dDeliveryAmt + dDeliveryTax1 + dDeliveryTax2, 2)
            JE.je_total = AR.ar_cr_amount

            delivery_charge = dDeliveryAmt
            delivery_tax1 = dDeliveryTax1
            delivery_tax2 = dDeliveryTax2

            For Each gl As REC_ON_GLTRAN In GLTranList
                If gl.gl_cd.Trim = sDeliveryChargeGL.Trim Then
                    gl.gl_dr_amount = delivery_charge
                End If
                If gl.gl_cd.Trim = sDeliveryTax1GL.Trim Then
                    gl.gl_dr_amount = delivery_tax1
                End If
                If gl.gl_cd.Trim = sDeliveryTax2GL.Trim Then
                    gl.gl_dr_amount = delivery_tax2
                End If
                If gl.gl_cd.Trim = sARGL.Trim Then
                    gl.gl_cr_amount = AR.ar_cr_amount
                End If
            Next
        End Sub

        Sub Insert(ByRef OraCmd As OracleClient.OracleCommand, ByVal dtTransDate As Date, ByVal sARRecNo As String, ByVal sARUser As String, ByVal dAr_Balance As Decimal)
            Dim iNewTransNo As Integer = 0

            If delivery_charge <> 0 Then
                Try
                    iNewTransNo = DALRoc.getNewGLTransNo(OraCmd)
                    AR.ar_rec_no = sARRecNo.PadLeft(2, "0")
                    AR.ar_balance = dAr_Balance

                    Me.setGLUser(sARUser)
                    setTranNo(iNewTransNo.ToString)
                    setTransDate(dtTransDate)
                    JE.Insert(OraCmd)
                    AR.Insert(OraCmd)

                    For Each gl As REC_ON_GLTRAN In GLTranList
                        gl.Insert(OraCmd)
                    Next
                Catch ex As Exception
                    Throw New Exception("ROC_DELIVERY_RETURN.Insert error->" & ex.Message.ToString)
                End Try
            End If

        End Sub


    End Class
End Namespace