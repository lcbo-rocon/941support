Namespace DataLayer
    Public Class ROC_CRHDR

        Private objREC_CRHDR As REC_CRHDR
        Public Property Header() As REC_CRHDR
            Get
                Return objREC_CRHDR
            End Get
            Set(ByVal value As REC_CRHDR)
                objREC_CRHDR = value
            End Set
        End Property


        Private lstREC_CRDTL As ArrayList
        Public Property ReturnItemList() As ArrayList
            Get
                Return lstREC_CRDTL
            End Get
            Set(ByVal value As ArrayList)
                lstREC_CRDTL = value
            End Set
        End Property

        Sub New()
            Header = New REC_CRHDR
            ReturnItemList = New ArrayList
        End Sub

        Sub SetInfo(ByVal drHeader As DataRow, ByVal dtReturnItems As DataTable)
            Header.SetInfo(drHeader)

            For Each drItem As DataRow In dtReturnItems.Rows
                Dim objItem As New REC_CRDTL
                objItem.SetInfo(drItem)
                AddItem(objItem)
            Next
        End Sub

        Sub setOrderNo(ByVal sOrderNum As String)
            Header.cr_odno = sOrderNum

            For Each itm As REC_CRDTL In ReturnItemList
                itm.cr_odno = sOrderNum
            Next
        End Sub

        Public Function AddItem(ByVal objItem As REC_CRDTL) As Boolean
            'validation cod_line must match
            Dim bValid As Boolean = True

            bValid = bValid AndAlso Header.cr_odno.Trim = objItem.cr_odno.Trim
            If bValid Then
                'CHECK IF RECORD ALREADY ADDED 
                If ReturnItemList.Contains(objItem) Then
                    Throw New Exception("Item already exist in list.!")
                Else
                    ReturnItemList.Add(objItem)
                End If
            End If
            Return bValid
        End Function

        Public Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)
            Header.Insert(oraCmd)
            For Each itm As REC_CRDTL In ReturnItemList
                itm.Insert(oraCmd)
            Next
        End Sub
    End Class
End Namespace