Namespace DataLayer
    Public Class ROC_COHDR

        Private objCOHDR As REC_COHDR
        Public Property Header() As REC_COHDR
            Get
                Return objCOHDR
            End Get
            Set(ByVal value As REC_COHDR)
                objCOHDR = value
            End Set
        End Property

        Private lstCODTL As ArrayList
        Public Property PickedItemList() As ArrayList
            Get
                Return lstCODTL
            End Get
            Set(ByVal value As ArrayList)
                lstCODTL = value
            End Set
        End Property

        Sub New()
            Header = New REC_COHDR
            PickedItemList = New ArrayList
        End Sub

        Sub SetInfo(ByVal drHeader As DataRow)
            Header.SetInfo(drHeader)
        End Sub

        Sub SetInfo(ByVal drHeader As DataRow, ByVal dtPickedItems As DataTable)
            SetInfo(drHeader)

            For Each drPickedItem As DataRow In dtPickedItems.Rows
                Dim objItem As New REC_CODTL
                objItem.SetInfo(drPickedItem)
                AddItem(objItem)
            Next
        End Sub

        Sub SetOrderNo(ByVal sOrderNum As String)
            Header.co_odno = sOrderNum

            For Each itm As REC_CODTL In PickedItemList
                itm.co_odno = sOrderNum
            Next
        End Sub
        Public Function AddItem(ByVal objItem As REC_CODTL) As Boolean
            'validation cod_line must match
            Dim bValid As Boolean = True

            bValid = bValid AndAlso Header.co_odno = objItem.co_odno
            If bValid Then
                'CHECK IF RECORD ALREADY ADDED 
                If PickedItemList.Contains(objItem) Then
                    Throw New Exception("Item already exist in list.!")
                Else
                    PickedItemList.Add(objItem)
                End If
            End If
            Return bValid
        End Function

        Public Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)
            Header.Insert(oraCmd)
            For Each itm As REC_CODTL In PickedItemList
                itm.Insert(oraCmd)
            Next
        End Sub
    End Class
End Namespace