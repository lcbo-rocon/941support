Imports System.Collections
Imports RSG_ROC.DataLayer

Namespace OrderClasses
    Public Class ROC_ON_INV_CO
        Implements IComparable

        Private objOn_Inv_Co As REC_ON_INV_CO
        Public Property Header() As REC_ON_INV_CO
            Get
                Return objOn_Inv_Co
            End Get
            Set(ByVal value As REC_ON_INV_CO)
                objOn_Inv_Co = value
            End Set
        End Property

        Private lstON_INV_CODTL_ITEMS As ArrayList
        Public Property ItemList() As ArrayList
            Get
                Return lstON_INV_CODTL_ITEMS
            End Get
            Set(ByVal value As ArrayList)
                lstON_INV_CODTL_ITEMS = value
            End Set
        End Property

        Private objON_Invoice As REC_ON_INVOICE
        Public Property on_invoice() As REC_ON_INVOICE
            Get
                Return objON_Invoice
            End Get
            Set(ByVal value As REC_ON_INVOICE)
                If objON_Invoice Is Nothing Then
                    objON_Invoice = New REC_ON_INVOICE
                End If
                objON_Invoice = value
                'objON_Invoice.inv_no = value.inv_no
                'objON_Invoice.site_cd = value.site_cd
                'objON_Invoice.inv_dt = value.inv_dt
                'objON_Invoice.inv_status = value.inv_status
                'objON_Invoice.inv_type = value.inv_type
                'objON_Invoice.tran_dt = value.tran_dt
                'objON_Invoice.amount_paid = value.amount_paid
                'objON_Invoice.paid_fl = value.amount_paid
                'objON_Invoice.inv_trans_no = value.inv_trans_no
                'objON_Invoice.inv_cu_no = value.inv_cu_no
                'objON_Invoice.archive_dt = value.archive_dt

                'inv_no = drInvoice("INV_NO")
                'site_cd = drInvoice("SITE_CD")
                'inv_dt = drInvoice("INV_DT")
                'inv_status = drInvoice("INV_STATUS")
                'inv_type = drInvoice("INV_TYPE")
                'tran_dt = drInvoice("TRAN_DT")
                'amount_paid = drInvoice("AMOUNT_PAID")
                'paid_fl = drInvoice("PAID_FL")

                'If IsDBNull(drInvoice("INV_TRANS_NO")) = False Then
                '    inv_trans_no = drInvoice("INV_TRANS_NO")
                'End If

                'inv_cu_no = drInvoice("INV_CU_NO")
                'archive_dt = drInvoice("ARCHIVE_DT")
            End Set
        End Property


        Private lstON_INV_PAY As ArrayList
        Public Property paymentList() As ArrayList
            Get
                Return lstON_INV_PAY
            End Get
            Set(ByVal value As ArrayList)
                lstON_INV_PAY = value
            End Set
        End Property

        Private bTransferrable As Boolean
        Public Property transferrable() As Boolean
            Get
                Return bTransferrable
            End Get
            Set(ByVal value As Boolean)
                bTransferrable = value
            End Set
        End Property

        Sub New()
            Header = New REC_ON_INV_CO
            ItemList = New ArrayList
            on_invoice = New REC_ON_INVOICE
            paymentList = New ArrayList

            transferrable = True
        End Sub

        Sub setInfo(ByVal drHeader As DataRow)
            Header.setInfo(drHeader)
            on_invoice.SetInfo(drHeader)
            transferrable = IIf(drHeader("TRANSFERRABLE") = "N", False, True)
            ItemList.Clear()
            paymentList.Clear()
        End Sub

        Sub setInfo(ByVal drHeader As DataRow, ByVal dtItems As DataTable)
            setInfo(drHeader)

            For Each drItem As DataRow In dtItems.Rows
                Dim objItem As ON_INV_CODTL_ITEM = New ON_INV_CODTL_ITEM
                Dim dtRules As DataTable = DALRoc.getOrderRules(drItem("CO_ODNO"), drItem("CO_INV_TYPE"), drItem("COD_LINE"))
                objItem.SetItemInfo(drItem, dtRules)
                AddItem(objItem)
            Next
        End Sub

        Sub setInfo(ByVal drHeader As DataRow, ByVal dtItems As DataTable, ByVal dtPayments As DataTable)
            setInfo(drHeader, dtItems)
            setPayInfo(dtPayments)
        End Sub

        Sub setPayInfo(ByVal dtPayments As DataTable)
            For Each drPayment As DataRow In dtPayments.Rows
                Dim objPay As New REC_ON_INV_PAY
                objPay.SetInfo(drPayment)
                AddPayment(objPay)
            Next
        End Sub
        Public Function AddItem(ByVal objItem As ON_INV_CODTL_ITEM) As Boolean
            'validation cod_line must match
            Dim bValid As Boolean = True

            bValid = Header.inv_no = objItem.ON_INV_CODTL.inv_no
            bValid = bValid AndAlso Header.co_odno = objItem.ON_INV_CODTL.co_odno
            bValid = bValid AndAlso Header.co_inv_type = objItem.ON_INV_CODTL.co_inv_type
            If bValid Then
                'CHECK IF RECORD ALREADY ADDED 
                If ItemList.Contains(objItem) Then
                    Throw New Exception("Item already exist in list.!")
                Else
                    ItemList.Add(objItem)
                End If
            End If
            Return bValid
        End Function

        Public Function AddPayment(ByVal objPayment As REC_ON_INV_PAY) As Boolean
            'validation cod_line must match
            Dim bValid As Boolean = True

            bValid = Header.inv_no = objPayment.inv_no

            If bValid Then
                'CHECK IF RECORD ALREADY ADDED 
                If paymentList.Contains(objPayment) Then
                    Throw New Exception("Payment already exist in list.!")
                Else
                    paymentList.Add(objPayment)
                End If
            End If
            Return bValid
        End Function

        Public Sub setInvNo(ByVal iInv_No As Integer)
            Me.Header.inv_no = iInv_No
            Me.on_invoice.inv_no = iInv_No

            For Each itm As ON_INV_CODTL_ITEM In ItemList
                itm.setInvNo(iInv_No)
            Next

            For Each pay As REC_ON_INV_PAY In paymentList
                pay.inv_no = iInv_No
            Next

        End Sub

        Public Sub setOrderNo(ByVal sOrderNum As String)
            Me.Header.co_odno = sOrderNum

            For Each itm As ON_INV_CODTL_ITEM In ItemList
                itm.setOrderNo(sOrderNum)
            Next

        End Sub

        Public Sub setOrderStatusType(ByVal sOrderStatusType As String)
            Me.Header.co_inv_type = sOrderStatusType

            For Each itm As ON_INV_CODTL_ITEM In ItemList
                itm.setOrderStatusType(sOrderStatusType)
            Next

        End Sub

        Public Sub setInvDt(ByVal dtInv_Dt As DateTime)
            Me.Header.inv_dt = dtInv_Dt
            Me.on_invoice.inv_dt = dtInv_Dt
            For Each pay As REC_ON_INV_PAY In paymentList
                pay.pay_dt = dtInv_Dt
            Next
        End Sub
        Public Sub setEmpNo(ByVal sEmpNo As String)
            Me.Header.em_no = sEmpNo
            For Each pay As REC_ON_INV_PAY In paymentList
                pay.emp_no = sEmpNo
            Next
        End Sub

        Public Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)

            on_invoice.Insert(oraCmd)
            Header.Insert(oraCmd)
            For Each itm As ON_INV_CODTL_ITEM In ItemList
                itm.Insert(oraCmd)
            Next
            For Each pay As REC_ON_INV_PAY In paymentList
                pay.Insert(oraCmd)
            Next
        End Sub
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Dim b As ROC_ON_INV_CO = obj
            Return Me.Header.CompareTo(b.Header)
        End Function

    End Class
End Namespace