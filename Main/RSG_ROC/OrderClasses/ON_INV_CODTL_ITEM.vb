Imports System.Collections
Imports RSG_ROC.DataLayer

Namespace OrderClasses
    Public Class ON_INV_CODTL_ITEM
        Implements IComparable

        Private objON_INV_CODTL As REC_ON_INV_CODTL
        Public Property ON_INV_CODTL() As REC_ON_INV_CODTL
            Get
                Return objON_INV_CODTL
            End Get
            Set(ByVal value As REC_ON_INV_CODTL)
                objON_INV_CODTL = value
            End Set
        End Property

        Private lstON_INV_CODTL_RULES As ArrayList
        Public Property ON_INV_CODTL_RULES() As ArrayList
            Get
                Return lstON_INV_CODTL_RULES
            End Get
            Set(ByVal value As ArrayList)
                lstON_INV_CODTL_RULES = value
            End Set
        End Property

        Public ReadOnly Property item_no() As String
            Get
                Return ON_INV_CODTL.item
            End Get
        End Property

        Public ReadOnly Property order_line() As Integer
            Get
                Return ON_INV_CODTL.cod_line
            End Get
        End Property

        Public ReadOnly Property levy() As Decimal
            Get
                Dim dDisc As Decimal = 0
                For Each rule As REC_ON_INV_CODTL_RULE In ON_INV_CODTL_RULES
                    If rule.rule.Trim = "LEVY" Then
                        dDisc = dDisc + rule.rule_amt
                    End If
                Next
                Return dDisc
            End Get
        End Property

        Public ReadOnly Property markup() As Decimal
            Get
                Dim dLMK As Decimal = 0
                For Each rule As REC_ON_INV_CODTL_RULE In ON_INV_CODTL_RULES
                    If rule.rule.Trim = "LMK" Then
                        dLMK = dLMK + rule.rule_amt
                    End If
                Next
                Return dLMK
            End Get
        End Property

        Public ReadOnly Property discount() As Decimal
            Get
                Dim dLevy As Decimal = 0
                For Each rule As REC_ON_INV_CODTL_RULE In ON_INV_CODTL_RULES
                    If rule.rule.Trim = "DISC" Or rule.rule.Trim = "DISC_ON" Or rule.rule.Trim = "DISC_NATO" Then
                        dLevy = dLevy + rule.rule_amt
                    End If
                Next
                Return dLevy
            End Get
        End Property

        Public ReadOnly Property hst() As Decimal
            Get
                Dim dHST As Decimal = 0
                For Each rule As REC_ON_INV_CODTL_RULE In ON_INV_CODTL_RULES
                    If rule.rule.Trim = "GST" Then
                        dHST = dHST + rule.rule_amt
                    End If
                Next
                Return dHST
            End Get
        End Property

        Public ReadOnly Property hst_rounding() As Decimal
            Get
                Dim dHST_Rounding As Decimal = 0
                For Each rule As REC_ON_INV_CODTL_RULE In ON_INV_CODTL_RULES
                    If rule.rule.Trim = "PST_ALC" Then
                        dHST_Rounding = dHST_Rounding + rule.rule_amt
                    End If
                Next
                Return dHST_Rounding
            End Get
        End Property

        Sub New()
            ON_INV_CODTL = New REC_ON_INV_CODTL
            ON_INV_CODTL_RULES = New ArrayList
        End Sub

        Sub New(ByVal DR As DataRow)
            Me.New()
            SetItemInfo(DR)
        End Sub
        Sub SetItemInfo(ByVal dr As DataRow)
            ON_INV_CODTL.inv_no = dr("INV_NO")
            ON_INV_CODTL.co_odno = dr("CO_ODNO")
            ON_INV_CODTL.co_inv_type = dr("CO_INV_TYPE")
            ON_INV_CODTL.cod_line = dr("COD_LINE")
            ON_INV_CODTL.Price = dr("PRICE")
            ON_INV_CODTL.extd_price = dr("EXTD_PRICE")
            ON_INV_CODTL.ttl_cases = dr("TTL_CASES")
            ON_INV_CODTL.ttl_units = dr("TTL_UNITS")
            ON_INV_CODTL.item = dr("ITEM")
            ON_INV_CODTL.packaging_code = dr("PACKAGING_CODE")
            ON_INV_CODTL.deposit_price = dr("DEPOSIT_PRICE")
            ON_INV_CODTL.qty = dr("QTY")
            ON_INV_CODTL.item_category = dr("ITEM_CATEGORY")
            ON_INV_CODTL.orig_price = dr("ORIG_PRICE")
            ON_INV_CODTL.price_chg_comment = dr("PRICE_CHG_COMMENT")
            ON_INV_CODTL.retail_price = dr("RETAIL_PRICE")
            ON_INV_CODTL.orig_retail_price = dr("ORIG_RETAIL_PRICE")

            ON_INV_CODTL_RULES.Clear()

        End Sub

        Sub SetItemInfo(ByVal drItem As DataRow, ByVal dtRules As DataTable)
            SetItemInfo(drItem)
            For Each drRule As DataRow In dtRules.Rows
                If AddRuleItem(drRule) = False Then
                    Throw New Exception("Error adding rule item " & drRule("co_odno") & "," & drRule("co_inv_type"))
                End If
            Next
        End Sub
        Public Function ADDRuleItem(ByVal objRule As REC_ON_INV_CODTL_RULE)
            'validation cod_line must match
            Dim bValid As Boolean = True

            bValid = ON_INV_CODTL.inv_no = objRule.inv_no
            bValid = bValid AndAlso ON_INV_CODTL.co_odno = objRule.co_odno
            bValid = bValid AndAlso ON_INV_CODTL.co_inv_type = objRule.co_inv_type
            bValid = bValid AndAlso ON_INV_CODTL.cod_line = objRule.cod_line

            If bValid Then
                If ON_INV_CODTL_RULES.Contains(objRule) Then
                    Throw New Exception("Item rule already exist in list.!")
                Else
                    'if rule amt is 0 then don't bother writing to table
                    If objRule.rule_amt <> 0 Then
                        ON_INV_CODTL_RULES.Add(objRule)
                    End If
                End If
            End If

            Return bValid
        End Function

        Public Function AddRuleItem(ByVal drRule As DataRow) As Boolean
            'validation cod_line must match
            Dim bValid As Boolean = True

            bValid = ON_INV_CODTL.inv_no = drRule("INV_NO")
            bValid = bValid AndAlso ON_INV_CODTL.co_odno = drRule("CO_ODNO")
            bValid = bValid AndAlso ON_INV_CODTL.co_inv_type = drRule("CO_INV_TYPE")
            bValid = bValid AndAlso ON_INV_CODTL.cod_line = drRule("COD_LINE")

            If bValid Then
                'CHECK IF RECORD ALREADY ADDED 
                Dim objRule As New REC_ON_INV_CODTL_RULE(drRule)

                If ON_INV_CODTL_RULES.Contains(objRule) Then
                    Throw New Exception("Item rule already exist in list.!")
                Else
                    'if rule amt is 0 then don't bother writing to table
                    If objRule.rule_amt <> 0 Then
                        ON_INV_CODTL_RULES.Add(objRule)
                    End If
                End If
            End If

            Return bValid
        End Function

        Public Sub setInvNo(ByVal iInv_No As Integer)
            ON_INV_CODTL.inv_no = iInv_No

            For Each objRule As REC_ON_INV_CODTL_RULE In ON_INV_CODTL_RULES
                objRule.inv_no = iInv_No
            Next
        End Sub

        Public Sub setOrderNo(ByVal sOrderNum As String)
            sOrderNum = Left(sOrderNum & "                ", 16)
            ON_INV_CODTL.co_odno = sOrderNum

            For Each objRule As REC_ON_INV_CODTL_RULE In ON_INV_CODTL_RULES
                objRule.co_odno = sOrderNum
            Next

        End Sub

        Public Sub setOrderStatusType(ByVal sOrderStatusType As String)
            ON_INV_CODTL.co_inv_type = sOrderStatusType

            For Each objRule As REC_ON_INV_CODTL_RULE In ON_INV_CODTL_RULES
                objRule.co_inv_type = sOrderStatusType
            Next

        End Sub

        Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)
            ON_INV_CODTL.Insert(oraCmd)

            For Each objRule As REC_ON_INV_CODTL_RULE In ON_INV_CODTL_RULES
                objRule.Insert(oraCmd)
            Next

        End Sub
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Dim b As ON_INV_CODTL_ITEM = obj

            Return Me.ON_INV_CODTL.CompareTo(b.ON_INV_CODTL)

        End Function
    End Class
End Namespace
