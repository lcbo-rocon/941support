Imports Microsoft.VisualBasic
Imports System.Collections
Imports RSG_ROC.OrderClasses
Imports RSG_ROC.DataLayer

Namespace LS_TRANSFERS
    Public Class ROC_LE_SELECT_ORDER
        Inherits REC_LCBO_LE_SELECT_ORDER_HDR

        Private lstOrderDetails As SortedList
        Public Property order_details() As SortedList
            Get
                Return lstOrderDetails
            End Get
            Set(ByVal value As SortedList)
                lstOrderDetails = value
            End Set
        End Property

        Public ReadOnly Property tblOrderDetails() As DataTable
            Get
                Dim tblItms As New DataTable
                Dim drItm As DataRow = Nothing

                With tblItms.Columns
                    .Add("LE_SELECT_ID")
                    .Add("COD_LINE", GetType(System.Int32))
                    .Add("PRICE", GetType(System.Decimal))
                    .Add("EXTD_PRICE", GetType(System.Decimal))
                    .Add("TTL_CASES")
                    .Add("TTL_UNITS")
                    .Add("ITEM")
                    .Add("ITEM_DESC")
                    .Add("IS_CATCHALL")
                    .Add("CATCHALL_DESC")
                    .Add("PACKAGING_CODE")
                    .Add("DEPOSIT_PRICE", GetType(System.Decimal))
                    .Add("QTY")
                    .Add("ITEM_CATEGORY")
                    .Add("ORIG_PRICE", GetType(System.Decimal))
                    .Add("PRICE_CHG_COMMENT")
                    .Add("RETAIL_PRICE", GetType(System.Decimal))
                    .Add("ORIG_RETAIL_PRICE", GetType(System.Decimal))
                    .Add("CASE_SZ")
                    .Add("PACK_FACTOR")
                    .Add("ACTION")
                End With

                For Each itm As ROC_LE_SELECT_ORDER_ITEM In order_details.GetValueList
                    drItm = tblItms.NewRow
                    drItm("LE_SELECT_ID") = itm.le_select_id
                    drItm("COD_LINE") = itm.cod_line
                    drItm("PRICE") = itm.price
                    drItm("EXTD_PRICE") = itm.extd_price
                    drItm("TTL_CASES") = itm.ttl_cases
                    drItm("TTL_UNITS") = itm.ttl_units
                    drItm("ITEM") = itm.item
                    drItm("ITEM_DESC") = itm.item_desc
                    drItm("IS_CATCHALL") = itm.is_Catchall
                    drItm("CATCHALL_DESC") = itm.catchall_desc
                    drItm("PACKAGING_CODE") = itm.packaging_code
                    drItm("DEPOSIT_PRICE") = itm.deposit_price
                    drItm("QTY") = itm.qty
                    drItm("ITEM_CATEGORY") = itm.item_category
                    drItm("ORIG_PRICE") = itm.orig_price
                    drItm("PRICE_CHG_COMMENT") = itm.price_chg_comment
                    drItm("RETAIL_PRICE") = itm.retail_price
                    drItm("ORIG_RETAIL_PRICE") = itm.orig_retail_price
                    drItm("CASE_SZ") = itm.case_sz
                    drItm("PACK_FACTOR") = itm.pack_factor
                    drItm("ACTION") = itm.action
                    tblItms.Rows.Add(drItm)
                Next
                Return tblItms
            End Get
        End Property

        Sub New()
            MyBase.New()
            order_details = New SortedList
        End Sub

        Sub New(ByVal i_Le_Select_ID As Integer)
            MyBase.New(i_Le_Select_ID)
            order_details = New SortedList

            Dim dtItms As DataTable = DALRoc.getLeSelectOrderDetails(i_Le_Select_ID)

            Dim itm As ROC_LE_SELECT_ORDER_ITEM = Nothing
            For Each dr As DataRow In dtItms.Rows
                Dim dtRules As DataTable = DALRoc.getLSItemRules(i_Le_Select_ID, dr("COD_LINE"))
                itm = New ROC_LE_SELECT_ORDER_ITEM(dr, dtRules)
                order_details.Add(itm.cod_line, itm)
            Next

        End Sub
        Sub AddItem(ByVal item As String, _
                    ByVal qty As Integer, _
                    ByVal unit_price As Decimal, _
                    ByVal sIsCatch_all As String, _
                    ByVal sCatchall_desc As String, _
                    ByVal sPrice_Chg_Comment As String, _
                    ByRef oraCmd As OracleClient.OracleCommand)

            Dim itm As ROC_LE_SELECT_ORDER_ITEM
            Dim price As Decimal = unit_price
            Dim dr As DataRow = Nothing

            Try
                dr = DALRoc.getItemInfo(item)
                itm = New ROC_LE_SELECT_ORDER_ITEM

                If unit_price <> 0 Then
                    price = unit_price
                Else
                    price = dr("UNIT_PRICE")
                End If

                With itm
                    .le_select_id = le_select_id
                    .cod_line = GetNextCODLine()
                    .price = price
                    .extd_price = price * qty
                    .ttl_cases = qty / dr("CASE_SZ")
                    .ttl_units = qty Mod dr("CASE_SZ")
                    .item = dr("ITEM")
                    .item_desc = dr("IM_DESC")
                    .catchall_desc = sCatchall_desc
                    .is_Catchall = IIf(sCatchall_desc.Trim <> "", "Y", "N")
                    .item_category = dr("ITEM_CATEGORY")
                    .packaging_code = dr("PACKAGING_CODE")
                    .deposit_price = dr("DEPOSIT_PRICE")
                    .qty = qty
                    .orig_price = dr("UNIT_PRICE")
                    .price_chg_comment = sPrice_Chg_Comment
                    .case_sz = dr("CASE_SZ")
                    .pack_factor = dr("PACK_FACTOR")
                    .action = "A"
                End With

                'add rule for item
                itm.AddRuleItem("LIC")
                itm.setAction("A")
                order_details.Add(itm.cod_line, itm)

                itm.Update(oraCmd)
                itm.setAction("U")

                UpdateCasePartTotals()
                Me.Update(oraCmd)
            Catch ex As Exception
                Throw New Exception("ROC_LE_SELECT_ORDER.ADDITEM->" & ex.Message.ToString)
            End Try

        End Sub
        Sub AddItem(ByVal drItem As DataRow, ByVal dtRules As DataTable)
            Dim itm As ROC_LE_SELECT_ORDER_ITEM
            Try
                itm = New ROC_LE_SELECT_ORDER_ITEM(drItem, dtRules)

                itm.setCODLine(GetNextCODLine())
                order_details.Add(itm.cod_line, itm)
                itm.setAction("A")
                itm.Update(DALRoc.getOraCmd)
                itm.setAction("U")
            Catch ex As Exception
                Throw New Exception("ROC_LE_SELECT_ORDER.ADDITEM->" & ex.Message.ToString)
            End Try

        End Sub

        Sub UpdateQty(ByVal cod_line As Integer, ByVal qty As Integer)
            Dim itm As ROC_LE_SELECT_ORDER_ITEM

            Try
                itm = GetItemByCODLine(cod_line)
                itm.qty = qty
                itm.ttl_cases = qty / itm.case_sz
                itm.ttl_units = qty Mod itm.case_sz
                itm.extd_price = qty * itm.price
                UpdateCasePartTotals()

                itm.setAction("U")

                If qty = 0 Then
                    itm.setAction("D")
                Else
                    itm.UpdateRuleItem("LIC")
                End If


                Me.Update(DALRoc.getOraCmd)
            Catch ex As Exception
                Throw New Exception("ROC_LE_SELECT_ORDER.UPDATEQTY(" & cod_line & "," & qty & ")->" & ex.Message.ToString)
            End Try


        End Sub

        Sub UpdateCasePartTotals()
            Dim full_cases As Integer = 0
            Dim part_cases As Integer = 0
            Dim divisor As Integer = 12
            Dim accumulator As Integer = 0

            For Each itm As ROC_LE_SELECT_ORDER_ITEM In order_details.GetValueList
                full_cases = full_cases + itm.ttl_cases
                'calculate the part cases
                accumulator = accumulator + (itm.ttl_units * itm.pack_factor)
            Next
            part_cases = accumulator / divisor
            If (accumulator Mod divisor > 0) Then
                part_cases = part_cases + 1
            End If

            Me.ttl_full_cases = full_cases
            Me.ttl_part_cases = part_cases

        End Sub
        Function GetNextCODLine() As Integer
            Dim tbl As DataTable = Me.tblOrderDetails
            Dim maxCodLine As Integer = 0

            If tbl.Rows.Count > 0 Then
                maxCodLine = tbl.Compute("MAX(COD_LINE)", "1=1")
            End If

            Return maxCodLine + 1
        End Function
        Function GetItemByCODLine(ByVal iCOD_LINE As Integer) As ROC_LE_SELECT_ORDER_ITEM
            Return order_details.Item(iCOD_LINE)
        End Function

        'Function GetItemBySku(ByVal sItem As String) As REC_LCBO_LE_SELECT_ORDER_DTL
        '    Dim dtl As REC_LCBO_LE_SELECT_ORDER_DTL = Nothing

        '    For Each itm As REC_LCBO_LE_SELECT_ORDER_DTL In order_details.GetValueList
        '        If itm.item.Trim = sItem.Trim Then
        '            dtl = itm
        '        End If
        '    Next

        '    Return dtl
        'End Function

        Overloads Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)
            MyBase.Insert(oraCmd)
            For Each itm As ROC_LE_SELECT_ORDER_ITEM In order_details.GetValueList
                itm.Update(oraCmd)
            Next
        End Sub

        Overloads Sub Update(ByRef oraCmd As OracleClient.OracleCommand)
            MyBase.Update(oraCmd)
            For Each itm As ROC_LE_SELECT_ORDER_ITEM In order_details.GetValueList
                itm.Update(oraCmd)
            Next
        End Sub

        Sub OrderLineResequence(ByRef oraCmd As OracleClient.OracleCommand)
            Dim iOrderLine As Integer = 1

            Try
                For Each itm As ROC_LE_SELECT_ORDER_ITEM In order_details.GetValueList
                    If itm.cod_line <> iOrderLine Then
                        itm.action = "D"
                        itm.Update(oraCmd)
                        itm.action = "A"
                        itm.cod_line = iOrderLine
                        itm.Update(oraCmd)
                    End If

                    iOrderLine = iOrderLine + 1
                Next
            Catch ex As Exception
                Throw New Exception("ROC_LE_SELECT_ORDER.OrderLineResequence error: " & Me.le_select_id & vbNewLine & _
                                    "Err Msg: " & ex.Message.ToString)
            End Try

        End Sub

        Public Function CreateROCOrder(ByVal newCustNo As String) As ROC_ORDER
            Dim roOrder As New ROC_ORDER

            roOrder.SetOrderStatusType("A")
            roOrder.PickedOrder.Header.order_type = "LIC"
            roOrder.setEmpNo(emp_no)
            roOrder.setCustomerNo(newCustNo)
            roOrder.SetInvDt(Now)

            Dim roItem As ON_INV_CODTL_ITEM = Nothing
            Dim roPickedItem As REC_CODTL = Nothing
            Dim roItemRule As REC_ON_INV_CODTL_RULE = Nothing

            With roOrder.PickedOrder.Header
                .order_dt_tm = Now
                .order_reqd_dt = Now.ToString("yyyy-MM-dd")
                .end_of_order_fl = "Y"
                .pu_order_fl = "Y"
                .order_canceled_y_or_n = "Y"
                .route_code = "9999"
            End With

            For Each objItem As ROC_LE_SELECT_ORDER_ITEM In Me.order_details.GetValueList

                roItem = New ON_INV_CODTL_ITEM

                With roItem.ON_INV_CODTL
                    '.inv_no = inv_no
                    .co_inv_type = "A"
                    '.co_odno = order_no
                    .item = objItem.item
                    .cod_line = objItem.cod_line
                    .Price = objItem.price
                    .extd_price = objItem.extd_price
                    .ttl_cases = objItem.ttl_cases
                    .ttl_units = objItem.ttl_units
                    .item = objItem.item
                    .packaging_code = objItem.packaging_code
                    .deposit_price = objItem.deposit_price
                    .qty = objItem.qty
                    .item_category = objItem.item_category
                    .orig_price = objItem.orig_price
                    .price_chg_comment = objItem.price_chg_comment
                    .retail_price = objItem.retail_price
                    .orig_retail_price = objItem.retail_price
                End With

                For Each rule As REC_LCBO_LE_SELECT_ORDER_DTL_RULE In objItem.item_rule_list
                    roItemRule = New REC_ON_INV_CODTL_RULE
                    With roItemRule
                        '   .inv_no = inv_no
                        .co_inv_type = "A"
                        '  .co_odno = order_no
                        .cod_line = rule.cod_line
                        .rate = rule.rate
                        .rule = rule.rule
                        .rule_gr = rule.rule_gr
                        .rate_cd = rule.rate_cd
                        .rule_amt = rule.rule_amt
                        .item_category = rule.item_category
                    End With
                    roItem.ADDRuleItem(roItemRule)
                Next
                roOrder.Invoice.AddItem(roItem)

                roPickedItem = New REC_CODTL

                With roPickedItem
                    '.co_odno = order_no
                    .cod_line = objItem.cod_line
                    .cod_qty = objItem.qty
                    .deleted_fl = "N"
                    .item = objItem.item
                    .pick_planned_y_or_n = "N"
                    .picked_qty = objItem.qty
                    .planned_qty = objItem.qty
                    .picked_cases = objItem.ttl_cases
                    .picked_units = objItem.ttl_units
                End With

                roOrder.PickedOrder.AddItem(roPickedItem)

            Next

            'Need to calculate invoice amount - Discount total is negative value already
            Dim inv_amt As Decimal = roOrder.Extended_Amount_Total + roOrder.Discount_Total + roOrder.Markup_Total + roOrder.HST_Total + roOrder.HST_Rounding_Total
            roOrder.Invoice.Header.inv_amt = inv_amt

            roOrder.Invoice.paymentList.Clear()
            'Add payment - to cash
            Dim pymt As REC_ON_INV_PAY = New REC_ON_INV_PAY
            With pymt
                '.inv_no = roOrder.inv_no
                .ref_seq = 1
                .ref_no = 1
                .pay_amt = roOrder.invoice_amt
                .tender_cd = "15"
                .tender_type = "15-Cash"
                .pay_dt = Now
                .pay_comment = "LICENSEE LOCATION TRANSFER"
                .emp_no = emp_no
                .link_ref_seq = 1
            End With
            roOrder.Invoice.paymentList.Add(pymt)

            Return roOrder
        End Function

        Public Function CreateROCReturn(ByVal origCustNo As String) As ROC_RETURN
            Dim rrOrder As New ROC_RETURN

            rrOrder.SetOrderStatusType("R")
            rrOrder.Credit.Header.order_type = "LIC"
            rrOrder.setEmpNo(emp_no)
            rrOrder.setCustomerNo(origCustNo)
            rrOrder.SetInvDt(Now)

            Dim rrItem As ON_INV_CODTL_ITEM = Nothing
            Dim rrItemRule As REC_ON_INV_CODTL_RULE = Nothing
            Dim rrReturnItem As REC_CRDTL = Nothing

            For Each objItem As ROC_LE_SELECT_ORDER_ITEM In Me.order_details.GetValueList

                rrItem = New ON_INV_CODTL_ITEM

                With rrItem.ON_INV_CODTL
                    ' .inv_no = inv_no
                    .co_inv_type = "R"
                    '.co_odno = order_no
                    .item = objItem.item
                    .cod_line = objItem.cod_line
                    .Price = objItem.price
                    .extd_price = objItem.extd_price
                    .ttl_cases = objItem.ttl_cases
                    .ttl_units = objItem.ttl_units
                    .item = objItem.item
                    .packaging_code = objItem.packaging_code
                    .deposit_price = objItem.deposit_price
                    .qty = objItem.qty
                    .item_category = objItem.item_category
                    .orig_price = objItem.orig_price
                    .price_chg_comment = objItem.price_chg_comment
                    .retail_price = objItem.retail_price
                    .orig_retail_price = objItem.retail_price
                End With

                For Each rule As REC_LCBO_LE_SELECT_ORDER_DTL_RULE In objItem.item_rule_list
                    rrItemRule = New REC_ON_INV_CODTL_RULE
                    With rrItemRule
                        '   .inv_no = inv_no
                        .co_inv_type = "R"
                        '  .co_odno = order_no
                        .cod_line = rule.cod_line
                        .rate = rule.rate
                        .rule = rule.rule
                        .rate_cd = rule.rate_cd
                        .rule_gr = rule.rule_gr
                        .rule_amt = rule.rule_amt
                        .item_category = rule.item_category
                    End With
                    rrItem.ADDRuleItem(rrItemRule)
                Next

                rrOrder.Invoice.AddItem(rrItem)
                'add to pick order
                rrReturnItem = New REC_CRDTL

                With rrReturnItem
                    '.cr_odno = order_no
                    .cr_line = objItem.cod_line
                    .item = objItem.item
                    .packaging_code = objItem.packaging_code
                    .expected_qty = objItem.qty
                    .received_qty = objItem.qty
                    .confirm_qty = objItem.qty
                End With
                rrOrder.Credit.AddItem(rrReturnItem)
            Next

            'Need to calculate invoice amount - Discount total is negative value already
            Dim inv_amt As Decimal = rrOrder.Extended_Amount_Total + rrOrder.Discount_Total + rrOrder.Markup_Total + rrOrder.HST_Total + rrOrder.HST_Rounding_Total
            rrOrder.Invoice.Header.inv_amt = inv_amt
            With rrOrder.Credit.Header
                ' .cr_odno = order_no
                .cr_status = "I"
                .cu_no = ""
                '.orig_odno = rrOrder.customer_no
                .order_dt_tm = Now
                .em_no = emp_no
                .freight_fl = "N"
                .order_type = "LIC"
                .delivery_fl = "N"
            End With

            rrOrder.Invoice.paymentList.Clear()
            'Add payment - to cash
            Dim pymt As REC_ON_INV_PAY = New REC_ON_INV_PAY
            With pymt
                '.inv_no = rrOrder.inv_no
                .ref_seq = 1
                .ref_no = 1
                .pay_amt = rrOrder.invoice_amt
                .tender_cd = "15"
                .tender_type = "15-Cash"
                .pay_dt = Now
                .pay_comment = "LICENSEE LOCATION TRANSFER"
                .emp_no = emp_no
                .link_ref_seq = 1
            End With
            rrOrder.Invoice.paymentList.Add(pymt)

            rrOrder.SetOrderStatusType("R")
            rrOrder.Credit.Header.order_type = "LIC"
            rrOrder.setEmpNo(emp_no)
            rrOrder.setCustomerNo(origCustNo)
            rrOrder.SetInvDt(Now)

            Return rrOrder
        End Function
    End Class
End Namespace
