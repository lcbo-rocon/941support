Imports Microsoft.VisualBasic
Imports System.Collections
Imports RSG_ROC.DataLayer
Imports RSG_ROC.LS_TRANSFERS
Imports RSG_ROC.GLClasses
Imports System.IO

Namespace LS_TRANSFERS
    Public Class ROC_LE_SELECT_ORDER_ITEM
        Inherits REC_LCBO_LE_SELECT_ORDER_DTL


        Private lsRuleItems As ArrayList
        Public Property item_rule_list() As ArrayList
            Get
                Return lsRuleItems
            End Get
            Set(ByVal value As ArrayList)
                lsRuleItems = value
            End Set
        End Property

        Sub setAction(ByVal act As String)
            Me.action = act
            For Each rule As REC_LCBO_LE_SELECT_ORDER_DTL_RULE In item_rule_list
                rule.action = act
            Next
        End Sub

        Sub setCODLine(ByVal line As Integer)
            Me.cod_line = line
            For Each rule As REC_LCBO_LE_SELECT_ORDER_DTL_RULE In item_rule_list
                rule.cod_line = line
            Next
        End Sub

        Sub New()
            MyBase.New()
            item_rule_list = New ArrayList
        End Sub

        Sub New(ByVal drItem As DataRow, ByVal dtRules As DataTable)
            MyBase.New(drItem)
            item_rule_list = New ArrayList
            Dim lsRules As REC_LCBO_LE_SELECT_ORDER_DTL_RULE = Nothing

            For Each drRule As DataRow In dtRules.Rows
                lsRules = New REC_LCBO_LE_SELECT_ORDER_DTL_RULE(drRule)
                item_rule_list.Add(lsRules)
            Next
        End Sub
        Sub AddRuleItem(ByVal order_type As String)
            Dim dtItemCategoryRules As DataTable = DALRoc.getItemCategoryOrderRules(Me.item_category, order_type)
            Dim basic_less_deposit As Decimal = Me.price - Me.deposit_price
            Dim discount As Decimal = 0
            Dim markup As Decimal = 0
            Dim levy As Decimal = 0
            Dim HST As Decimal = 0
            Dim objRule As REC_LCBO_LE_SELECT_ORDER_DTL_RULE

            If Me.price <> Me.orig_price Then
                objRule = New REC_LCBO_LE_SELECT_ORDER_DTL_RULE

                With objRule
                    .le_select_id = Me.le_select_id
                    .cod_line = Me.cod_line
                    .rate = 0D
                    .rule = "OVERRIDE"
                    .rate_cd = "O"
                    .rule_amt = (Me.price - Me.orig_price) * Me.qty
                    .item_category = Me.item_category
                    .rule_gr = "OVERRIDE"
                    .action = "A"
                End With
                Me.item_rule_list.Add(objRule)
            End If
            'Loop through and get the proper values for calculations first
            For Each drRule As DataRow In dtItemCategoryRules.Select("RULE_GR = 'DISCOUNT  '")
                discount = Math.Round((drRule("RATE") / 100 * basic_less_deposit), 2, MidpointRounding.AwayFromZero)
                objRule = New REC_LCBO_LE_SELECT_ORDER_DTL_RULE

                With objRule
                    .le_select_id = le_select_id
                    .cod_line = Me.cod_line
                    .rate = drRule("RATE")
                    .rule = drRule("RULE")
                    .rate_cd = drRule("RATE_CD")
                    .rule_amt = Math.Abs(Math.Round(discount * Me.qty, 2, MidpointRounding.AwayFromZero)) * -1
                    .item_category = Me.item_category
                    .rule_gr = drRule("RULE_GR")
                    .action = "A"
                End With

                Me.item_rule_list.Add(objRule)
            Next

            For Each drRule As DataRow In dtItemCategoryRules.Select("RULE_GR = 'LICMARKUP '")
                Dim mkprice As Decimal = basic_less_deposit - discount
                markup = Math.Round((drRule("RATE") / 100 * mkprice), 2, MidpointRounding.AwayFromZero)
                objRule = New REC_LCBO_LE_SELECT_ORDER_DTL_RULE

                With objRule
                    .le_select_id = le_select_id
                    .cod_line = Me.cod_line
                    .rate = drRule("RATE")
                    .rule = drRule("RULE")
                    .rate_cd = drRule("RATE_CD")
                    .rule_amt = Math.Round(markup * Me.qty, 2, MidpointRounding.AwayFromZero)
                    .item_category = Me.item_category
                    .rule_gr = drRule("RULE_GR")
                    .action = "A"
                End With

                Me.item_rule_list.Add(objRule)
            Next

            For Each drRule As DataRow In dtItemCategoryRules.Select("RULE_GR = 'LEVY      '")
                Dim LevyPrice As Decimal = basic_less_deposit - discount
                levy = Math.Round((drRule("RATE") / 100 * LevyPrice), 2, MidpointRounding.AwayFromZero)
                objRule = New REC_LCBO_LE_SELECT_ORDER_DTL_RULE

                With objRule
                    .le_select_id = le_select_id
                    .cod_line = Me.cod_line
                    .rate = drRule("RATE")
                    .rule = drRule("RULE")
                    .rate_cd = drRule("RATE_CD")
                    .rule_amt = Math.Round(levy * Me.qty, 2, MidpointRounding.AwayFromZero)
                    .item_category = Me.item_category
                    .rule_gr = drRule("RULE_GR")
                    .action = "A"
                End With

                Me.item_rule_list.Add(objRule)
            Next

            For Each drRule As DataRow In dtItemCategoryRules.Select("RULE_GR = 'TAX_GST   '")
                Dim GSTPrice As Decimal = basic_less_deposit - discount + markup + levy
                HST = Math.Round((drRule("RATE") / 100 * GSTPrice), 2, MidpointRounding.AwayFromZero)
                objRule = New REC_LCBO_LE_SELECT_ORDER_DTL_RULE

                With objRule
                    .le_select_id = le_select_id
                    .cod_line = Me.cod_line
                    .rate = drRule("RATE")
                    .rule = drRule("RULE")
                    .rate_cd = drRule("RATE_CD")
                    .rule_amt = Math.Round(HST * Me.qty, 2, MidpointRounding.AwayFromZero)
                    .item_category = Me.item_category
                    .rule_gr = drRule("RULE_GR")
                    .action = "A"
                End With

                Me.item_rule_list.Add(objRule)
            Next

        End Sub
        Sub UpdateRuleItem(ByVal order_type As String)
            Dim dtItemCategoryRules As DataTable = DALRoc.getItemCategoryOrderRules(Me.item_category, order_type)
            Dim basic_less_deposit As Decimal = Me.price - Me.deposit_price
            Dim discount As Decimal = 0
            Dim markup As Decimal = 0
            Dim levy As Decimal = 0
            Dim HST As Decimal = 0
            Dim objRule As REC_LCBO_LE_SELECT_ORDER_DTL_RULE

            If Me.price <> Me.orig_price Then
                objRule = New REC_LCBO_LE_SELECT_ORDER_DTL_RULE

                With objRule
                    .le_select_id = Me.le_select_id
                    .cod_line = Me.cod_line
                    .rate = 0D
                    .rule = "OVERRIDE"
                    .rate_cd = "O"
                    .rule_amt = (Me.price - Me.orig_price) * Me.qty
                    .item_category = Me.item_category
                    .rule_gr = "OVERRIDE"
                    .action = "U"
                End With
                
                Me.item_rule_list.Add(objRule)
            End If
            'Loop through and get the proper values for calculations first
            For Each drRule As DataRow In dtItemCategoryRules.Select("RULE_GR = 'DISCOUNT  '")
                discount = Math.Round((drRule("RATE") / 100 * basic_less_deposit), 2, MidpointRounding.AwayFromZero)
                objRule = New REC_LCBO_LE_SELECT_ORDER_DTL_RULE

                With objRule
                    .le_select_id = le_select_id
                    .cod_line = Me.cod_line
                    .rate = drRule("RATE")
                    .rule = drRule("RULE")
                    .rate_cd = drRule("RATE_CD")
                    .rule_amt = Math.Abs(Math.Round(discount * Me.qty, 2, MidpointRounding.AwayFromZero)) * -1
                    .item_category = Me.item_category
                    .rule_gr = drRule("RULE_GR")
                    .action = "U"
                End With
               
                Me.item_rule_list.Add(objRule)
            Next

            For Each drRule As DataRow In dtItemCategoryRules.Select("RULE_GR = 'LICMARKUP '")
                Dim mkprice As Decimal = basic_less_deposit - discount
                markup = Math.Round((drRule("RATE") / 100 * mkprice), 2, MidpointRounding.AwayFromZero)
                objRule = New REC_LCBO_LE_SELECT_ORDER_DTL_RULE

                With objRule
                    .le_select_id = le_select_id
                    .cod_line = Me.cod_line
                    .rate = drRule("RATE")
                    .rule = drRule("RULE")
                    .rate_cd = drRule("RATE_CD")
                    .rule_amt = Math.Round(markup * Me.qty, 2, MidpointRounding.AwayFromZero)
                    .item_category = Me.item_category
                    .rule_gr = drRule("RULE_GR")
                    .action = "U"
                End With
               
                Me.item_rule_list.Add(objRule)
            Next

            For Each drRule As DataRow In dtItemCategoryRules.Select("RULE_GR = 'LEVY      '")
                Dim LevyPrice As Decimal = basic_less_deposit - discount
                levy = Math.Round((drRule("RATE") / 100 * LevyPrice), 2, MidpointRounding.AwayFromZero)
                objRule = New REC_LCBO_LE_SELECT_ORDER_DTL_RULE

                With objRule
                    .le_select_id = le_select_id
                    .cod_line = Me.cod_line
                    .rate = drRule("RATE")
                    .rule = drRule("RULE")
                    .rate_cd = drRule("RATE_CD")
                    .rule_amt = Math.Round(levy * Me.qty, 2, MidpointRounding.AwayFromZero)
                    .item_category = Me.item_category
                    .rule_gr = drRule("RULE_GR")
                    .action = "U"
                End With
                
                Me.item_rule_list.Add(objRule)
            Next

            For Each drRule As DataRow In dtItemCategoryRules.Select("RULE_GR = 'TAX_GST   '")
                Dim GSTPrice As Decimal = basic_less_deposit - discount + markup + levy
                HST = Math.Round((drRule("RATE") / 100 * GSTPrice), 2, MidpointRounding.AwayFromZero)
                objRule = New REC_LCBO_LE_SELECT_ORDER_DTL_RULE

                With objRule
                    .le_select_id = le_select_id
                    .cod_line = Me.cod_line
                    .rate = drRule("RATE")
                    .rule = drRule("RULE")
                    .rate_cd = drRule("RATE_CD")
                    .rule_amt = Math.Round(HST * Me.qty, 2, MidpointRounding.AwayFromZero)
                    .item_category = Me.item_category
                    .rule_gr = drRule("RULE_GR")
                    .action = "U"
                End With
              
                Me.item_rule_list.Add(objRule)
            Next

        End Sub

        Overloads Sub Update(ByRef oracmd As OracleClient.OracleCommand)

            Dim sQry As String = ""

            Try

                'UPDATE THE ITEM INFO FIRST
                MyBase.Update(oracmd)

                'UPDATE ITEM RULE INFO
                For Each rule As REC_LCBO_LE_SELECT_ORDER_DTL_RULE In item_rule_list
                    rule.Update(oracmd)
                Next

            Catch ex As Exception
                Throw New Exception("LCBO_LE_SELECT_ORDER_ITM.UPDATE->" & le_select_id & "," & cod_line & vbNewLine & _
                                    "QRY: " & sQry & vbNewLine & _
                                    "ErrMsg:" & ex.Message.ToString)
            End Try

        End Sub

    End Class
End Namespace

