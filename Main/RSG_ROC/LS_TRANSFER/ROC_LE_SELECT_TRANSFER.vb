Imports Microsoft.VisualBasic
Imports System.Collections
Imports RSG_ROC.DataLayer
Imports RSG_ROC.LS_TRANSFERS
Imports RSG_ROC.GLClasses
Imports System.IO

Namespace LS_TRANSFERS
    Public Class ROC_LE_SELECT_TRANSFER
        Inherits REC_LCBO_LE_SELECT_TRANSFER

        Private lsOrder As ROC_LE_SELECT_ORDER
        Public Property le_select_order() As ROC_LE_SELECT_ORDER
            Get
                Return lsOrder
            End Get
            Set(ByVal value As ROC_LE_SELECT_ORDER)
                lsOrder = value
            End Set
        End Property

        Private lsCheckList As SortedList
        Public Property le_select_checklist() As SortedList
            Get
                Return lsCheckList
            End Get
            Set(ByVal value As SortedList)
                lsCheckList = value
            End Set
        End Property

        Private lsDocumentList As SortedList
        Public Property le_select_document_list() As SortedList
            Get
                Return lsDocumentList
            End Get
            Set(ByVal value As SortedList)
                lsDocumentList = value
            End Set
        End Property

        Public ReadOnly Property CheckList() As DataTable
            Get
                Dim dt As New DataTable
                Dim dtCalc As New DataTable
                Dim dr As DataRow = Nothing
                With dt.Columns
                    .Add("TASK_ID")
                    .Add("CHECK_ID")
                    .Add("TASK_DESCRIPTION")
                    .Add("LAST_UPDATE")
                    .Add("COMPLETED_TASKS")
                    .Add("TOTAL_TASKS")
                    .Add("PROGRESS")
                    .Add("STATUS")
                    .Add("AUTOMATED_CHECK")
                End With

                With dtCalc.Columns
                    .Add("CHECK_ID")
                    .Add("CHECK_ID_HDR")
                    .Add("IS_CHECK_HDR")
                    .Add("CHECKED")
                    .Add("TOTAL_TASKS")
                    .Add("PROGRESS")
                    .Add("STATUS")
                    .Add("CHECK_DT")
                End With

                Dim iCurrentTaskID As Integer = 0
                For Each ck As REC_LCBO_LE_SELECT_CHECK_LIST In le_select_checklist.GetValueList
                    If ck.is_check_hdr = "Y" Then
                        dr = dt.NewRow
                        dr("TASK_ID") = ck.task_id
                        dr("CHECK_ID") = ck.check_id
                        dr("TASK_DESCRIPTION") = ck.check_desc
                        dr("LAST_UPDATE") = ck.check_dt
                        dr("STATUS") = IIf(ck.checked = "Y", "True", "False")
                        dr("AUTOMATED_CHECK") = IIf(ck.automated_check = "Y", "True", "False")
                        dt.Rows.Add(dr)
                    End If

                    dr = dtCalc.NewRow
                    dr("CHECK_ID") = ck.check_id
                    dr("CHECK_ID_HDR") = ck.check_id_hdr
                    dr("IS_CHECK_HDR") = ck.is_check_hdr
                    dr("CHECKED") = ck.checked
                    dr("TOTAL_TASKS") = 0
                    dr("PROGRESS") = 0
                    dr("STATUS") = ck.checked
                    dr("CHECK_DT") = IIf(ck.check_dt.Year < 2000, "", ck.check_dt.ToString("yyyy-MM-dd HH:mm:ss"))
                    dtCalc.Rows.Add(dr)
                Next

                For Each dr In dt.Rows
                    Dim total_task As Integer = dtCalc.Compute("COUNT(CHECK_ID)", "CHECK_ID_HDR = '" & dr("CHECK_ID") & "'")
                    Dim completed_task As Integer = dtCalc.Compute("COUNT(CHECK_ID)", "CHECKED = 'Y' AND CHECK_ID_HDR = '" & dr("CHECK_ID") & "'")
                    Dim hasSubTasks As Boolean = False

                    For Each drCalc As DataRow In dtCalc.Select("CHECK_ID_HDR = " & dr("CHECK_ID") & " AND IS_CHECK_HDR = 'N'")
                        hasSubTasks = True
                    Next

                    If hasSubTasks = True Then
                        total_task = dtCalc.Compute("COUNT(CHECK_ID)", "CHECK_ID_HDR = '" & dr("CHECK_ID") & "' AND IS_CHECK_HDR = 'N'")
                        completed_task = dtCalc.Compute("COUNT(CHECK_ID)", "CHECKED = 'Y' AND CHECK_ID_HDR = '" & dr("CHECK_ID") & "' AND IS_CHECK_HDR = 'N'")
                    End If
                    If total_task = 0 Then
                        total_task = 1
                    End If
                    Dim progress As Decimal = Math.Round(completed_task / total_task * 100, 2)
                    dr("TOTAL_TASKS") = total_task
                    dr("COMPLETED_TASKS") = completed_task
                    dr("PROGRESS") = progress
                    dr("LAST_UPDATE") = dtCalc.Compute("MAX(CHECK_DT)", "CHECK_ID_HDR = '" & dr("CHECK_ID") & "'")

                    If dr("AUTOMATED_CHECK") = "Y" Then
                        If progress = 100D And dr("STATUS") <> "Y" Then
                            dr("STATUS") = "Y"
                        End If
                    End If
                Next
                Return dt
            End Get
        End Property

        Public ReadOnly Property Uploaded_Documents() As DataTable
            Get
                Dim dtDocs As New DataTable
                Dim drDocs As DataRow = Nothing
                With dtDocs.Columns
                    .Add("LE_SELECT_ID")
                    .Add("DOC_ID")
                    .Add("DOC_TYPE_ID")
                    .Add("DOC_FILEPATH")
                    .Add("UPLOAD_DT")
                    .Add("UPLOAD_BY")
                    .Add("DOC_DESC")
                    .Add("DOC_REQUIRED")
                    .Add("ACTION")
                End With

                For Each DOC As REC_LCBO_LE_SELECT_DOCUMENTS In lsDocumentList.GetValueList
                    If DOC.action = "E" Then 'FROM DATABASE SO IT IS UPLOADED
                        drDocs = dtDocs.NewRow
                        drDocs("LE_SELECT_ID") = DOC.le_select_id
                        drDocs("DOC_ID") = DOC.doc_id
                        drDocs("DOC_TYPE_ID") = DOC.doc_type_id
                        drDocs("DOC_FILEPATH") = DOC.doc_filepath
                        drDocs("UPLOAD_DT") = DOC.upload_dt
                        drDocs("UPLOAD_BY") = DOC.upload_by
                        drDocs("DOC_DESC") = DOC.doc_desc
                        drDocs("DOC_REQUIRED") = DOC.doc_required
                        drDocs("ACTION") = DOC.action
                        dtDocs.Rows.Add(drDocs)
                    End If
                Next

                Return dtDocs
            End Get
        End Property

        Public ReadOnly Property Pending_Documents() As DataTable
            Get
                Dim dtDocs As New DataTable
                Dim drDocs As DataRow = Nothing
                With dtDocs.Columns
                    .Add("LE_SELECT_ID")
                    .Add("DOC_ID")
                    .Add("DOC_TYPE_ID")
                    .Add("DOC_FILEPATH")
                    .Add("UPLOAD_DT")
                    .Add("UPLOAD_BY")
                    .Add("DOC_DESC")
                    .Add("DOC_REQUIRED")
                    .Add("ACTION")
                End With

                For Each DOC As REC_LCBO_LE_SELECT_DOCUMENTS In lsDocumentList.GetValueList
                    If DOC.action = "N" AndAlso DOC.doc_required = "Y" Then 'new and required
                        drDocs = dtDocs.NewRow
                        drDocs("LE_SELECT_ID") = DOC.le_select_id
                        drDocs("DOC_ID") = DOC.doc_id
                        drDocs("DOC_TYPE_ID") = DOC.doc_type_id
                        drDocs("DOC_FILEPATH") = DOC.doc_filepath
                        drDocs("UPLOAD_DT") = DOC.upload_dt
                        drDocs("UPLOAD_BY") = DOC.upload_by
                        drDocs("DOC_DESC") = DOC.doc_desc
                        drDocs("DOC_REQUIRED") = DOC.doc_required
                        drDocs("ACTION") = DOC.action
                        dtDocs.Rows.Add(drDocs)
                    End If
                Next

                Return dtDocs
            End Get
        End Property

        Private Sub setLeSelectID(ByVal ID As Integer)
            Me.le_select_id = ID
            Me.le_select_order.le_select_id = ID

            For Each ck As REC_LCBO_LE_SELECT_CHECK_LIST In lsCheckList.GetValueList
                ck.le_select_id = ID
            Next

            For Each doc As REC_LCBO_LE_SELECT_DOCUMENTS In lsDocumentList.GetValueList
                doc.le_select_id = ID
            Next
        End Sub

        Sub New()
            MyBase.New()
            le_select_order = New ROC_LE_SELECT_ORDER
            lsCheckList = New SortedList
            lsDocumentList = New SortedList

            'build checlist
            Dim dtCheckList As DataTable = DALRoc.getLeSelectCheckList(le_select_id)
            Dim taskid As Integer = 0
            Dim check As REC_LCBO_LE_SELECT_CHECK_LIST = Nothing

            For Each drCheck As DataRow In dtCheckList.Rows
                check = New REC_LCBO_LE_SELECT_CHECK_LIST(drCheck)
                If drCheck("IS_CHECK_HDR") = "Y" Then
                    taskid = taskid + 1
                End If
                check.task_id = taskid
                lsCheckList.Add(check.check_id, check)
            Next

            Dim dtDocumentList As DataTable = DALRoc.getLeSelectDocumentList(le_select_id)
            Dim doc As REC_LCBO_LE_SELECT_DOCUMENTS = Nothing

            For Each drDoc As DataRow In dtDocumentList.Rows
                doc = New REC_LCBO_LE_SELECT_DOCUMENTS(drDoc)
                lsDocumentList.Add(doc.doc_id, doc)
            Next
        End Sub
        Sub New(ByVal sOrigCustomerNo As String, ByVal sContactName As String, ByVal sCorporateEmail As String, ByVal sUser As String)
            Me.New()
            'Get customer information from db
            Dim drCust As DataRow = DALRoc.getCustInfo(sOrigCustomerNo)
            SetOrigCustInfo(drCust)
            le_select_contact_name = sContactName
            le_select_corporate_email = sCorporateEmail
            prepared_by = sUser
        End Sub
        Sub SetOrigCustInfo(ByVal drCust As DataRow)

            If IsDBNull(drCust("CU_NO")) = False Then
                Me.orig_customer_no = drCust("CU_NO")
            End If

            If IsDBNull(drCust("CU_NAME")) = False Then
                Me.orig_cust_name = drCust("CU_NAME")
            End If

            If IsDBNull(drCust("ADD1_SH")) = False Then
                Me.orig_cust_address = drCust("ADD1_SH")
            End If

            If IsDBNull(drCust("ADD2_SH")) = False Then
                Me.orig_cust_address = Me.orig_cust_address & " " & drCust("ADD2_SH")
            End If

            If IsDBNull(drCust("CITY_SH")) = False Then
                Me.orig_cust_city = drCust("CITY_SH")
            End If

            If IsDBNull(drCust("PROV_SH")) = False Then
                Me.orig_cust_prov = drCust("PROV_SH")
            End If

            If IsDBNull(drCust("POST_CODE")) = False Then
                Me.orig_cust_post_code = drCust("POST_CODE")
            End If

            If IsDBNull(drCust("PHONE_NU")) = False Then
                Me.orig_cust_phone = drCust("PHONE_NU")
            End If

        End Sub
        Sub New(ByVal iLeSelectID As Integer)
            MyBase.New(iLeSelectID)
            le_select_order = New ROC_LE_SELECT_ORDER(iLeSelectID)
            lsCheckList = New SortedList
            lsDocumentList = New SortedList

            Dim dtCheckList As DataTable = DALRoc.getLeSelectCheckList(iLeSelectID)
            Dim taskid As Integer = 0
            Dim check As REC_LCBO_LE_SELECT_CHECK_LIST = Nothing

            For Each drCheck As DataRow In dtCheckList.Rows
                check = New REC_LCBO_LE_SELECT_CHECK_LIST(drCheck)
                If drCheck("IS_CHECK_HDR") = "Y" Then
                    taskid = taskid + 1
                End If
                check.task_id = taskid
                lsCheckList.Add(check.check_id, check)
            Next

            Dim dtDocumentList As DataTable = DALRoc.getLeSelectDocumentList(iLeSelectID)
            Dim doc As REC_LCBO_LE_SELECT_DOCUMENTS = Nothing

            For Each drDoc As DataRow In dtDocumentList.Rows
                doc = New REC_LCBO_LE_SELECT_DOCUMENTS(drDoc)
                lsDocumentList.Add(doc.doc_id, doc)
            Next
        End Sub
        Overloads Sub Insert(ByRef oracmd As OracleClient.OracleCommand)
            MyBase.Insert(oracmd)
            setLeSelectID(Me.le_select_id)
            le_select_order.emp_no = prepared_by
            le_select_order.status = "NEW"
            le_select_order.Insert(oracmd)

        End Sub
        Sub SetCheck(ByVal check_id As Integer, ByVal bState As Boolean, ByRef oraCmd As OracleClient.OracleCommand)
            Dim check As REC_LCBO_LE_SELECT_CHECK_LIST = lsCheckList.Item(check_id)
            Dim hdrCheck As REC_LCBO_LE_SELECT_CHECK_LIST = lsCheckList.Item(check.check_id_hdr)

            check.checked = IIf(bState, "Y", "N")
            check.check_dt = Now

            If check.action = "N" Then
                check.action = "A"
            ElseIf check.action = "E" Then
                check.action = "U"
            End If

            check.UPDATE(oraCmd)
            check.action = "E"

            If check.check_id <> hdrCheck.check_id Then
                'need to check if all subtask is fulfilled, if so check off the header task as well
                For Each dr As DataRow In Me.CheckList.Select("CHECK_ID = " & hdrCheck.check_id)
                    If hdrCheck.action = "E" Then
                        hdrCheck.action = "U"
                    Else
                        hdrCheck.action = "A"
                    End If

                    If dr("PROGRESS") = 100 Then
                        hdrCheck.checked = "Y"
                    Else
                        hdrCheck.checked = "N"
                    End If
                    hdrCheck.UPDATE(oraCmd)
                    hdrCheck.action = "E"
                Next

            End If

            'Update status
            Dim status As String = "COMPLETE"
            For Each ck As DataRow In CheckList.Rows
                If ck("PROGRESS") <> 100 Then
                    status = "IN PROGRESS"
                End If
            Next

            If Me.status <> status Then
                Me.status = status
                Me.Update(oraCmd)
            End If


        End Sub

        Overloads Sub Update(ByRef oracmd As OracleClient.OracleCommand)
            MyBase.UPDATE(oracmd)

            le_select_order.Update(oracmd)
            Dim removeRecords As New ArrayList

            For Each ck As REC_LCBO_LE_SELECT_CHECK_LIST In lsCheckList.GetValueList
                If (ck.action = "A" Or ck.action = "U") Then
                    ck.UPDATE(oracmd)
                    ck.action = "E"
                End If
            Next

            For Each doc As REC_LCBO_LE_SELECT_DOCUMENTS In lsDocumentList.GetValueList
                'generate the doc_id
                If doc.action = "A" Then
                    doc.doc_id = Right("00000000" & le_select_id, 8) & "-" & doc.doc_type_id & "-" & Now.ToString("yyyyMMddHHmmssfff")
                End If
                If (doc.action = "A" Or doc.action = "U" Or doc.action = "D") Then

                    doc.Update(oracmd)
                    If doc.action = "D" Then
                        'reset the record
                        If doc.doc_type_id <> 9.999 Then
                            doc.doc_filepath = ""
                            doc.upload_dt = New DateTime
                            doc.upload_by = ""
                        Else
                            'need to remove from list as well
                            removeRecords.Add(doc.doc_id)
                        End If
                        doc.action = "N"
                    Else
                        doc.action = "E"
                    End If
                End If
            Next

            For Each rec As String In removeRecords
                lsDocumentList.Remove(rec)
            Next
        End Sub

        Public Sub DeleteLeSelectDocument(ByVal doc_id As String, ByRef oracmd As OracleClient.OracleCommand)

            Dim doc As REC_LCBO_LE_SELECT_DOCUMENTS = lsDocumentList.Item(doc_id)
            Try
                doc.action = "D"
                doc.Update(oracmd)
                doc.action = "N" 'RESET FLAG SO TO INDICATE THAT THIS RECORD IS NOT IN THE DB ANYMORE
                SetCheck(doc.check_id, False, oracmd)
                'File.Delete(doc.doc_filepath)
            Catch ex As Exception
                Throw New Exception("ROC_LE_SELECT_TRANSFER.DeleteLeSelectDocument(" & doc_id & ")" & vbNewLine & _
                                    "FILE: " & doc.doc_filepath & vbNewLine & _
                                    "ERR MSG: " & ex.Message.ToString)

            End Try

        End Sub
        Private Function getFileExtension(ByVal filename As String) As String
            Dim filename_parts As String() = filename.Split(".")
            Dim file_ext As String = "txt"

            If filename_parts.Length > 1 Then 'there is no extension - making it a txt
                file_ext = filename_parts(filename_parts.Length - 1)
            End If

            Return file_ext
        End Function
        Private Function formatFolderName(ByVal sSharedFolder As String) As String
            Dim formatted As String = ""
            If sSharedFolder.EndsWith("\") Then
                formatted = sSharedFolder.Remove(sSharedFolder.Length - 1, 1)
            Else
                formatted = sSharedFolder
            End If

            If Directory.Exists(formatted) = False Then
                Directory.CreateDirectory(formatted)
            End If
            Return formatted
        End Function
        Public Function AddLeSelectDocument(ByVal doc_type_id As Decimal, _
                                       ByVal sSharedFolder As String, _
                                       ByVal orig_filename As String, _
                                       ByVal upload_by As String, _
                                       ByRef oraCmd As OracleClient.OracleCommand) As String

            Dim uploadfilename As String = ""
            sSharedFolder = Me.formatFolderName(sSharedFolder)
            Try
                For Each doc As REC_LCBO_LE_SELECT_DOCUMENTS In lsDocumentList.GetValueList
                    If doc.doc_type_id = doc_type_id Then
                        'check to see if existing record needs to be overwritten
                        If doc.action = "E" Or doc.action = "U" Then
                            DeleteLeSelectDocument(doc.doc_id, oraCmd)
                        End If

                        doc.action = "A"
                        doc.doc_id = Right("00000000" & le_select_id, 8) & "-" & doc.doc_type_id & "-" & Now.ToString("yyyyMMddHHmmssfff")
                        uploadfilename = sSharedFolder & "\" & doc.doc_id & "." & getFileExtension(orig_filename)
                        doc.doc_filepath = uploadfilename
                        doc.orig_filename = orig_filename
                        doc.upload_dt = Now
                        doc.upload_by = upload_by
                        doc.Update(oraCmd)
                        doc.action = "E"

                        'UPDATE THE CHECK FLAG
                        If doc.doc_required = "Y" Then
                            SetCheck(doc.check_id, True, oraCmd)
                        End If
                    End If
                Next
            Catch ex As Exception
                Throw New Exception("ROC_LE_SELECT_TRANSFER.AddLeSelectDocument(" & doc_type_id & "," & sSharedFolder & "," & orig_filename & "," & upload_by & ")" & vbNewLine & _
                                    "ERR MSG: " & ex.Message.ToString)

            End Try

            Return uploadfilename
        End Function
        Private Function validateCustomerInfo() As Boolean
            Dim bValid As Boolean = True

            bValid = bValid AndAlso orig_customer_no.Trim <> ""
            bValid = bValid AndAlso orig_cust_name.Trim <> ""
            bValid = bValid AndAlso orig_cust_address.Trim <> ""
            bValid = bValid AndAlso orig_cust_city.Trim <> ""
            bValid = bValid AndAlso orig_cust_post_code.Trim <> ""
            bValid = bValid AndAlso orig_cust_prov.Trim <> ""
            bValid = bValid AndAlso new_customer_no.Trim <> ""
            bValid = bValid AndAlso new_cust_name.Trim <> ""
            bValid = bValid AndAlso new_cust_address.Trim <> ""
            bValid = bValid AndAlso new_cust_city.Trim <> ""
            bValid = bValid AndAlso new_cust_post_code.Trim <> ""
            bValid = bValid AndAlso new_cust_prov.Trim <> ""

            Return bValid

        End Function

        Public Sub FinalizeOrder(ByVal sUser As String, _
                                 ByRef oraCmd As OracleClient.OracleCommand)
            Try
                'Check if order and return is already created.
                Try
                    Dim drow As DataRow = DALRoc.getLeSelectOrderHeader(le_select_id)
                    If IsDBNull(drow("STATUS")) = False Then
                        If drow("STATUS") = "FINALIZED" Then
                            Throw New Exception("LE SELECT ORDER HAS ALREADY BEEN FINALIZED!" & vbNewLine & _
                                                "LE SELECT ID: " & le_select_id & vbNewLine & _
                                                "RETURN ORD#: " & drow("CR_ORDER_NO") & vbNewLine & _
                                                "NEW ORD#: " & drow("ORDER_NO"))
                        End If
                    End If
                Catch ex As Exception
                    'no record found than we should be good
                End Try

                'validate if customer information is complete
                If validateCustomerInfo() = False Then
                    Throw New Exception("ROC_LE_SELECT_TRANSFER.FINALIZEORDER->ERROR: All Required Customer information is not complete!")
                End If

                '1. validate all items are qty > 0
                For Each itm As REC_LCBO_LE_SELECT_ORDER_DTL In le_select_order.order_details.GetValueList
                    If itm.qty <= 0 Then
                        Throw New Exception("ROC_LE_SELECT_TRANSFER.FinaizeOrder->STEP 1: ITEM: " & itm.cod_line & "," & itm.item & " HAS INVALID QTY!")
                    End If
                Next
                '2. Resequence
                le_select_order.OrderLineResequence(oraCmd)

                '3. Create Return
                Dim rrOrder As RSG_ROC.OrderClasses.ROC_RETURN = Nothing
                rrOrder = le_select_order.CreateROCReturn(Me.orig_customer_no)
                rrOrder.Insert(oraCmd, False)

                Me.le_select_order.cr_order_no = rrOrder.order_no
                Me.le_select_return_no = rrOrder.order_no
                Me.le_select_order.cr_inv_no = rrOrder.inv_no

                Me.Update(oraCmd)
                ' Dim dtGLBreakdown As DataTable = DALRoc.getGLOrderGLBreakdown(rrOrder.order_no, rrOrder.order_status_type, rrOrder.Order_Type, oraCmd)
                Dim dtGLBreakdown As DataTable = DALRoc.getLSGLBreakdown(Me.le_select_id, "R")
                Dim rrReturnTransaction As New ROC_RETURN_TRANSACTION(Now, sUser, rrOrder, dtGLBreakdown)
                rrReturnTransaction.PostTransaction(oraCmd)

                Dim rrPayTrans As ROC_PAYMENT_TRANSACTION = Nothing
                For Each pymt As REC_ON_INV_PAY In rrOrder.Invoice.paymentList
                    rrPayTrans = New ROC_PAYMENT_TRANSACTION(rrOrder.Invoice, pymt, Now, sUser)
                    rrPayTrans.PostTransaction(oraCmd)
                Next

                'Add comments to return
                DALRoc.AddOrderComments(rrOrder.order_no, "R", "LICENSEE LOCATION TRANSFER" & Me.new_customer_no.Trim, oraCmd)
                DALRoc.AddOrderComments(rrOrder.order_no, "R", "NEW CUST NO: " & Me.new_customer_no.Trim, oraCmd)

                '4. Create Order
                Dim roOrder As RSG_ROC.OrderClasses.ROC_ORDER = Nothing
                roOrder = le_select_order.CreateROCOrder(Me.new_customer_no)
                roOrder.Insert(oraCmd)


                Me.le_select_order_no = roOrder.order_no
                Me.le_select_order.order_no = roOrder.order_no
                Me.le_select_order.inv_no = roOrder.inv_no

                Me.Update(oraCmd)
                DALRoc.AddOrderComments(roOrder.order_no, "A", "LIC.LOC.TRANSFER - " & Me.orig_customer_no.Trim, oraCmd)
                DALRoc.AddOrderComments(roOrder.order_no, "A", "FINAL SALE, NO RETURNS", oraCmd)

                'post this order
                'dtGLBreakdown = DALRoc.getGLOrderGLBreakdown(roOrder.order_no, roOrder.order_status_type, roOrder.Order_Type, oraCmd)
                dtGLBreakdown = DALRoc.getLSGLBreakdown(Me.le_select_id, "R")

                Dim roSalesTransaction As New ROC_SALES_TRANSACTION(Now, sUser, roOrder, dtGLBreakdown)
                roSalesTransaction.PostTransaction(oraCmd)
                Dim roPayTrans As ROC_PAYMENT_TRANSACTION = Nothing
                For Each pymt As REC_ON_INV_PAY In rrOrder.Invoice.paymentList
                    roPayTrans = New ROC_PAYMENT_TRANSACTION(roOrder.Invoice, pymt, Now, sUser)
                    roPayTrans.PostTransaction(oraCmd)
                Next
                '5. Update Transfer Status
                le_select_order.status = "FINALIZED"

                '6. Update Checklist - CHECK_ID = 6 - CREATE INVENTORY TRANSFER
                Me.SetCheck(6, True, oraCmd)

                Me.Update(oraCmd)

            Catch ex As Exception
                Throw New Exception("ROC_LE_SELECT_TRANSFER.FinalizeOrder() LeSelectID: " & le_select_id & vbNewLine & _
                                    "ERR MSG: " & ex.Message.ToString)
            End Try

        End Sub
        Private Function generateDOCID(ByVal doc_type_id As Decimal) As String
            Return Right("00000000" & le_select_id, 8) & "-" & doc_type_id & "-" & Now.ToString("yyyyMMddHHmmssfff")
        End Function
    End Class
End Namespace
