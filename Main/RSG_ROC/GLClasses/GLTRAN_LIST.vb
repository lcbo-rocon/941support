Imports RSG_ROC.DataLayer
Imports RSG_ROC.Exceptions

Namespace GLClasses
    Public Class GLTRAN_LIST
        Inherits SortedList

        Private sGL_TRANS_NO As String
        Public Property gl_trans_no() As String
            Get
                Return sGL_TRANS_NO
            End Get
            Set(ByVal value As String)
                sGL_TRANS_NO = value
            End Set
        End Property

        Sub New()
            MyBase.New()
            gl_trans_no = ""
        End Sub

        Public Sub AddGLRecord(ByVal objGLRec As REC_ON_GLTRAN)
            Dim sKey As String = ""

            If gl_trans_no = "" Then
                gl_trans_no = objGLRec.gl_trans_no
            End If
            'validation if gltrans_no is different then need to reject
            If objGLRec.gl_trans_no <> gl_trans_no Then
                Throw New Exception("NOT THE SAME TRANS NUMBER. Expected:" & gl_trans_no & " GOT: " & objGLRec.gl_trans_no)
            End If

            Try
                sKey = objGLRec.gl_cd.Trim
                If objGLRec.gl_cd.Trim = DALRoc.getARGL Then
                    sKey = IIf(objGLRec.gl_dr_amount <> 0, "DR", "CR") & sKey
                End If
                MyBase.Add(sKey, objGLRec)

            Catch ex As Exception
                Throw New Exception("GLTRAN_LIST.AddGLRecord(" & gl_trans_no & ")->" & ex.ToString)
            End Try

        End Sub

        ''' <summary>
        ''' DR values should equal CR values
        ''' </summary>
        ''' <remarks></remarks>
        Public Function validate() As Boolean

            Dim iDebitAmounts As Int64 = 0
            Dim iCreditAmounts As Int64 = 0
            Dim bBalanced As Boolean = True
            For Each glkey As String In Me.Keys
                Dim glrec As REC_ON_GLTRAN = Me.Item(glkey)

                If glrec.gl_cr_amount <> 0 Then
                    iCreditAmounts = iCreditAmounts + glrec.gl_cr_amount
                End If
                If glrec.gl_dr_amount <> 0 Then
                    iDebitAmounts = iDebitAmounts + glrec.gl_dr_amount
                End If
            Next

            If iCreditAmounts <> iDebitAmounts Then
                bBalanced = False
                Throw New GLOutOfBalanceException("GLTRAN_LIST.Validate()->Debits: " & iDebitAmounts & " <> Credits: " & iCreditAmounts)
            End If

            Return bBalanced
        End Function

    End Class
End Namespace
