Imports RSG_ROC.DataLayer
Imports RSG_ROC.OrderClasses

Namespace GLClasses
    Public Class ROC_RETURN_TRANSACTION
        Inherits ROC_Transaction

        Private dDelivery_Charge As Decimal
        Public Property delivery_charge() As Decimal
            Get
                Return dDelivery_Charge
            End Get
            Set(ByVal value As Decimal)
                dDelivery_Charge = value
            End Set
        End Property

        Private dDelivery_Tax As Decimal
        Public Property delivery_tax() As Decimal
            Get
                Return dDelivery_Tax
            End Get
            Set(ByVal value As Decimal)
                dDelivery_Tax = value
            End Set
        End Property

        Private iInv_no As Integer
        Public Property inv_no() As Integer
            Get
                Return iInv_no
            End Get
            Set(ByVal value As Integer)
                iInv_no = value
            End Set
        End Property

        Private sOrderNum As String
        Public Property order_num() As String
            Get
                Return sOrderNum
            End Get
            Set(ByVal value As String)
                sOrderNum = value
            End Set
        End Property
        Sub New(ByVal dtmBusinessDate As DateTime, ByVal aruser As String)
            MyBase.New(dtmBusinessDate, aruser)
            offset_ar_account = DALRoc.getARGL
        End Sub

        Sub New(ByVal rrOrder As ROC_RETURN, ByVal dtmBusinessDate As DateTime, ByVal arUser As String)
            Me.New(dtmBusinessDate, arUser)
            SetARInfo(rrOrder)
            ' Dim dtGLBreakdown As DataTable = DALRoc.getGLOrderGLBreakdown(rrOrder.order_no, rrOrder.order_status_type, rrOrder.Order_Type)
            'If dtGLBreakdown.Rows.Count <= 0 Then
            '    Throw New Exception("Return Order " & rrOrder.order_no & " not found in ROC Database!")
            'End If
            'SetGLInfo(dtGLBreakdown)
        End Sub

        Sub New(ByVal dtmBusinessDate As DateTime, ByVal arUser As String, ByVal rrOrder As ROC_RETURN, ByVal dtGLBreakdown As DataTable)
            Me.New(dtmBusinessDate, arUser)
            SetARInfo(rrOrder)
            'Dim dtGLBreakdown As DataTable = DALRoc.getLSGLBreakdown(lsTransfer.le_select_id, "R")
            If dtGLBreakdown.Rows.Count <= 0 Then
                Throw New Exception("Return Order " & rrOrder.order_no & " not found in ROC Database!")
            End If
            SetGLInfo(dtGLBreakdown)
        End Sub

        Public Sub SetARInfo(ByVal rrOrder As ROC_RETURN)
            Dim dtLinkTrans As DataTable = Nothing

            inv_no = rrOrder.inv_no
            order_num = rrOrder.order_no

            header.ar_date = post_date
            header.ar_link_date = calcLinkDate(post_date)
            header.ar_ref_no = rrOrder.order_no
            header.ar_cust_no = rrOrder.customer_no
            header.ar_trans_type = "C"

            'debit ar account
            header.ar_dr_amount = 0
            header.ar_cr_amount = rrOrder.invoice_amt

            'delivery_charge = rrOrder.delivery_charge
            'delivery_tax = rrOrder.delivery_tax2


        End Sub

        Public Sub SetGLInfo(ByVal dtGLBreakdown As DataTable)
            Dim glrec As REC_ON_GLTRAN = Nothing
            'Dim dTotals as 
            ' To ar/dr account DR
            glrec = New REC_ON_GLTRAN
            glrec.gl_trans_no = header.ar_trans_no
            glrec.gl_date = header.ar_date
            glrec.gl_rec_no = 0
            glrec.gl_link_date = header.ar_link_date
            glrec.gl_dept = DALRoc.getStore
            glrec.gl_cd = offset_ar_account
            glrec.gl_memo = header.ar_ref_no
            glrec.gl_dr_amount = 0
            glrec.gl_cr_amount = header.ar_cr_amount
            glrec.gl_user = header.ar_user

            DetailList.AddGLRecord(glrec)

            For Each drGL As DataRow In dtGLBreakdown.Rows
                ' To sales account CR
                glrec = New REC_ON_GLTRAN
                glrec.gl_trans_no = header.ar_trans_no
                glrec.gl_date = header.ar_date
                glrec.gl_rec_no = 0
                glrec.gl_link_date = header.ar_link_date
                glrec.gl_dept = DALRoc.getStore
                glrec.gl_cd = drGL("GL_CD")
                glrec.gl_memo = header.ar_ref_no
                glrec.gl_dr_amount = drGL("GL_AMOUNT")
                glrec.gl_cr_amount = 0
                glrec.gl_user = header.ar_user

                DetailList.AddGLRecord(glrec)
            Next

        End Sub

        Public Overrides Function Validate() As Boolean
            Dim bValid As Boolean = True

            Dim glrec As REC_ON_GLTRAN = DetailList.Item("CR" & offset_ar_account)

            If glrec Is Nothing Then
                glrec = DetailList.Item("DR" & offset_ar_account)
            End If

            'Check if the AR values matches the GL value
            '  If parent_transfer = False Then
            bValid = (glrec.gl_dr_amount = header.ar_dr_amount And glrec.gl_cr_amount = header.ar_cr_amount)
            ' End If

            'Check the details if gl debit amount = gl credit amount
            If bValid = True Then
                bValid = DetailList.validate
            End If

            Return bValid
        End Function

        Public Overrides Sub PostTransaction(ByRef cmdOracle As System.Data.OracleClient.OracleCommand)
            MyBase.PostTransaction(cmdOracle)
            DALRoc.UpdateInvoiceTransNo(inv_no, trans_no, cmdOracle)
            DALRoc.UpdateInvoicePostFlag(order_num, "R", cmdOracle)
        End Sub

    End Class
End Namespace
