Imports RSG_ROC.DataLayer

Namespace GLClasses
    Public MustInherit Class ROC_Transaction
        Implements IComparable

        Private dtmPostDate As DateTime
        Public Property post_date() As DateTime
            Get
                Return dtmPostDate
            End Get
            Set(ByVal value As DateTime)
                dtmPostDate = value
            End Set
        End Property

        Private sTrans_no As String
        Public Property trans_no() As String
            Get
                Return sTrans_no
            End Get
            Set(ByVal value As String)
                sTrans_no = value
                SetTransNo()
            End Set
        End Property

        ''' <summary>
        ''' Get this value from config file "PAY_ORACLE_GL"
        ''' </summary>
        ''' <remarks></remarks>
        Private sOffsetArAccount As String
        Public Property offset_ar_account() As String
            Get
                Return sOffsetArAccount
            End Get
            Set(ByVal value As String)
                sOffsetArAccount = value
            End Set
        End Property

        Private arHeader As REC_ON_ARTRAN
        Public Property header() As REC_ON_ARTRAN
            Get
                Return arHeader
            End Get
            Set(ByVal value As REC_ON_ARTRAN)
                arHeader = value
            End Set
        End Property

        Private glDetailList As GLTRAN_LIST
        Public Property DetailList() As GLTRAN_LIST
            Get
                Return glDetailList
            End Get
            Set(ByVal value As GLTRAN_LIST)
                glDetailList = value
            End Set
        End Property

        Sub New(ByVal dtmBusinessDate As DateTime, ByVal aruser As String)
            post_date = dtmBusinessDate
            offset_ar_account = DALRoc.getARGL
            header = New REC_ON_ARTRAN
            header.ar_user = Right(aruser.Trim, 3)
            DetailList = New GLTRAN_LIST
            trans_no = ""
        End Sub

        Private Sub SetTransNo()
            header.ar_trans_no = trans_no

            If header.ar_link_trans_no = "" Then
                header.ar_link_trans_no = header.ar_trans_no
            End If

            For Each GLRec As REC_ON_GLTRAN In DetailList.GetValueList
                GLRec.gl_trans_no = trans_no
            Next
        End Sub

        'Public MustOverride Sub SetARInfo(ByVal drInfo As DataRow)
        'Public MustOverride Sub SetGLInfo(ByVal dtInfo As DataTable)
        Public MustOverride Function Validate() As Boolean

        Public Overridable Sub PostTransaction(ByRef cmdOracle As OracleClient.OracleCommand)
            Try
                trans_no = DALRoc.getNewGLTransNo(cmdOracle).ToString
                'Set ArLinkInfo here
                setARLinkInfo(cmdOracle)
                'DALRoc.PostTransaction(Me, cmdOracle)
                header.Insert(cmdOracle)
                For Each glrec As REC_ON_GLTRAN In DetailList.GetValueList
                    glrec.Insert(cmdOracle)
                Next
            Catch ex As Exception
                Throw New Exception("ROC_TRANSACTION.PostTransaction(): transaction: " & trans_no & vbNewLine & _
                                    "Err Msg: " & ex.Message.ToString)
            End Try

        End Sub
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Dim b As ROC_Transaction = obj
            Return Me.trans_no.CompareTo(b.trans_no)
        End Function
        Protected Function calcLinkDate(ByVal dtmDate As DateTime) As String
            Return dtmDate.Year & Right("00" & dtmDate.DayOfYear, 3)
        End Function
        Protected Sub setARLinkInfo(ByRef cmdOracle As OracleClient.OracleCommand)
            'GET LINK INFO
            Dim dtLinkTrans As DataTable = DALRoc.getTransLinkInfo(header, cmdOracle)

            'Calculate the AR Balance
            If dtLinkTrans.Rows.Count = 0 Then
                header.ar_rec_no = 0
                header.ar_link_trans_no = header.ar_trans_no
                header.ar_balance = (header.ar_dr_amount - header.ar_cr_amount)
            Else
                header.ar_rec_no = DALRoc.getNextARRecNo(header.ar_ref_no, cmdOracle)
                Dim objValue As Object = Nothing
                Dim dValue As Decimal = 0D
                Dim iValue As Integer = 0

                'calculate balance
                objValue = dtLinkTrans.Compute("SUM(AR_DR_AMOUNT) - SUM(AR_CR_AMOUNT)", "1=1")
                If IsDBNull(objValue) = False Then
                    dValue = Convert.ToDecimal(objValue)
                    header.ar_balance = dValue + (header.ar_dr_amount - header.ar_cr_amount)
                End If

                objValue = dtLinkTrans.Compute("MIN(AR_LINK_TRANS_NO)", "AR_REC_NO = 0")
                If IsDBNull(objValue) = False Then
                    iValue = Convert.ToInt64(objValue)
                    header.ar_link_trans_no = iValue
                End If
            End If
        End Sub
    End Class
End Namespace
