Imports RSG_ROC.DataLayer
Imports RSG_ROC.OrderClasses
Imports RSG_ROC.Exceptions

Namespace GLClasses
    Public Class ROC_PAYMENT_TRANSACTION
        Inherits ROC_Transaction


        Private sPaymentType As String
        Public Property payment_type() As String
            Get
                Return sPaymentType
            End Get
            Set(ByVal value As String)
                sPaymentType = value
            End Set
        End Property

        Private sOrder_No As String
        Public Property order_no() As String
            Get
                Return sOrder_No
            End Get
            Set(ByVal value As String)
                sOrder_No = value
            End Set
        End Property

        Private iInv_no As Integer
        Public Property inv_no() As Integer
            Get
                Return iInv_no
            End Get
            Set(ByVal value As Integer)
                iInv_no = value
            End Set
        End Property

        Private iRef_Seq As Integer
        Public Property ref_seq() As Integer
            Get
                Return iRef_Seq
            End Get
            Set(ByVal value As Integer)
                iRef_Seq = value
            End Set
        End Property

        Sub New(ByVal dtmBusinessDate As DateTime, ByVal aruser As String)
            MyBase.New(dtmBusinessDate, aruser)
            offset_ar_account = DALRoc.getARGL
            payment_type = ""
        End Sub

        Sub New(ByVal invoice As ROC_ON_INV_CO, ByVal pymt As REC_ON_INV_PAY, ByVal dtmBusinessDate As DateTime, ByVal aruser As String)
            Me.New(dtmBusinessDate, aruser)
            SetInfo(invoice, pymt)
        End Sub

        Public Sub SetInfo(ByVal invoice As ROC_ON_INV_CO, ByVal pymt As REC_ON_INV_PAY)
            'determine if it is a return payment or payment
            Dim dtLinkTrans As DataTable = Nothing
            inv_no = invoice.Header.inv_no
            order_no = invoice.Header.co_odno
            ref_seq = pymt.ref_seq
            payment_type = invoice.Header.co_inv_type

            header.ar_date = post_date
            header.ar_link_date = Me.calcLinkDate(post_date)
            header.ar_ref_no = invoice.Header.co_odno
            header.ar_cust_no = invoice.on_invoice.inv_cu_no

            header.ar_trans_type = "P"
            If payment_type = "R" Then 'return
                header.ar_dr_amount = Math.Abs(pymt.pay_amt)
                header.ar_cr_amount = 0

            ElseIf payment_type = "A" Then
                header.ar_dr_amount = 0
                header.ar_cr_amount = Math.Abs(pymt.pay_amt)

            Else
                Throw New Exception("ROC_PAYMENT_TRANSACTION.SetInfo(" & invoice.Header.co_odno & "," & pymt.inv_no & "," & pymt.ref_seq & ")" & vbNewLine & _
                                    "ERR MSG: Unknown payment type!")
            End If

            SetGLInfo(pymt)

        End Sub

        Private Sub SetGLInfo(ByVal pymt As REC_ON_INV_PAY)

            Dim glrec As REC_ON_GLTRAN = Nothing

            ' If payment_type = "R" Then
            

            If pymt.pay_amt = 0 Then
                Throw New GLOutOfBalanceException("ROC_PAYMENT_TRANSACTION.SETGLInfo(" & pymt.inv_no & "," & pymt.ref_seq & ") - PAYMENT AMOUNT IS 0.00!")
            Else
                glrec = New REC_ON_GLTRAN
                glrec.gl_trans_no = header.ar_trans_no
                glrec.gl_date = header.ar_date
                glrec.gl_rec_no = 0
                glrec.gl_link_date = header.ar_link_date
                glrec.gl_dept = DALRoc.getStore
                glrec.gl_cd = offset_ar_account
                glrec.gl_memo = header.ar_ref_no
                glrec.gl_dr_amount = header.ar_dr_amount
                glrec.gl_cr_amount = header.ar_cr_amount
                glrec.gl_user = header.ar_user

                DetailList.AddGLRecord(glrec)

                glrec = New REC_ON_GLTRAN
                glrec.gl_trans_no = header.ar_trans_no
                glrec.gl_date = header.ar_date
                glrec.gl_rec_no = 0
                glrec.gl_link_date = header.ar_link_date
                glrec.gl_dept = DALRoc.getStore
                glrec.gl_cd = DALRoc.getTenderGLCd(pymt.tender_cd)
                glrec.gl_memo = header.ar_ref_no
                glrec.gl_dr_amount = header.ar_cr_amount
                glrec.gl_cr_amount = header.ar_dr_amount
                glrec.gl_user = header.ar_user

                DetailList.AddGLRecord(glrec)
            End If

        End Sub

        Public Overrides Function validate() As Boolean
            Dim bValid As Boolean = True
            Dim glrec As REC_ON_GLTRAN = DetailList.Item("CR" & offset_ar_account)

            If glrec Is Nothing Then
                glrec = DetailList.Item("DR" & offset_ar_account)
            End If

            'Check if the AR values matches the GL value
            '  If parent_transfer = False Then
            bValid = (glrec.gl_dr_amount = header.ar_dr_amount And glrec.gl_cr_amount = header.ar_cr_amount)
            ' End If

            'Check the details if gl debit amount = gl credit amount
            If bValid = True Then
                bValid = DetailList.validate
            End If

            Return bValid
        End Function

        Public Overrides Sub PostTransaction(ByRef cmdOracle As System.Data.OracleClient.OracleCommand)
            MyBase.PostTransaction(cmdOracle)

            'UPDATE ACC_POST_DT FLAG ON ON_INV_PAY TABLE
            DALRoc.UpdatePaymentPostFlag(inv_no, ref_seq, trans_no, cmdOracle)
        End Sub
    End Class
End Namespace

