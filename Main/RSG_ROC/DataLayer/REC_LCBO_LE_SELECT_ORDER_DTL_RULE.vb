Namespace DataLayer
    Public Class REC_LCBO_LE_SELECT_ORDER_DTL_RULE
        Implements IComparable

        Private iLeSelectID As Integer
        Public Property le_select_id() As Integer
            Get
                Return iLeSelectID
            End Get
            Set(ByVal value As Integer)
                iLeSelectID = value
            End Set
        End Property


        Private iCodLine As Integer
        Public Property cod_line() As Integer
            Get
                Return iCodLine
            End Get
            Set(ByVal value As Integer)
                iCodLine = value
            End Set
        End Property


        Private sRule As String
        Public Property rule() As String
            Get
                Return sRule
            End Get
            Set(ByVal value As String)
                sRule = value
            End Set
        End Property

        Private sRuleGr As String
        Public Property rule_gr() As String
            Get
                Return sRuleGr
            End Get
            Set(ByVal value As String)
                sRuleGr = value
            End Set
        End Property


        Private sRate As Decimal
        Public Property rate() As Decimal
            Get
                Return sRate
            End Get
            Set(ByVal value As Decimal)
                sRate = value
            End Set
        End Property

        Private sRateCd As String
        Public Property rate_cd() As String
            Get
                Return sRateCd
            End Get
            Set(ByVal value As String)
                sRateCd = value
            End Set
        End Property

        Private dRuleAmt As Decimal
        Public Property rule_amt() As Decimal
            Get
                Return dRuleAmt
            End Get
            Set(ByVal value As Decimal)
                dRuleAmt = value
            End Set
        End Property

        Private sItemCategory As String
        Public Property item_category() As String
            Get
                Return sItemCategory
            End Get
            Set(ByVal value As String)
                sItemCategory = value
            End Set
        End Property

        Private sAction As String
        Public Property action() As String
            Get
                Return sAction
            End Get
            Set(ByVal value As String)
                sAction = value
            End Set
        End Property

        Sub New()
            le_select_id = 0
            cod_line = 0
            rule = ""
            rule_gr = ""
            rate = 0D
            rate_cd = ""
            rule_amt = 0D
            item_category = ""
            action = "N"
        End Sub

        Sub New(ByVal drRules As DataRow)
            If IsDBNull(drRules("LE_SELECT_ID")) = False Then
                le_select_id = drRules("LE_SELECT_ID")
            End If

            If IsDBNull(drRules("COD_LINE")) = False Then
                cod_line = drRules("COD_LINE")
            End If

            If IsDBNull(drRules("RULE")) = False Then
                rule = drRules("RULE")
            End If

            If IsDBNull(drRules("RULE_GR")) = False Then
                rule_gr = drRules("RULE_GR")
            End If

            If IsDBNull(drRules("RATE")) = False Then
                rate = drRules("RATE")
            End If

            If IsDBNull(drRules("RATE_CD")) = False Then
                rate_cd = drRules("RATE_CD")
            End If

            If IsDBNull(drRules("RULE_AMT")) = False Then
                rule_amt = drRules("RULE_AMT")
            End If

            If IsDBNull(drRules("ITEM_CATEGORY")) = False Then
                item_category = drRules("ITEM_CATEGORY")
            End If

            If IsDBNull(drRules("ACTION")) = False Then
                action = drRules("ACTION")
            End If

        End Sub

        Function validate() As Boolean
            Dim bValid As Boolean = True

            If action = "E" Then
                action = "U"
            End If

            If action = "N" Then
                action = "A"
            End If

            If (action = "A" Or _
               action = "U" Or _
               action = "D") = False Then
                bValid = False
            End If



            bValid = bValid AndAlso IsNumeric(le_select_id)
            bValid = bValid AndAlso le_select_id <> 0
            bValid = bValid AndAlso cod_line <> 0


            If action <> "D" Then
                bValid = bValid AndAlso rule.Trim <> ""
                bValid = bValid AndAlso rate_cd.Trim <> ""
                bValid = bValid AndAlso item_category.Trim <> ""

            End If

            Return bValid
        End Function

        Sub Update(ByRef oracmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()

            Dim sQry As String = ""

            Try
                If bValid = False Then
                    Throw New Exception("Error validating LCBO_LE_SELECT_ORDER_DTL_RULES record")
                End If

                If action = "U" Then
                    sQry = " UPDATE DBO.LCBO_LE_SELECT_ORDER_DTL_RULES " & vbNewLine & _
                                "SET  RULE_AMT  = " & rule_amt & vbNewLine & _
                                ", ITEM_CATEGORY = '" & item_category.Trim & "' " & vbNewLine & _
                                " WHERE LE_SELECT_ID = " & le_select_id & vbNewLine & _
                                " AND COD_LINE = " & cod_line & vbNewLine & _
                                " AND TRIM(RULE) = '" & rule.Trim & "'"


                ElseIf action = "D" Then
                    sQry = "DELETE FROM DBO.LCBO_LE_SELECT_ORDER_DTL_RULES " & vbNewLine & _
                           " WHERE LE_SELECT_ID = " & le_select_id & vbNewLine & _
                           " AND COD_LINE = " & cod_line & vbNewLine & _
                           " AND TRIM(RULE) = '" & rule.Trim & "'"

                ElseIf action = "A" Then
                    sQry = "INSERT INTO DBO.LCBO_LE_SELECT_ORDER_DTL_RULES (" & vbNewLine & _
                                 " LE_SELECT_ID        " & vbNewLine & _
                                 ", COD_LINE           " & vbNewLine & _
                                ", RULE              " & vbNewLine & _
                                ", RULE_GR         " & vbNewLine & _
                                ", RATE          " & vbNewLine & _
                                ", RATE_CD          " & vbNewLine & _
                                ", RULE_AMT               " & vbNewLine & _
                                ", ITEM_CATEGORY          " & vbNewLine & _
                                ") VALUES (" & vbNewLine & _
                                le_select_id & vbNewLine & _
                                "," & cod_line & vbNewLine & _
                                ",'" & rule.Trim & "'" & vbNewLine & _
                                ",'" & rule_gr.Trim & "'" & vbNewLine & _
                                "," & rate & vbNewLine & _
                                ",'" & rate_cd.Trim & "'" & vbNewLine & _
                                "," & rule_amt & vbNewLine & _
                                ",'" & item_category.Trim & "'" & vbNewLine & _
                                ")"

                End If

                oracmd.CommandText = sQry
                oracmd.Parameters.Clear()

                If oracmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error UPDATING LCBO_LE_SELECT_ORDER_DTL!")
                End If

            Catch ex As Exception
                Throw New Exception("LCBO_LE_SELECT_ORDER_DTL_RULES.UPDATE->" & le_select_id & "," & cod_line & vbNewLine & _
                                    "QRY: " & sQry & vbNewLine & _
                                    "ErrMsg:" & ex.Message.ToString)
            End Try

        End Sub


        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo

        End Function
    End Class
End Namespace
