Imports System.Data
Imports System.Collections

Namespace DataLayer
    Public Class REC_CRDTL
        Implements IComparable
        ''' <summary>
        ''' CR_ODNO,CHAR,16,,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sCr_Odno As String
        Public Property cr_odno() As String
            Get
                Return sCr_Odno
            End Get
            Set(ByVal value As String)
                sCr_Odno = value
            End Set
        End Property

        ''' <summary>
        ''' CR_LINE,NUMBER,10,,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sCR_Line As Integer
        Public Property cr_line() As Integer
            Get
                Return sCR_Line
            End Get
            Set(ByVal value As Integer)
                sCR_Line = value
            End Set
        End Property

        ''' <summary>
        ''' ITEM,CHAR,20,,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sItem As String
        Public Property item() As String
            Get
                Return sItem
            End Get
            Set(ByVal value As String)
                sItem = value
            End Set
        End Property

        ''' <summary>
        ''' PACKAGING_CODE,CHAR,1,,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sPackaging_Code As String
        Public Property packaging_code() As String
            Get
                Return sPackaging_Code
            End Get
            Set(ByVal value As String)
                sPackaging_Code = value
            End Set
        End Property

        ''' <summary>
        ''' EXPECTED_QTY,NUMBER,10,,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private iExpected_Qty As Integer
        Public Property expected_qty() As Integer
            Get
                Return iExpected_Qty
            End Get
            Set(ByVal value As Integer)
                iExpected_Qty = value
            End Set
        End Property

        ''' <summary>
        ''' RECEIVED_QTY,NUMBER,10,,Y,,
        ''' </summary>
        ''' <remarks></remarks>
        Private iReceived_Qty As Integer
        Public Property received_qty() As Integer
            Get
                Return iReceived_Qty
            End Get
            Set(ByVal value As Integer)
                iReceived_Qty = value
            End Set
        End Property

        'RETURN_CODE,CHAR,6,,Y,,
        'REASON_CODE,CHAR,2,,Y,,
        'MESSAGE,VARCHAR2,80,,Y,,
        'LAST_RCV_DT_TM,DATE,7,,Y,,
        'EM_NO,CHAR,11,,Y,,
        'NEW_ITEM,CHAR,20,,Y,,
        'NEW_PACKAGING_CODE,CHAR,1,,Y,,

        ''' <summary>
        ''' REF_SEQ_NO,NUMBER,10,,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private iRef_Seq_No As Integer
        Public Property ref_seq() As Integer
            Get
                Return iRef_Seq_No
            End Get
            Set(ByVal value As Integer)
                iRef_Seq_No = value
            End Set
        End Property

        'SCRAP_CODE,CHAR,2,,Y,,
        'SCRAP_DESC,VARCHAR2,17,,Y,,
        'FILL_FROM_ITEM,CHAR,20,,Y,,
        'FILL_FROM_PACKAGING_CODE,CHAR,1,,N,,' '
        'UNIT_OF_MEASURE,CHAR,4,,Y,,
        'CONVERSION_FACTOR,NUMBER,14,4,Y,,
        'SALES_UNIT_OF_MEASURE,CHAR,4,,Y,,
        'SALES_QUANTITY,NUMBER,10,,Y,,
        'SCRAP_FL,CHAR,1,,Y,,
        'TRANS_ORDER_FL,CHAR,1,,Y,,
        'TRANS_ORDER_DEST,CHAR,10,,Y,,
        'FREIGHT_FORWARD_FL,CHAR,1,,Y,,
        'PACK_DATE_CODE,CHAR,5,,Y,,
        'PACK_VENDOR_CODE,CHAR,7,,Y,,
        'TOTAL_CREDIT_AMT,NUMBER,14,2,Y,,

        ''' <summary>
        ''' CONFIRM_QTY,NUMBER,10,,N,,0
        ''' </summary>
        ''' <remarks></remarks>
        Private iConfirm_Qty As Integer
        Public Property confirm_qty() As Integer
            Get
                Return iConfirm_Qty
            End Get
            Set(ByVal value As Integer)
                iConfirm_Qty = value
            End Set
        End Property

        Sub New()
            cr_odno = ""
            cr_line = 0
            item = ""
            packaging_code = " "
            expected_qty = 0
            received_qty = 0
            confirm_qty = 0
        End Sub

        Sub SetInfo(ByVal dr As DataRow)
            cr_odno = dr("CR_ODNO")
            cr_line = dr("CR_LINE")
            item = dr("ITEM")
            packaging_code = dr("PACKAGING_CODE")
            expected_qty = dr("EXPECTED_QTY")
            received_qty = dr("RECEIVED_QTY")
            confirm_qty = dr("CONFIRM_QTY")
        End Sub

        Public Function validate() As Boolean
            Dim bValid As Boolean = True
            bValid = bValid AndAlso (cr_odno.Trim <> "")
            bValid = bValid AndAlso (cr_line > 0)
            bValid = bValid AndAlso (item.Trim <> "")
            bValid = bValid AndAlso (expected_qty + received_qty + confirm_qty > 0)

            Return bValid
        End Function
        Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()

            Dim sQry As String = "INSERT INTO DBO.CRDTL (" & vbNewLine & _
                                 " CR_ODNO " & vbNewLine & _
                                 ",CR_LINE " & vbNewLine & _
                                 ",ITEM " & vbNewLine & _
                                 ",PACKAGING_CODE " & vbNewLine & _
                                 ",EXPECTED_QTY " & vbNewLine & _
                                 ",RECEIVED_QTY " & vbNewLine & _
                                 ",CONFIRM_QTY " & vbNewLine & _
                                 ") VALUES (" & vbNewLine & _
                                 "'" & cr_odno & "'" & vbNewLine & _
                                 "," & cr_line & vbNewLine & _
                                 ",'" & item & "'" & vbNewLine & _
                                 ",'" & packaging_code & "'" & vbNewLine & _
                                 "," & expected_qty & vbNewLine & _
                                 "," & received_qty & vbNewLine & _
                                 "," & confirm_qty & vbNewLine & _
                                 ")"

            Try
                If bValid = False Then
                    Throw New Exception("Error validating CRDTL record")
                End If
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting CRDTL record query: " & sQry & "!")
                End If

            Catch ex As Exception
                Throw New Exception("CRDTL.Insert->" & cr_odno & " qry:" & sQry & ".Error Message:" & ex.Message.ToString)
            End Try

        End Sub
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Dim b As REC_CRDTL = obj
            Return Me.cr_line.CompareTo(b.cr_line)
        End Function
    End Class
End Namespace
'CR_ODNO,CHAR,16,,N,,
'CR_LINE,NUMBER,10,,N,,
'ITEM,CHAR,20,,N,,
'PACKAGING_CODE,CHAR,1,,N,,
'EXPECTED_QTY,NUMBER,10,,N,,
'RECEIVED_QTY,NUMBER,10,,Y,,
'TIMESTAMP,DATE,7,,Y,,
'RETURN_CODE,CHAR,6,,Y,,
'REASON_CODE,CHAR,2,,Y,,
'MESSAGE,VARCHAR2,80,,Y,,
'LAST_RCV_DT_TM,DATE,7,,Y,,
'EM_NO,CHAR,11,,Y,,
'NEW_ITEM,CHAR,20,,Y,,
'NEW_PACKAGING_CODE,CHAR,1,,Y,,
'REF_SEQ_NO,NUMBER,10,,N,,
'SCRAP_CODE,CHAR,2,,Y,,
'SCRAP_DESC,VARCHAR2,17,,Y,,
'FILL_FROM_ITEM,CHAR,20,,Y,,
'FILL_FROM_PACKAGING_CODE,CHAR,1,,N,,' '
'UNIT_OF_MEASURE,CHAR,4,,Y,,
'CONVERSION_FACTOR,NUMBER,14,4,Y,,
'SALES_UNIT_OF_MEASURE,CHAR,4,,Y,,
'SALES_QUANTITY,NUMBER,10,,Y,,
'SCRAP_FL,CHAR,1,,Y,,
'TRANS_ORDER_FL,CHAR,1,,Y,,
'TRANS_ORDER_DEST,CHAR,10,,Y,,
'FREIGHT_FORWARD_FL,CHAR,1,,Y,,
'PACK_DATE_CODE,CHAR,5,,Y,,
'PACK_VENDOR_CODE,CHAR,7,,Y,,
'TOTAL_CREDIT_AMT,NUMBER,14,2,Y,,
'CONFIRM_QTY,NUMBER,10,,N,,0