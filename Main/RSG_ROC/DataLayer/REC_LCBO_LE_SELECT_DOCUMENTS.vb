Imports Microsoft.VisualBasic
Imports System.Collections
Imports RSG_ROC.DataLayer

Namespace DataLayer

    Public Class REC_LCBO_LE_SELECT_DOCUMENTS
        Implements IComparable

        Private iLeSelectID As Integer
        Public Property le_select_id() As Integer
            Get
                Return iLeSelectID
            End Get
            Set(ByVal value As Integer)
                iLeSelectID = value
            End Set
        End Property

        Private sDocID As String
        Public Property doc_id() As String
            Get
                Return sDocID
            End Get
            Set(ByVal value As String)
                sDocID = value
            End Set
        End Property


        Private dDoc_Type_ID As Decimal
        Public Property doc_type_id() As Decimal
            Get
                Return dDoc_Type_ID
            End Get
            Set(ByVal value As Decimal)
                dDoc_Type_ID = value
            End Set
        End Property


        Private sDoc_FilePath As String
        Public Property doc_filepath() As String
            Get
                Return sDoc_FilePath
            End Get
            Set(ByVal value As String)
                sDoc_FilePath = value
            End Set
        End Property

        Private sOrig_Filename As String
        Public Property orig_filename() As String
            Get
                Return sOrig_Filename
            End Get
            Set(ByVal value As String)
                sOrig_Filename = value
            End Set
        End Property

        Private dtUploadDt As DateTime
        Public Property upload_dt() As DateTime
            Get
                Return dtUploadDt
            End Get
            Set(ByVal value As DateTime)
                dtUploadDt = value
            End Set
        End Property

        Private sUploadBy As String
        Public Property upload_by() As String
            Get
                Return sUploadBy
            End Get
            Set(ByVal value As String)
                sUploadBy = value
            End Set
        End Property


        Private sDocumentDescription As String
        Public Property doc_desc() As String
            Get
                Return sDocumentDescription
            End Get
            Set(ByVal value As String)
                sDocumentDescription = value
            End Set
        End Property

        Private sDocumentRequired As String
        Public Property doc_required() As String
            Get
                Return sDocumentRequired
            End Get
            Set(ByVal value As String)
                sDocumentRequired = value
            End Set
        End Property


        Private sAction As String
        Public Property action() As String
            Get
                Return sAction
            End Get
            Set(ByVal value As String)
                sAction = value
            End Set
        End Property

        Private iCheck_ID As Integer
        Public Property check_id() As Integer
            Get
                Return iCheck_ID
            End Get
            Set(ByVal value As Integer)
                iCheck_ID = value
            End Set
        End Property


        Sub New()
            le_select_id = 0
            doc_id = ""
            doc_type_id = 0
            doc_filepath = ""
            upload_dt = New DateTime
            upload_by = ""
            doc_desc = ""
            doc_required = ""
            action = "A"
            check_id = 0
            sOrig_Filename = ""

        End Sub

        Sub New(ByVal dr As DataRow)
            le_select_id = dr("LE_SELECT_ID")
            If IsDBNull(dr("DOC_ID")) = False Then
                doc_id = dr("DOC_ID")
            End If
            If IsDBNull(dr("DOC_TYPE_ID")) = False Then
                doc_type_id = dr("DOC_TYPE_ID")
            End If
            If IsDBNull(dr("DOC_FILEPATH")) = False Then
                doc_filepath = dr("DOC_FILEPATH")
            End If
            If IsDBNull(dr("UPLOAD_DT")) = False Then
                upload_dt = dr("UPLOAD_DT")
            End If
            If IsDBNull(dr("UPLOAD_BY")) = False Then
                upload_by = dr("UPLOAD_BY")
            End If
            If IsDBNull(dr("DOC_DESC")) = False Then
                doc_desc = dr("DOC_DESC")
            End If
            If IsDBNull(dr("DOC_REQUIRED")) = False Then
                doc_required = dr("DOC_REQUIRED")
            End If
            If IsDBNull(dr("ACTION")) = False Then
                action = dr("ACTION")
            End If
            If IsDBNull(dr("CHECK_ID")) = False Then
                check_id = dr("CHECK_ID")
            End If
            If IsDBNull(dr("ORIG_FILENAME")) = False Then
                orig_filename = dr("ORIG_FILENAME")
            End If

        End Sub
        Function validate() As Boolean
            Dim bValid As Boolean = True

            bValid = bValid AndAlso IsNumeric(le_select_id)
            bValid = bValid AndAlso le_select_id <> 0
            bValid = bValid AndAlso doc_id.Trim <> ""
            bValid = bValid AndAlso doc_filepath.Trim <> ""
            bValid = bValid AndAlso upload_dt.Year > 200

            Return bValid
        End Function

        Sub Update(ByVal oracmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()
            Dim sQry As String = ""
            If action = "A" Then
                sQry = " INSERT INTO DBO.LCBO_LE_SELECT_DOCUMENTS (" & vbNewLine & _
                            "LE_SELECT_ID " & vbNewLine & _
                            ",DOC_ID " & vbNewLine & _
                            ",DOC_TYPE_ID " & vbNewLine & _
                            ",DOC_FILEPATH " & vbNewLine & _
                            ",ORIG_FILENAME " & vbNewLine & _
                            ",UPLOAD_DT " & vbNewLine & _
                            ",UPLOAD_BY " & vbNewLine & _
                            ") VALUES (" & vbNewLine & _
                            le_select_id & vbNewLine & _
                            ",'" & doc_id.Trim & "'" & vbNewLine & _
                            "," & doc_type_id & vbNewLine & _
                            ",'" & doc_filepath & "'" & vbNewLine & _
                            ",'" & orig_filename & "'" & vbNewLine & _
                            ",TO_DATE('" & upload_dt.ToString("yyyyMMddHHmmss") & "','YYYYMMDDHH24MISS')" & vbNewLine & _
                            ",'" & upload_by & "'" & vbNewLine & _
                            ")"

            ElseIf action = "U" Then
                sQry = "UPDATE DBO.LCBO_LE_SELECT_DOCUMENTS " & vbNewLine & _
                            "SET DOC_TYPE_ID = " & doc_type_id & vbNewLine & _
                            ",DOC_FILEPATH  = '" & doc_filepath.Trim & "'" & vbNewLine & _
                            ",ORIG_FILENAME = '" & orig_filename & "'" & vbNewLine & _
                            ",UPLOAD_DT = SYSDATE " & vbNewLine & _
                            ",UPLOAD_BY = '" & upload_by & "'" & vbNewLine & _
                           "WHERE LE_SELECT_ID = " & le_select_id & vbNewLine & _
                           " AND DOC_TYPE_ID = '" & doc_type_id & "'"

            ElseIf action = "D" Then
                sQry = " DELETE FROM DBO.LCBO_LE_SELECT_DOCUMENTS " & vbNewLine & _
                        " WHERE LE_SELECT_ID = " & le_select_id & vbNewLine & _
                        " AND DOC_ID = '" & doc_id & "'"
            End If

            Try
                If bValid = False Then
                    Throw New Exception("Error validating LCBO_LE_SELECT_CHECK_LIST record")
                End If
                oracmd.CommandText = sQry
                oracmd.Parameters.Clear()

                If oracmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting LCBO_LE_SELECT_CHECK_LIST!")
                End If

            Catch ex As Exception
                Throw New Exception("LCBO_LE_SELECT_DOCUMENTS.UPDATE->" & le_select_id & "," & doc_id & vbNewLine & _
                                    ".Error Message:" & ex.Message.ToString)
            End Try
        End Sub
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Dim b As REC_LCBO_LE_SELECT_DOCUMENTS = obj
            Return b.doc_type_id.CompareTo(Me.doc_type_id)
        End Function
    End Class
End Namespace

