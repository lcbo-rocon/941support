Namespace DataLayer
    Public Class REC_LCBO_TRANSFERRED_ORDERS

        ''' <summary>
        ''' ORDER_NO,CHAR,16,,N,
        ''' </summary>
        ''' <remarks></remarks>
        Private sOrder_No As String
        Public Property order_no() As String
            Get
                Return sOrder_No
            End Get
            Set(ByVal value As String)
                sOrder_No = value
            End Set
        End Property

        ''' <summary>
        ''' CU_NO,CHAR,10,,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private sCu_No As String
        Public Property cu_no() As String
            Get
                Return sCu_No
            End Get
            Set(ByVal value As String)
                sCu_No = value
            End Set
        End Property

        ''' <summary>
        ''' INV_AMT,NUMBER,14,2,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private dInv_Amt As Decimal
        Public Property inv_amt() As Decimal
            Get
                Return dInv_Amt
            End Get
            Set(ByVal value As Decimal)
                dInv_Amt = value
            End Set
        End Property

        ''' <summary>
        ''' ORIG_ORDER_NO,CHAR,16,,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private sOrig_Order_No As String
        Public Property orig_order_no() As String
            Get
                Return sOrig_Order_No
            End Get
            Set(ByVal value As String)
                sOrig_Order_No = value
            End Set
        End Property

        ''' <summary>
        ''' ORIG_CU_NO,CHAR,10,,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private sOrig_Cu_No As String
        Public Property orig_cu_no() As String
            Get
                Return sOrig_Cu_No
            End Get
            Set(ByVal value As String)
                sOrig_Cu_No = value
            End Set
        End Property

        ''' <summary>
        ''' ORIG_INV_AMT,NUMBER,14,2,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private dOrig_Inv_Amt As Decimal
        Public Property orig_inv_amt() As Decimal
            Get
                Return dOrig_Inv_Amt
            End Get
            Set(ByVal value As Decimal)
                dOrig_Inv_Amt = value
            End Set
        End Property

        ''' <summary>
        ''' DELIVERY_CHG_TRANS,CHAR,6,,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private sDelivery_Chg_Trans As String
        Public Property delivery_chg_trans() As String
            Get
                Return sDelivery_Chg_Trans
            End Get
            Set(ByVal value As String)
                sDelivery_Chg_Trans = value
            End Set
        End Property

        ''' <summary>
        ''' JE_PAYMENT_TRANS_NO,CHAR,6,,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private sJe_Payment_Trans_No As String
        Public Property je_payment_trans_no() As String
            Get
                Return sJe_Payment_Trans_No
            End Get
            Set(ByVal value As String)
                sJe_Payment_Trans_No = value
            End Set
        End Property

        ''' <summary>
        ''' CR_ORDER_NO,CHAR,16,,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private sCr_Order_No As String
        Public Property cr_order_no() As String
            Get
                Return sCr_Order_No
            End Get
            Set(ByVal value As String)
                sCr_Order_No = value
            End Set
        End Property

        ''' <summary>
        ''' CR_CU_NO,CHAR,10,,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private sCr_Cu_No As String
        Public Property cr_cu_no() As String
            Get
                Return sCr_Cu_No
            End Get
            Set(ByVal value As String)
                sCr_Cu_No = value
            End Set
        End Property

        Private dCr_Inv_Amt As Decimal
        Public Property cr_inv_amt() As Decimal
            Get
                Return dCr_Inv_Amt
            End Get
            Set(ByVal value As Decimal)
                dCr_Inv_Amt = value
            End Set
        End Property

        ''' <summary>
        ''' TRANSFER_COMMENTS,VARCHAR2,100,,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private sTransfer_Comments As String
        Public Property transfer_comments() As String
            Get
                Return sTransfer_Comments
            End Get
            Set(ByVal value As String)
                sTransfer_Comments = value
            End Set
        End Property

        ''' <summary>
        ''' EMP_NO,CHAR,10,,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private sEmp_No As String
        Public Property emp_no() As String
            Get
                Return sEmp_No
            End Get
            Set(ByVal value As String)
                sEmp_No = value
            End Set
        End Property

        ''' <summary>
        ''' TRANSFER_DT,DATE,7,,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private sTransfer_Dt As String
        Public Property transfer_dt() As String
            Get
                Return sTransfer_Dt
            End Get
            Set(ByVal value As String)
                sTransfer_Dt = value
            End Set
        End Property


        Private iOrig_Payment_Count As Integer
        Public Property orig_payment_count() As Integer
            Get
                Return iOrig_Payment_Count
            End Get
            Set(ByVal value As Integer)
                iOrig_Payment_Count = value
            End Set
        End Property

        Private dOrig_Payment_Total As Decimal
        Public Property orig_payment_total() As Decimal
            Get
                Return dOrig_Payment_Total
            End Get
            Set(ByVal value As Decimal)
                dOrig_Payment_Total = value
            End Set
        End Property

        Private dOrig_Credit_Pay_Total As Decimal
        Public Property orig_credit_pay_total() As Decimal
            Get
                Return dOrig_Credit_Pay_Total
            End Get
            Set(ByVal value As Decimal)
                dOrig_Credit_Pay_Total = value
            End Set
        End Property



        Sub New()
            order_no = ""
            cu_no = ""
            inv_amt = 0
            orig_order_no = ""
            orig_cu_no = ""
            orig_inv_amt = 0
            delivery_chg_trans = ""
            je_payment_trans_no = ""
            cr_order_no = ""
            cr_cu_no = ""
            cr_inv_amt = 0
            orig_payment_count = 0
            orig_payment_total = 0D
            orig_credit_pay_total = 0D
            transfer_comments = ""
            emp_no = ""
            transfer_dt = ""


        End Sub

        Function validate() As Boolean
            Dim bValid As Boolean = True
            bValid = bValid AndAlso order_no.Trim <> ""
            bValid = bValid AndAlso cu_no.Trim <> ""
            bValid = bValid AndAlso inv_amt <> 0
            bValid = bValid AndAlso orig_order_no.Trim <> ""
            bValid = bValid AndAlso orig_cu_no.Trim <> ""
            bValid = bValid AndAlso orig_inv_amt <> 0
            bValid = bValid AndAlso cr_order_no.Trim <> ""
            bValid = bValid AndAlso cr_cu_no.Trim <> ""
            bValid = bValid AndAlso cr_inv_amt <> 0
            bValid = bValid AndAlso transfer_comments.Trim <> ""
            bValid = bValid AndAlso emp_no.Trim <> ""
            bValid = bValid AndAlso transfer_dt <> ""

            'orig_amt should be the same as new order amt
            bValid = bValid AndAlso inv_amt = orig_inv_amt

            'return customer should be the same as the original
            bValid = bValid AndAlso orig_cu_no.Trim = cr_cu_no.Trim

            'cannot freight forward to the same customer
            bValid = bValid AndAlso orig_cu_no.Trim <> cu_no.Trim

            Return bValid
        End Function
        Sub Insert(ByVal oraCmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()

            Dim sQry As String = "INSERT INTO DBO.LCBO_TRANSFERRED_ORDERS (" & vbNewLine & _
                                 " ORDER_NO             " & vbNewLine & _
                                " ,CU_NO               " & vbNewLine & _
                                " ,INV_AMT             " & vbNewLine & _
                                " ,ORIG_ORDER_NO       " & vbNewLine & _
                                " ,ORIG_CU_NO          " & vbNewLine & _
                                " ,ORIG_INV_AMT        " & vbNewLine & _
                                " ,CR_ORDER_NO         " & vbNewLine & _
                                " ,CR_CU_NO            " & vbNewLine & _
                                " ,CR_INV_AMT          " & vbNewLine & _
                                " ,DELIVERY_CHG_TRANS  " & vbNewLine & _
                                " ,ORIG_PAYMENT_COUNT  " & vbNewLine & _
                                " ,ORIG_PAYMENT_TOTAL  " & vbNewLine & _
                                " ,ORIG_CREDIT_PAY_TOTAL " & vbNewLine & _
                                " ,TRANSFER_COMMENTS   " & vbNewLine & _
                                " ,EMP_NO              " & vbNewLine & _
                                " ,TRANSFER_DT         " & vbNewLine & _
                                ") values (" & vbNewLine & _
                                " '" & order_no & "'" & vbNewLine & _
                                " ,'" & cu_no.Trim & "'" & vbNewLine & _
                                " ," & inv_amt & vbNewLine & _
                                " ,'" & orig_order_no.Trim & "'" & vbNewLine & _
                                " ,'" & orig_cu_no.Trim & "'" & vbNewLine & _
                                " , " & orig_inv_amt & vbNewLine & _
                                " ,'" & cr_order_no.Trim & "'" & vbNewLine & _
                                " ,'" & cr_cu_no.Trim & "'" & vbNewLine & _
                                " ," & cr_inv_amt & vbNewLine & _
                                ", '" & Me.delivery_chg_trans & "'" & vbNewLine & _
                                " ," & orig_payment_count & vbNewLine & _
                                " ," & orig_payment_total & vbNewLine & _
                                " ," & orig_credit_pay_total & vbNewLine & _
                                " ,'" & transfer_comments.Trim & "'" & vbNewLine & _
                                " ,'" & emp_no.Trim & "'" & vbNewLine & _
                                " ,TO_DATE('" & transfer_dt.Trim & "','YYYY-MM-DD') " & vbNewLine & _
                                ")"

            Try
                If bValid = False Then
                    Throw New Exception("Error validating LCBO_TRANSFERRED_ORDERS record")
                End If
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting LCBO_TRANSFERRED_ORDERS record query: " & sQry & "!")
                End If

            Catch ex As Exception
                Throw New Exception("LCBO_TRANSFERRED_ORDERS.Insert->" & order_no & ".Error Message:" & ex.Message.ToString)
            End Try

        End Sub
    End Class
End Namespace
'ORDER_NO,CHAR,16,,N,
'CU_NO,CHAR,10,,Y,
'INV_AMT,NUMBER,14,2,Y,
'ORIG_ORDER_NO,CHAR,16,,Y,
'ORIG_CU_NO,CHAR,10,,Y,
'ORIG_INV_AMT,NUMBER,14,2,Y,
'DELIVERY_CHG_TRANS,CHAR,6,,Y,
'JE_PAYMENT_TRANS_NO,CHAR,6,,Y,
'CR_ORDER_NO,CHAR,16,,Y,
'CR_CU_NO,CHAR,10,,Y,
'CR_INV_AMT,NUMBER,14,2,Y,
'TRANSFER_COMMENTS,VARCHAR2,100,,Y,
'EMP_NO,CHAR,10,,Y,
'TRANSFER_DT,DATE,7,,Y,