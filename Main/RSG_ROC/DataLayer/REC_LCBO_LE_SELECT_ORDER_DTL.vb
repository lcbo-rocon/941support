Imports Microsoft.VisualBasic
Imports System.Collections

Namespace DataLayer
    Public Class REC_LCBO_LE_SELECT_ORDER_DTL
        Implements IComparable

        Private iLeSelectID As Integer
        Public Property le_select_id() As Integer
            Get
                Return iLeSelectID
            End Get
            Set(ByVal value As Integer)
                iLeSelectID = value
            End Set
        End Property

        Private iCOD_Line As Integer
        Public Property cod_line() As Integer
            Get
                Return iCOD_Line
            End Get
            Set(ByVal value As Integer)
                iCOD_Line = value
            End Set
        End Property

        Private dPrice As Decimal
        Public Property price() As Decimal
            Get
                Return dPrice
            End Get
            Set(ByVal value As Decimal)
                dPrice = value
            End Set
        End Property

        Private dExtd_Price As Decimal
        Public Property extd_price() As Decimal
            Get
                Return dExtd_Price
            End Get
            Set(ByVal value As Decimal)
                dExtd_Price = value
            End Set
        End Property

        Private iTTL_Cases As Integer
        Public Property ttl_cases() As Integer
            Get
                Return iTTL_Cases
            End Get
            Set(ByVal value As Integer)
                iTTL_Cases = value
            End Set
        End Property


        Private iTTL_Units As Integer
        Public Property ttl_units() As Integer
            Get
                Return iTTL_Units
            End Get
            Set(ByVal value As Integer)
                iTTL_Units = value
            End Set
        End Property

        Private sItem As String
        Public Property item() As String
            Get
                Return sItem
            End Get
            Set(ByVal value As String)
                sItem = value
            End Set
        End Property

        Private sItemDesc As String
        Public Property item_desc() As String
            Get
                Return sItemDesc
            End Get
            Set(ByVal value As String)
                sItemDesc = value
            End Set
        End Property

        Private sCatchallDesc As String
        Public Property catchall_desc() As String
            Get
                Return sCatchallDesc
            End Get
            Set(ByVal value As String)
                sCatchallDesc = value
            End Set
        End Property


        Private sIsCatchAll As String
        Public Property is_Catchall() As String
            Get
                Return sIsCatchAll
            End Get
            Set(ByVal value As String)
                sIsCatchAll = value
            End Set
        End Property

        Private sPackaging_Code As String
        Public Property packaging_code() As String
            Get
                Return sPackaging_Code
            End Get
            Set(ByVal value As String)
                sPackaging_Code = value
            End Set
        End Property


        Private dDeposit_Price As Decimal
        Public Property deposit_price() As Decimal
            Get
                Return dDeposit_Price
            End Get
            Set(ByVal value As Decimal)
                dDeposit_Price = value
            End Set
        End Property

        Private iQty As Integer
        Public Property qty() As Integer
            Get
                Return iQty
            End Get
            Set(ByVal value As Integer)
                iQty = value

            End Set
        End Property

        Private sItem_Category As String
        Public Property item_category() As String
            Get
                Return sItem_Category
            End Get
            Set(ByVal value As String)
                sItem_Category = value
            End Set
        End Property

        Private dOrig_Price As Decimal
        Public Property orig_price() As Decimal
            Get
                Return dOrig_Price
            End Get
            Set(ByVal value As Decimal)
                dOrig_Price = value
            End Set
        End Property


        Private sPrice_Chg_Comment As String
        Public Property price_chg_comment() As String
            Get
                Return sPrice_Chg_Comment
            End Get
            Set(ByVal value As String)
                sPrice_Chg_Comment = value
            End Set
        End Property

        Private dRetail_Price As Decimal
        Public Property retail_price() As Decimal
            Get
                Return dRetail_Price
            End Get
            Set(ByVal value As Decimal)
                dRetail_Price = value
            End Set
        End Property

        Private dOrig_Retail_Price As Decimal
        Public Property orig_retail_price() As Decimal
            Get
                Return dOrig_Retail_Price
            End Get
            Set(ByVal value As Decimal)
                dOrig_Retail_Price = value
            End Set
        End Property

        Private sAction As String
        Public Property action() As String
            Get
                Return sAction
            End Get
            Set(ByVal value As String)
                sAction = value
            End Set
        End Property


        Private iCase_Sz As Integer
        Public Property case_sz() As Integer
            Get
                Return iCase_Sz
            End Get
            Set(ByVal value As Integer)
                iCase_Sz = value
            End Set
        End Property

        Private iPack_Factor As Integer
        Public Property pack_factor() As Integer
            Get
                Return iPack_Factor
            End Get
            Set(ByVal value As Integer)
                iPack_Factor = value
            End Set
        End Property

        Sub New()
            le_select_id = 0
            cod_line = 0
            price = 0D
            extd_price = 0D
            ttl_cases = 0
            ttl_units = 0
            item = ""
            item_desc = ""
            catchall_desc = ""
            is_Catchall = "N"
            packaging_code = " "
            deposit_price = 0D
            qty = 0
            item_category = ""
            orig_price = 0D
            price_chg_comment = ""
            retail_price = 0D
            orig_retail_price = 0D
            action = "A"
            case_sz = 1
            pack_factor = 1
        End Sub

        Sub New(ByVal dr As DataRow)
            Me.New()
            SetInfo(dr)
        End Sub

        Sub New(ByVal Cp As REC_LCBO_LE_SELECT_ORDER_DTL)
            Me.New()
            le_select_id = Cp.le_select_id
            cod_line = Cp.cod_line
            price = Cp.price
            extd_price = Cp.extd_price
            ttl_cases = Cp.ttl_cases
            ttl_units = Cp.ttl_units
            item = Cp.item
            item_desc = Cp.item_desc
            catchall_desc = Cp.catchall_desc
            is_Catchall = Cp.is_Catchall
            packaging_code = Cp.packaging_code
            deposit_price = Cp.deposit_price
            qty = Cp.qty
            item_category = Cp.item_category
            orig_price = Cp.orig_price
            price_chg_comment = Cp.price_chg_comment
            retail_price = Cp.retail_price
            orig_retail_price = Cp.orig_retail_price
            action = Cp.action
            case_sz = Cp.case_sz
            pack_factor = Cp.pack_factor
        End Sub

        Sub SetInfo(ByVal dr As DataRow)
            le_select_id = dr("LE_SELECT_ID")

            If IsDBNull(dr("COD_LINE")) = False Then
                cod_line = dr("COD_LINE")
            End If
            If IsDBNull(dr("PRICE")) = False Then
                price = dr("PRICE")
            End If
            If IsDBNull(dr("EXTD_PRICE")) = False Then
                extd_price = dr("EXTD_PRICE")
            End If
            If IsDBNull(dr("TTL_CASES")) = False Then
                ttl_cases = dr("TTL_CASES")
            End If
            If IsDBNull(dr("TTL_UNITS")) = False Then
                ttl_units = dr("TTL_UNITS")
            End If
            If IsDBNull(dr("ITEM")) = False Then
                item = dr("ITEM")
            End If
            If IsDBNull(dr("ITEM_DESC")) = False Then
                item_desc = dr("ITEM_DESC")
            End If
            If IsDBNull(dr("CATCHALL_DESC")) = False Then
                catchall_desc = dr("CATCHALL_DESC")
            End If
            If IsDBNull(dr("IS_CATCHALL")) = False Then
                is_Catchall = dr("IS_CATCHALL")
            End If
            If IsDBNull(dr("PACKAGING_CODE")) = False Then
                packaging_code = dr("PACKAGING_CODE")
            End If
            If IsDBNull(dr("DEPOSIT_PRICE")) = False Then
                deposit_price = dr("DEPOSIT_PRICE")
            End If
            If IsDBNull(dr("QTY")) = False Then
                qty = dr("QTY")
            End If
            If IsDBNull(dr("ITEM_CATEGORY")) = False Then
                item_category = dr("ITEM_CATEGORY")
            End If
            If IsDBNull(dr("ORIG_PRICE")) = False Then
                orig_price = dr("ORIG_PRICE")
            End If
            If IsDBNull(dr("PRICE_CHG_COMMENT")) = False Then
                price_chg_comment = dr("PRICE_CHG_COMMENT")
            End If
            If IsDBNull(dr("RETAIL_PRICE")) = False Then
                retail_price = dr("RETAIL_PRICE")
            End If
            If IsDBNull(dr("ORIG_RETAIL_PRICE")) = False Then
                orig_retail_price = dr("ORIG_RETAIL_PRICE")
            End If
            If IsDBNull(dr("ACTION")) = False Then
                action = dr("ACTION")
            End If
            If IsDBNull(dr("CASE_SZ")) = False Then
                case_sz = dr("CASE_SZ")
            End If
            If IsDBNull(dr("PACK_FACTOR")) = False Then
                pack_factor = dr("PACK_FACTOR")
            End If
        End Sub
        Function validate() As Boolean
            Dim bValid As Boolean = True

            If (action = "A" Or _
               action = "U" Or _
               action = "D") = False Then
                bValid = False
            End If

            bValid = bValid AndAlso IsNumeric(le_select_id)
            bValid = bValid AndAlso le_select_id <> 0
            bValid = bValid AndAlso cod_line <> 0
            bValid = bValid AndAlso item.Trim <> ""

            If action <> "D" Then
                bValid = bValid AndAlso price <> 0D
                bValid = bValid AndAlso extd_price <> 0D
                bValid = bValid AndAlso ttl_cases + ttl_units <> 0
                bValid = bValid AndAlso item_desc <> ""
                bValid = bValid AndAlso qty <> 0
                bValid = bValid AndAlso item_category <> ""
                bValid = bValid AndAlso orig_price <> 0D
                bValid = bValid AndAlso case_sz > 0
                bValid = bValid AndAlso pack_factor > 0


                'VALIDATE OTHER FIELDS THAT ARE NOT MANDANTORY
                If bValid = True Then
                    If catchall_desc.Trim = "" Then
                        is_Catchall = "N"
                    Else
                        is_Catchall = "Y"
                    End If

                    If packaging_code.Trim = "" Then
                        packaging_code = " "
                    End If
                End If
            End If

            Return bValid
        End Function

        Sub Update(ByRef oracmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()

            Dim sQry As String = ""

            Try
                If bValid = False Then
                    If price = 0D Or orig_price = 0D Then
                        Throw New Exception("Cannot use item with original price of zero (probably a deleted item). Please use a Catch All.")
                    End If
                    Throw New Exception("Error validating LCBO_LE_SELECT_ORDER_DTL record")
                End If

                If action = "U" Then
                    sQry = " UPDATE DBO.LCBO_LE_SELECT_ORDER_DTL " & vbNewLine & _
                                "SET COD_LINE  = " & cod_line & vbNewLine & _
                                ", PRICE        = " & price & vbNewLine & _
                                ", EXTD_PRICE   = " & extd_price & vbNewLine & _
                                ", TTL_CASES    = " & ttl_cases & vbNewLine & _
                                ", TTL_UNITS    = " & ttl_units & vbNewLine & _
                                ", ITEM         = '" & item.Trim & "'" & vbNewLine & _
                                ", ITEM_DESC    = '" & escapeSingleQuote(item_desc.Trim) & "'" & vbNewLine & _
                                ", IS_CATCHALL  = '" & is_Catchall.Trim & "'" & vbNewLine & _
                                ", CATCHALL_DESC = '" & catchall_desc.Trim & "'" & vbNewLine & _
                                ", PACKAGING_CODE  = '" & packaging_code & "'" & vbNewLine & _
                                ", DEPOSIT_PRICE  = " & deposit_price & vbNewLine & _
                                ", QTY            = " & qty & vbNewLine & _
                                ", ITEM_CATEGORY = '" & item_category.Trim & "' " & vbNewLine & _
                                ", ORIG_PRICE    = " & orig_price & vbNewLine & _
                                ", PRICE_CHG_COMMENT  = '" & price_chg_comment & "'" & vbNewLine & _
                                ", RETAIL_PRICE  = " & retail_price & vbNewLine & _
                                ", ORIG_RETAIL_PRICE  = " & orig_retail_price & vbNewLine & _
                                ", LAST_UPDATE    = SYSDATE  " & vbNewLine & _
                                " WHERE LE_SELECT_ID = " & le_select_id & vbNewLine & _
                                " AND COD_LINE = " & cod_line & vbNewLine & _
                                " AND TRIM(ITEM) = '" & item.Trim & "'"

                ElseIf action = "D" Then
                    sQry = "DELETE FROM DBO.LCBO_LE_SELECT_ORDER_DTL " & vbNewLine & _
                           " WHERE LE_SELECT_ID = " & le_select_id & vbNewLine & _
                           " AND COD_LINE = " & cod_line & _
                           " AND TRIM(ITEM) = '" & item.Trim & "'"
                ElseIf action = "A" Then
                    sQry = "INSERT INTO DBO.LCBO_LE_SELECT_ORDER_DTL (" & vbNewLine & _
                                 " LE_SELECT_ID        " & vbNewLine & _
                                 ", COD_LINE           " & vbNewLine & _
                                ", PRICE              " & vbNewLine & _
                                ", EXTD_PRICE         " & vbNewLine & _
                                ", TTL_CASES          " & vbNewLine & _
                                ", TTL_UNITS          " & vbNewLine & _
                                ", ITEM               " & vbNewLine & _
                                ", ITEM_DESC          " & vbNewLine & _
                                ", IS_CATCHALL        " & vbNewLine & _
                                ", CATCHALL_DESC      " & vbNewLine & _
                                ", PACKAGING_CODE     " & vbNewLine & _
                                ", DEPOSIT_PRICE      " & vbNewLine & _
                                ", QTY                " & vbNewLine & _
                                ", ITEM_CATEGORY      " & vbNewLine & _
                                ", ORIG_PRICE         " & vbNewLine & _
                                ", PRICE_CHG_COMMENT  " & vbNewLine & _
                                ", RETAIL_PRICE       " & vbNewLine & _
                                ", ORIG_RETAIL_PRICE  " & vbNewLine & _
                                ", LAST_UPDATE        " & vbNewLine & _
                                ", CASE_SZ " & vbNewLine & _
                                ", PACK_FACTOR " & vbNewLine & _
                                ") VALUES (" & vbNewLine & _
                                le_select_id & vbNewLine & _
                                "," & cod_line & vbNewLine & _
                                "," & price & vbNewLine & _
                                "," & extd_price & vbNewLine & _
                                "," & ttl_cases & vbNewLine & _
                                "," & ttl_units & vbNewLine & _
                                ",'" & item.Trim & "'" & vbNewLine & _
                                ",'" & escapeSingleQuote(item_desc.Trim) & "'" & vbNewLine & _
                                ",'" & is_Catchall.Trim & "'" & vbNewLine & _
                                ",'" & catchall_desc.Trim & "'" & vbNewLine & _
                                ",'" & packaging_code & "'" & vbNewLine & _
                                "," & deposit_price & vbNewLine & _
                                "," & qty & vbNewLine & _
                                ",'" & item_category.Trim & "'" & vbNewLine & _
                                "," & orig_price & vbNewLine & _
                                ",'" & price_chg_comment & "'" & vbNewLine & _
                                "," & retail_price & vbNewLine & _
                                "," & orig_retail_price & vbNewLine & _
                                ", SYSDATE " & vbNewLine & _
                                "," & case_sz & vbNewLine & _
                                "," & pack_factor & vbNewLine & _
                                ")"

                End If

                oracmd.CommandText = sQry
                oracmd.Parameters.Clear()

                If oracmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error UPDATING LCBO_LE_SELECT_ORDER_DTL!")
                End If

            Catch ex As Exception
                Throw New Exception("LCBO_LE_SELECT_ORDER_DTL.UPDATE->" & le_select_id & "," & cod_line & vbNewLine & _
                                    "QRY: " & sQry & vbNewLine & _
                                    "ErrMsg:" & ex.Message.ToString)
            End Try

        End Sub

        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Dim objB As REC_LCBO_LE_SELECT_ORDER_DTL = obj
            Dim a As String = Right("00000000" & Me.le_select_id, 8) & Right("000" & Me.cod_line, 3)
            Dim b As String = Right("00000000" & objB.le_select_id, 8) & Right("000" & objB.cod_line, 3)
            Return a.CompareTo(b)
        End Function

        Public Function escapeSingleQuote(ByVal text As String)
            Return text.Replace("'", "''")
        End Function
    End Class
End Namespace
