Imports Microsoft.VisualBasic
Imports System.Collections

Namespace DataLayer
    Public Class REC_ON_INV_CODTL_RULE
        Implements IComparable

        Private iInv_no As Integer
        Public Property inv_no() As Integer
            Get
                Return iInv_no
            End Get
            Set(ByVal value As Integer)
                iInv_no = value
            End Set
        End Property


        Private sCo_odno As String
        Public Property co_odno() As String
            Get
                Return sCo_odno
            End Get
            Set(ByVal value As String)
                sCo_odno = value
            End Set
        End Property

        ''' <summary>
        ''' E = Estimate
        ''' A = Actual
        ''' R = Return
        ''' </summary>
        ''' <remarks></remarks>
        Private sCo_Inv_Type As String
        Public Property co_inv_type() As String
            Get
                Return sCo_Inv_Type
            End Get
            Set(ByVal value As String)
                sCo_Inv_Type = value
            End Set
        End Property

        ''' <summary>
        ''' orderline
        ''' </summary>
        ''' <remarks></remarks>
        Private iCod_Line As Integer
        Public Property cod_line() As Integer
            Get
                Return iCod_Line
            End Get
            Set(ByVal value As Integer)
                iCod_Line = value
            End Set
        End Property

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <remarks></remarks>
        Private sRule As String
        Public Property rule() As String
            Get
                Return sRule
            End Get
            Set(ByVal value As String)
                sRule = value
            End Set
        End Property

        Private sRule_Gr As String
        Public Property rule_gr() As String
            Get
                Return sRule_Gr
            End Get
            Set(ByVal value As String)
                sRule_Gr = value
            End Set
        End Property

        Private dRate As Decimal
        Public Property rate() As Decimal
            Get
                Return dRate
            End Get
            Set(ByVal value As Decimal)
                dRate = value
            End Set
        End Property


        Private sRate_Cd As String
        Public Property rate_cd() As String
            Get
                Return sRate_Cd
            End Get
            Set(ByVal value As String)
                sRate_Cd = value
            End Set
        End Property


        Private dRule_Amt As Decimal
        Public Property rule_amt() As Decimal
            Get
                Return dRule_Amt
            End Get
            Set(ByVal value As Decimal)
                dRule_Amt = value
            End Set
        End Property


        Private sItem_Category As String
        Public Property item_category() As String
            Get
                Return sItem_Category
            End Get
            Set(ByVal value As String)
                sItem_Category = value
            End Set
        End Property

        Sub New()
            inv_no = 0
            co_odno = ""
            co_inv_type = ""
            rule = ""
            rule_gr = ""
            rate = 0D
            rate_cd = ""
            rule_amt = 0D
            item_category = ""
        End Sub

        Sub New(ByVal dr As DataRow)
            setRuleInfo(dr)
        End Sub

        Public Sub setRuleInfo(ByVal dr As DataRow)

            inv_no = dr("INV_NO")
            co_odno = dr("CO_ODNO")
            co_inv_type = dr("CO_INV_TYPE")
            cod_line = dr("COD_LINE")
            rule = dr("RULE")
            rule_gr = dr("RULE_GR")
            rate = dr("RATE")
            rate_cd = dr("RATE_CD")
            rule_amt = dr("RULE_AMT")
            item_category = dr("ITEM_CATEGORY")

        End Sub

        Public Function validate() As Boolean
            Dim bValid As Boolean = True
            bValid = inv_no > 0
            bValid = bValid AndAlso (co_odno.Trim <> "")
            bValid = bValid AndAlso (co_inv_type.Trim <> "")
            bValid = bValid AndAlso (cod_line > 0)
            bValid = bValid AndAlso (rule.Trim <> "")
            bValid = bValid AndAlso (rule_gr.Trim <> "")
            bValid = bValid AndAlso (item_category.Trim <> "")

            Return bValid
        End Function
        Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()
            Dim sQry As String = "INSERT INTO DBO.ON_INV_CODTL_RULE (" & vbNewLine & _
                                 "INV_NO " & vbNewLine & _
                                 ",CO_ODNO " & vbNewLine & _
                                 ",CO_INV_TYPE " & vbNewLine & _
                                 ",COD_LINE " & vbNewLine & _
                                 ",RULE " & vbNewLine & _
                                 ",RULE_GR " & vbNewLine & _
                                 ",RATE " & vbNewLine & _
                                 ",RATE_CD " & vbNewLine & _
                                 ",RULE_AMT " & vbNewLine & _
                                 ",ITEM_CATEGORY " & vbNewLine & _
                                 ") VALUES (" & vbNewLine & _
                                 inv_no & vbNewLine & _
                                 ",'" & co_odno & "'" & vbNewLine & _
                                 ",'" & co_inv_type & "'" & vbNewLine & _
                                 "," & cod_line & vbNewLine & _
                                 ",'" & rule & "'" & vbNewLine & _
                                 ",'" & rule_gr & "'" & vbNewLine & _
                                 "," & rate & vbNewLine & _
                                 ",'" & rate_cd & "'" & vbNewLine & _
                                 "," & rule_amt & vbNewLine & _
                                 ",'" & item_category & "'" & vbNewLine & _
                                 ")"

            Try
                If bValid = False Then
                    Throw New Exception("Error validating ON_INV_CODTL_RULE record")
                End If
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting ON_INV_CODTL_RULE record query: " & sQry & "!")
                End If

            Catch ex As Exception
                Throw New Exception("ON_INV_CODTL_RULE.Insert->" & co_odno & "," & cod_line & "," & rule & " Qry: " & sQry & ".Error Message:" & ex.Message.ToString)
            End Try

        End Sub
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Dim b As REC_ON_INV_CODTL_RULE = obj
            Dim strCompare As String = Right("0000" & Me.cod_line.ToString, 4) & Me.sRule

            Return (Right("0000" & Me.cod_line.ToString, 4) & Me.sRule).CompareTo(Right("0000" & b.cod_line.ToString, 4) & b.sRule)
        End Function
    End Class
End Namespace
'INV_NO,NUMBER,10,,N
'CO_ODNO,CHAR,16,,N
'CO_INV_TYPE,CHAR,1,,N
'COD_LINE,NUMBER,10,,N
'RULE,CHAR,10,,N
'RULE_GR,CHAR,10,,Y
'RATE,NUMBER,14,2,Y
'RATE_CD,CHAR,1,,Y
'RULE_AMT,NUMBER,14,2,Y
'ITEM_CATEGORY,CHAR,10,,Y