Imports Microsoft.VisualBasic

Namespace DataLayer
    Public Class REC_ON_JETRAN
        ''' <summary>
        ''' JE_TRANS_NO,CHAR,6,,N,
        ''' </summary>
        ''' <remarks></remarks>
        Private sJe_Trans_No As String
        Public Property je_trans_no() As String
            Get
                Return sJe_Trans_No
            End Get
            Set(ByVal value As String)
                sJe_Trans_No = value
            End Set
        End Property

        ''' <summary>
        ''' JE_TRANS_ID,NUMBER,4,,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private iJe_Trans_ID As Integer
        Public Property je_trans_id() As Integer
            Get
                Return iJe_Trans_ID
            End Get
            Set(ByVal value As Integer)
                iJe_Trans_ID = value
            End Set
        End Property

        ''' <summary>
        ''' JE_POST_DT,DATE,7,,N,
        ''' date in YYYY-MM-DD format
        ''' </summary>
        ''' <remarks></remarks>
        Private sJe_Post_dt As String
        Public Property je_post_dt() As DateTime
            Get
                Return sJe_Post_dt
            End Get
            Set(ByVal value As DateTime)
                sJe_Post_dt = value
            End Set
        End Property

        ''' <summary>
        ''' JE_TOTAL,NUMBER,14,2,N,
        ''' </summary>
        ''' <remarks></remarks>
        Private dJe_Total As Decimal
        Public Property je_total() As Decimal
            Get
                Return dJe_Total
            End Get
            Set(ByVal value As Decimal)
                dJe_Total = value
            End Set
        End Property

        ''' <summary>
        ''' JE_COMMENT,VARCHAR2,50,,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private sJE_comment As String
        Public Property je_comment() As String
            Get
                Return sJE_comment
            End Get
            Set(ByVal value As String)
                sJE_comment = value
            End Set
        End Property

        ''' <summary>
        ''' JE_CUST_NO,CHAR,10,,Y,
        ''' </summary>
        ''' <remarks></remarks>
        Private sJE_Cust_No As String
        Public Property je_cust_no() As String
            Get
                Return sJE_Cust_No
            End Get
            Set(ByVal value As String)
                sJE_Cust_No = value
            End Set
        End Property

        Sub New()
            je_trans_no = ""
            je_trans_id = -1
            je_post_dt = New DateTime
            je_total = 0D
            je_comment = ""
            je_cust_no = ""
        End Sub

        Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)

            Dim sQry As String = "INSERT INTO DBO.ON_JETRAN (" & vbNewLine & _
                                    " JE_TRANS_NO " & vbNewLine & _
                                    ",JE_POST_DT  " & vbNewLine & _
                                    ",JE_TOTAL    " & vbNewLine & _
                                    ",JE_COMMENT  " & vbNewLine & _
                                    ",JE_CUST_NO  " & vbNewLine & _
                                 ") VALUES (" & vbNewLine & _
                                 "'" & je_trans_no & "'" & vbNewLine & _
                                 ",TO_DATE('" & je_post_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS')" & vbNewLine & _
                                 "," & je_total & vbNewLine & _
                                 ",'" & je_comment & "'" & vbNewLine & _
                                 ",'" & je_cust_no & "'" & vbNewLine & _
                                 ")"

            Try
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting ON_INV_PAY record query: " & sQry & "!")
                End If

                If je_trans_id <> -1 Then
                    sQry = "UPDATE DBO.ON_JETRAN SET JE_TRANS_ID = " & je_trans_id & vbNewLine & _
                           " WHERE JE_TRANS_NO = '" & je_trans_no & "'"

                    oraCmd.CommandText = sQry
                    oraCmd.Parameters.Clear()
                    If oraCmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error UPDATING ON_JETRAN.JE_TRANS_ID record query: " & sQry & "!")
                    End If

                End If

            Catch ex As Exception
                Throw New Exception("ON_JETRAN.Insert->" & je_trans_no & ".Error Message:" & ex.Message.ToString)
            End Try
        End Sub

    End Class
End Namespace
'JE_TRANS_NO,CHAR,6,,N,
'JE_TRANS_ID,NUMBER,4,,Y,
'JE_POST_DT,DATE,7,,N,
'JE_TOTAL,NUMBER,14,2,N,
'JE_COMMENT,VARCHAR2,50,,Y,
'JE_CUST_NO,CHAR,10,,Y,