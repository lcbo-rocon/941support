Imports System.Data.OracleClient
Imports System.Data
Imports System.Configuration
Imports RSG_ROC.Exceptions
Imports RSG_ROC.OrderClasses
Imports RSG_ROC.GLClasses

Namespace DataLayer
    ''' <summary>
    ''' Data Access Layer for ROC Database
    ''' Is responsible to communicate with the databases for information 
    ''' 
    ''' Database connection objects:
    ''' daROC - connection to ROC Database for selecting data
    ''' cmdROC - connection to ROC database for updating ROC Flags
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Public Class DALRoc
        ''' <summary>
        ''' env variable is used to determine which ROC database.
        ''' Possible Values are:
        ''' TEST
        ''' QA
        ''' PROD
        ''' 
        ''' This value is set from the either the Environment Variables are Application Configuration File.
        ''' </summary>
        ''' <remarks>Default TEST</remarks>
        Private Shared env As String = ConfigurationManager.AppSettings("ENV")

        ''' <summary>
        ''' ROC dataAdapter used to retrieve data from the ROC Database
        ''' </summary>
        ''' <remarks></remarks>
        Private Shared daROC As New OracleDataAdapter("SELECT SYSDATE FROM DUAL", getConnectionstring)

        ''' <summary>
        ''' ROC Oracle Command Object used to perform Update/Insert Statements to ROC Database
        ''' </summary>
        ''' <remarks></remarks>
        Private Shared cmdRoc As New OracleCommand("SELECT SYSDATE FROM DUAL", New OracleConnection(getConnectionstring))

        Private iErrorCode As Integer
        Public Property error_code() As Integer
            Get
                Return iErrorCode
            End Get
            Set(ByVal value As Integer)
                iErrorCode = value
            End Set
        End Property

        ''' <summary>
        ''' Sets the ENV variable to determine which ROC database to retrieve and update data.
        ''' 
        ''' </summary>
        ''' <param name="sEnv"></param>
        ''' <remarks></remarks>
        Public Shared Sub setEnv(ByVal sEnv As String)
            env = sEnv
            CloseOracleSelectConnection()
            'reset the connection string
            daROC.SelectCommand.Connection.ConnectionString = getConnectionstring()
            cmdRoc.Connection = New OracleConnection(getConnectionstring())
        End Sub

        ''' <summary>
        ''' Returns the connection string that is listed in the configuration file based on variable ENV
        ''' </summary>
        ''' <returns>Connection string to ROC Database</returns>
        ''' <remarks></remarks>
        Public Shared Function getConnectionstring() As String
            If env = "" Then
                setEnv(ConfigurationManager.AppSettings("ENV"))
            End If
            Dim connstring = ConfigurationManager.AppSettings(env)
            Return connstring
        End Function

        ''' <summary>
        ''' Returns the connection string based on the variable passed listed in the application configuration file
        ''' </summary>
        ''' <param name="env">Connection string listed in the configuration File</param>
        ''' <returns>Connection String to database based on the env passed</returns>
        ''' <remarks></remarks>
        Public Shared Function getConnectionString(ByVal env As String) As String
            Dim connstring As String
            connstring = ConfigurationManager.AppSettings(env)
            Return connstring
        End Function
        Public Shared Function getOraCmd() As OracleCommand
            If cmdRoc.Connection.State <> ConnectionState.Open Then
                cmdRoc.Connection.Open()
            End If

            Return cmdRoc
        End Function
       
        Public Shared Sub CloseOracleSelectConnection()
            If daROC Is Nothing = False Then
                If daROC.SelectCommand.Connection.State <> ConnectionState.Closed Then
                    daROC.SelectCommand.Connection.Close()
                End If
            End If

            If cmdRoc Is Nothing = False Then
                If cmdRoc.Connection.State <> ConnectionState.Closed Then
                    cmdRoc.Connection.Close()
                End If
            End If
        End Sub

        Shared Function getOrderHeader(ByVal sOrderNum As String, ByVal sOrderStatusType As String) As DataRow
            Dim dt As New DataTable
            Dim dr As DataRow = Nothing

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If



            Dim sQry As String = " Select HDR.INV_NO     " & vbNewLine & _
                                                " , HDR.CO_ODNO         " & vbNewLine & _
                                                " , HDR.CO_INV_TYPE     " & vbNewLine & _
                                                " , HDR.INV_AMT         " & vbNewLine & _
                                                " , HDR.DELIVERY_CHG    " & vbNewLine & _
                                                " , HDR.DELIVERY_TAX1   " & vbNewLine & _
                                                " , HDR.DELIVERY_TAX2   " & vbNewLine & _
                                                " , HDR.TTL_FULL_CASES  " & vbNewLine & _
                                                " , HDR.TTL_PART_CASES  " & vbNewLine & _
                                                " , HDR.INV_DT          " & vbNewLine & _
                                                " , NVL(TO_CHAR(HDR.UPLOAD1_DT,'YYYY-MM-DD HH24:MI:SS'),' ') AS UPLOAD1_DT " & vbNewLine & _
                                                " , NVL(TO_CHAR(HDR.UPLOAD2_DT, 'YYYY-MM-DD HH24:MI:SS'), ' ') AS UPLOAD2_DT " & vbNewLine & _
                                                " , HDR.EM_NO           " & vbNewLine & _
                                                " , NVL(TO_CHAR(HDR.ACC_POST_DT,'YYYY-MM-DD HH24:MI:SS'), ' ') AS ACC_POST_DT " & vbNewLine & _
                                                " , NVL(HDR.LINK_CO_ODNO,' ') AS LINK_CO_ODNO " & vbNewLine & _
                                                " , DECODE(TRN.ORIG_ORDER_NO,NULL,'Y','N') AS TRANSFERRABLE         " & vbNewLine & _
                                                " , NVL(INV.SITE_CD,3) AS SITE_CD " & vbNewLine & _
                                                " , INV.INV_STATUS " & vbNewLine & _
                                                " , INV.INV_TYPE " & vbNewLine & _
                                                " , NVL(TO_CHAR(INV.TRAN_DT,'YYYY-MM-DD'),' ') AS TRAN_DT " & vbNewLine & _
                                                " , INV.AMOUNT_PAID " & vbNewLine & _
                                                " , INV.INV_TRANS_NO " & vbNewLine & _
                                                " , INV.PAID_FL " & vbNewLine & _
                                                " , NVL(INV.INV_TRANS_NO,' ') AS INV_TRANS_NO " & vbNewLine & _
                                                " , NVL(TO_CHAR(INV.PAID_DT,'YYYY-MM-DD HH24:MI:SS'),' ') AS PAID_DT " & vbNewLine & _
                                                " , INV.INV_CU_NO " & vbNewLine & _
                                                " , NVL(TO_CHAR(INV.ARCHIVE_DT,'YYYY-MM-DD HH24:MI:SS'),' ') AS ARCHIVE_DT " & vbNewLine & _
                                                " , NVL(TRN.ORDER_NO,' ') AS TRANSF_ORDER_NO                        " & vbNewLine & _
                                                " , NVL(TRN.CU_NO,' ') AS TRANSF_CU_NO                              " & vbNewLine & _
                                                " , NVL(TRN.INV_AMT, 0) AS TRANSF_INV_AMT                           " & vbNewLine & _
                                                " , NVL(TRN.ORIG_ORDER_NO, ' ') AS ORIG_ORDER_NO                    " & vbNewLine & _
                                                " , NVL(TRN.ORIG_CU_NO, ' ') AS ORIG_CU_NO                          " & vbNewLine & _
                                                " , NVL(TRN.ORIG_INV_AMT, 0) AS ORIG_INV_AMT                        " & vbNewLine & _
                                                " , NVL(TRN.DELIVERY_CHG_TRANS, ' ') AS DELIVERY_CHG_TRANS          " & vbNewLine & _
                                                " , NVL(TRN.JE_PAYMENT_TRANS_NO, ' ') AS JE_PAYMENT_TRANS_NO        " & vbNewLine & _
                                                " , NVL(TRN.CR_ORDER_NO, ' ') AS CR_ORDER_NO                        " & vbNewLine & _
                                                " , NVL(TRN.CR_CU_NO, ' ') AS CR_CU_NO                              " & vbNewLine & _
                                                " , NVL(TRN.CR_INV_AMT, 0) AS CR_INV_AMT                            " & vbNewLine & _
                                                " , NVL(TRN.TRANSFER_COMMENTS,' ') AS TRANSFER_COMMENTS             " & vbNewLine & _
                                                " , NVL(TRN.EMP_NO,' ') AS EMP_NO                                   " & vbNewLine & _
                                                " , NVL(TO_CHAR(TRN.TRANSFER_DT,'YYYY-MM-DD'), ' ') AS TRANSFER_DT  " & vbNewLine & _
                                                " from DBO.ON_INV_CO HDR LEFT JOIN DBO.LCBO_TRANSFERRED_ORDERS TRN  " & vbNewLine & _
                                                " ON HDR.CO_ODNO = TRN.ORIG_ORDER_NO                                " & vbNewLine & _
                                                " INNER JOIN DBO.ON_INVOICE INV ON INV.INV_NO = HDR.INV_NO " & vbNewLine & _
                                                 " WHERE HDR.CO_ODNO = '" & sOrderNum & "' " & vbNewLine & _
                                                " AND HDR.CO_INV_TYPE = '" & sOrderStatusType & "' "
            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

                If dt.Rows.Count > 0 Then
                    dr = dt.Rows(0)
                Else
                    Throw New Exception("No records found!")
                End If

            Catch ex As Exception
                Throw New Exception("getOrderHeader(" & sOrderNum & "," & sOrderStatusType & "):" & vbNewLine & _
                                    "qry: " & sQry & vbNewLine & "Error msg: " & ex.Message.ToString)
            End Try

            Return dr
        End Function
        Shared Function getCOHDRInfo(ByVal sOrderNum As String) As DataRow
            Dim dt As New DataTable
            Dim dr As DataRow = Nothing

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If



            Dim sQry As String = " Select HDR.CO_ODNO          " & vbNewLine & _
                                                " , HDR.CO_STATUS             " & vbNewLine & _
                                                " , HDR.ROUTE_CODE            " & vbNewLine & _
                                                " , NVL(HDR.PRIOR_NAME,' ') AS PRIOR_NAME " & vbNewLine & _
                                                " , HDR.CU_NO                 " & vbNewLine & _
                                                " , HDR.SHIP_TO               " & vbNewLine & _
                                                " , HDR.ORDER_DT_TM           " & vbNewLine & _
                                                " , HDR.ORDER_REQD_DT         " & vbNewLine & _
                                                " , HDR.ORDER_TYPE            " & vbNewLine & _
                                                " , HDR.TRANSPORT_MODE        " & vbNewLine & _
                                                " , HDR.ORDER_CANCELED_Y_OR_N " & vbNewLine & _
                                                " , HDR.STOP_SEQ_NO           " & vbNewLine & _
                                                " , NVL(HDR.COHDR_COMMENT ,' ') AS COHDR_COMMENT " & vbNewLine & _
                                                " , HDR.END_OF_ORDER_FL       " & vbNewLine & _
                                                " , NVL(HDR.PAY_METHOD, ' ') AS PAY_METHOD " & vbNewLine & _
                                                " , NVL(TO_CHAR(HDR.ORDER_DEL_DT, 'YYYY-MM-DD HH24:MI:SS'),' ') AS ORDER_DEL_DT " & vbNewLine & _
                                                " , HDR.TOTAL_CASES           " & vbNewLine & _
                                                " , NVL(HDR.TRAFFIC_REF_NO,' ') AS TRAFFIC_REF_NO  " & vbNewLine & _
                                                " , HDR.ENTRY_FL              " & vbNewLine & _
                                                " , HDR.PU_ORDER_FL           " & vbNewLine & _
                                                " , HDR.DELIVERY_FL           " & vbNewLine & _
                                                " from DBO.COHDR HDR          " & vbNewLine & _
                                                " WHERE HDR.CO_ODNO = '" & sOrderNum & "' "
            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

                If dt.Rows.Count > 0 Then
                    dr = dt.Rows(0)
                Else
                    Throw New Exception("No records found!")
                End If

            Catch ex As Exception
                Throw New Exception("getCOHDRInfo(" & sOrderNum & ") " & vbNewLine & _
                                    "qry: " & sQry & vbNewLine & _
                                    "ERROR Msg: " & ex.Message.ToString)
            End Try

            Return dr
        End Function
        Shared Function getPickedItems(ByVal sOrderNum As String) As DataTable
            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database. Abending program." & ex.Message.ToString)
                End Try

            End If


            Dim sQry As String = " SELECT CO_ODNO           " & vbNewLine & _
                                                " , COD_LINE               " & vbNewLine & _
                                                " , ITEM                   " & vbNewLine & _
                                                " , PICKED_QTY             " & vbNewLine & _
                                                " , nvl(UNIT_PRICE,0) UNIT_PRICE " & vbNewLine & _
                                                " , COD_QTY                " & vbNewLine & _
                                                " , TOTAL_ALLOCATED_QTY    " & vbNewLine & _
                                                " , PLANNED_QTY            " & vbNewLine & _
                                                " , USE_BLOCKED_STOCK_FL   " & vbNewLine & _
                                                " , SHIPPED_QTY            " & vbNewLine & _
                                                " , DELETED_FL             " & vbNewLine & _
                                                " , PICK_PLANNED_Y_OR_N    " & vbNewLine & _
                                                " , PICKED_CASES           " & vbNewLine & _
                                                " , PICKED_UNITS           " & vbNewLine & _
                                                " FROM DBO.CODTL           " & vbNewLine & _
                                                " WHERE CO_ODNO = '" & sOrderNum & "' " & vbNewLine & _
                                                " AND DELETED_FL = 'N' " & vbNewLine & _
                                                " AND PICKED_QTY > 0 " & vbNewLine & _
                                                " ORDER BY COD_LINE ASC "
            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("getPickedItems(" & sOrderNum & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Error Msg: " & ex.Message.ToString)
            End Try

            Return dt
        End Function
        Shared Function getCreditPaymentRecord(ByVal pay_amt As Decimal) As REC_ON_INV_PAY
            Dim pymt As New REC_ON_INV_PAY

            With pymt
                .inv_no = 0
                .ref_seq = 1
                .link_ref_seq = 1
                .emp_no = ""
                .tender_cd = 6
                .tender_type = "06-Credit Note"
                .pay_amt = pay_amt
                .pay_dt = Now
            End With

            Return pymt
        End Function
        Shared Function getPaymentList(ByVal iInv_No As Integer) As DataTable
            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database. Abending program." & ex.Message.ToString)
                End Try

            End If


            Dim sQry As String = " SELECT INV_NO                                " & vbNewLine & _
                                                " , REF_SEQ                                    " & vbNewLine & _
                                                " , NVL(REF_NO,' ') AS REF_NO                  " & vbNewLine & _
                                                " , EMP_NO                                     " & vbNewLine & _
                                                " , TENDER_CD                                  " & vbNewLine & _
                                                " , TENDER_TYPE                                " & vbNewLine & _
                                                " , PAY_AMT                                    " & vbNewLine & _
                                                " , NVL(CREDIT_CARD_NO, ' ') AS CREDIT_CARD_NO " & vbNewLine & _
                                                " , NVL(CREDIT_AUTH,' ') AS CREDIT_AUTH        " & vbNewLine & _
                                                " , NVL(PAY_COMMENT, ' ') AS PAY_COMMENT       " & vbNewLine & _
                                                " , NVL(TO_CHAR(ACC_POST_DT,'YYYY-MM-DD HH24:MI:SS'),' ') AS ACC_POST_DT " & vbNewLine & _
                                                " , LINK_REF_SEQ                                                         " & vbNewLine & _
                                                " , NVL(TO_CHAR(UPLOAD_DT,'YYYY-MM-DD HH24:MI:SS'),' ') AS UPLOAD_DT     " & vbNewLine & _
                                                " , NVL(PAY_TRANS_NO,' ') AS PAY_TRANS_NO                                " & vbNewLine & _
                                                " , NVL(TO_CHAR(PAY_DT,'YYYY-MM-DD HH24:MI:SS'),TO_CHAR(TIMESTAMP,'YYYY-MM-DD HH24:MI:SS')) AS PAY_DT           " & vbNewLine & _
                                                " from DBO.ON_INV_PAY                                                    " & vbNewLine & _
                                                " WHERE INV_NO = " & iInv_No & vbNewLine & _
                                                " ORDER BY REF_SEQ ASC "
            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("DALRoc.getPaymentList(" & iInv_No & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Error Msg: " & ex.Message.ToString)
            End Try

            Return dt
        End Function
        Shared Function getCustomerOrderList(ByVal sCustomerNumber As String) As DataTable
            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If


            Dim sQry As String = " Select COHDR.CU_NO     " & vbNewLine & _
                                " , ON_CUST_MAS.CU_NAME  " & vbNewLine & _
                                " , COHDR.CO_ODNO        " & vbNewLine & _
                                " , ON_INV_CO.INV_DT     " & vbNewLine & _
                                " , COHDR.ORDER_TYPE     " & vbNewLine & _
                                " , ON_INV_CO.INV_AMT    " & vbNewLine & _
                                " , (SELECT SUM(PAY_AMT) FROM DBO.ON_INV_PAY WHERE ON_INV_PAY.INV_NO = ON_INV_CO.INV_NO AND ON_INV_CO.CO_INV_TYPE = 'A') AS PAID_AMOUNT " & vbNewLine & _
                                " , DECODE( (SELECT 1 FROM DBO.LCBO_TRANSFERRED_ORDERS WHERE ORIG_ORDER_NO = COHDR.CO_ODNO),1,'FORWARDED','INVOICED') AS STATUS         " & vbNewLine & _
                                " FROM DBO.COHDR INNER JOIN DBO.ON_CUST_MAS ON COHDR.CU_NO = ON_CUST_MAS.CU_NO     " & vbNewLine & _
                                " INNER JOIN ON_INV_CO ON COHDR.CO_ODNO = ON_INV_CO.CO_ODNO                " & vbNewLine & _
                                " WHERE CO_INV_TYPE = 'A'                                                  " & vbNewLine & _
                                " AND COHDR.ENTRY_FL <> 'W'  " & vbNewLine & _
                                " AND ON_INV_CO.ACC_POST_DT IS NOT NULL " & vbNewLine & _
                                " AND COHDR.CU_NO = '" & Left(sCustomerNumber & "          ", 10) & "'"


            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("DALRoc.getCustomerOrderList(" & sCustomerNumber & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Error Msg: " & ex.Message.ToString)
            End Try

            Return dt
        End Function
        Shared Function getOrderItems(ByVal sOrderNum As String, ByVal sOrderType As String) As DataTable
            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database. Abending program." & ex.Message.ToString)
                End Try

            End If


            Dim sQry As String = " Select ITM.INV_NO          " & vbNewLine & _
                                                " , ITM.CO_ODNO              " & vbNewLine & _
                                                " , ITM.CO_INV_TYPE          " & vbNewLine & _
                                                " , ITM.COD_LINE             " & vbNewLine & _
                                                " , ITM.PRICE                " & vbNewLine & _
                                                " , ITM.EXTD_PRICE           " & vbNewLine & _
                                                " , ITM.TTL_CASES            " & vbNewLine & _
                                                " , ITM.TTL_UNITS            " & vbNewLine & _
                                                " , ITM.ITEM                 " & vbNewLine & _
                                                " , ITM.PACKAGING_CODE       " & vbNewLine & _
                                                " , ITM.DEPOSIT_PRICE        " & vbNewLine & _
                                                " , ITM.QTY                  " & vbNewLine & _
                                                " , NVL(ITM.ITEM_CATEGORY,' ') AS ITEM_CATEGORY " & vbNewLine & _
                                                " , ITM.ORIG_PRICE           " & vbNewLine & _
                                                " , NVL(ITM.PRICE_CHG_COMMENT,' ') AS PRICE_CHG_COMMENT " & vbNewLine & _
                                                " , NVL(ITM.RETAIL_PRICE,0) as RETAIL_PRICE  " & vbNewLine & _
                                                " , NVL(ITM.ORIG_RETAIL_PRICE,0) AS ORIG_RETAIL_PRICE " & vbNewLine & _
                                                " FROM DBO.ON_INV_CODTL ITM  " & vbNewLine & _
                                                " WHERE ITM.CO_ODNO = '" & sOrderNum & "' " & vbNewLine & _
                                                " AND ITM.CO_INV_TYPE = '" & sOrderType & "' " & vbNewLine & _
                                                " ORDER BY ITM.COD_LINE ASC "

            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("DALRoc.getOrderItems(" & sOrderNum & "," & sOrderType & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Error Msg: " & ex.Message.ToString)
            End Try

            Return dt
        End Function
        Shared Function getOrderRules(ByVal sOrderNum As String, ByVal sOrderType As String, ByVal iOrderLine As Integer) As DataTable
            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database. Abending program." & ex.Message.ToString)
                End Try

            End If

            Dim sQry As String = " Select RULE.INV_NO          " & vbNewLine & _
                                " , RULE.CO_ODNO              " & vbNewLine & _
                                " , RULE.CO_INV_TYPE          " & vbNewLine & _
                                " , RULE.COD_LINE             " & vbNewLine & _
                                " , RULE.RULE                " & vbNewLine & _
                                " , NVL(RULE.RULE_GR,' ') AS RULE_GR " & vbNewLine & _
                                " , NVL(RULE.RATE,0) AS RATE " & vbNewLine & _
                                " , NVL(RULE.RATE_CD,' ') AS RATE_CD " & vbNewLine & _
                                " , NVL(RULE.RULE_AMT ,0) AS RULE_AMT " & vbNewLine & _
                                " , NVL(RULE.ITEM_CATEGORY,' ') AS ITEM_CATEGORY       " & vbNewLine & _
                                " FROM DBO.ON_INV_CODTL_RULE RULE   " & vbNewLine & _
                                " WHERE RULE.CO_ODNO = '" & sOrderNum & "' " & vbNewLine & _
                                " AND RULE.CO_INV_TYPE = '" & sOrderType & "' " & vbNewLine & _
                                " AND RULE.COD_LINE = " & iOrderLine & vbNewLine & _
                                " ORDER BY RULE.RULE ASC "
            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("DALROC.getOrderRules(" & sOrderNum & "," & sOrderType & "," & iOrderLine & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Error Msg: " & ex.Message.ToString)
            End Try

            Return dt
        End Function
        Shared Function getReturnInfo(ByVal sReturnNum As String) As DataRow
            Dim dt As New DataTable
            Dim dr As DataRow = Nothing

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If



            Dim sQry As String = " Select                            " & vbNewLine & _
                                                " CR_ODNO                            " & vbNewLine & _
                                                " , CR_STATUS                        " & vbNewLine & _
                                                " , CU_NO                            " & vbNewLine & _
                                                " , nvl(ORIG_ODNO, ' ') as ORIG_ODNO   " & vbNewLine & _
                                                " , NVL(TO_CHAR(ORDER_DT_TM,'YYYY-MM-DD HH24:MI:SS'),' ') AS ORDER_DT_TM " & vbNewLine & _
                                                " , NVL(RETURN_CODE,' ') AS RETURN_CODE " & vbNewLine & _
                                                " , EM_NO                               " & vbNewLine & _
                                                " , NVL(PAY_METHOD,' ') AS PAY_METHOD   " & vbNewLine & _
                                                " , NVL(REF_NO,0) AS REF_NO             " & vbNewLine & _
                                                " , NVL(FREIGHT_FL,'N') AS FREIGHT_FL   " & vbNewLine & _
                                                " , ORDER_TYPE                          " & vbNewLine & _
                                                " , NVL(DELIVERY_FL,'N') AS DELIVERY_FL " & vbNewLine & _
                                                " from DBO.CRHDR                        " & vbNewLine & _
                                                " WHERE CR_ODNO = '" & sReturnNum & "' "


            Try
                daROC.SelectCommand.CommandText = sQRy
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

                If dt.Rows.Count > 0 Then
                    dr = dt.Rows(0)
                Else
                    Throw New Exception("No records found!")
                End If

            Catch ex As Exception
                Throw New Exception("DALRoc.getReturnInfo(" & sReturnNum & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Error Msg: " & ex.Message.ToString)
            End Try

            Return dr
        End Function
        Shared Function getReturnItems(ByVal sReturnNum As String) As DataTable
            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database. Abending program." & ex.Message.ToString)
                End Try

            End If


            Dim sQry As String = " Select CR_ODNO   " & vbNewLine & _
                                                " , CR_LINE        " & vbNewLine & _
                                                " , ITEM           " & vbNewLine & _
                                                " , PACKAGING_CODE " & vbNewLine & _
                                                " , EXPECTED_QTY   " & vbNewLine & _
                                                " , RECEIVED_QTY   " & vbNewLine & _
                                                " , REF_SEQ_NO     " & vbNewLine & _
                                                " , CONFIRM_QTY    " & vbNewLine & _
                                                " from DBO.CRDTL   " & vbNewLine & _
                                                " WHERE CR_ODNO = '" & sReturnNum & "' " & vbNewLine & _
                                                " ORDER BY CR_LINE ASC "
            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("DALROC.getReturnItems(" & sReturnNum & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Error msg: " & ex.Message.ToString)
            End Try

            Return dt
        End Function

        'Shared Function CreateReturn(ByVal objOrder As ROC_ORDER, ByVal sUser As String, ByVal dtDate As Date) As ROC_RETURN
        '    Dim objReturn As New ROC_RETURN
        '    With objReturn
        '        .Invoice = objOrder.Invoice
        '        .setCustomerNo(objOrder.PickedOrder.Header.cu_no)
        '        .setEmpNo(sUser)
        '        .SetOrderStatusType("R")
        '        .SetInvDt(dtDate.ToString("yyyy-MM-dd"))
        '        .Credit.Header.order_type = objOrder.PickedOrder.Header.order_type
        '        .Invoice.Header.link_co_odno = objOrder.Invoice.Header.co_odno
        '    End With

        '    With objReturn.Credit.Header
        '        .cr_status = "I"
        '        .cu_no = objOrder.PickedOrder.Header.cu_no
        '        .orig_odno = objOrder.Invoice.Header.co_odno
        '    End With

        '    For Each itm As ON_INV_CODTL_ITEM In objOrder.Invoice.ItemList
        '        Dim objReturnItem As New REC_CRDTL
        '        With objReturnItem
        '            .cr_line = itm.ON_INV_CODTL.cod_line
        '            .item = itm.ON_INV_CODTL.item
        '            .packaging_code = itm.ON_INV_CODTL.packaging_code
        '            .expected_qty = itm.ON_INV_CODTL.qty
        '            .received_qty = itm.ON_INV_CODTL.qty
        '            .confirm_qty = itm.ON_INV_CODTL.qty
        '        End With

        '        objReturn.Credit.AddItem(objReturnItem)
        '    Next

        '    'set argl tables
        '    Dim drConfig As DataRow = DALRoc.getONConfigGL
        '    Dim sDeliveryChargeGL As String = drConfig("DELIVERY_CHG_GL")
        '    Dim sDeliveryTax2GL As String = drConfig("DELIVERY_TAX2_GL")

        '    Dim drARData As DataRow = DALRoc.getARTRAN(objOrder.Invoice.on_invoice.inv_trans_no)
        '    objReturn.objARRecord.SetInfo(drARData)
        '    objReturn.objARRecord.ar_trans_no = DALRoc.getNewGLTransNo(DALRoc.getOraCmd)
        '    objReturn.objARRecord.ar_date = Now.ToString("yyyy-MM-dd")

        '    'this step only if delivery charge needs to write as a journal entry 
        '    'objARRecord.ar_cr_amount = Invoice.Header.inv_amt - (Invoice.Header.delivery_chg + Invoice.Header.delivery_tax2)
        '    objARRecord.ar_cr_amount = Invoice.Header.inv_amt

        '    Dim dtGLData As DataTable = DALRoc.getGLTRAN(objOrder.Invoice.on_invoice.inv_trans_no)

        '    Dim objTmpGL As REC_ON_GLTRAN = Nothing
        '    Dim bValid As Boolean = True
        '    For Each dr As DataRow In dtGLData.Rows

        '        objTmpGL = New REC_ON_GLTRAN
        '        objTmpGL.SetInfo(dr)
        '        objTmpGL.gl_trans_no = objARRecord.ar_trans_no
        '        'bValid = (objTmpGL.gl_cd.Trim <> sDeliveryChargeGL.Trim)
        '        'bValid = bValid AndAlso (objTmpGL.gl_cd.Trim <> sDeliveryTax2GL.Trim)
        '        objGLList.Add(objTmpGL)

        '    Next

        '    Return objReturn
        'End Function
        Shared Function getARGL() As String
            Dim dr As DataRow = getONConfigGL()
            Return dr("AR_GL")
        End Function
        Shared Function getStore() As String
            Dim dr As DataRow = getONConfigGL()
            Return dr("INVOICE_DEPT")
        End Function
        Shared Function getONConfigGL() As DataRow
            Dim dt As New DataTable
            Dim drow As DataRow = Nothing

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database. Abending program." & ex.Message.ToString)
                End Try

            End If


            Dim sQry As String = " Select AR_GL " & vbNewLine & _
                                 ",INVOICE_DEPT " & vbNewLine & _
                                " , DELIVERY_CHG_GL " & vbNewLine & _
                                " , DELIVERY_TAX1_GL     " & vbNewLine & _
                                " , DELIVERY_TAX2_GL     " & vbNewLine & _
                                " from DBO.ON_CONFIG   "
            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

                If dt.Rows.Count > 0 Then
                    drow = dt.Rows(0)
                Else
                    Throw New Exception("No records returned!")
                End If
            Catch ex As Exception
                Throw New Exception("DALRoc.getONConfigGL()" & vbNewLine & _
                                    "QRY: " & sQry & vbNewLine & _
                                    "ERROR MSG: " & ex.Message.ToString)
            End Try

            Return drow
        End Function

        Shared Function getNewInvNo(ByRef oraCmd As OracleClient.OracleCommand) As Integer
            Dim iNewInvNo As Integer = 0
            Dim iInvNo As Integer = 0

            Dim sQry As String = "UPDATE DBO.ON_INV_SEQ SET INV_SEQ_NO=INV_SEQ_NO"

            Try
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()
                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("ERROR LOCKING DBO.ON_INV_SEQ FOR UPDATING")
                End If

                sQry = "SELECT INV_SEQ_NO FROM DBO.ON_INV_SEQ WHERE ROWNUM = 1"
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()
                Dim oraReader As OracleDataReader = oraCmd.ExecuteReader

                While oraReader.Read
                    iInvNo = oraReader("INV_SEQ_NO")
                End While
                oraReader.Close()

                iNewInvNo = iInvNo + 1
                If iNewInvNo >= 9999999999 Then
                    iNewInvNo = 1
                End If

                sQry = "Update DBO.ON_INV_SEQ SET INV_SEQ_NO = " & iNewInvNo

                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()
                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error updaing new return order number!")
                End If

            Catch ex As Exception
                Throw New Exception("DALROC.getNewInvNo error->" & ex.Message.ToString)
            End Try

            Return iInvNo
        End Function

        Shared Function getNewOrderNo(ByRef oraCmd As OracleClient.OracleCommand) As Integer
            Dim iNewOrderNum As Integer = 0
            Dim iOrderNum As Integer = 0
           
            Dim sQry As String = "UPDATE DBO.ON_CUST_ORDER_SEQ SET SEQ_NO=SEQ_NO"

            Try
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()
                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("ERROR LOCKING DBO.RETURN_ORDER_NO_TBL FOR UPDATING")
                End If

                sQry = "SELECT SEQ_NO FROM DBO.ON_CUST_ORDER_SEQ WHERE ROWNUM = 1"
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()
                Dim oraReader As OracleDataReader = oraCmd.ExecuteReader

                While oraReader.Read
                    iOrderNum = oraReader("SEQ_NO")
                End While
                oraReader.Close()

                iNewOrderNum = iOrderNum + 1
                If iNewOrderNum >= 999999 Then
                    iNewOrderNum = 100000
                End If

                sQry = "Update DBO.ON_CUST_ORDER_SEQ SET SEQ_NO = " & iNewOrderNum
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error updaing new order number!")
                End If

            Catch ex As Exception
                Throw New Exception("DALROC.getNewOrderNo error->" & ex.Message.ToString)
            End Try

            Return iOrderNum
        End Function


        Shared Function getNewReturnOrderNo(ByRef oraCmd As OracleClient.OracleCommand) As Integer
            Dim iNewOrderNum As Integer = 0
            Dim iOrderNum As Integer = 0
          
            Dim sQry As String = "UPDATE DBO.RETURN_ORDER_NO_TBL SET RETURN_ORDER_NO=RETURN_ORDER_NO"

            Try
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()
                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("ERROR LOCKING DBO.RETURN_ORDER_NO_TBL FOR UPDATING")
                End If

                sQry = "SELECT RETURN_ORDER_NO FROM DBO.RETURN_ORDER_NO_TBL where ROWNUM = 1"
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()
                Dim oraReader As OracleDataReader = oraCmd.ExecuteReader

                While oraReader.Read
                    iOrderNum = oraReader("RETURN_ORDER_NO")
                End While
                oraReader.Close()

                iNewOrderNum = iOrderNum + 1
                If iNewOrderNum >= 99999 Then
                    iNewOrderNum = 1
                End If

                sQry = "Update DBO.RETURN_ORDER_NO_TBL SET RETURN_ORDER_NO = " & iNewOrderNum
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error updaing new return order number!")
                End If

            Catch ex As Exception
                Throw New Exception("DALROC.getNewReturnOrderNo error->" & ex.Message.ToString)
            End Try

            Return iOrderNum
        End Function

        Shared Function getNewGLTransNo(ByRef oraCmd As OracleClient.OracleCommand) As Integer
            Dim iNewTransNum As Integer = 0
            Dim iTransNum As Integer = 0
           
            Dim sQry As String = "UPDATE DBO.ON_AR_SEQ SET AR_SEQ_NO=AR_SEQ_NO"

            Try
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()
                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("ERROR LOCKING DBO.ON_AR_SEQ FOR UPDATING")
                End If

                sQry = "SELECT AR_SEQ_NO FROM DBO.ON_AR_SEQ where ROWNUM = 1"
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()
                Dim oraReader As OracleDataReader = oraCmd.ExecuteReader

                While oraReader.Read
                    iTransNum = oraReader("AR_SEQ_NO")
                End While
                oraReader.Close()

                iNewTransNum = iTransNum + 1
                If iNewTransNum >= 999999 Then
                    iNewTransNum = 1
                End If

                sQry = "Update DBO.ON_AR_SEQ SET AR_SEQ_NO = '" & iNewTransNum.ToString & "'"
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error updating new return order number!")
                End If

            Catch ex As Exception
                Throw New Exception("DALROC.getNewGLTransNo error->" & ex.Message.ToString)
            End Try

            Return iTransNum
        End Function

        Shared Function getTenderGLCd(ByVal tender_cd As String) As String
            Dim gl_cd As String = ""

            If cmdRoc.Connection.State <> ConnectionState.Open Then
                Try
                    cmdRoc.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try
            End If

            Dim sQry As String = "SELECT GL_CD FROM DBO.TENDER_MAS WHERE TENDER_CD = " & tender_cd


            Try
                cmdRoc.CommandText = sQry
                cmdRoc.Parameters.Clear()

                Dim oraReader As OracleDataReader = cmdRoc.ExecuteReader

                While oraReader.Read
                    gl_cd = oraReader("GL_CD")
                End While
                oraReader.Close()

            Catch ex As Exception
                Throw New Exception("DALROC.getTenderGLCD()->" & ex.Message.ToString)
            End Try

            Return gl_cd
        End Function
        Shared Function getARLinkDate(ByVal sOrderNo As String) As String
            Dim sLinkDate As String = ""

            sOrderNo = Left(sOrderNo & "                ", 16)
            If cmdRoc.Connection.State <> ConnectionState.Open Then
                Try
                    cmdRoc.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try
            End If
            Dim SQry As String = " 	SELECT AR_LINK_DATE FROM DBO.ON_ARTRAN               " & vbNewLine & _
                                " 	WHERE AR_TRANS_NO IN (                                   " & vbNewLine & _
                                " 		SELECT INV.INV_TRANS_NO FROM ON_INVOICE INV            " & vbNewLine & _
                                " 		WHERE INV_NO IN (SELECT INV_NO FROM ON_INV_CO          " & vbNewLine & _
                                " 	                 	WHERE CO_ODNO = '" & sOrderNo & "'))  "



            Try
                cmdRoc.CommandText = SQry
                cmdRoc.Parameters.Clear()

                Dim oraReader As OracleDataReader = cmdRoc.ExecuteReader
                While oraReader.Read
                    sLinkDate = oraReader("AR_LINK_DATE")
                End While
                oraReader.Close()

            Catch ex As Exception
                Throw New Exception("DALRoc.getARLinkDate('" & sOrderNo & "')->" & ex.Message.ToString)
            End Try

            Return sLinkDate

        End Function
        Shared Function getNextARRecNo(ByVal sOrderNo As String, ByRef cmdOracle As OracleCommand) As Integer
            Dim iRecNo As Integer = 0

            sOrderNo = Left(sOrderNo & "                ", 16)

            Dim SQry As String = "SELECT MAX(TO_NUMBER(AR_REC_NO)) + 1 as AR_REC_NO FROM DBO.ON_ARTRAN " & vbNewLine & _
                                " WHERE AR_LINK_TRANS_NO IN (" & vbNewLine & _
                                " 	SELECT AR_LINK_TRANS_NO FROM DBO.ON_ARTRAN               " & vbNewLine & _
                                " 	WHERE AR_TRANS_NO IN (                                   " & vbNewLine & _
                                " 		SELECT INV.INV_TRANS_NO FROM ON_INVOICE INV            " & vbNewLine & _
                                " 		WHERE INV_NO IN (SELECT INV_NO FROM ON_INV_CO          " & vbNewLine & _
                                " 	                 	WHERE CO_ODNO = '" & sOrderNo & "')))  "



            Try
                cmdOracle.CommandText = SQry
                cmdOracle.Parameters.Clear()

                Dim oraReader As OracleDataReader = cmdOracle.ExecuteReader
                While oraReader.Read
                    If IsDBNull(oraReader("AR_REC_NO")) = False Then
                        iRecNo = oraReader("AR_REC_NO")
                    End If
                End While
                '  oraReader.Close()

            Catch ex As Exception
                Throw New Exception("DALRoc.getNextARRecNo('" & sOrderNo & "')->'" & ex.Message.Trim.ToString & "'" & vbNewLine & _
                                    "Qry: " & SQry)
            End Try

            Return iRecNo

        End Function
        Shared Function getARTRAN(ByVal sARTransNo As String) As DataRow
            Dim dt As New DataTable
            Dim drAR As DataRow = Nothing

            'PADD THE ARTRANS NO TO MATCH DATABASE FOR FASTER RETRIEVAL
            sARTransNo = Left(sARTransNo & "      ", 6)

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            Dim sQry As String = "SELECT AR_TRANS_NO " & vbNewLine & _
                                                  ",AR_DATE " & vbNewLine & _
                                                  ",AR_LINK_DATE " & vbNewLine & _
                                                  ",AR_CUST_NO " & vbNewLine & _
                                                  ",AR_REC_NO " & vbNewLine & _
                                                  ",AR_REF_NO " & vbNewLine & _
                                                  ",AR_DR_AMOUNT " & vbNewLine & _
                                                  ",AR_CR_AMOUNT " & vbNewLine & _
                                                  ",AR_TRANS_TYPE " & vbNewLine & _
                                                  ",AR_USER " & vbNewLine & _
                                                  ",AR_LINK_TRANS_NO " & vbNewLine & _
                                                  ",AR_BALANCE " & vbNewLine & _
                                                  " FROM DBO.ON_ARTRAN " & vbNewLine & _
                                                  " WHERE AR_TRANS_NO = '" & sARTransNo & "'"

            Try

                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

                If dt.Rows.Count > 0 Then
                    drAR = dt.Rows(0)
                Else
                    Throw New Exception("No records found!")
                End If

            Catch ex As Exception
                Throw New Exception("DALROC.getARTRAN(" & sARTransNo & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Err Msg: " & ex.Message.ToString)
            End Try

            Return drAR

        End Function
        Shared Function getGLTRAN(ByVal sGLTransNo As String) As DataTable
            Dim dt As New DataTable

            sGLTransNo = Left(sGLTransNo & "      ", 6)

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database. Abending program." & ex.Message.ToString)
                End Try

            End If

            Dim sQry As String = " Select GL_TRANS_NO  " & vbNewLine & _
                                                    " , GL_CD           " & vbNewLine & _
                                                    " , GL_DEPT         " & vbNewLine & _
                                                    " , GL_DATE         " & vbNewLine & _
                                                    " , GL_LINK_DATE    " & vbNewLine & _
                                                    " , GL_REC_NO       " & vbNewLine & _
                                                    " , GL_MEMO         " & vbNewLine & _
                                                    " , GL_DR_AMOUNT    " & vbNewLine & _
                                                    " , GL_CR_AMOUNT    " & vbNewLine & _
                                                    " , GL_USER         " & vbNewLine & _
                                                    " FROM DBO.ON_GLTRAN  " & vbNewLine & _
                                                    " WHERE GL_TRANS_NO = '" & sGLTransNo & "'" & vbNewLine & _
                                                    " ORDER BY ROWNUM ASC "

            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("DALRoc.getGLTran(" & sGLTransNo & ")" & vbNewLine & _
                                    "QRY: " & sQry & vbNewLine & _
                                    "Err Msg: " & ex.Message.ToString)
            End Try

            Return dt
        End Function
        Public Shared Function isFreightForward(ByVal sOrderNum As String, ByVal sCustNo As String) As Boolean
            Dim orderFound As Integer = 0

            sOrderNum = Left(sOrderNum & "                ", 16)
            sCustNo = Left(sCustNo & "          ", 10)

            If cmdRoc.Connection.State <> ConnectionState.Open Then
                Try
                    cmdRoc.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try
            End If

            Dim sQry As String = "SELECT COUNT(1) as ORDER_COUNT FROM DBO.LCBO_TRANSFERRED_ORDERS " & vbNewLine & _
                                 " WHERE ORIG_ORDER_NO = '" & sOrderNum & "'" & vbNewLine & _
                                 " AND ORIG_CU_NO = '" & sCustNo & "'"


            Try
                cmdRoc.CommandText = sQry
                cmdRoc.Parameters.Clear()

                Dim oraReader As OracleDataReader = cmdRoc.ExecuteReader

                While oraReader.Read
                    orderFound = oraReader("ORDER_COUNT")
                End While
                oraReader.Close()

            Catch ex As Exception
                Throw New Exception("DALROC.isFreightForward(" & sOrderNum & "," & sCustNo & ")->" & ex.Message.ToString)
            End Try


            Return (orderFound > 0)
        End Function
        Public Shared Function ProcessFreightForward(ByVal sOrigOrderNum As String, _
                                                     ByVal sNewCustomer As String, _
                                                     ByVal sEmpNo As String, _
                                                     ByVal sTransferComments As String, _
                                                     ByRef oraCmd As OracleClient.OracleCommand) As ROC_TRANSFERRED_ORDERS

            Dim roOrigOrder As ROC_ORDER = Nothing
            Dim roNewOrder As ROC_ORDER = Nothing
            Dim rrReturn As ROC_RETURN = Nothing
            Dim JE_Return_Pay As ROC_JOURNAL_ENTRY = Nothing
            Dim JE_New_Order_Pay As ROC_JOURNAL_ENTRY = Nothing
            Dim rTransferRecord As New ROC_TRANSFERRED_ORDERS
            Dim iPayCount As Integer = 0
            Dim dPayTotal As Decimal = 0D
            Dim dCreditTotal As Decimal = 0D

            Try
                If oraCmd.Connection.State <> ConnectionState.Open Then
                    oraCmd.Connection.Open()
                End If

                roOrigOrder = New ROC_ORDER(sOrigOrderNum, "A")

                'check if order is already freight forwarded first
                If DALRoc.isFreightForward(roOrigOrder.Invoice.Header.co_odno, roOrigOrder.Invoice.on_invoice.inv_cu_no) Then
                    Throw New Exception("ERROR: Order: " & roOrigOrder.Invoice.Header.co_odno & " already freight forwarded!")
                End If

                rTransferRecord.orig_order_no = roOrigOrder.order_no
                rTransferRecord.orig_cu_no = roOrigOrder.customer_no
                rTransferRecord.orig_inv_amt = roOrigOrder.invoice_amt

                rrReturn = New ROC_RETURN(sOrigOrderNum)
                'creating return order first with original customer #
                rrReturn.setEmpNo(sEmpNo)
                rrReturn.SetInvDt(Now)

                rrReturn.Insert(oraCmd, True)

                DALRoc.AddOrderComments(rrReturn.order_no, "R", "FREIGHT FORWARD RETURN", oraCmd)
                DALRoc.AddOrderComments(rrReturn.order_no, "R", sTransferComments.Trim, oraCmd)


                rTransferRecord.cr_order_no = rrReturn.order_no
                rTransferRecord.cr_cu_no = rrReturn.customer_no
                rTransferRecord.cr_inv_amt = rrReturn.invoice_amt
                rTransferRecord.delivery_chg_trans = rrReturn.Delivery_Return.AR.ar_trans_no

                'create new order
                roNewOrder = DALRoc.FreightForwardCopyOrder(sOrigOrderNum, sNewCustomer, sEmpNo)
                'change order required date
                roNewOrder.PickedOrder.Header.order_reqd_dt = Now.ToString("yyyy-MM-dd")
                roNewOrder.Insert(oraCmd)

                'Post new order info
                Dim trans_no As String = DALRoc.PostFreightForwardOrder(roNewOrder, sEmpNo, oraCmd)
                roNewOrder.Invoice.on_invoice.inv_trans_no = trans_no
                DALRoc.UpdateInvTransNo(roNewOrder.inv_no, roNewOrder.invoice_trans_no, oraCmd)

                rTransferRecord.order_no = roNewOrder.order_no
                rTransferRecord.cu_no = roNewOrder.customer_no
                rTransferRecord.inv_amt = roNewOrder.invoice_amt

                'IF CONTAINS CREDIT CARD PAYMENT
                Dim hasCCPay As Boolean = False

                'Journal Entries for the payments
                'Post Journal entries for the payments
                For Each PYMT As REC_ON_INV_PAY In roOrigOrder.Invoice.paymentList
                    If PYMT.tender_cd = 10 Or _
                    PYMT.tender_cd = 11 Or _
                    PYMT.tender_cd = 12 Then
                        hasCCPay = True
                    End If

                    If PYMT.tender_cd <> 6 Then
                        'do the opposite
                        rTransferRecord.AddJEReturnPayRecord(PYMT.tender_cd, _
                                                      PYMT.pay_amt * -1, _
                                                      "FF ENTRY - transfer pay to new order:" & roNewOrder.Invoice.Header.co_odno.Trim, _
                                                      rrReturn.inv_dt.ToString("yyyy-MM-dd HH:mm:ss"), _
                                                      DALRoc.getARLinkDate(roOrigOrder.order_no), _
                                                      roOrigOrder.invoice_trans_no, _
                                                      sEmpNo, _
                                                      oraCmd)

                        'payment for new order
                        rTransferRecord.AddJENewOrderPayRecord(PYMT.tender_cd, _
                                                      PYMT.pay_amt, _
                                                      "FF ENTRY - transfer pay from order:" & roOrigOrder.Invoice.Header.co_odno.Trim, _
                                                      roNewOrder.inv_dt.ToString("yyyy-MM-dd HH:mm:ss"), _
                                                      Now.ToString("yyyy") & Right("000" & Now.DayOfYear, 3), _
                                                      roNewOrder.invoice_trans_no, _
                                                      sEmpNo, _
                                                      oraCmd)
                    Else
                        dCreditTotal = dCreditTotal + PYMT.pay_amt

                    End If
                    dPayTotal = dPayTotal + PYMT.pay_amt

                    iPayCount = iPayCount + 1
                Next
                'Add logic to insert into the the transferred orders table
                rTransferRecord.transfer_comments = sTransferComments
                rTransferRecord.transfer_dt = Now.ToString("yyyy-MM-dd")
                rTransferRecord.emp_no = sEmpNo
                rTransferRecord.orig_payment_count = iPayCount
                rTransferRecord.orig_payment_total = dPayTotal
                rTransferRecord.orig_credit_pay_total = dCreditTotal
                rTransferRecord.Insert(oraCmd)

                'add comment on order for payment
                If hasCCPay = True Then
                    DALRoc.AddOrderComments(roNewOrder.order_no, "A", "Reference order# " & rTransferRecord.orig_order_no.Trim & " for CC Payment", oraCmd)
                End If

                'ADD COMMENT TO ORIGINAL ORDER
                DALRoc.AddOrderComments(sOrigOrderNum, "A", "RETURN " & rrReturn.order_no.Trim & " COMPLETED", oraCmd)
                DALRoc.AddOrderComments(sOrigOrderNum, "A", "RE-INVOICED TO ORDER: " & roNewOrder.order_no.Trim, oraCmd)
            Catch ex As Exception
                Throw New Exception("DALROC.ProcessFreightForward(" & sOrigOrderNum & "," & sNewCustomer & "," & sEmpNo & ")" & vbNewLine & _
                                    "err msg: " & ex.Message.ToString)
            End Try

            Return rTransferRecord
        End Function
        Public Shared Function FreightForwardCopyOrder(ByVal sOrderNum As String, _
                                                       ByVal sNewCustomer As String, _
                                                       ByVal sEmpNo As String) As ROC_ORDER
            Dim roNewOrder As New ROC_ORDER
            roNewOrder.CopyOrderInfo(sOrderNum, "A")
            roNewOrder.setCustomerNo(sNewCustomer)
            roNewOrder.SetInvDt(Now)
            roNewOrder.Invoice.Header.upload1_dt = ""
            roNewOrder.Invoice.Header.acc_post_dt = Now.ToString("yyyy-MM-dd HH:mm:ss")
            roNewOrder.Invoice.Header.link_co_odno = roNewOrder.order_no
            roNewOrder.setEmpNo(sEmpNo)

            For Each payment As REC_ON_INV_PAY In roNewOrder.Invoice.paymentList
                With payment
                    .pay_comment = "FF Pay.Orig Order#" & roNewOrder.Link_Order_No.Trim
                    .acc_post_dt = Now.ToString("yyyy-MM-dd HH:mm:ss")
                    .pay_trans_no = 0
                    .upload_dt = Now.ToString("yyyy-MM-dd HH:mm:ss")
                    .pay_dt = Now
                End With
            Next

            Return roNewOrder
        End Function
       
        Public Shared Function PostFreightForwardOrder(ByVal objFFOrder As ROC_ORDER, ByVal sUser As String, ByRef cmdRoc As OracleCommand) As String
            Dim origOrder As New ROC_ORDER()
            Dim objARRecord As New REC_ON_ARTRAN
            Dim objGLList As New ArrayList

            origOrder.CopyOrderInfo(objFFOrder.Link_Order_No, "A")

            Dim drARData As DataRow = DALRoc.getARTRAN(origOrder.invoice_trans_no)
            objARRecord.SetInfo(drARData)

            objARRecord.ar_trans_no = DALRoc.getNewGLTransNo(cmdRoc)
            objARRecord.ar_date = Now.ToString("yyyy-MM-dd")
            objARRecord.ar_link_date = Now.ToString("yyyy") & Right("000" & Now.DayOfYear, 3)
            objARRecord.ar_link_trans_no = objARRecord.ar_trans_no

            objARRecord.ar_cust_no = objFFOrder.customer_no
            objARRecord.ar_ref_no = objFFOrder.order_no
            objARRecord.ar_dr_amount = drARData("ar_dr_amount")
            objARRecord.ar_cr_amount = drARData("ar_cr_amount")
            objARRecord.ar_user = Right(sUser.Trim, 3)

            Dim dtGLData As DataTable = DALRoc.getGLTRAN(origOrder.Invoice.on_invoice.inv_trans_no)

            Dim objTmpGL As REC_ON_GLTRAN = Nothing
            Dim bValid As Boolean = True

            For Each dr As DataRow In dtGLData.Rows
                objTmpGL = New REC_ON_GLTRAN
                objTmpGL.SetInfo(dr)
                objTmpGL.gl_memo = objFFOrder.Invoice.Header.co_odno.Trim
                objTmpGL.gl_trans_no = objARRecord.ar_trans_no
                objTmpGL.gl_user = Right(sUser.Trim, 3)
                objTmpGL.gl_date = objARRecord.ar_date
                objTmpGL.gl_link_date = objARRecord.ar_link_date
                objGLList.Add(objTmpGL)
            Next

            'insert to table
            objARRecord.Insert(cmdRoc)
            For Each glrec As REC_ON_GLTRAN In objGLList
                glrec.Insert(cmdRoc)
            Next

            Return objARRecord.ar_trans_no
        End Function
        Public Shared Function getTransLinkInfo(ByVal artran As REC_ON_ARTRAN, ByRef cmdOracle As OracleClient.OracleCommand) As DataTable
            Dim dtLinkTran As New DataTable
            Dim dr As DataRow = Nothing

            Dim sQry As String = " SELECT  AR_TRANS_NO   " & vbNewLine & _
                                " 	,AR_DATE            " & vbNewLine & _
                                " 	,AR_LINK_DATE       " & vbNewLine & _
                                " 	,AR_CUST_NO         " & vbNewLine & _
                                " 	,AR_REC_NO          " & vbNewLine & _
                                " 	,AR_REF_NO          " & vbNewLine & _
                                " 	,AR_DR_AMOUNT       " & vbNewLine & _
                                " 	,AR_CR_AMOUNT       " & vbNewLine & _
                                " 	,AR_TRANS_TYPE      " & vbNewLine & _
                                " 	,AR_USER            " & vbNewLine & _
                                " 	,AR_POST_DT         " & vbNewLine & _
                                " 	,AR_LINK_TRANS_NO   " & vbNewLine & _
                                " 	,AR_BALANCE         " & vbNewLine & _
                                " FROM DBO.ON_ARTRAN  " & vbNewLine & _
                                " WHERE TRIM(AR_REF_NO) = '" & artran.ar_ref_no.Trim & "'" & vbNewLine & _
                                " AND TRIM(AR_CUST_NO) = '" & artran.ar_cust_no.Trim & "'"


            Try
                cmdOracle.CommandText = sQry
                cmdOracle.Parameters.Clear()

                Dim drReader As OracleDataReader = cmdOracle.ExecuteReader
                dtLinkTran.Load(drReader)
                drReader.Close()

            Catch ex As Exception
                Throw New Exception("DAL_ROC.getTransLinkInfo()" & vbNewLine & _
                                    "qry:" & sQry & vbNewLine & _
                                    "ERROR:" & ex.Message)
            End Try

            Return dtLinkTran

        End Function

        Shared Function isTransferFinalized(ByVal iLe_Select_ID As Integer) As Boolean
            Dim finalized As Boolean = False

            Dim drow As DataRow = DALRoc.getLeSelectOrderHeader(iLe_Select_ID)
            finalized = drow("STATUS") = "FINALIZED"

            Return finalized
        End Function

        Shared Sub UpdateInvTransNo(ByVal iInv_No As Integer, ByVal sInv_Trans_no As String, ByRef cmdRoc As OracleCommand)
            Dim sQry As String = "UPDATE DBO.ON_INVOICE SET INV_TRANS_NO = '" & sInv_Trans_no & "'" & vbNewLine & _
                                 "WHERE INV_NO = " & iInv_No

            Try
                cmdRoc.CommandText = sQry
                cmdRoc.Parameters.Clear()

                If cmdRoc.ExecuteNonQuery <= 0 Then
                    Throw New Exception("ERROR UPDATING INV_TRANS_NO FOR ORDER " & iInv_No)
                End If
            Catch ex As Exception
                Throw New Exception("UpdateInvTransNo(" & iInv_No & "," & sInv_Trans_no & ")" & vbNewLine & _
                  "Qry: " & sQry & vbNewLine & _
                  "ERROR MSG: " & ex.Message.Trim)
            End Try
        End Sub
        Shared Function getCOCUSInfo(ByVal sOrderNum As String) As DataRow
            Dim dt As New DataTable
            Dim dr As DataRow = Nothing

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            Try

                daROC.SelectCommand.CommandText = " SELECT CO_ODNO         " & vbNewLine & _
                                            " , NVL(SHIP_TO_NAME,' ')  SHIP_TO_NAME " & vbNewLine & _
                                            " , NVL(SHIP_TO_ADD1,' ')  SHIP_TO_ADD1 " & vbNewLine & _
                                            " , NVL(SHIP_TO_ADD2,' ')  SHIP_TO_ADD2 " & vbNewLine & _
                                            " , NVL(SHIP_TO_CITY,' ')  SHIP_TO_CITY " & vbNewLine & _
                                            " , NVL(SHIP_TO_PROV,' ')  SHIP_TO_PROV " & vbNewLine & _
                                            " , NVL(SHIP_TO_POST_CD, ' ')  SHIP_TO_POST_CD    " & vbNewLine & _
                                            " , NVL(SHIP_TO_PHONE  , ' ')  SHIP_TO_PHONE      " & vbNewLine & _
                                            " , NVL(SHIP_TO_FAX    , ' ')  SHIP_TO_FAX        " & vbNewLine & _
                                            " , NVL(SHIP_TO_CONTACT, ' ')  SHIP_TO_CONTACT    " & vbNewLine & _
                                            " , NVL(BILL_TO_NAME,' ') BILL_TO_NAME        " & vbNewLine & _
                                            " , NVL(BILL_TO_ADD1,' ') BILL_TO_ADD1        " & vbNewLine & _
                                            " , NVL(BILL_TO_ADD2,' ') BILL_TO_ADD2        " & vbNewLine & _
                                            " , NVL(BILL_TO_CITY,' ') BILL_TO_CITY        " & vbNewLine & _
                                            " , NVL(BILL_TO_PROV,' ') BILL_TO_PROV        " & vbNewLine & _
                                            " , NVL(BILL_TO_POST_CD,' ') BILL_TO_POST_CD     " & vbNewLine & _
                                            " , NVL(BILL_TO_PHONE  ,' ') BILL_TO_PHONE       " & vbNewLine & _
                                            " , NVL(BILL_TO_FAX    ,' ') BILL_TO_FAX         " & vbNewLine & _
                                            " , NVL(SHIP_TO        ,' ') SHIP_TO             " & vbNewLine & _
                                            " , NVL(SHIP_VIA       ,' ') SHIP_VIA            " & vbNewLine & _
                                            " , NVL(FACING_DEPOT_CODE,' ') FACING_DEPOT_CODE   " & vbNewLine & _
                                            " , NVL(SHIP_TO_COUNTRY  ,' ') SHIP_TO_COUNTRY     " & vbNewLine & _
                                            " , NVL(BILL_TO_COUNTRY  ,' ') BILL_TO_COUNTRY     " & vbNewLine & _
                                            " , NVL(SHIP_TO_EMAIL    ,' ') SHIP_TO_EMAIL       " & vbNewLine & _
                                            " , NVL(BILL_TO_EMAIL    ,' ') BILL_TO_EMAIL       " & vbNewLine & _
                                            " , NVL(BILL_TO_CONTACT  ,' ') BILL_TO_CONTACT     " & vbNewLine & _
                                            " from DBO.COCUS         " & vbNewLine & _
                                            " WHERE trim(CO_ODNO) = '" & sOrderNum.Trim & "'"


                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

                If dt.Rows.Count > 0 Then
                    dr = dt.Rows(0)
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message.ToString)
            End Try

            Return dr
        End Function

        Shared Function getCOCUSInfo(ByVal sOrderNum As String, ByVal sCustNum As String) As DataRow
            Dim dt As New DataTable
            Dim dr As DataRow = Nothing

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            Try

                daROC.SelectCommand.CommandText = " SELECT '" & sOrderNum.Trim & "' AS CO_ODNO   " & vbNewLine & _
                                            " , NVL(CU_NAME,' ')  SHIP_TO_NAME " & vbNewLine & _
                                            " , NVL(ADD1_SH,' ')  SHIP_TO_ADD1 " & vbNewLine & _
                                            " , NVL(ADD2_SH,' ')  SHIP_TO_ADD2 " & vbNewLine & _
                                            " , NVL(CITY_SH,' ')  SHIP_TO_CITY " & vbNewLine & _
                                            " , NVL(PROV_SH,' ')  SHIP_TO_PROV " & vbNewLine & _
                                            " , NVL(POST_CODE, ' ')  SHIP_TO_POST_CD    " & vbNewLine & _
                                            " , NVL(PHONE_NU  , ' ')  SHIP_TO_PHONE      " & vbNewLine & _
                                            " , NVL(FAX_NU    , ' ')  SHIP_TO_FAX        " & vbNewLine & _
                                            " , NVL(CONTACT, ' ')  SHIP_TO_CONTACT    " & vbNewLine & _
                                            " , NVL(CU_NAME,' ') BILL_TO_NAME        " & vbNewLine & _
                                            " , NVL(ADD1_SH,' ') BILL_TO_ADD1        " & vbNewLine & _
                                            " , NVL(ADD2_SH,' ') BILL_TO_ADD2        " & vbNewLine & _
                                            " , NVL(CITY_SH,' ') BILL_TO_CITY        " & vbNewLine & _
                                            " , NVL(PROV_SH,' ') BILL_TO_PROV        " & vbNewLine & _
                                            " , NVL(POST_CODE,' ') BILL_TO_POST_CD     " & vbNewLine & _
                                            " , NVL(PHONE_NU  ,' ') BILL_TO_PHONE       " & vbNewLine & _
                                            " , NVL(FAX_NU    ,' ') BILL_TO_FAX         " & vbNewLine & _
                                            " , TRIM(CU_NO)  SHIP_TO             " & vbNewLine & _
                                            " , ' ' SHIP_VIA            " & vbNewLine & _
                                            " , ' ' FACING_DEPOT_CODE   " & vbNewLine & _
                                            " , NVL(COUNTRY, ' ')  SHIP_TO_COUNTRY     " & vbNewLine & _
                                            " , NVL(COUNTRY  ,' ') BILL_TO_COUNTRY     " & vbNewLine & _
                                            " , NVL(EMAIL    ,' ') SHIP_TO_EMAIL       " & vbNewLine & _
                                            " , NVL(EMAIL    ,' ') BILL_TO_EMAIL       " & vbNewLine & _
                                            " , NVL(CONTACT  ,' ') BILL_TO_CONTACT     " & vbNewLine & _
                                            " from DBO.ON_CUST_MAS         " & vbNewLine & _
                                            " WHERE trim(CU_NO) = '" & sCustNum.Trim & "'"


                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

                If dt.Rows.Count > 0 Then
                    dr = dt.Rows(0)
                Else
                    Throw New Exception("No records found!")
                End If

            Catch ex As Exception
                Throw New Exception(ex.Message.ToString)
            End Try

            Return dr
        End Function

        Public Shared Function CreateJEPayRecord(ByVal sOrderNum As String, _
                                                 ByVal sCustomer As String, _
                                                 ByVal tender_cd As Integer, _
                                                 ByVal Pay_Amt As Decimal, _
                                                 ByVal sComments As String, _
                                                 ByVal sPostDt As String, _
                                                 ByVal sLinkDt As String, _
                                                 ByVal sLinkTransNo As String, _
                                                 ByVal sEmpno As String) As ROC_JOURNAL_ENTRY
            Dim jePay As New ROC_JOURNAL_ENTRY

            With jePay.JE_Record
                .je_total = Pay_Amt
                .je_post_dt = sPostDt
                .je_cust_no = sCustomer.Trim
                .je_comment = sComments
            End With

            With jePay.AR_SubLedger
                .ar_date = sPostDt
                .ar_link_date = sLinkDt
                .ar_cust_no = sCustomer.Trim
                .ar_ref_no = sOrderNum.Trim
                .ar_trans_type = "P"
                .ar_user = Right(sEmpno.Trim, 3)
                .ar_link_trans_no = sLinkTransNo
            End With

            If Pay_Amt < 0 Then 'it is a return, need to debit ar/credit tender
                jePay.AR_SubLedger.ar_cr_amount = 0
                jePay.AR_SubLedger.ar_dr_amount = Math.Abs(Pay_Amt)
            Else
                jePay.AR_SubLedger.ar_cr_amount = Math.Abs(Pay_Amt)
                jePay.AR_SubLedger.ar_dr_amount = 0
            End If

            Dim GLRec As New REC_ON_GLTRAN

            With GLRec
                .gl_cd = DALRoc.getTenderGLCd(6).Trim
                .gl_date = sPostDt
                .gl_dept = "941"
                .gl_memo = sOrderNum.Trim
                .gl_link_date = sLinkDt
                .gl_cr_amount = jePay.AR_SubLedger.ar_cr_amount
                .gl_dr_amount = jePay.AR_SubLedger.ar_dr_amount
                .gl_user = jePay.AR_SubLedger.ar_user
            End With

            jePay.GL_Records.Add(GLRec)


            GLRec = New REC_ON_GLTRAN
            With GLRec
                .gl_cd = DALRoc.getTenderGLCd(tender_cd).Trim
                .gl_date = sPostDt
                .gl_dept = "941"
                .gl_memo = sOrderNum.Trim
                .gl_link_date = sLinkDt
                .gl_cr_amount = jePay.AR_SubLedger.ar_dr_amount
                .gl_dr_amount = jePay.AR_SubLedger.ar_cr_amount
                .gl_user = jePay.AR_SubLedger.ar_user
            End With

            jePay.GL_Records.Add(GLRec)

            Return jePay
        End Function
        ''' <summary>
        ''' Checks if the customer is valid
        ''' 1) Active
        ''' 2) not no serve (ON_CUST_ATTR.VALUE != 'FF = No Serve')
        ''' 3) Exist
        ''' </summary>
        ''' <param name="sCustomerNo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function isValidCustomer(ByVal sCustomerNo As String) As Boolean
            Dim bValidCustomer As Boolean = False
            Dim iCount As Integer = 0
            Dim sQry As String = "SELECT COUNT(1) as VALID FROM DBO.ON_CUST_ATTR ATTR " & vbNewLine & _
                                 " INNER JOIN DBO.ON_CUST_MAS CU" & vbNewLine & _
                                 " ON ATTR.CU_NO = CU.CU_NO " & vbNewLine & _
                                 " AND CU.CU_NO = '" & sCustomerNo.Trim.PadRight(10) & "' " & vbNewLine & _
                                 " AND NVL(ACTIVE_FL,'N') = 'Y' " & vbNewLine & _
                                 " AND (TRIM(ATTR.ATTR_ID) = 'CU_ATTR6' AND TRIM(ATTR.ATTR_ID) <> 'FF = No Serve')"
            Try
                If cmdRoc.Connection.State <> ConnectionState.Open Then
                    cmdRoc.Connection.Open()
                End If

                cmdRoc.CommandText = sQry
                cmdRoc.Parameters.Clear()

                Dim oraReader As OracleClient.OracleDataReader = cmdRoc.ExecuteReader

                While oraReader.Read
                    iCount = oraReader("VALID")
                End While
                oraReader.Close()
            Catch ex As Exception
                Throw New Exception("DALROC.isValidCustomer(" & sCustomerNo & ")->" & ex.Message.ToString)
            End Try

            bValidCustomer = (iCount > 0)

            Return bValidCustomer

        End Function
        Shared Function getLeSelectTransfer(ByVal sLeSelectID As String _
                                            , ByVal sOrigCustomerNo As String _
                                            , ByVal sOrigCustName As String _
                                            , ByVal sNewCustomerNo As String _
                                            , ByVal sNewCustName As String _
                                            , ByVal sContactName As String _
                                            , ByVal sCorporateEmail As String _
                                            , ByVal sCreationDateFrom As String _
                                            , ByVal sCreationDateTo As String _
                                            , ByVal sExpectedTransferDateFrom As String _
                                            , ByVal sExpectedTransferDateTo As String _
                                            , ByVal sReturnOrderNo As String _
                                            , ByVal sTransferOrderNo As String _
                                            , ByVal sPreparedBy As String _
                                            , ByVal sStatus As String) As DataTable

            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            Dim sWhere As String = ""

            If sLeSelectID.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & " LE_SELECT_ID = " & sLeSelectID & vbNewLine
            End If

            If sOrigCustomerNo.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & " ORIG_CUSTOMER_NO = '" & Left(sOrigCustomerNo.Trim & "          ", 10) & "'" & vbNewLine
            End If

            If sOrigCustName.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & " TRIM(ORIG_CUST_NAME) LIKE '%" & sOrigCustName.Trim.Replace("'", "''") & "%'" & vbNewLine
            End If

            If sNewCustomerNo.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & " NEW_CUSTOMER_NO = '" & Left(sNewCustomerNo.Trim & "          ", 10) & "'" & vbNewLine
            End If

            If sNewCustName.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & "TRIM(NEW_CUST_NAME) LIKE '%" & sNewCustName.Trim.Replace("'", "''") & "%'" & vbNewLine
            End If

            If sContactName.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & "TRIM(LE_SELECT_CONTACT_NAME) LIKE '%" & sContactName.Trim & "%'" & vbNewLine
            End If

            If sCorporateEmail.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & "TRIM(LE_SELECT_CORPORATE_EMAIL) LIKE '%" & sCorporateEmail.Trim & "%'" & vbNewLine
            End If

            If sCreationDateFrom.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & " CREATION_DT >= TO_DATE('" & sCreationDateFrom & "','YYYY-MM-DD')" & vbNewLine
            End If

            If sCreationDateTo.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & " CREATION_DT <= TO_DATE('" & sCreationDateTo & "','YYYY-MM-DD')" & vbNewLine
            End If

            If sExpectedTransferDateFrom.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & " EXPECTED_TRANSFER_DT >= TO_DATE('" & sExpectedTransferDateFrom & "','YYYY-MM-DD')" & vbNewLine
            End If

            If sExpectedTransferDateTo.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & " EXPECTED_TRANSFER_DT <= TO_DATE('" & sExpectedTransferDateTo & "','YYYY-MM-DD')" & vbNewLine
            End If

            If sReturnOrderNo.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & " LE_SELECT_RETURN_NO = '" & Left(sReturnOrderNo.Trim & "                ", 16) & "'" & vbNewLine
            End If

            If sTransferOrderNo.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & " LE_SELECT_ORDER_NO = '" & Left(sTransferOrderNo.Trim & "                ", 16) & "'" & vbNewLine
            End If

            If sPreparedBy.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & "TRIM(PREPARED_BY) LIKE '%" & sPreparedBy.Trim & "%'" & vbNewLine
            End If

            If sStatus.Trim <> "" Then
                If sWhere <> "" Then
                    sWhere = sWhere & " AND "
                End If
                sWhere = sWhere & "upper(TRIM(STATUS)) = '" & sStatus.ToUpper.Trim & "'" & vbNewLine
            End If


            Dim sQry As String = " Select LE_SELECT_ID         " & vbNewLine & _
                                " , ORIG_CUSTOMER_NO          " & vbNewLine & _
                                " , ORIG_CUST_NAME            " & vbNewLine & _
                                " , ORIG_CUST_ADDRESS         " & vbNewLine & _
                                " , ORIG_CUST_CITY            " & vbNewLine & _
                                " , ORIG_CUST_POST_CODE       " & vbNewLine & _
                                " , ORIG_CUST_PROV            " & vbNewLine & _
                                " , ORIG_CUST_PHONE           " & vbNewLine & _
                                " , NEW_CUSTOMER_NO           " & vbNewLine & _
                                " , NEW_CUST_NAME             " & vbNewLine & _
                                " , NEW_CUST_ADDRESS         " & vbNewLine & _
                                " , NEW_CUST_CITY             " & vbNewLine & _
                                " , NEW_CUST_POST_CODE        " & vbNewLine & _
                                " , NEW_CUST_PROV             " & vbNewLine & _
                                " , CREATION_DT               " & vbNewLine & _
                                " , EXPECTED_TRANSFER_DT      " & vbNewLine & _
                                " , LE_SELECT_CONTACT_NAME    " & vbNewLine & _
                                " , LE_SELECT_CORPORATE_EMAIL " & vbNewLine & _
                                " , LE_SELECT_RETURN_NO       " & vbNewLine & _
                                " , LE_SELECT_ORDER_NO        " & vbNewLine & _
                                " , PREPARED_BY               " & vbNewLine & _
                                " , AUTHORIZED_BY             " & vbNewLine & _
                                " , COMMENTS                  " & vbNewLine & _
                                " , STATUS                    " & vbNewLine & _
                                " , LAST_UPDATE               " & vbNewLine & _
                                " from DBO.LCBO_LE_SELECT_TRANSFER "

            If sWhere <> "" Then
                sQry = sQry & " WHERE " & sWhere
            End If


            Try

                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("DALROC.getLeSelectTransfer(" & sLeSelectID & vbNewLine & _
                                            ", " & sOrigCustomerNo & vbNewLine & _
                                            ", " & sOrigCustName & vbNewLine & _
                                            ", " & sNewCustomerNo & vbNewLine & _
                                            ", " & sNewCustName & vbNewLine & _
                                            ", " & sContactName & vbNewLine & _
                                            ", " & sCorporateEmail & vbNewLine & _
                                            ", " & sCreationDateFrom & vbNewLine & _
                                            ", " & sCreationDateTo & vbNewLine & _
                                            ", " & sExpectedTransferDateFrom & vbNewLine & _
                                            ", " & sExpectedTransferDateTo & vbNewLine & _
                                            ", " & sReturnOrderNo & vbNewLine & _
                                            ", " & sTransferOrderNo & vbNewLine & _
                                            ", " & sPreparedBy & vbNewLine & _
                                            ", " & sStatus & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Err Msg: " & ex.Message.ToString)
            End Try

            Return dt

        End Function
        Shared Function getLeSelectTransfer() As DataTable
            Dim dt As New DataTable
            Dim drLS As DataRow = Nothing

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            Dim sQry As String = " Select LE_SELECT_ID         " & vbNewLine & _
                             " , ORIG_CUSTOMER_NO          " & vbNewLine & _
                             " , ORIG_CUST_NAME            " & vbNewLine & _
                             " , ORIG_CUST_ADDRESS         " & vbNewLine & _
                             " , ORIG_CUST_CITY            " & vbNewLine & _
                             " , ORIG_CUST_POST_CODE       " & vbNewLine & _
                             " , ORIG_CUST_PROV            " & vbNewLine & _
                             " , ORIG_CUST_PHONE           " & vbNewLine & _
                             " , NEW_CUSTOMER_NO           " & vbNewLine & _
                             " , NEW_CUST_NAME             " & vbNewLine & _
                             " , NEW_CUST_ADDRESS         " & vbNewLine & _
                             " , NEW_CUST_CITY             " & vbNewLine & _
                             " , NEW_CUST_POST_CODE        " & vbNewLine & _
                             " , NEW_CUST_PROV             " & vbNewLine & _
                             " , CREATION_DT               " & vbNewLine & _
                             " , EXPECTED_TRANSFER_DT      " & vbNewLine & _
                             " , LE_SELECT_CONTACT_NAME    " & vbNewLine & _
                             " , LE_SELECT_CORPORATE_EMAIL " & vbNewLine & _
                             " , LE_SELECT_RETURN_NO       " & vbNewLine & _
                             " , LE_SELECT_ORDER_NO        " & vbNewLine & _
                             " , PREPARED_BY               " & vbNewLine & _
                             " , AUTHORIZED_BY             " & vbNewLine & _
                             " , COMMENTS                  " & vbNewLine & _
                             " , STATUS                    " & vbNewLine & _
                             " , LAST_UPDATE               " & vbNewLine & _
                             " from DBO.LCBO_LE_SELECT_TRANSFER "


            Try

                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("DALROC.getLeSelectTransfer()" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Err Msg: " & ex.Message.ToString)
            End Try

            Return dt

        End Function
        Shared Function getDocumentNameList() As DataTable
            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            Dim sQry As String = " Select DOC_DESC " & vbNewLine & _
                                " , DOC_REQUIRED " & vbNewLine & _
                                " , DOC_TYPE_ID " & vbNewLine & _
                                " , CHECK_ID " & vbNewLine & _
                                " from DBO.LCBO_LE_SELECT_DOC_MAS " & vbNewLine & _
                                " order by doc_type_id asc "
            Try

                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("DALROC.getDocumentNameList()" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Err Msg: " & ex.Message.ToString)
            End Try

            Return dt

        End Function
        Shared Function getLeSelectTransfer(ByVal iLeSelectID As Integer) As DataRow
            Dim dt As New DataTable
            Dim drLS As DataRow = Nothing

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            Dim sQry As String = " Select LE_SELECT_ID         " & vbNewLine & _
                              " , ORIG_CUSTOMER_NO          " & vbNewLine & _
                              " , ORIG_CUST_NAME            " & vbNewLine & _
                              " , ORIG_CUST_ADDRESS         " & vbNewLine & _
                              " , ORIG_CUST_CITY            " & vbNewLine & _
                              " , ORIG_CUST_POST_CODE       " & vbNewLine & _
                              " , ORIG_CUST_PROV            " & vbNewLine & _
                              " , ORIG_CUST_PHONE           " & vbNewLine & _
                              " , NEW_CUSTOMER_NO           " & vbNewLine & _
                              " , NEW_CUST_NAME             " & vbNewLine & _
                              " , NEW_CUST_ADDRESS         " & vbNewLine & _
                              " , NEW_CUST_CITY             " & vbNewLine & _
                              " , NEW_CUST_POST_CODE        " & vbNewLine & _
                              " , NEW_CUST_PROV             " & vbNewLine & _
                              " , CREATION_DT               " & vbNewLine & _
                              " , EXPECTED_TRANSFER_DT      " & vbNewLine & _
                              " , LE_SELECT_CONTACT_NAME    " & vbNewLine & _
                              " , LE_SELECT_CORPORATE_EMAIL " & vbNewLine & _
                              " , LE_SELECT_RETURN_NO       " & vbNewLine & _
                              " , LE_SELECT_ORDER_NO        " & vbNewLine & _
                              " , PREPARED_BY               " & vbNewLine & _
                              " , AUTHORIZED_BY             " & vbNewLine & _
                              " , COMMENTS                  " & vbNewLine & _
                              " , STATUS                    " & vbNewLine & _
                              " , LAST_UPDATE               " & vbNewLine & _
                              " from DBO.LCBO_LE_SELECT_TRANSFER " & vbNewLine & _
                              " WHERE LE_SELECT_ID = " & iLeSelectID

            Try

                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

                If dt.Rows.Count > 0 Then
                    drLS = dt.Rows(0)
                Else
                    Throw New Exception("No records found!")
                End If

            Catch ex As Exception
                Throw New Exception("DALROC.getLeSelectTransfer(" & iLeSelectID & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Err Msg: " & ex.Message.ToString)
            End Try

            Return drLS

        End Function
        Shared Function getLeSelectCheckList(ByVal iLeSelectID As Integer) As DataTable
            Dim dt As New DataTable
            Dim drLS As DataRow = Nothing

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            Dim sQry As String = " Select T1.LE_SELECT_ID " & vbNewLine & _
                                " , T2.CHECK_ID          " & vbNewLine & _
                                " , T2.CHECK_SEQUENCE    " & vbNewLine & _
                                " , T2.CHECK_ID_HDR      " & vbNewLine & _
                                " , T2.IS_CHECK_HDR      " & vbNewLine & _
                                " , T2.CHECK_DESC        " & vbNewLine & _
                                " , T1.CHECKED           " & vbNewLine & _
                                " , T1.CHECK_DT          " & vbNewLine & _
                                " , T1.COMMENTS          " & vbNewLine & _
                                " , T2.AUTOMATED_CHECK   " & vbNewLine & _
                                " , 'E' AS ACTION        " & vbNewLine & _
                                " FROM DBO.LCBO_LE_SELECT_CHECK_LIST T1           " & vbNewLine & _
                                " INNER JOIN DBO.LCBO_LE_SELECT_CHECK_LIST_MAS T2 " & vbNewLine & _
                                " ON T1.CHECK_ID = T2.CHECK_ID                    " & vbNewLine & _
                                " WHERE T1.LE_SELECT_ID = " & iLeSelectID & vbNewLine & _
                                " UNION                                           " & vbNewLine & _
                                " SELECT " & iLeSelectID & vbNewLine & _
                                " , T3.CHECK_ID                                   " & vbNewLine & _
                                " , T3.CHECK_SEQUENCE                             " & vbNewLine & _
                                " , T3.CHECK_ID_HDR                               " & vbNewLine & _
                                " , T3.IS_CHECK_HDR                               " & vbNewLine & _
                                " , T3.CHECK_DESC                                 " & vbNewLine & _
                                " ,'N'                                            " & vbNewLine & _
                                " ,NULL                                           " & vbNewLine & _
                                " ,NULL                                           " & vbNewLine & _
                                " ,T3.AUTOMATED_CHECK                             " & vbNewLine & _
                                " ,'N' AS ACTION                                  " & vbNewLine & _
                                " FROM DBO.LCBO_LE_SELECT_CHECK_LIST_MAS T3       " & vbNewLine & _
                                " WHERE CHECK_ID NOT IN (SELECT CHECK_ID          " & vbNewLine & _
                                "                        FROM DBO.LCBO_LE_SELECT_CHECK_LIST " & vbNewLine & _
                                "                        WHERE LE_SELECT_ID = " & iLeSelectID & ")  " & vbNewLine & _
                                " ORDER BY CHECK_SEQUENCE ASC   "



            Try

                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("DALROC.getLeSelectCheckList()" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Err Msg: " & ex.Message.ToString)
            End Try

            Return dt

        End Function
        Shared Function getLeSelectDocumentList(ByVal iLeSelectID As Integer) As DataTable
            Dim dt As New DataTable
            Dim drLS As DataRow = Nothing

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            Dim sQry As String = " Select T1.LE_SELECT_ID " & vbNewLine & _
                                    " , T1.DOC_ID            " & vbNewLine & _
                                    " , T1.DOC_TYPE_ID       " & vbNewLine & _
                                    " , T1.DOC_FILEPATH      " & vbNewLine & _
                                    " , T1.ORIG_FILENAME     " & vbNewLine & _
                                    " , T1.UPLOAD_DT         " & vbNewLine & _
                                    " , T1.UPLOAD_BY         " & vbNewLine & _
                                    " , T2.DOC_DESC          " & vbNewLine & _
                                    " , T2.DOC_REQUIRED      " & vbNewLine & _
                                    " , T2.CHECK_ID          " & vbNewLine & _
                                    " , 'E' AS ACTION        " & vbNewLine & _
                                    " from DBO.LCBO_LE_SELECT_DOCUMENTS T1 " & vbNewLine & _
                                    " inner join DBO.LCBO_LE_SELECT_DOC_MAS T2 ON T1.DOC_TYPE_ID = T2.DOC_TYPE_ID " & vbNewLine & _
                                    " WHERE T1.LE_SELECT_ID = " & iLeSelectID & vbNewLine & _
                                    " UNION ALL                          " & vbNewLine & _
                                    " SELECT " & iLeSelectID & " as LE_SELECT_ID           " & vbNewLine & _
                                    " ,TO_CHAR(DOC_TYPE_ID) AS DOC_ID                      " & vbNewLine & _
                                    " ,T3.DOC_TYPE_ID AS DOC_TYPE_ID     " & vbNewLine & _
                                    " ,' ' AS DOC_FILEPATH               " & vbNewLine & _
                                    " ,' ' AS ORIG_FILENAME              " & vbNewLine & _
                                    " ,SYSDATE AS UPLOAD_DT              " & vbNewLine & _
                                    " ,' ' AS UPLOAD_BY                  " & vbNewLine & _
                                    " ,T3.DOC_DESC AS DOC_DESC           " & vbNewLine & _
                                    " ,T3.DOC_REQUIRED AS DOC_REQUIRED   " & vbNewLine & _
                                    " ,T3.CHECK_ID                       " & vbNewLine & _
                                    " ,'N' AS ACTION                     " & vbNewLine & _
                                    " FROM DBO.LCBO_LE_SELECT_DOC_MAS T3 " & vbNewLine & _
                                    " WHERE T3.DOC_TYPE_ID NOT IN (SELECT NVL(T4.DOC_TYPE_ID,0) FROM DBO.LCBO_LE_SELECT_DOCUMENTS T4 " & vbNewLine & _
                                    "                              WHERE T4.LE_SELECT_ID = " & iLeSelectID & ")                                        " & vbNewLine & _
                                    " ORDER BY DOC_TYPE_ID ASC, ACTION DESC                                                                       "

            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)
            Catch ex As Exception
                Throw New Exception("DALROC.getLeSelectCheckList()" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Err Msg: " & ex.Message.ToString)
            End Try

            Return dt

        End Function
        Shared Sub AddOrderComments(ByVal ordernum As String, ByVal ordertype As String, ByVal comment As String, ByRef cmdRoc As OracleCommand)

            Dim sQry As String = ""
            Dim line As Integer = 0
            Dim oraReader As OracleDataReader = Nothing

            If ordertype = "R" Then
                sQry = "Select nvl(max(RC_SEQN),-1) as LINE from DBO.CRCMT where CR_ODNO = '" & ordernum.PadRight(10) & "'"
            Else
                sQry = "SELECT nvl(max(OC_SEQN),-1) AS LINE FROM DBO.ODCMT WHERE CO_ODNO = '" & ordernum.PadRight(10) & "' AND OC_TYPE = 'L'"
            End If
            

            Try
                cmdRoc.CommandText = sQry
                oraReader = cmdRoc.ExecuteReader

                While oraReader.Read
                    line = oraReader("LINE")
                End While

                line = line + 1
                If ordertype = "R" Then
                    sQry = "INSERT INTO DBO.CRCMT (" & vbNewLine & _
                           " CR_ODNO " & vbNewLine & _
                           ",RC_SEQN " & vbNewLine & _
                           ",RC_LINE " & vbNewLine & _
                           ",RC_COMMENT " & vbNewLine & _
                           ") VALUES (" & vbNewLine & _
                           "'" & ordernum.PadRight(10) & "'" & vbNewLine & _
                           "," & line & vbNewLine & _
                           ",0" & vbNewLine & _
                           ",'" & comment.Trim & "')"
                ElseIf ordertype = "A" Then
                    sQry = "INSERT INTO DBO.ODCMT (" & vbNewLine & _
                    " CO_ODNO         " & vbNewLine & _
                    " ,OC_SEQN        " & vbNewLine & _
                    " ,OC_LINE        " & vbNewLine & _
                    " ,DOWNLOAD_CNT   " & vbNewLine & _
                    " ,ODCMT_COMMENT  " & vbNewLine & _
                    " ,OC_TYPE        " & vbNewLine & _
                    ") VALUES ( " & vbNewLine & _
                    "'" & ordernum.PadRight(10) & "'" & vbNewLine & _
                    "," & line & vbNewLine & _
                    ",0,0 " & _
                    ",'" & comment.Trim & "'" & vbNewLine & _
                    ",'L') "
                End If

                cmdRoc.CommandText = sQry
                cmdRoc.Parameters.Clear()

                If cmdRoc.ExecuteNonQuery < 0 Then
                    Throw New Exception("Failed update ADDING COMMENTS.")
                End If
            Catch ex As Exception
                Throw New Exception("DAL_ROC.AddOrderComments(" & ordernum & "," & ordertype & "," & comment & " )->" & vbNewLine & _
                                                "qry:" & sQry & vbNewLine & _
                                                "ERROR:" & ex.Message)
            End Try

        End Sub
        Shared Function getLeSelectOrderDetails(ByVal iLeSelectID As Integer) As DataTable
            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            Dim sQry As String = " Select LE_SELECT_ID  " & vbNewLine & _
                                    ", COD_LINE           " & vbNewLine & _
                                    ", PRICE              " & vbNewLine & _
                                    ", EXTD_PRICE         " & vbNewLine & _
                                    ", TTL_CASES          " & vbNewLine & _
                                    ", TTL_UNITS          " & vbNewLine & _
                                    ", ITEM               " & vbNewLine & _
                                    ", ITEM_DESC          " & vbNewLine & _
                                    ", CATCHALL_DESC      " & vbNewLine & _
                                    ", IS_CATCHALL        " & vbNewLine & _
                                    ", PACKAGING_CODE     " & vbNewLine & _
                                    ", DEPOSIT_PRICE      " & vbNewLine & _
                                    ", QTY                " & vbNewLine & _
                                    ", ITEM_CATEGORY      " & vbNewLine & _
                                    ", ORIG_PRICE         " & vbNewLine & _
                                    ", PRICE_CHG_COMMENT  " & vbNewLine & _
                                    ", RETAIL_PRICE       " & vbNewLine & _
                                    ", ORIG_RETAIL_PRICE  " & vbNewLine & _
                                    ", LAST_UPDATE        " & vbNewLine & _
                                    ", CASE_SZ            " & vbNewLine & _
                                    ", PACK_FACTOR        " & vbNewLine & _
                                    " , 'U' AS ACTION " & vbNewLine & _
                                    " FROM DBO.LCBO_LE_SELECT_ORDER_DTL " & vbNewLine & _
                                    " WHERE LE_SELECT_ID = " & iLeSelectID

            Try

                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("DALROC.getLeSelectOrderDetails(" & iLeSelectID & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Err Msg: " & ex.Message.ToString)
            End Try
            Return dt
        End Function
        Shared Function getLeSelectOrderHeader(ByVal iLeSelectID As Integer) As DataRow
            Dim dt As New DataTable
            Dim drLS As DataRow = Nothing

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            Dim sQry As String = " Select LE_SELECT_ID  " & vbNewLine & _
                                    " , INV_NO          " & vbNewLine & _
                                    " , ORDER_NO        " & vbNewLine & _
                                    " , CR_INV_NO       " & vbNewLine & _
                                    " , CR_ORDER_NO     " & vbNewLine & _
                                    " , TTL_FULL_CASES  " & vbNewLine & _
                                    " , TTL_PART_CASES  " & vbNewLine & _
                                    " , EMP_NO          " & vbNewLine & _
                                    " , STATUS          " & vbNewLine & _
                                    " FROM DBO.LCBO_LE_SELECT_ORDER_HDR " & vbNewLine & _
                                    " WHERE LE_SELECT_ID = " & iLeSelectID

            Try

                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

                If dt.Rows.Count > 0 Then
                    drLS = dt.Rows(0)
                Else
                    Throw New Exception("No records found!")
                End If

            Catch ex As Exception
                Throw New Exception("DALROC.getLeSelectOrderHeader(" & iLeSelectID & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Err Msg: " & ex.Message.ToString)
            End Try

            Return drLS

        End Function

        Shared Function getItemInfo(ByVal sItem As String) As DataRow
            Dim dt As New DataTable
            Dim dr As DataRow

            sItem = Left(sItem & "                    ", 20)

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            Dim SQry As String = " 	SELECT T1.ITEM " & vbNewLine & _
                                 "  ,T1.UNIT_PRICE " & vbNewLine & _
                                 "  ,NVL(T1.CASE_SZ,1) AS CASE_SZ " & vbNewLine & _
                                 "  ,T1.ITEM_CATEGORY " & vbNewLine & _
                                 "  ,T1.IM_DESC " & vbNewLine & _
                                 "  ,T1.PACK_FACTOR " & vbNewLine & _
                                 "  ,T1.PACKAGING_CODE " & vbNewLine & _
                                 "  ,T2.ATTR6 AS DEPOSIT_PRICE " & vbNewLine & _
                                 "  FROM DBO.IMMAS T1 INNER JOIN DBO.ON_CUST_CATALOG T2 " & vbNewLine & _
                                 "  ON T1.ITEM = T2.ITEM AND T1.PACKAGING_CODE = T2.PACKAGING_CODE " & vbNewLine & _
                                 " 	WHERE T1.ITEM = '" & sItem & "'"

            Try

                daROC.SelectCommand.CommandText = SQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

                If dt.Rows.Count > 0 Then
                    dr = dt.Rows(0)
                Else
                    Throw New Exception("No records found!")
                End If

            Catch ex As Exception
                Throw New Exception("DALRoc.getItemInfo('" & sItem & "')->" & ex.Message.ToString)
            End Try

            Return dr
        End Function

        Shared Function getCustInfo(ByVal sCustNo As String) As DataRow
            Dim dt As New DataTable
            Dim dr As DataRow = Nothing

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            Dim SQry As String = " Select T1.CU_NO         " & vbNewLine & _
                                " , PROV_SH            " & vbNewLine & _
                                " , CU_NAME            " & vbNewLine & _
                                " , ADD1_SH            " & vbNewLine & _
                                " , ADD2_SH            " & vbNewLine & _
                                " , CITY_SH            " & vbNewLine & _
                                " , COUNTRY            " & vbNewLine & _
                                " , POST_CODE          " & vbNewLine & _
                                " , PHONE_NU           " & vbNewLine & _
                                " , FAX_NU             " & vbNewLine & _
                                " , CONTACT            " & vbNewLine & _
                                " , EMAIL              " & vbNewLine & _
                                " , CU_TYPE            " & vbNewLine & _
                                " , ACTIVE_FL          " & vbNewLine & _
                                " , CREDIT_AMOUNT      " & vbNewLine & _
                                " , PU_ORDER_FL        " & vbNewLine & _
                                " , NVL((SELECT T2.ATTR_VALUE FROM ON_CUST_ATTR T2 WHERE TRIM(T2.ATTR_ID) = 'CU_ATTR6' AND T1.CU_NO = T2.CU_NO),'INACTIVE') AS STATUS " & vbNewLine & _
                                " from DBO.ON_CUST_MAS T1" & vbNewLine & _
                                " WHERE T1.CU_NO = '" & sCustNo.Trim.PadRight(10) & "'" 
            Try

                daROC.SelectCommand.CommandText = SQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

                If dt.Rows.Count > 0 Then
                    dr = dt.Rows(0)
                    'Else
                    '    Throw New Exception("No records found!")
                End If

            Catch ex As Exception
                Throw New Exception("DALRoc.getCustInfo('" & sCustNo & "')->" & ex.Message.ToString)
            End Try

            Return dr
        End Function
        Shared Function isValidOrigCust(ByVal cu_no As String) As Boolean
            Dim bValid As Boolean = True
            Dim drCust As DataRow = getCustInfo(cu_no)

            bValid = DALRoc.isValidCustomer(cu_no)

            If bValid Then
                Dim dt As DataTable = DALRoc.getLeSelectTransfer("", drCust("CU_NO"), drCust("CU_NAME"), "", "", "", "", "", "", "", "", "", "", "", "")
                If dt.Rows.Count > 0 Then
                    bValid = False
                End If
            End If

            Return bValid

        End Function
        Shared Function getCustomerList(Optional ByVal sCustNo = "") As DataTable
            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            Dim SQry As String = " Select CU_NO         " & vbNewLine & _
                                " , PROV_SH            " & vbNewLine & _
                                " , CU_NAME            " & vbNewLine & _
                                " , ADD1_SH            " & vbNewLine & _
                                " , ADD2_SH            " & vbNewLine & _
                                " , CITY_SH            " & vbNewLine & _
                                " , COUNTRY            " & vbNewLine & _
                                " , POST_CODE          " & vbNewLine & _
                                " , PHONE_NU           " & vbNewLine & _
                                " , FAX_NU             " & vbNewLine & _
                                " , CONTACT            " & vbNewLine & _
                                " , EMAIL              " & vbNewLine & _
                                " , CU_TYPE            " & vbNewLine & _
                                " , ACTIVE_FL          " & vbNewLine & _
                                " , CREDIT_AMOUNT      " & vbNewLine & _
                                " , PU_ORDER_FL        " & vbNewLine & _
                                " from DBO.ON_CUST_MAS " & vbNewLine & _
                                " WHERE ACTIVE_FL = 'Y' " & vbNewLine & _
                                " AND CU_NO LIKE '" & sCustNo & "%' " & vbNewLine & _
                                " ORDER BY LPAD(TRIM(CU_NO),10,'0') ASC "

            Try

                daROC.SelectCommand.CommandText = SQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

                'If dt.Rows.Count <= 0 Then
                '    Throw New Exception("No records found!")
                'End If

            Catch ex As Exception
                Throw New Exception("DALRoc.getCustomerList()->" & ex.Message.ToString)
            End Try

            Return dt
        End Function
        Shared Function getLicenseeList(ByVal sCustNo As String, ByVal sCustName As String) As DataTable
            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                End Try

            End If

            If sCustNo = "" Then
                sCustNo = "%"
            End If

            Dim SQry As String = " Select CU_NO         " & vbNewLine & _
                                " , PROV_SH            " & vbNewLine & _
                                " , CU_NAME            " & vbNewLine & _
                                " , ADD1_SH            " & vbNewLine & _
                                " , ADD2_SH            " & vbNewLine & _
                                " , CITY_SH            " & vbNewLine & _
                                " , COUNTRY            " & vbNewLine & _
                                " , POST_CODE          " & vbNewLine & _
                                " , PHONE_NU           " & vbNewLine & _
                                " , FAX_NU             " & vbNewLine & _
                                " , CONTACT            " & vbNewLine & _
                                " , EMAIL              " & vbNewLine & _
                                " , CU_TYPE            " & vbNewLine & _
                                " , ACTIVE_FL          " & vbNewLine & _
                                " , CREDIT_AMOUNT      " & vbNewLine & _
                                " , PU_ORDER_FL        " & vbNewLine & _
                                " from DBO.ON_CUST_MAS " & vbNewLine & _
                                " WHERE ACTIVE_FL = 'Y' " & vbNewLine & _
                                " AND CU_NO LIKE '" & sCustNo & "%' " & vbNewLine & _
                                " AND CU_NAME LIKE '%" & sCustName & "%'" & vbNewLine & _
                                " AND TRIM(CU_TYPE) = 'LICENSEE' " & vbNewLine & _
                                " ORDER BY LPAD(TRIM(CU_NO),10,'0') ASC "

            Try

                daROC.SelectCommand.CommandText = SQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

                'If dt.Rows.Count <= 0 Then
                '    Throw New Exception("No records found!")
                'End If

            Catch ex As Exception
                Throw New Exception("DALRoc.getCustomerList()->" & ex.Message.ToString)
            End Try

            Return dt
        End Function
        Shared Function getNextLeSelectID() As Integer

            Dim iLeSelectID As Integer = 0

            Dim sQry As String = "SELECT LCBO_LE_SELECT_TRANSFER_SEQ.NEXTVAL FROM DUAL"

            Try
                If cmdRoc.Connection.State <> ConnectionState.Open Then
                    Try
                        cmdRoc.Connection.Open()
                    Catch ex As Exception
                        Throw New OracleNoConnectException("ERROR Connecting to ROC Database." & ex.Message.ToString)
                    End Try
                End If
                cmdRoc.CommandText = sQry
                cmdRoc.Parameters.Clear()

                Dim oraReader As OracleDataReader = cmdRoc.ExecuteReader

                While oraReader.Read
                    iLeSelectID = oraReader("NEXTVAL")
                End While
                oraReader.Close()

            Catch ex As Exception
                Throw New Exception("DALROC.getNextLeSelectID error->" & ex.Message.ToString)
            End Try

            Return iLeSelectID
        End Function
        Shared Function isCatchAllSku(ByVal item As String) As Boolean
            Dim dt As DataTable = getCatchAllSkus()
            Dim bIsCatchall As Boolean = False

            For Each dr As DataRow In dt.Rows
                If item.Trim = dr("ITEM").ToString.Trim Then
                    bIsCatchall = True
                End If
            Next

            Return bIsCatchall

        End Function
        Shared Function getCatchAllSkus() As DataTable
            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database. Abending program." & ex.Message.ToString)
                End Try

            End If


            Dim sQry As String = " Select ITEM      " & vbNewLine & _
                                " , RETAIL_PRICE   " & vbNewLine & _
                                " , PROD_DESC      " & vbNewLine & _
                                " , PROD_SIZE      " & vbNewLine & _
                                " , BOTTLE_DEPOSIT " & vbNewLine & _
                                " from DBO.LCBO_CATCHALL_SKU " & vbNewLine & _
                                " ORDER BY PROD_DESC ASC     "
            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("DALROC.getCatchAllSkus()" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Error msg: " & ex.Message.ToString)
            End Try

            Return dt
        End Function
        Shared Function getLSItemRulesBreakdown(ByVal iLeSelectID As Integer, ByVal sCoInvType As String) As DataTable
            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database. Abending program." & ex.Message.ToString)
                End Try

            End If


            Dim sQry As String = " Select T2.GL_CD                                               " & vbNewLine & _
                                " ,  SUM(T1.RULE_AMT) AS GL_AMOUNT                              " & vbNewLine & _
                                " from DBO.LCBO_LE_SELECT_ORDER_DTL_RULES T1                    " & vbNewLine & _
                                " INNER JOIN DBO.GL_RESOLUTION T2                               " & vbNewLine & _
                                " ON T1.RULE = T2.RULE AND T1.ITEM_CATEGORY = T2.ITEM_CATEGORY  " & vbNewLine & _
                                " WHERE T2.CU_TYPE = 'LICENSEE' AND T2.ORDER_TYPE = 'LIC'       " & vbNewLine & _
                                " AND T1.LE_SELECT_ID = " & iLeSelectID & vbNewLine & _
                                " GROUP BY T2.GL_CD, T1.RULE                                    "
                                  

            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("DALROC.getLSItemRulesBreakdown(" & iLeSelectID & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Error Msg: " & ex.Message.ToString)
            End Try

            Return dt
        End Function
        Shared Function getLSItemRules(ByVal iLeSelectID As Integer, ByVal codline As Integer) As DataTable
            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database. Abending program." & ex.Message.ToString)
                End Try

            End If


            Dim sQry As String = " Select LE_SELECT_ID " & vbNewLine & _
                                    " , COD_LINE          " & vbNewLine & _
                                    " , RULE              " & vbNewLine & _
                                    " , RULE_GR           " & vbNewLine & _
                                    " , RATE              " & vbNewLine & _
                                    " , RATE_CD           " & vbNewLine & _
                                    " , RULE_AMT          " & vbNewLine & _
                                    " , ITEM_CATEGORY     " & vbNewLine & _
                                    " , 'E' as ACTION     " & vbNewLine & _
                                    " from DBO.LCBO_LE_SELECT_ORDER_DTL_RULES " & vbNewLine & _
                                    " WHERE LE_SELECT_ID = " & iLeSelectID & vbNewLine & _
                                    " AND COD_LINE = " & codline & vbNewLine & _
                                    " ORDER BY COD_LINE ASC "

            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("DALROC.getLSItemRules(" & iLeSelectID & "," & codline & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Error Msg: " & ex.Message.ToString)
            End Try

            Return dt
        End Function
        Shared Function getItemCategoryOrderRules(ByVal sItem_Category As String, ByVal sOrder_Type As String) As DataTable
            Dim dt As New DataTable

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database. Abending program." & ex.Message.ToString)
                End Try

            End If

            Dim sQry As String = " Select            " & vbNewLine & _
                                " T1.ITEM_CATEGORY  " & vbNewLine & _
                                " ,T1.RULE          " & vbNewLine & _
                                " ,T2.CU_TYPE       " & vbNewLine & _
                                " ,T2.ORDER_TYPE    " & vbNewLine & _
                                " ,T3.RULE          " & vbNewLine & _
                                " ,T3.RULE_DESC     " & vbNewLine & _
                                " ,T3.RATE          " & vbNewLine & _
                                " ,T3.RATE_CD       " & vbNewLine & _
                                " ,T3.RULE_GR       " & vbNewLine & _
                                " FROM ON_CATEGORY_RULE T1 " & vbNewLine & _
                                " INNER JOIN ON_CUST_ORDER_RULE T2 ON T1.RULE = T2.RULE " & vbNewLine & _
                                " INNER JOIN ON_RULE_MAS T3 ON T1.RULE = T3.RULE        " & vbNewLine & _
                                " WHERE TRIM(ITEM_CATEGORY) = '" & sItem_Category.Trim & "' AND TRIM(ORDER_TYPE) = '" & sOrder_Type.Trim & "'" & vbNewLine & _
                                " AND RATE <> 0 "

            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()
                daROC.Fill(dt)

            Catch ex As Exception
                Throw New Exception("getItemCategoryOrderRules(" & sItem_Category & "," & sOrder_Type & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Error Msg: " & ex.Message.ToString)
            End Try

            Return dt
        End Function
        Public Shared Function getLSGLBreakdown(ByVal iLeSelectID As Integer, _
                                                ByVal sCoInvType As String) As DataTable
            Dim dtItemGLBreakdown As New DataTable
            'Dim dtItemRuleBreakdown As DataTable = DALRoc.getLSItemRulesBreakdown(iLeSelectID, sCoInvType)

            Dim dtGLBreakdown As New DataTable
            dtGLBreakdown.Columns.Add("GL_CD")
            dtGLBreakdown.Columns.Add("GL_AMOUNT", GetType(System.Decimal))

            Dim dNonCommonDepositAmt As Decimal = 0
            Dim dCommonDepositAmt As Decimal = 0

            Dim sNonCommonDepositGL As String = "46102"
            Dim sCommonDepositGL As String = "46101"

            Dim sQry As String = "( SELECT T1.GL_CD                                                         " & vbNewLine & _
                                    " , SUM(t2.QTY * (t2.ORIG_PRICE - t2.DEPOSIT_PRICE)) AS AMOUNT             " & vbNewLine & _
                                    " , t1.RULE                                                                " & vbNewLine & _
                                    " , SUM(t2.QTY * t2.DEPOSIT_PRICE)   AS DEPOSIT                            " & vbNewLine & _
                                    " , t2.ITEM                                                                " & vbNewLine & _
                                    " , t2.ITEM_CATEGORY                                                       " & vbNewLine & _
                                    " FROM GL_RESOLUTION t1, LCBO_LE_SELECT_ORDER_DTL t2                       " & vbNewLine & _
                                    " WHERE t2.LE_SELECT_id = " & iLeSelectID & vbNewLine & _
                                    " AND t2.ORIG_PRICE != t2.PRICE                                            " & vbNewLine & _
                                    " AND t1.ITEM_CATEGORY = t2.ITEM_CATEGORY                                  " & vbNewLine & _
                                    " AND t1.RULE = 'BASE'                                                     " & vbNewLine & _
                                    " AND t1.ORDER_TYPE = 'LIC'                                                " & vbNewLine & _
                                    " GROUP BY T1.GL_CD, t1.RULE, t2.ITEM, t2.ITEM_CATEGORY, t2.PACKAGING_CODE " & vbNewLine & _
                                    " UNION ALL                                                                " & vbNewLine & _
                                    " SELECT T1.GL_CD                                                          " & vbNewLine & _
                                    " , SUM(t2.EXTD_PRICE - t2.QTY * t2.DEPOSIT_PRICE) AS AMOUNT               " & vbNewLine & _
                                    " , t1.RULE                                                                " & vbNewLine & _
                                    " , SUM(t2.QTY * t2.DEPOSIT_PRICE)  AS DEPOSIT                             " & vbNewLine & _
                                    " , t2.ITEM                                                                " & vbNewLine & _
                                    " , t2.ITEM_CATEGORY                                                       " & vbNewLine & _
                                    " FROM GL_RESOLUTION t1, LCBO_LE_SELECT_ORDER_DTL t2                       " & vbNewLine & _
                                    " WHERE t2.LE_SELECT_ID = " & iLeSelectID & vbNewLine & _
                                    " AND t2.ORIG_PRICE = t2.PRICE                                             " & vbNewLine & _
                                    " AND t1.ITEM_CATEGORY = t2.ITEM_CATEGORY                                  " & vbNewLine & _
                                    " AND t1.RULE = 'BASE'                                                     " & vbNewLine & _
                                    " AND t1.ORDER_TYPE = 'LIC'                                                " & vbNewLine & _
                                    " GROUP BY T1.GL_CD, t1.RULE, t2.ITEM, T2.ITEM_CATEGORY, t2.PACKAGING_CODE " & vbNewLine & _
                                    " UNION ALL                                                                " & vbNewLine & _
                                    " SELECT GL_CD                                                             " & vbNewLine & _
                                    " , SUM(t2.RULE_AMT)     AS AMOUNT                                         " & vbNewLine & _
                                    " , t1.RULE                                                                " & vbNewLine & _
                                    " , 0                                                                      " & vbNewLine & _
                                    " , ' '                                                                    " & vbNewLine & _
                                    " , ' '                                                                    " & vbNewLine & _
                                    " FROM GL_RESOLUTION t1, LCBO_LE_SELECT_ORDER_DTL_RULES t2                 " & vbNewLine & _
                                    " WHERE t1.ORDER_TYPE = 'LIC'                                              " & vbNewLine & _
                                    " AND t1.ITEM_CATEGORY = t2.ITEM_CATEGORY                                  " & vbNewLine & _
                                    " AND t1.RULE = t2.RULE                                                    " & vbNewLine & _
                                    " AND t2.LE_SELECT_ID = " & iLeSelectID & vbNewLine & _
                                    " GROUP BY T1.GL_CD, t1.RULE                                               " & vbNewLine & _
                                    " ) ORDER BY 1 ASC     "

            If daROC.SelectCommand.Connection.State <> ConnectionState.Open Then
                Try
                    daROC.SelectCommand.Connection.Open()
                Catch ex As Exception
                    Throw New OracleNoConnectException("ERROR Connecting to ROC Database. Abending program." & ex.Message.ToString)
                End Try

            End If

            Try
                daROC.SelectCommand.CommandText = sQry
                daROC.SelectCommand.Parameters.Clear()

                daROC.Fill(dtItemGLBreakdown)


                'Get distinct columns in table
                Dim dvGLs As DataView = dtItemGLBreakdown.DefaultView
                Dim dtGLs As DataTable = dvGLs.ToTable(True, "GL_CD")
                Dim dGLAmount As Decimal = 0
                Dim objValue As Object = Nothing
                Dim drGLBreakdown As DataRow = Nothing

                For Each gl As DataRow In dtGLs.Rows
                    Console.WriteLine("GL" & gl("GL_CD"))
                    objValue = dtItemGLBreakdown.Compute("SUM(AMOUNT)", "GL_CD='" & gl("GL_CD") & "' AND ITEM_CATEGORY <> 'D9'")
                    If IsDBNull(objValue) = False Then
                        dGLAmount = Convert.ToDecimal(objValue)
                        drGLBreakdown = dtGLBreakdown.NewRow()
                        drGLBreakdown("GL_CD") = gl("GL_CD")
                        drGLBreakdown("GL_AMOUNT") = dGLAmount
                        dtGLBreakdown.Rows.Add(drGLBreakdown)
                    End If
                Next

                'For Each glrule As DataRow In dtItemRuleBreakdown.Rows
                '    drGLBreakdown = dtGLBreakdown.NewRow()
                '    drGLBreakdown("GL_CD") = glrule("GL_CD")
                '    drGLBreakdown("GL_AMOUNT") = glrule("GL_AMOUNT")
                '    dtGLBreakdown.Rows.Add(drGLBreakdown)
                'Next

                'need to add bottle deposit GLs
                For Each drDeposit As DataRow In dtItemGLBreakdown.Select("ITEM <> ' '")
                    If IsDBNull(drDeposit("ITEM_CATEGORY")) = False Then
                        If drDeposit("ITEM_CATEGORY").ToString.Trim = "B" Then
                            dCommonDepositAmt = dCommonDepositAmt + drDeposit("DEPOSIT")
                        Else
                            dNonCommonDepositAmt = dNonCommonDepositAmt + drDeposit("DEPOSIT")
                        End If
                    End If
                Next

                If dCommonDepositAmt <> 0D Then
                    drGLBreakdown = dtGLBreakdown.NewRow()
                    drGLBreakdown("GL_CD") = sCommonDepositGL
                    drGLBreakdown("GL_AMOUNT") = dCommonDepositAmt
                    dtGLBreakdown.Rows.Add(drGLBreakdown)
                End If

                If dNonCommonDepositAmt <> 0D Then
                    drGLBreakdown = dtGLBreakdown.NewRow()
                    drGLBreakdown("GL_CD") = sNonCommonDepositGL
                    drGLBreakdown("GL_AMOUNT") = dNonCommonDepositAmt
                    dtGLBreakdown.Rows.Add(drGLBreakdown)
                End If

                'For Each gl As DataRow In dtGLBreakdown.Rows
                '    Console.WriteLine("GL: " & gl("ORACLE_GL") & " AMOUNT: " & gl("GL_AMOUNT"))
                'Next

                'Now add rules


            Catch ex As Exception
                Throw New Exception("DALRoc.getLSGLBreakdown(" & iLeSelectID & "," & sCoInvType & ")" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Err Msg: " & ex.Message.ToString)
            End Try

            Return dtGLBreakdown

        End Function
        Public Shared Function getGLOrderGLBreakdown(ByVal sOrderNum As String, _
                                                     ByVal sCoInvType As String, _
                                                     ByVal sOrderType As String, _
                                                     ByRef oraCmd As OracleCommand) As DataTable
            Dim dtItemGLBreakdown As New DataTable
            Dim dtGLBreakdown As New DataTable
            dtGLBreakdown.Columns.Add("GL_CD")
            dtGLBreakdown.Columns.Add("GL_AMOUNT", GetType(System.Decimal))

            sOrderNum = Left(sOrderNum & "                ", 16)
            Dim dNonCommonDepositAmt As Decimal = 0
            Dim dCommonDepositAmt As Decimal = 0

            Dim sNonCommonDepositGL As String = "46102"
            Dim sCommonDepositGL As String = "46101"

            Dim sQry As String = "( SELECT T1.GL_CD    " & vbNewLine & _
                            " , SUM(t2.QTY * (t2.ORIG_PRICE - t2.DEPOSIT_PRICE)) AS AMOUNT   " & vbNewLine & _
                            " , t1.RULE                                     " & vbNewLine & _
                            " , SUM(t2.QTY * t2.DEPOSIT_PRICE)   AS DEPOSIT " & vbNewLine & _
                            " , t2.ITEM                                     " & vbNewLine & _
                            " , t2.ITEM_CATEGORY                            " & vbNewLine & _
                            " FROM GL_RESOLUTION t1, ON_INV_CODTL t2   " & vbNewLine & _
                            " WHERE t2.CO_ODNO = '" & sOrderNum & "'        " & vbNewLine & _
                            " AND t2.CO_INV_TYPE = '" & sCoInvType & "'     " & vbNewLine & _
                            " AND t2.ORIG_PRICE != t2.PRICE                 " & vbNewLine & _
                            " AND t1.ITEM_CATEGORY = t2.ITEM_CATEGORY       " & vbNewLine & _
                            " AND t1.RULE = 'BASE'                          " & vbNewLine & _
                            " AND t1.ORDER_TYPE = '" & sOrderType & "'      " & vbNewLine & _
                            " GROUP BY T1.GL_CD, t1.RULE, t2.ITEM, t2.ITEM_CATEGORY, t2.PACKAGING_CODE  " & vbNewLine & _
                            " UNION ALL               " & vbNewLine & _
                            " SELECT T1.GL_CD          " & vbNewLine & _
                            " , SUM(t2.EXTD_PRICE - t2.QTY * t2.DEPOSIT_PRICE) AS AMOUNT  " & vbNewLine & _
                            " , t1.RULE                                    " & vbNewLine & _
                            " , SUM(t2.QTY * t2.DEPOSIT_PRICE)  AS DEPOSIT " & vbNewLine & _
                            " , t2.ITEM                                    " & vbNewLine & _
                            " , t2.ITEM_CATEGORY                           " & vbNewLine & _
                            " FROM GL_RESOLUTION t1, ON_INV_CODTL t2  " & vbNewLine & _
                            " WHERE t2.CO_ODNO = '" & sOrderNum & "'       " & vbNewLine & _
                            " AND t2.CO_INV_TYPE = '" & sCoInvType & "'    " & vbNewLine & _
                            " AND t2.ORIG_PRICE = t2.PRICE                 " & vbNewLine & _
                            " AND t1.ITEM_CATEGORY = t2.ITEM_CATEGORY      " & vbNewLine & _
                            " AND t1.RULE = 'BASE'                         " & vbNewLine & _
                            " AND t1.ORDER_TYPE = '" & sOrderType & "'     " & vbNewLine & _
                            " GROUP BY T1.GL_CD, t1.RULE, t2.ITEM, T2.ITEM_CATEGORY, t2.PACKAGING_CODE  " & vbNewLine & _
                            " UNION ALL                               " & vbNewLine & _
                            " SELECT GL_CD                             " & vbNewLine & _
                            " , SUM(t2.RULE_AMT)     AS AMOUNT        " & vbNewLine & _
                            " , t1.RULE                               " & vbNewLine & _
                            " , 0                                     " & vbNewLine & _
                            " , ' '                                  " & vbNewLine & _
                            " , ' '                                  " & vbNewLine & _
                            " FROM GL_RESOLUTION t1, ON_INV_CODTL_RULE t2  " & vbNewLine & _
                            " WHERE t1.ORDER_TYPE = '" & sOrderType & "'        " & vbNewLine & _
                            " AND t1.ITEM_CATEGORY = t2.ITEM_CATEGORY           " & vbNewLine & _
                            " AND t1.RULE = t2.RULE                             " & vbNewLine & _
                            " AND t2.CO_ODNO = '" & sOrderNum & "'              " & vbNewLine & _
                            " AND t2.CO_INV_TYPE = '" & sCoInvType & "'         " & vbNewLine & _
                            " GROUP BY T1.GL_CD, t1.RULE                   " & vbNewLine & _
                            " UNION ALL                                        " & vbNewLine & _
                            " SELECT T1.DELIVERY_CHG_GL                        " & vbNewLine & _
                            " , SUM(t2.DELIVERY_CHG)     AS AMOUNT             " & vbNewLine & _
                            " , 'DELIVERY_CHG'                                 " & vbNewLine & _
                            " , 0                                              " & vbNewLine & _
                            " , ' '                                            " & vbNewLine & _
                            " , ' '                                            " & vbNewLine & _
                            " FROM ON_CONFIG T1, ON_INV_CO t2                " & vbNewLine & _
                            " WHERE t2.CO_ODNO = '" & sOrderNum & "'           " & vbNewLine & _
                            " AND t2.CO_INV_TYPE = '" & sCoInvType & "'        " & vbNewLine & _
                            " AND t2.DELIVERY_CHG <> 0                         " & vbNewLine & _
                            " GROUP BY T1.DELIVERY_CHG_GL                      " & vbNewLine & _
                            "UNION ALL                                        " & vbNewLine & _
                            " SELECT T1.DELIVERY_TAX2_GL                        " & vbNewLine & _
                            " , SUM(t2.DELIVERY_TAX2)     AS AMOUNT            " & vbNewLine & _
                            " , 'DELIVERY_TAX'                                 " & vbNewLine & _
                            " , 0                                              " & vbNewLine & _
                            " , ' '                                            " & vbNewLine & _
                            " , ' '                                            " & vbNewLine & _
                            " FROM ON_CONFIG t1, ON_INV_CO t2                " & vbNewLine & _
                            " WHERE  t2.CO_ODNO = '" & sOrderNum & "'          " & vbNewLine & _
                            " AND t2.CO_INV_TYPE = '" & sCoInvType & "'        " & vbNewLine & _
                            " AND T2.DELIVERY_TAX2 <> 0                        " & vbNewLine & _
                            " GROUP BY T1.DELIVERY_TAX2_GL                      " & vbNewLine & _
                            " ) ORDER BY 1 ASC                                  "

            Try
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                Dim oraReader As OracleDataReader = oraCmd.ExecuteReader
                dtItemGLBreakdown.Load(oraReader)

                'Get distinct columns in table
                Dim dvGLs As DataView = dtItemGLBreakdown.DefaultView
                Dim dtGLs As DataTable = dvGLs.ToTable(True, "GL_CD")
                Dim dGLAmount As Decimal = 0
                Dim objValue As Object = Nothing
                Dim drGLBreakdown As DataRow = Nothing

                For Each gl As DataRow In dtGLs.Rows
                    Console.WriteLine("GL" & gl("GL_CD"))
                    objValue = dtItemGLBreakdown.Compute("SUM(AMOUNT)", "GL_CD='" & gl("GL_CD") & "' AND ITEM_CATEGORY <> 'D9'")
                    If IsDBNull(objValue) = False Then
                        dGLAmount = Convert.ToDecimal(objValue)
                        drGLBreakdown = dtGLBreakdown.NewRow()
                        drGLBreakdown("GL_CD") = gl("GL_CD")
                        drGLBreakdown("GL_AMOUNT") = dGLAmount
                        dtGLBreakdown.Rows.Add(drGLBreakdown)
                    End If
                Next

                'need to add bottle deposit GLs
                For Each drDeposit As DataRow In dtItemGLBreakdown.Select("ITEM <> ' '")
                    If IsDBNull(drDeposit("ITEM_CATEGORY")) = False Then
                        If drDeposit("ITEM_CATEGORY").ToString.Trim = "B" Then
                            dCommonDepositAmt = dCommonDepositAmt + drDeposit("DEPOSIT")
                        Else
                            dNonCommonDepositAmt = dNonCommonDepositAmt + drDeposit("DEPOSIT")
                        End If
                    End If
                Next

                If dCommonDepositAmt <> 0D Then
                    drGLBreakdown = dtGLBreakdown.NewRow()
                    drGLBreakdown("GL_CD") = sCommonDepositGL
                    drGLBreakdown("GL_AMOUNT") = dCommonDepositAmt
                    dtGLBreakdown.Rows.Add(drGLBreakdown)
                End If

                If dNonCommonDepositAmt <> 0D Then
                    drGLBreakdown = dtGLBreakdown.NewRow()
                    drGLBreakdown("GL_CD") = sNonCommonDepositGL
                    drGLBreakdown("GL_AMOUNT") = dNonCommonDepositAmt
                    dtGLBreakdown.Rows.Add(drGLBreakdown)
                End If

                'For Each gl As DataRow In dtGLBreakdown.Rows
                '    Console.WriteLine("GL: " & gl("ORACLE_GL") & " AMOUNT: " & gl("GL_AMOUNT"))
                'Next

            Catch ex As Exception
                Throw New Exception("DALRoc.getGLOrderGLBreakdown()" & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Err Msg: " & ex.Message.ToString)
            End Try

            Return dtGLBreakdown

        End Function
        Public Shared Sub UpdateInvoiceTransNo(ByVal iInv_no As Integer, ByVal sTransNo As String, ByRef cmdOracle As OracleCommand)

            Dim sQry As String = " UPDATE DBO.ON_INVOICE SET INV_TRANS_NO = '" & sTransNo & "'" & vbNewLine & _
                                 " WHERE INV_NO = " & iInv_no

            Try

                cmdOracle.CommandText = sQry
                cmdOracle.Parameters.Clear()

                If cmdOracle.ExecuteNonQuery < 0 Then
                    Throw New Exception("Failed update on_invoice Record.")
                End If
            Catch ex As Exception
                Throw New Exception("DAL_ROC.UpdateInvoiceTransNo(" & iInv_no & "," & sTransNo & ")->" & vbNewLine & _
                                                "qry:" & sQry & vbNewLine & _
                                                "ERROR:" & ex.Message)
            End Try

        End Sub
        Public Shared Sub UpdateInvoicePostFlag(ByVal sOrderNum As String, ByVal sCoInvType As String, ByRef cmdOracle As OracleCommand)

            Dim sQry As String = " UPDATE DBO.ON_INV_CO SET ACC_POST_DT = SYSDATE " & vbNewLine & _
                                 " WHERE CO_ODNO = '" & sOrderNum & "'" & vbNewLine & _
                                 " AND CO_INV_TYPE = '" & sCoInvType & "'"

            Try

                cmdOracle.CommandText = sQry
                cmdOracle.Parameters.Clear()

                If cmdOracle.ExecuteNonQuery < 0 Then
                    Throw New Exception("Failed update on_inv_co.acc_post_dt.")
                End If
            Catch ex As Exception
                Throw New Exception("DAL_ROC.UpdateInvoicePostFlag(" & sOrderNum & "," & sCoInvType & ")->" & vbNewLine & _
                                                "qry:" & sQry & vbNewLine & _
                                                "ERROR:" & ex.Message)
            End Try

        End Sub

        Public Shared Sub UpdatePaymentPostFlag(ByVal iInv_No As Integer, ByVal iRef_seq As Integer, ByVal sPayTransNo As String, ByRef cmdOracle As OracleCommand)

            Dim sQry As String = " UPDATE DBO.ON_INV_PAY SET ACC_POST_DT = SYSDATE, PAY_TRANS_NO = '" & sPayTransNo.Trim & "' " & vbNewLine & _
                                 " WHERE INV_NO = " & iInv_No & " AND REF_SEQ = " & iRef_seq
            Try

                cmdOracle.CommandText = sQry
                cmdOracle.Parameters.Clear()

                If cmdOracle.ExecuteNonQuery < 0 Then
                    Throw New Exception("Failed update on_inv_pay record.")
                End If
            Catch ex As Exception
                Throw New Exception("DAL_ROC.UpdatePaymentPostFlag(" & iInv_No & "," & iRef_seq & "," & sPayTransNo & ")->" & vbNewLine & _
                                                "qry:" & sQry & vbNewLine & _
                                                "ERROR:" & ex.Message)
            End Try

        End Sub

    End Class
End Namespace
