Imports Microsoft.VisualBasic
Imports System.Collections

Namespace DataLayer
    Public Class REC_ON_GLTRAN
        Implements IComparable

        ''' <summary>
        ''' GL_TRANS_NO,CHAR,6,,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sGL_Trans_No As String
        Public Property gl_trans_no() As String
            Get
                Return sGL_Trans_No
            End Get
            Set(ByVal value As String)
                sGL_Trans_No = value
            End Set
        End Property

        ''' <summary>
        ''' GL_CD,CHAR,10,,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sGL_Cd As String
        Public Property gl_cd() As String
            Get
                Return sGL_Cd
            End Get
            Set(ByVal value As String)
                sGL_Cd = value
            End Set
        End Property

        ''' <summary>
        ''' GL_DEPT,CHAR,3,,N,,
        ''' ALWAYS 941
        ''' </summary>
        ''' <remarks></remarks>
        Private sGL_Dept As String
        Public Property gl_dept() As String
            Get
                Return sGL_Dept
            End Get
            Set(ByVal value As String)
                sGL_Dept = value
            End Set
        End Property

        ''' <summary>
        ''' GL_DATE,DATE,7,,N,,
        ''' date in YYYY-MM-DD format
        ''' </summary>
        ''' <remarks></remarks>
        Private sGL_Date As String
        Public Property gl_date() As String
            Get
                Return sGL_Date
            End Get
            Set(ByVal value As String)
                sGL_Date = value
            End Set
        End Property

        ''' <summary>
        ''' GL_LINK_DATE,CHAR,7,,N,,
        ''' in YYYYDDD format
        ''' </summary>
        ''' <remarks></remarks>
        Private sGL_Link_Date As String
        Public Property gl_link_date() As String
            Get
                Return sGL_Link_Date
            End Get
            Set(ByVal value As String)
                sGL_Link_Date = value
            End Set
        End Property

        ''' <summary>
        ''' GL_REC_NO,CHAR,2,,N,,'00'
        ''' 
        ''' </summary>
        ''' <remarks></remarks>
        Private sGL_Rec_No As String
        Public Property gl_rec_no() As String
            Get
                Return sGL_Rec_No
            End Get
            Set(ByVal value As String)
                sGL_Rec_No = value
            End Set
        End Property

        ''' <summary>
        ''' GL_MEMO,CHAR,20,,Y,,
        ''' Order Number
        ''' </summary>
        ''' <remarks></remarks>
        Private sGL_Memo As String
        Public Property gl_memo() As String
            Get
                Return sGL_Memo
            End Get
            Set(ByVal value As String)
                sGL_Memo = value
            End Set
        End Property

        ''' <summary>
        ''' GL_DR_AMOUNT,NUMBER,14,2,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private dGL_DR_Amount As Decimal
        Public Property gl_dr_amount() As Decimal
            Get
                Return dGL_DR_Amount
            End Get
            Set(ByVal value As Decimal)
                dGL_DR_Amount = value
            End Set
        End Property

        ''' <summary>
        ''' GL_CR_AMOUNT,NUMBER,14,2,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private dGL_CR_Amount As Decimal
        Public Property gl_cr_amount() As Decimal
            Get
                Return dGL_CR_Amount
            End Get
            Set(ByVal value As Decimal)
                dGL_CR_Amount = value
            End Set
        End Property

        ''' <summary>
        ''' GL_USER,CHAR,3,,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sGL_User As String
        Public Property gl_user() As String
            Get
                Return sGL_User
            End Get
            Set(ByVal value As String)
                sGL_User = value
            End Set
        End Property

        Sub New()
            gl_trans_no = ""
            gl_cd = ""
            gl_dept = "941"
            gl_date = ""
            gl_link_date = ""
            gl_rec_no = "00"
            gl_memo = ""
            gl_dr_amount = 0D
            gl_cr_amount = 0D
            gl_user = ""
        End Sub

        Sub SetInfo(ByVal gl As DataRow)
            gl_trans_no = gl("GL_TRANS_NO")
            gl_cd = gl("GL_CD")
            gl_dept = gl("GL_DEPT")
            gl_date = gl("GL_DATE")
            gl_link_date = gl("GL_LINK_DATE")
            gl_rec_no = gl("GL_REC_NO")
            gl_memo = gl("GL_MEMO")
            gl_dr_amount = gl("GL_DR_AMOUNT")
            gl_cr_amount = gl("GL_CR_AMOUNT")
            gl_user = gl("GL_USER")
        End Sub

        Public Function validate() As Boolean
            Dim bValid As Boolean = True
            bValid = bValid And (gl_trans_no.Trim <> "")
            bValid = bValid And (gl_cd.Trim <> "")
            bValid = bValid And (gl_dept.Trim <> "")
            bValid = bValid And (gl_date.Trim <> "")
            bValid = bValid And (gl_link_date.Trim <> "")
            bValid = bValid And (gl_rec_no.Trim <> "")
            bValid = bValid And (gl_memo.Trim <> "")
            'bValid = bValid AndAlso (gl_dr_amount + gl_cr_amount <> 0)
            bValid = bValid AndAlso (gl_user.Trim <> "")

            Dim tmpDate As DateTime
            If bValid AndAlso DateTime.TryParse(gl_date, tmpDate) Then
                gl_date = tmpDate.ToString("yyyy-MM-dd")
            End If

            Return bValid
        End Function

        Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()

            Dim sQry As String = "INSERT INTO DBO.ON_GLTRAN (" & vbNewLine & _
                                 " GL_TRANS_NO " & vbNewLine & _
                                 ",GL_CD " & vbNewLine & _
                                 ",GL_DEPT " & vbNewLine & _
                                 ",GL_DATE " & vbNewLine & _
                                 ",GL_LINK_DATE " & vbNewLine & _
                                 ",GL_REC_NO " & vbNewLine & _
                                 ",GL_MEMO " & vbNewLine & _
                                 ",GL_DR_AMOUNT " & vbNewLine & _
                                 ",GL_CR_AMOUNT " & vbNewLine & _
                                 ",GL_USER " & vbNewLine & _
                                 ") VALUES (" & vbNewLine & _
                                 "'" & gl_trans_no & "'" & vbNewLine & _
                                 ",'" & gl_cd & "'" & vbNewLine & _
                                 ",'" & gl_dept & "'" & vbNewLine & _
                                 ",TO_DATE('" & gl_date & "','YYYY-MM-DD')" & vbNewLine & _
                                 ",'" & gl_link_date & "'" & vbNewLine & _
                                 ",'" & gl_rec_no & "'" & vbNewLine & _
                                 ",'" & gl_memo & "'" & vbNewLine & _
                                 "," & gl_dr_amount & vbNewLine & _
                                 "," & gl_cr_amount & vbNewLine & _
                                 ",'" & gl_user & "'" & vbNewLine & _
                                 ")"

            Try
                If bValid = False Then
                    Throw New Exception("Error validating ON_GLTRAN record")
                End If
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting ON_GLTRAN record query: " & sQry & "!")
                End If

            Catch ex As Exception
                Throw New Exception("ON_GLTRAN.Insert->" & gl_memo & ".Error Message:" & ex.Message.ToString)
            End Try

        End Sub

        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Dim b As REC_ON_GLTRAN = obj
            Return Me.gl_cd.CompareTo(b.gl_cd)
        End Function
    End Class
End Namespace

'GL_TRANS_NO,CHAR,6,,N,,
'GL_CD,CHAR,10,,N,,
'GL_DEPT,CHAR,3,,N,,
'GL_DATE,DATE,7,,N,,
'GL_LINK_DATE,CHAR,7,,N,,
'GL_REC_NO,CHAR,2,,N,,'00'
'GL_MEMO,CHAR,20,,Y,,
'GL_DR_AMOUNT,NUMBER,14,2,N,,
'GL_CR_AMOUNT,NUMBER,14,2,N,,
'GL_USER,CHAR,3,,N,,