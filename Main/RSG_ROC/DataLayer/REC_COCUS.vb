Imports Microsoft.VisualBasic
Namespace DataLayer
    Public Class REC_COCUS

        Private sCo_Odno As String
        Public Property co_odno() As String
            Get
                Return sCo_Odno
            End Get
            Set(ByVal value As String)
                sCo_Odno = value
            End Set
        End Property

        Private sShip_To_Name As String
        Public Property ship_to_name() As String
            Get
                Return sShip_To_Name
            End Get
            Set(ByVal value As String)
                sShip_To_Name = value.Replace("'", "''")
            End Set
        End Property

        Private sShip_To_Add1 As String
        Public Property ship_to_add1() As String
            Get
                Return sShip_To_Add1
            End Get
            Set(ByVal value As String)
                sShip_To_Add1 = value.Replace("'", "''")
            End Set
        End Property


        Private sShip_To_Add2 As String
        Public Property ship_to_add2() As String
            Get
                Return sShip_To_Add2
            End Get
            Set(ByVal value As String)
                sShip_To_Add2 = value.Replace("'", "''")
            End Set
        End Property


        Private sShip_To_City As String
        Public Property ship_to_city() As String
            Get
                Return sShip_To_City
            End Get
            Set(ByVal value As String)
                sShip_To_City = value.Replace("'", "''")
            End Set
        End Property

        Private sShip_To_Prov As String
        Public Property ship_to_prov() As String
            Get
                Return sShip_To_Prov
            End Get
            Set(ByVal value As String)
                sShip_To_Prov = value.Replace("'", "''")
            End Set
        End Property

        'SHIP_TO_POST_CD
        Private sShip_To_Post_Cd As String
        Public Property ship_to_post_cd() As String
            Get
                Return sShip_To_Post_Cd
            End Get
            Set(ByVal value As String)
                sShip_To_Post_Cd = value.Replace("'", "''")
            End Set
        End Property

        Private sShip_To_Phone As String
        Public Property ship_to_phone() As String
            Get
                Return sShip_To_Phone
            End Get
            Set(ByVal value As String)
                sShip_To_Phone = value
            End Set
        End Property

        Private sShip_To_Fax As String
        Public Property ship_to_fax() As String
            Get
                Return sShip_To_Fax
            End Get
            Set(ByVal value As String)
                sShip_To_Fax = value
            End Set
        End Property

        Private sShip_to_Contact As String
        Public Property ship_to_contact() As String
            Get
                Return sShip_to_Contact
            End Get
            Set(ByVal value As String)
                sShip_to_Contact = value.Replace("'", "''")
            End Set
        End Property


        Private sBill_To_Name As String
        Public Property bill_to_name() As String
            Get
                Return sBill_To_Name
            End Get
            Set(ByVal value As String)
                sBill_To_Name = value.Replace("'", "''")
            End Set
        End Property


        Private sBill_To_Add1 As String
        Public Property bill_to_add1() As String
            Get
                Return sBill_To_Add1
            End Get
            Set(ByVal value As String)
                sBill_To_Add1 = value.Replace("'", "''")
            End Set
        End Property


        Private sBill_To_Add2 As String
        Public Property bill_to_add2() As String
            Get
                Return sBill_To_Add2
            End Get
            Set(ByVal value As String)
                sBill_To_Add2 = value.Replace("'", "''")
            End Set
        End Property


        Private sBill_To_City As String
        Public Property bill_to_city() As String
            Get
                Return sBill_To_City
            End Get
            Set(ByVal value As String)
                sBill_To_City = value.Replace("'", "''")
            End Set
        End Property


        Private sBill_To_Prov As String
        Public Property bill_to_prov() As String
            Get
                Return sBill_To_Prov
            End Get
            Set(ByVal value As String)
                sBill_To_Prov = value.Replace("'", "''")
            End Set
        End Property


        Private sBill_To_Post_Cd As String
        Public Property bill_to_post_cd() As String
            Get
                Return sBill_To_Post_Cd
            End Get
            Set(ByVal value As String)
                sBill_To_Post_Cd = value.Replace("'", "''")
            End Set
        End Property


        Private sBill_To_Phone As String
        Public Property bill_to_phone() As String
            Get
                Return sBill_To_Phone
            End Get
            Set(ByVal value As String)
                sBill_To_Phone = value
            End Set
        End Property


        Private sBill_To_Fax As String
        Public Property bill_to_fax() As String
            Get
                Return sBill_To_Fax
            End Get
            Set(ByVal value As String)
                sBill_To_Fax = value
            End Set
        End Property


        Private sShip_To As String
        Public Property ship_to() As String
            Get
                Return sShip_To
            End Get
            Set(ByVal value As String)
                sShip_To = value.Replace("'", "''")
            End Set
        End Property

        Private sShip_Via As String
        Public Property ship_via() As String
            Get
                Return sShip_Via
            End Get
            Set(ByVal value As String)
                sShip_Via = value.Replace("'", "''")
            End Set
        End Property

        'FACING_DEPOT_CODE
        Private sFacing_Depot_Code As String
        Public Property facing_depot_code() As String
            Get
                Return sFacing_Depot_Code
            End Get
            Set(ByVal value As String)
                sFacing_Depot_Code = value.Replace("'", "''")
            End Set
        End Property

        Private sShip_To_Country As String
        Public Property ship_to_country() As String
            Get
                Return sShip_To_Country
            End Get
            Set(ByVal value As String)
                sShip_To_Country = value.Replace("'", "''")
            End Set
        End Property


        Private sBill_To_Country As String
        Public Property bill_to_country() As String
            Get
                Return sBill_To_Country
            End Get
            Set(ByVal value As String)
                sBill_To_Country = value.Replace("'", "''")
            End Set
        End Property


        Private sShip_To_Email As String
        Public Property ship_to_email() As String
            Get
                Return sShip_To_Email
            End Get
            Set(ByVal value As String)
                sShip_To_Email = value.Replace("'", "''")
            End Set
        End Property

        Private sBill_To_Email As String
        Public Property bill_to_email() As String
            Get
                Return sBill_To_Email
            End Get
            Set(ByVal value As String)
                sBill_To_Email = value.Replace("'", "''")
            End Set
        End Property

        Private sBill_To_Contact As String
        Public Property bill_to_contact() As String
            Get
                Return sBill_To_Contact
            End Get
            Set(ByVal value As String)
                sBill_To_Contact = value.Replace("'", "''")
            End Set
        End Property

        Sub New()

        End Sub

        Sub New(ByVal dr As DataRow)
            Me.New()
            SetInfo(dr)
        End Sub

        Sub SetInfo(ByVal dr As DataRow)
            If Not dr Is Nothing Then
                If IsDBNull(dr("co_odno")) = False Then
                    co_odno = dr("co_odno")
                End If
                ship_to_name = dr("ship_to_name")
                ship_to_add1 = dr("ship_to_add1")
                ship_to_add2 = dr("ship_to_add2")
                ship_to_city = dr("ship_to_city")
                ship_to_prov = dr("ship_to_prov")
                ship_to_post_cd = dr("ship_to_post_cd")
                ship_to_phone = dr("ship_to_phone")
                ship_to_fax = dr("ship_to_fax")
                ship_to_contact = dr("ship_to_contact")
                bill_to_name = dr("bill_to_name")
                bill_to_add1 = dr("bill_to_add1")
                bill_to_add2 = dr("bill_to_add2")
                bill_to_city = dr("bill_to_city")
                bill_to_prov = dr("bill_to_prov")
                bill_to_post_cd = dr("bill_to_post_cd")
                bill_to_phone = dr("bill_to_phone")
                bill_to_fax = dr("bill_to_fax")
                ship_to = dr("ship_to")
                ship_via = dr("ship_via")
                facing_depot_code = dr("facing_depot_code")
                ship_to_country = dr("ship_to_country")
                bill_to_country = dr("bill_to_country")
                ship_to_email = dr("ship_to_email")
                bill_to_email = dr("bill_to_email")
                bill_to_contact = dr("bill_to_contact")
            End If
        End Sub

        Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = True

            Dim sQry As String = "INSERT INTO DBO.COCUS (" & vbNewLine & _
                                 " CO_ODNO            " & vbNewLine & _
                                " ,SHIP_TO_NAME      " & vbNewLine & _
                                " ,SHIP_TO_ADD1      " & vbNewLine & _
                                " ,SHIP_TO_ADD2      " & vbNewLine & _
                                " ,SHIP_TO_CITY      " & vbNewLine & _
                                " ,SHIP_TO_PROV      " & vbNewLine & _
                                " ,SHIP_TO_POST_CD   " & vbNewLine & _
                                " ,SHIP_TO_PHONE     " & vbNewLine & _
                                " ,SHIP_TO_FAX       " & vbNewLine & _
                                " ,SHIP_TO_CONTACT   " & vbNewLine & _
                                " ,BILL_TO_NAME      " & vbNewLine & _
                                " ,BILL_TO_ADD1      " & vbNewLine & _
                                " ,BILL_TO_ADD2      " & vbNewLine & _
                                " ,BILL_TO_CITY      " & vbNewLine & _
                                " ,BILL_TO_PROV      " & vbNewLine & _
                                " ,BILL_TO_POST_CD   " & vbNewLine & _
                                " ,BILL_TO_PHONE     " & vbNewLine & _
                                " ,BILL_TO_FAX       " & vbNewLine & _
                                " ,SHIP_TO           " & vbNewLine & _
                                " ,SHIP_VIA          " & vbNewLine & _
                                " ,FACING_DEPOT_CODE " & vbNewLine & _
                                " ,SHIP_TO_COUNTRY   " & vbNewLine & _
                                " ,BILL_TO_COUNTRY   " & vbNewLine & _
                                " ,SHIP_TO_EMAIL     " & vbNewLine & _
                                " ,BILL_TO_EMAIL     " & vbNewLine & _
                                " ,BILL_TO_CONTACT   " & vbNewLine & _
                                ") VALUES ( " & vbNewLine & _
                                " '" & co_odno.Trim & "'" & vbNewLine & _
                                ",'" & ship_to_name.Trim & "'" & vbNewLine & _
                                ",'" & ship_to_add1.Trim & "'" & vbNewLine & _
                                ",'" & ship_to_add2.Trim & "'" & vbNewLine & _
                                ",'" & ship_to_city.Trim & "'" & vbNewLine & _
                                ",'" & ship_to_prov.Trim & "'" & vbNewLine & _
                                ",'" & ship_to_post_cd.Trim & "'" & vbNewLine & _
                                ",'" & ship_to_phone.Trim & "'" & vbNewLine & _
                                ",'" & ship_to_fax.Trim & "'" & vbNewLine & _
                                ",'" & ship_to_contact.Trim & "'" & vbNewLine & _
                                ",'" & bill_to_name.Trim & "'" & vbNewLine & _
                                ",'" & bill_to_add1.Trim & "'" & vbNewLine & _
                                ",'" & bill_to_add2.Trim & "'" & vbNewLine & _
                                ",'" & bill_to_city.Trim & "'" & vbNewLine & _
                                ",'" & bill_to_prov.Trim & "'" & vbNewLine & _
                                ",'" & bill_to_post_cd.Trim & "'" & vbNewLine & _
                                ",'" & bill_to_phone.Trim & "'" & vbNewLine & _
                                ",'" & bill_to_fax.Trim & "'" & vbNewLine & _
                                ",'" & ship_to.Trim & "'" & vbNewLine & _
                                ",'" & ship_via.Trim & "'" & vbNewLine & _
                                ",'" & facing_depot_code.Trim & "'" & vbNewLine & _
                                ",'" & ship_to_country.Trim & "'" & vbNewLine & _
                                ",'" & bill_to_country.Trim & "'" & vbNewLine & _
                                ",'" & ship_to_email.Trim & "'" & vbNewLine & _
                                ",'" & bill_to_email.Trim & "'" & vbNewLine & _
                                ",'" & bill_to_contact.Trim & "'" & vbNewLine & _
                                ")"

            Try
                If bValid = False Then
                    Throw New Exception("Error validating cocus record")
                End If
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting cocus record query: " & sQry & "!")
                End If

            Catch ex As Exception
                Throw New Exception("COCUS.Insert->" & co_odno & "," & ship_to & ".Error Message:" & ex.Message.ToString)
            End Try


        End Sub
    End Class
End Namespace
'CO_ODNO,CHAR,16,,N
'SHIP_TO_NAME,VARCHAR2,36,,N
'SHIP_TO_ADD1,VARCHAR2,36,,Y
'SHIP_TO_ADD2,VARCHAR2,36,,Y
'SHIP_TO_CITY,CHAR,30,,Y
'SHIP_TO_PROV,CHAR,2,,Y
'SHIP_TO_POST_CD,CHAR,10,,Y
'SHIP_TO_PHONE,VARCHAR2,17,,Y
'SHIP_TO_FAX,VARCHAR2,17,,Y
'SHIP_TO_CONTACT,VARCHAR2,41,,Y
'BILL_TO_NAME,VARCHAR2,36,,Y
'BILL_TO_ADD1,VARCHAR2,36,,Y
'BILL_TO_ADD2,VARCHAR2,36,,Y
'BILL_TO_CITY,CHAR,30,,Y
'BILL_TO_PROV,CHAR,2,,Y
'BILL_TO_POST_CD,CHAR,10,,Y
'BILL_TO_PHONE,VARCHAR2,17,,Y
'BILL_TO_FAX,VARCHAR2,17,,Y
'SHIP_TO,VARCHAR2,10,,Y
'SHIP_VIA,VARCHAR2,10,,Y
'FACING_DEPOT_CODE,VARCHAR2,7,,Y
'SHIP_TO_COUNTRY,VARCHAR2,30,,Y
'BILL_TO_COUNTRY,VARCHAR2,30,,Y
'SHIP_TO_EMAIL,VARCHAR2,64,,Y
'BILL_TO_EMAIL,VARCHAR2,64,,Y
'BILL_TO_CONTACT,VARCHAR2,41,,Y