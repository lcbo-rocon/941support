Imports Microsoft.VisualBasic
Imports System.Collections

Namespace DataLayer
    Public Class REC_LCBO_LE_SELECT_ORDER_HDR
        Implements IComparable

        Private iLeSelectID As Integer
        Public Property le_select_id() As Integer
            Get
                Return iLeSelectID
            End Get
            Set(ByVal value As Integer)
                iLeSelectID = value
            End Set
        End Property

        Private iInv_No As Integer
        Public Property inv_no() As Integer
            Get
                Return iInv_No
            End Get
            Set(ByVal value As Integer)
                iInv_No = value
            End Set
        End Property

        Private sOrder_No As String
        Public Property order_no() As String
            Get
                Return sOrder_No
            End Get
            Set(ByVal value As String)
                sOrder_No = value
            End Set
        End Property


        Private iCr_Inv_No As Integer
        Public Property cr_inv_no() As Integer
            Get
                Return iCr_Inv_No
            End Get
            Set(ByVal value As Integer)
                iCr_Inv_No = value
            End Set
        End Property

        Private sCR_Order_No As String
        Public Property cr_order_no() As String
            Get
                Return sCR_Order_No
            End Get
            Set(ByVal value As String)
                sCR_Order_No = value
            End Set
        End Property

        Private iTTL_Full_Cases As Integer
        Public Property ttl_full_cases() As Integer
            Get
                Return iTTL_Full_Cases
            End Get
            Set(ByVal value As Integer)
                iTTL_Full_Cases = value
            End Set
        End Property


        Private iTTL_Part_Cases As Integer
        Public Property ttl_part_cases() As Integer
            Get
                Return iTTL_Part_Cases
            End Get
            Set(ByVal value As Integer)
                iTTL_Part_Cases = value
            End Set
        End Property

        Private sEmp_No As String
        Public Property emp_no() As String
            Get
                Return sEmp_No
            End Get
            Set(ByVal value As String)
                sEmp_No = value
            End Set
        End Property

        Private sStatus As String
        Public Property status() As String
            Get
                Return sStatus
            End Get
            Set(ByVal value As String)
                sStatus = value
            End Set
        End Property

        Sub New()
            le_select_id = 0
            inv_no = 0
            order_no = ""
            cr_inv_no = 0
            cr_order_no = ""
            ttl_full_cases = 0
            ttl_part_cases = 0
            emp_no = ""
            status = ""
        End Sub

        Sub New(ByVal iLe_Select_ID As Integer)
            Me.New()
            Dim dr As DataRow = DALRoc.getLeSelectOrderHeader(iLe_Select_ID)
            SetInfo(dr)
        End Sub

        Sub SetInfo(ByVal dr As DataRow)
            Try
                le_select_id = dr("LE_SELECT_ID")

                If IsDBNull(dr("INV_NO")) = False Then
                    inv_no = dr("INV_NO")
                End If

                If IsDBNull(dr("ORDER_NO")) = False Then
                    order_no = dr("ORDER_NO")
                End If

                If IsDBNull(dr("CR_INV_NO")) = False Then
                    cr_inv_no = dr("CR_INV_NO")
                End If

                If IsDBNull(dr("CR_ORDER_NO")) = False Then
                    cr_order_no = dr("CR_ORDER_NO")
                End If

                If IsDBNull(dr("TTL_FULL_CASES")) = False Then
                    ttl_full_cases = dr("TTL_FULL_CASES")
                End If

                If IsDBNull(dr("TTL_PART_CASES")) = False Then
                    ttl_part_cases = dr("TTL_PART_CASES")
                End If

                If IsDBNull(dr("EMP_NO")) = False Then
                    emp_no = dr("EMP_NO")
                End If

                If IsDBNull(dr("STATUS")) = False Then
                    status = dr("STATUS")
                End If
            Catch ex As Exception

            End Try
        End Sub
        Function validate() As Boolean
            Dim bValid As Boolean = True

            bValid = bValid AndAlso IsNumeric(le_select_id)
            bValid = bValid AndAlso le_select_id <> 0
            bValid = bValid AndAlso emp_no.Trim <> ""
            bValid = bValid AndAlso status.Trim <> ""

            'VALIDATE OTHER FIELDS THAT ARE NOT MANDANTORY
            If bValid = True Then
                If order_no.Trim <> "" Then
                    bValid = bValid AndAlso inv_no <> 0
                    bValid = bValid AndAlso IsNumeric(order_no.Trim)
                End If
                If inv_no <> 0 Then
                    bValid = bValid AndAlso order_no.Trim <> ""
                    bValid = bValid AndAlso IsNumeric(order_no.Trim)
                End If

                If cr_order_no.Trim <> "" Then
                    bValid = bValid AndAlso cr_inv_no <> 0
                    bValid = bValid AndAlso IsNumeric(cr_order_no.Trim)
                End If
                If cr_inv_no <> 0 Then
                    bValid = bValid AndAlso cr_order_no.Trim <> ""
                    bValid = bValid AndAlso IsNumeric(cr_order_no.Trim)
                End If

            End If

            Return bValid
        End Function

        Sub Insert(ByRef oracmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()

            Dim sQry As String = "INSERT INTO DBO.LCBO_LE_SELECT_ORDER_HDR (" & vbNewLine & _
                                 " LE_SELECT_ID        " & vbNewLine & _
                                 " ,TTL_FULL_CASES " & vbNewLine & _
                                 " ,TTL_PART_CASES " & vbNewLine & _
                                 " ,EMP_NO        " & vbNewLine & _
                                 "  ,STATUS  " & vbNewLine & _
                                 " ,LAST_UPDATE " & vbNewLine & _
                                ") values (" & vbNewLine & _
                                le_select_id & vbNewLine & _
                                "," & ttl_full_cases & vbNewLine & _
                                "," & ttl_part_cases & vbNewLine & _
                                " ,'" & emp_no.Trim & "'" & vbNewLine & _
                                " ,'" & status.Trim & "'" & vbNewLine & _
                                " , SYSDATE " & vbNewLine & _
                                ")"

            Try
                If bValid = False Then
                    Throw New Exception("Error validating LCBO_LE_SELECT_ORDER_HDR record")
                End If
                oracmd.CommandText = sQry
                oracmd.Parameters.Clear()

                If oracmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting LCBO_LE_SELECT_ORDER_HDR!")
                End If

                If order_no.Trim <> "" Then
                    sQry = "UPDATE DBO.LCBO_LE_SELECT_ORDER_HDR " & vbNewLine & _
                           " SET ORDER_NO = '" & order_no.Trim & "'" & _
                           ",INV_NO = " & inv_no & vbNewLine & _
                           ",LAST_UPDATE = SYSDATE " & vbNewLine & _
                           " WHERE LE_SELECT_ID = " & le_select_id

                    oracmd.CommandText = sQry
                    oracmd.Parameters.Clear()

                    If oracmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error UPDATING LCBO_LE_SELECT_ORDER_HDR ORDER_NO " & order_no.Trim)
                    End If
                End If

                If cr_order_no.Trim <> "" Then
                    sQry = "UPDATE DBO.LCBO_LE_SELECT_ORDER_HDR " & vbNewLine & _
                           " SET CR_ORDER_NO = '" & cr_order_no.Trim & "'" & _
                           " ,CR_INV_NO = " & cr_inv_no & vbNewLine & _
                           " ,LAST_UPDATE = SYSDATE " & vbNewLine & _
                           " WHERE LE_SELECT_ID = " & le_select_id

                    oracmd.CommandText = sQry
                    oracmd.Parameters.Clear()

                    If oracmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error UPDATING LCBO_LE_SELECT_ORDER_HDR CR_ORDER_NO " & cr_order_no.Trim)
                    End If
                End If

            Catch ex As Exception
                Throw New Exception("LCBO_LE_SELECT_ORDER_HDR.Insert->" & le_select_id & vbNewLine & _
                                    "Qry: " & sQry & vbNewLine & _
                                    "Error Message:" & ex.Message.ToString)
            End Try

        End Sub

        Sub Update(ByRef oracmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()

            Dim sQry As String = "UPDATE DBO.LCBO_LE_SELECT_ORDER_HDR " & vbNewLine & _
                                 " set TTL_FULL_CASES = " & ttl_full_cases & vbNewLine & _
                                 " ,TTL_PART_CASES = " & ttl_part_cases & vbNewLine & _
                                 " ,EMP_NO = '" & emp_no.Trim & "'  " & vbNewLine & _
                                 "  ,STATUS  = '" & status.Trim & "' " & vbNewLine & _
                                 " ,LAST_UPDATE = SYSDATE " & vbNewLine & _
                               " WHERE LE_SELECT_ID = " & le_select_id
            Try
                If bValid = False Then
                    Throw New Exception("Error validating LCBO_LE_SELECT_TRANSFER record")
                End If
                oracmd.CommandText = sQry
                oracmd.Parameters.Clear()

                If oracmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting LCBO_LE_SELECT_TRANSFER!")
                End If

                If order_no.Trim <> "" Then
                    sQry = "UPDATE DBO.LCBO_LE_SELECT_ORDER_HDR " & vbNewLine & _
                           " SET ORDER_NO = '" & order_no.Trim & "'" & _
                           ",INV_NO = " & inv_no & vbNewLine & _
                           ",LAST_UPDATE = SYSDATE " & vbNewLine & _
                           " WHERE LE_SELECT_ID = " & le_select_id

                    oracmd.CommandText = sQry
                    oracmd.Parameters.Clear()

                    If oracmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error UPDATING LCBO_LE_SELECT_ORDER_HDR ORDER_NO " & order_no)
                    End If
                End If

                If cr_order_no.Trim <> "" Then
                    sQry = "UPDATE DBO.LCBO_LE_SELECT_ORDER_HDR " & vbNewLine & _
                           " SET CR_ORDER_NO = '" & cr_order_no.Trim & "'" & _
                           " ,CR_INV_NO = " & cr_inv_no & vbNewLine & _
                           " ,LAST_UPDATE = SYSDATE " & vbNewLine & _
                           " WHERE LE_SELECT_ID = " & le_select_id

                    oracmd.CommandText = sQry
                    oracmd.Parameters.Clear()

                    If oracmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error UPDATING LCBO_LE_SELECT_ORDER_HDR CR_ORDER_NO " & cr_order_no)
                    End If
                End If

            Catch ex As Exception
                Throw New Exception("LCBO_LE_SELECT_TRANSFER.Insert->" & le_select_id & vbNewLine & _
                                    "Error Message:" & ex.Message.ToString)
            End Try

        End Sub


        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Dim b As REC_LCBO_LE_SELECT_ORDER_HDR = obj
            Return Me.le_select_id.CompareTo(b.le_select_id)
        End Function
    End Class
End Namespace
