Imports Microsoft.VisualBasic

Namespace DataLayer
    Public Class REC_ON_INV_PAY

        Private iInv_no As Integer
        Public Property inv_no() As Integer
            Get
                Return iInv_no
            End Get
            Set(ByVal value As Integer)
                iInv_no = value
            End Set
        End Property

        ''' <summary>
        ''' Pay sequence
        ''' </summary>
        ''' <remarks></remarks>
        Private iRef_Seq As Integer
        Public Property ref_seq() As Integer
            Get
                Return iRef_Seq
            End Get
            Set(ByVal value As Integer)
                iRef_Seq = value
            End Set
        End Property


        Private sRef_No As String
        Public Property ref_no() As String
            Get
                Return sRef_No
            End Get
            Set(ByVal value As String)
                sRef_No = value
            End Set
        End Property

        'timestamp is automatic so no need to populate


        Private sEmp_No As String
        Public Property emp_no() As String
            Get
                Return sEmp_No
            End Get
            Set(ByVal value As String)
                sEmp_No = value
            End Set
        End Property


        Private iTender_Cd As Integer
        Public Property tender_cd() As Integer
            Get
                Return iTender_Cd
            End Get
            Set(ByVal value As Integer)
                iTender_Cd = value
            End Set
        End Property

        Private sTender_Type As String
        Public Property tender_type() As String
            Get
                Return sTender_Type
            End Get
            Set(ByVal value As String)
                sTender_Type = value
            End Set
        End Property


        Private dPay_Amt As Decimal
        Public Property pay_amt() As Decimal
            Get
                Return dPay_Amt
            End Get
            Set(ByVal value As Decimal)
                dPay_Amt = value
            End Set
        End Property


        Private sCredit_Card_No As String
        Public Property credit_card_no() As String
            Get
                Return sCredit_Card_No
            End Get
            Set(ByVal value As String)
                sCredit_Card_No = value
            End Set
        End Property


        Private sCredit_Auth As String
        Public Property credit_auth() As String
            Get
                Return sCredit_Auth
            End Get
            Set(ByVal value As String)
                sCredit_Auth = value
            End Set
        End Property


        Private sPay_Comment As String
        Public Property pay_comment() As String
            Get
                Return sPay_Comment
            End Get
            Set(ByVal value As String)
                sPay_Comment = value
            End Set
        End Property

        Private sAcc_Post_Dt As String
        Public Property acc_post_dt() As String
            Get
                Return sAcc_Post_Dt
            End Get
            Set(ByVal value As String)
                sAcc_Post_Dt = value
            End Set
        End Property
        ''' <summary>
        ''' Numeric sequence number indicating when the payment was entered.
        ''' Starting at 1. Used for muliple tenders to show how many times payment was entered
        ''' 
        ''' Example: day 1 user enters two tenders in system. then the two tenders link_ref_seq will be 1
        ''' day 2 user enters 1 more tender in system. then the tender link_ref_seq will be 2
        ''' 
        ''' </summary>
        ''' <remarks></remarks>
        Private iLink_Ref_Seq As Integer
        Public Property link_ref_seq() As Integer
            Get
                Return iLink_Ref_Seq
            End Get
            Set(ByVal value As Integer)
                iLink_Ref_Seq = value
            End Set
        End Property

        ''' <summary>
        ''' Date the payment was written to the T5 in YYYY-MM-DD HH:MI:SS format
        ''' </summary>
        ''' <remarks></remarks>
        Private sUpload_dt As String
        Public Property upload_dt() As String
            Get
                Return sUpload_dt
            End Get
            Set(ByVal value As String)
                sUpload_dt = value
            End Set
        End Property

        ''' <summary>
        ''' gl/ar trans no for the payment
        ''' </summary>
        ''' <remarks></remarks>
        Private sPay_Trans_No As String
        Public Property pay_trans_no() As String
            Get
                Return sPay_Trans_No
            End Get
            Set(ByVal value As String)
                sPay_Trans_No = value
            End Set
        End Property

        ''' <summary>
        ''' Date payment is received. in YYYY-MM-DD HH24:MI:SS format
        ''' </summary>
        ''' <remarks></remarks>
        Private dtPay_Dt As DateTime
        Public Property pay_dt() As DateTime
            Get
                Return dtPay_Dt
            End Get
            Set(ByVal value As DateTime)
                dtPay_Dt = value
            End Set
        End Property

        Sub New()
            inv_no = 0
            ref_seq = 0
            ref_no = ""
            emp_no = ""
            tender_cd = -1
            tender_type = ""
            pay_amt = 0D
            credit_card_no = ""
            credit_auth = ""
            pay_comment = ""
            acc_post_dt = ""
            link_ref_seq = -1
            upload_dt = ""
            pay_trans_no = ""
            pay_dt = New DateTime
        End Sub

        Sub SetInfo(ByVal dr As DataRow)
            inv_no = dr("INV_NO")
            ref_seq = dr("REF_SEQ")
            ref_no = dr("REF_NO").ToString.Trim
            emp_no = dr("EMP_NO").ToString.Trim
            tender_cd = dr("TENDER_CD")
            tender_type = dr("TENDER_TYPE").ToString.Trim
            pay_amt = dr("PAY_AMT")
            credit_card_no = dr("CREDIT_CARD_NO").ToString.Trim
            credit_auth = dr("CREDIT_AUTH").ToString.Trim
            pay_comment = dr("PAY_COMMENT").ToString.Trim
            acc_post_dt = dr("ACC_POST_DT").ToString.Trim
            link_ref_seq = dr("LINK_REF_SEQ")
            upload_dt = dr("UPLOAD_DT").ToString.Trim
            pay_trans_no = dr("PAY_TRANS_NO").ToString.Trim
            pay_dt = dr("PAY_DT")
        End Sub

        Public Function validate() As Boolean
            Dim bValid As Boolean = True

            bValid = (inv_no > 0)
            bValid = bValid AndAlso (ref_seq > 0)
            bValid = bValid AndAlso (emp_no.Trim <> "")
            bValid = bValid AndAlso (tender_type.Trim <> "")
            bValid = bValid AndAlso (pay_amt <> 0)
            bValid = bValid AndAlso (link_ref_seq >= 0)

            Return bValid
        End Function

        Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()

            Dim sQry As String = "INSERT INTO DBO.ON_INV_PAY (" & vbNewLine & _
                                 "INV_NO " & vbNewLine & _
                                 ",REF_SEQ " & vbNewLine & _
                                 ",REF_NO " & vbNewLine & _
                                 ",EMP_NO " & vbNewLine & _
                                 ",TENDER_CD" & vbNewLine & _
                                 ",TENDER_TYPE " & vbNewLine & _
                                 ",PAY_AMT " & vbNewLine & _
                                 ",CREDIT_CARD_NO " & vbNewLine & _
                                 ",CREDIT_AUTH " & vbNewLine & _
                                 ",PAY_COMMENT " & vbNewLine & _
                                 ",LINK_REF_SEQ " & vbNewLine & _
                                  ",PAY_DT " & vbNewLine & _
                                 ") VALUES (" & vbNewLine & _
                                 inv_no & vbNewLine & _
                                 "," & ref_seq & vbNewLine & _
                                 ",'" & ref_no & "'" & vbNewLine & _
                                 ",'" & emp_no & "'" & vbNewLine & _
                                 "," & tender_cd & vbNewLine & _
                                 ",'" & tender_type & "'" & vbNewLine & _
                                 "," & pay_amt & vbNewLine & _
                                 ",'" & credit_card_no & "'" & vbNewLine & _
                                 ",'" & credit_auth & "'" & vbNewLine & _
                                 ",'" & pay_comment & "'" & vbNewLine & _
                                 ",'" & link_ref_seq & "'" & vbNewLine & _
                                 ",TO_DATE('" & pay_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS') " & vbNewLine & _
                                 ")"

            Try
                If bValid = False Then
                    Throw New Exception("Error validating ON_INV_PAY record")
                End If
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting ON_INV_PAY record query: " & sQry & "!")
                End If

                If acc_post_dt <> "" Then
                    sQry = "UPDATE DBO.ON_INV_PAY SET ACC_POST_DT = TO_DATE('" & acc_post_dt & "','YYYY-MM-DD HH24:MI:SS') " & vbNewLine & _
                           " WHERE INV_NO =" & inv_no & " AND REF_SEQ = " & ref_seq
                    oraCmd.CommandText = sQry
                    oraCmd.Parameters.Clear()
                    If oraCmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error UPDATING ON_INV_PAY.ACC_POST_DT record query: " & sQry & "!")
                    End If

                End If


                If upload_dt <> "" Then
                    sQry = "UPDATE DBO.ON_INV_PAY SET UPLOAD_DT = TO_DATE('" & upload_dt & "','YYYY-MM-DD HH24:MI:SS') " & vbNewLine & _
                           " WHERE INV_NO =" & inv_no & " AND REF_SEQ = " & ref_seq
                    oraCmd.CommandText = sQry
                    oraCmd.Parameters.Clear()
                    If oraCmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error UPDATING ON_INV_PAY.ACC_POST_DT record query: " & sQry & "!")
                    End If

                End If

            Catch ex As Exception
                Throw New Exception("ON_INV_PAY.Insert->" & inv_no & ".Error Message:" & ex.Message.ToString)
            End Try

        End Sub
    End Class
End Namespace
'INV_NO,NUMBER,10,,N
'REF_SEQ,NUMBER,10,,N
'REF_NO,CHAR,20,,Y
'TIMESTAMP,DATE,7,,N
'EMP_NO,CHAR,11,,N
'TENDER_CD,NUMBER,10,,N
'TENDER_TYPE,CHAR,16,,Y
'PAY_AMT,NUMBER,14,2,N
'CREDIT_CARD_NO,VARCHAR2,32,,Y
'CREDIT_AUTH,VARCHAR2,16,,Y
'PAY_COMMENT,VARCHAR2,32,,Y
'ACC_POST_DT,DATE,7,,Y
'LINK_REF_SEQ,NUMBER,10,,N
'UPLOAD_DT,DATE,7,,Y
'PAY_TRANS_NO,CHAR,6,,Y
'PAY_DT,DATE,7,,Y