Imports System.Data
Imports System.Collections

Namespace DataLayer
    Public Class REC_CRHDR
        Implements IComparable

        ''' <summary>
        ''' CR_ODNO,CHAR,16,,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sCR_Odno As String
        Public Property cr_odno() As String
            Get
                Return sCR_Odno
            End Get
            Set(ByVal value As String)
                sCR_Odno = value
            End Set
        End Property

        ''' <summary>
        ''' I = Closed
        ''' O = Open
        ''' </summary>
        ''' <remarks></remarks>
        Private sCR_Status As String
        Public Property cr_status() As String
            Get
                Return sCR_Status
            End Get
            Set(ByVal value As String)
                sCR_Status = value
            End Set
        End Property

        ''' <summary>
        ''' CU_NO,CHAR,10,,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sCu_No As String
        Public Property cu_no() As String
            Get
                Return sCu_No
            End Get
            Set(ByVal value As String)
                sCu_No = value
            End Set
        End Property

        ''' <summary>
        ''' ORIG_ODNO,CHAR,16,,Y,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sOrig_Odno As String
        Public Property orig_odno() As String
            Get
                Return sOrig_Odno
            End Get
            Set(ByVal value As String)
                sOrig_Odno = value
            End Set
        End Property

        ''' <summary>
        ''' date in YYYY-MM-DD HH24:MI:SS format
        ''' </summary>
        ''' <remarks></remarks>
        Private dtOrder_dt_tm As DateTime
        Public Property order_dt_tm() As DateTime
            Get
                Return dtOrder_dt_tm
            End Get
            Set(ByVal value As DateTime)
                dtOrder_dt_tm = value
            End Set
        End Property

        'HOST_DOWNLOAD_Y_OR_N,CHAR,1,,Y,,

        ''' <summary>
        ''' RETURN_CODE,CHAR,6,,Y,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sReturn_Code As String
        Public Property return_code() As String
            Get
                Return sReturn_Code
            End Get
            Set(ByVal value As String)
                sReturn_Code = value
            End Set
        End Property

        'MESSAGE,VARCHAR2,20,,Y,,

        ''' <summary>
        ''' EM_NO,CHAR,11,,Y,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sEm_No As String
        Public Property em_no() As String
            Get
                Return sEm_No
            End Get
            Set(ByVal value As String)
                sEm_No = value
            End Set
        End Property

        'CONT_LABEL_ID,CHAR,20,,Y,,
        'PRIOR_NAME,CHAR,6,,Y,,
        'INFO,CHAR,4,,Y,,
        'CARRIER_CD,VARCHAR2,15,,Y,,

        ''' <summary>
        ''' PAY_METHOD,VARCHAR2,16,,Y,,
        ''' can be null. airmiles?
        ''' </summary>
        ''' <remarks></remarks>
        Private sPay_Method As String
        Public Property pay_method() As String
            Get
                Return sPay_Method
            End Get
            Set(ByVal value As String)
                sPay_Method = value
            End Set
        End Property

        'FREIGHT_AMT,NUMBER,14,2,Y,,
        'WAYBILL_NO,VARCHAR2,20,,Y,,

        ''' <summary>
        ''' REF_NO,VARCHAR2,25,,Y,,
        ''' SOP Number
        ''' </summary>
        ''' <remarks></remarks>
        Private sREF_No As String
        Public Property ref_no() As String
            Get
                Return sREF_No
            End Get
            Set(ByVal value As String)
                sREF_No = value
            End Set
        End Property

        'CLI_NO,CHAR,10,,Y,,

        ''' <summary>
        ''' FREIGHT_FL,CHAR,1,,Y,,
        ''' Y = delivery charge for return
        ''' N = no charge on delivery
        ''' </summary>
        ''' <remarks></remarks>
        Private sFreight_Fl As String
        Public Property freight_fl() As String
            Get
                Return sFreight_Fl
            End Get
            Set(ByVal value As String)
                sFreight_Fl = value
            End Set
        End Property


        ''' <summary>
        ''' ORDER_TYPE,CHAR,3,,Y,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sOrder_Type As String
        Public Property order_type() As String
            Get
                Return sOrder_Type
            End Get
            Set(ByVal value As String)
                sOrder_Type = value
            End Set
        End Property

        'DELIVERY_FL,CHAR,1,,Y,,

        Private sDelivery_Fl As String
        Public Property delivery_fl() As String
            Get
                Return sDelivery_Fl
            End Get
            Set(ByVal value As String)
                sDelivery_Fl = value
            End Set
        End Property

        Sub New()
            cr_odno = ""
            cr_status = ""
            cu_no = ""
            orig_odno = ""
            order_dt_tm = New DateTime
            return_code = ""
            em_no = ""
            pay_method = ""
            ref_no = ""
            freight_fl = "N"
            order_type = ""
            delivery_fl = "N"
        End Sub

        Sub SetInfo(ByVal dr As DataRow)
            cr_odno = dr("CR_ODNO").ToString.Trim
            cr_status = dr("CR_STATUS").ToString.Trim
            cu_no = dr("CU_NO").ToString.Trim
            orig_odno = dr("ORIG_ODNO").ToString.Trim
            order_dt_tm = dr("ORDER_DT_TM").ToString.Trim
            return_code = dr("RETURN_CODE").ToString.Trim
            em_no = dr("EM_NO").ToString.Trim
            pay_method = dr("PAY_METHOD").ToString.Trim
            ref_no = dr("REF_NO").ToString.Trim
            freight_fl = dr("FREIGHT_FL").ToString.Trim
            order_type = dr("ORDER_TYPE").ToString.Trim
            delivery_fl = dr("DELIVERY_FL").ToString.Trim
        End Sub

        Public Function validate() As Boolean
            Dim bValid As Boolean = True
            bValid = (cr_odno.Trim <> "")
            bValid = bValid AndAlso (cr_status.Trim <> "")
            bValid = bValid AndAlso (cu_no.Trim <> "")
            bValid = bValid AndAlso (em_no.Trim <> "")
            bValid = bValid AndAlso (order_type.Trim <> "")

            Dim tmpDate As DateTime
            If bValid AndAlso DateTime.TryParse(order_dt_tm, tmpDate) = True Then
                order_dt_tm = tmpDate.ToString("yyyy-MM-dd HH:mm:ss")
            End If

            Return bValid
        End Function
        Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()

            Dim sQry As String = "INSERT INTO DBO.CRHDR (" & vbNewLine & _
                                 " CR_ODNO " & vbNewLine & _
                                 ",CR_STATUS " & vbNewLine & _
                                 ",CU_NO " & vbNewLine & _
                                 ",ORIG_ODNO " & vbNewLine & _
                                 ",ORDER_DT_TM " & vbNewLine & _
                                 ",RETURN_CODE " & vbNewLine & _
                                 ",EM_NO " & vbNewLine & _
                                 ",PAY_METHOD " & vbNewLine & _
                                 ",REF_NO " & vbNewLine & _
                                 ",FREIGHT_FL " & vbNewLine & _
                                 ",ORDER_TYPE " & vbNewLine & _
                                 ",DELIVERY_FL " & vbNewLine & _
                                 ") VALUES (" & vbNewLine & _
                                 "'" & cr_odno & "'" & vbNewLine & _
                                 ",'" & cr_status & "'" & vbNewLine & _
                                 ",'" & cu_no & "'" & vbNewLine & _
                                 ",'" & orig_odno & "'" & vbNewLine & _
                                 ",TO_DATE('" & order_dt_tm.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS')" & vbNewLine & _
                                 ",'" & return_code & "'" & vbNewLine & _
                                 ",'" & em_no & "'" & vbNewLine & _
                                 ",'" & pay_method & "'" & vbNewLine & _
                                 ",'" & ref_no & "'" & vbNewLine & _
                                 ",'" & freight_fl & "'" & vbNewLine & _
                                 ",'" & order_type & "'" & vbNewLine & _
                                 ",'" & delivery_fl & "'" & vbNewLine & _
                                 ")"

            Try
                If bValid = False Then
                    Throw New Exception("Error validating CRHDR record")
                End If
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting CRHDR record query: " & sQry & "!")
                End If

            Catch ex As Exception
                Throw New Exception("CRHDR.Insert->" & cr_odno & ".Error Message:" & ex.Message.ToString)
            End Try

        End Sub
        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Dim b As REC_CRHDR = obj
            Return Me.cr_odno.CompareTo(b.cr_odno)
        End Function
    End Class
End Namespace
'CR_ODNO,CHAR,16,,N,,
'CR_STATUS,CHAR,1,,N,,'O'
'CU_NO,CHAR,10,,N,,
'ORIG_ODNO,CHAR,16,,Y,,
'ORDER_DT_TM,DATE,7,,N,,
'TIMESTAMP,DATE,7,,Y,,
'HOST_DOWNLOAD_Y_OR_N,CHAR,1,,Y,,
'RETURN_CODE,CHAR,6,,Y,,
'MESSAGE,VARCHAR2,20,,Y,,
'EM_NO,CHAR,11,,Y,,
'CONT_LABEL_ID,CHAR,20,,Y,,
'PRIOR_NAME,CHAR,6,,Y,,
'INFO,CHAR,4,,Y,,
'CARRIER_CD,VARCHAR2,15,,Y,,
'PAY_METHOD,VARCHAR2,16,,Y,,
'FREIGHT_AMT,NUMBER,14,2,Y,,
'WAYBILL_NO,VARCHAR2,20,,Y,,
'REF_NO,VARCHAR2,25,,Y,,
'CLI_NO,CHAR,10,,Y,,
'FREIGHT_FL,CHAR,1,,Y,,
'ORDER_TYPE,CHAR,3,,Y,,
'DELIVERY_FL,CHAR,1,,Y,,
