Imports Microsoft.VisualBasic
Imports System.Collections

Namespace DataLayer
    Public Class REC_LCBO_LE_SELECT_TRANSFER
        Implements IComparable

        Private iLeSelectID As Integer
        Public Property le_select_id() As Integer
            Get
                Return iLeSelectID
            End Get
            Set(ByVal value As Integer)
                iLeSelectID = value
            End Set
        End Property

        Private sOrigCustomerNo As String
        Public Property orig_customer_no() As String
            Get
                Return sOrigCustomerNo
            End Get
            Set(ByVal value As String)
                sOrigCustomerNo = value
            End Set
        End Property

        Private sOrigCustname As String
        Public Property orig_cust_name() As String
            Get
                Return sOrigCustname
            End Get
            Set(ByVal value As String)
                sOrigCustname = value
            End Set
        End Property


        Private sOrigCustAddress As String
        Public Property orig_cust_address() As String
            Get
                Return sOrigCustAddress
            End Get
            Set(ByVal value As String)
                sOrigCustAddress = value
            End Set
        End Property


        Private sOrigCustCity As String
        Public Property orig_cust_city() As String
            Get
                Return sOrigCustCity
            End Get
            Set(ByVal value As String)
                sOrigCustCity = value
            End Set
        End Property

        Private sOrigCustPostCode As String
        Public Property orig_cust_post_code() As String
            Get
                Return sOrigCustPostCode
            End Get
            Set(ByVal value As String)
                sOrigCustPostCode = value
            End Set
        End Property


        Private sOrigCustProv As String
        Public Property orig_cust_prov() As String
            Get
                Return sOrigCustProv
            End Get
            Set(ByVal value As String)
                sOrigCustProv = value
            End Set
        End Property


        Private sOrigCustPhone As String
        Public Property orig_cust_phone() As String
            Get
                Return sOrigCustPhone
            End Get
            Set(ByVal value As String)
                sOrigCustPhone = value
            End Set
        End Property


        Private sNewCustomerNo As String
        Public Property new_customer_no() As String
            Get
                Return sNewCustomerNo
            End Get
            Set(ByVal value As String)
                sNewCustomerNo = value
            End Set
        End Property


        Private sNewCustName As String
        Public Property new_cust_name() As String
            Get
                Return sNewCustName
            End Get
            Set(ByVal value As String)
                sNewCustName = value
            End Set
        End Property

        Private sNewCustAddress As String
        Public Property new_cust_address() As String
            Get
                Return sNewCustAddress
            End Get
            Set(ByVal value As String)
                sNewCustAddress = value
            End Set
        End Property


        Private sNewCustCity As String
        Public Property new_cust_city() As String
            Get
                Return sNewCustCity
            End Get
            Set(ByVal value As String)
                sNewCustCity = value
            End Set
        End Property

        Private sNewCustPostCode As String
        Public Property new_cust_post_code() As String
            Get
                Return sNewCustPostCode
            End Get
            Set(ByVal value As String)
                sNewCustPostCode = value
            End Set
        End Property

        Private sNewCustProv As String
        Public Property new_cust_prov() As String
            Get
                Return sNewCustProv
            End Get
            Set(ByVal value As String)
                sNewCustProv = value
            End Set
        End Property

        Private dtCreationDt As DateTime
        Public Property creation_dt() As DateTime
            Get
                Return dtCreationDt
            End Get
            Set(ByVal value As DateTime)
                dtCreationDt = value
            End Set
        End Property

        Private dtExpectedTransferDt As DateTime
        Public Property expected_transfer_dt() As DateTime
            Get
                Return dtExpectedTransferDt
            End Get
            Set(ByVal value As DateTime)
                dtExpectedTransferDt = value
            End Set
        End Property

        Private sLeSelectContactName As String
        Public Property le_select_contact_name() As String
            Get
                Return sLeSelectContactName
            End Get
            Set(ByVal value As String)
                sLeSelectContactName = value
            End Set
        End Property

        Private sLeSelectCorporateEmail As String
        Public Property le_select_corporate_email() As String
            Get
                Return sLeSelectCorporateEmail
            End Get
            Set(ByVal value As String)
                sLeSelectCorporateEmail = value
            End Set
        End Property


        Private sLeSelectReturnNo As String
        Public Property le_select_return_no() As String
            Get
                Return sLeSelectReturnNo
            End Get
            Set(ByVal value As String)
                sLeSelectReturnNo = value
            End Set
        End Property


        Private sLeSelectOrderNo As String
        Public Property le_select_order_no() As String
            Get
                Return sLeSelectOrderNo
            End Get
            Set(ByVal value As String)
                sLeSelectOrderNo = value
            End Set
        End Property

        Private sPreparedBy As String
        Public Property prepared_by() As String
            Get
                Return sPreparedBy
            End Get
            Set(ByVal value As String)
                sPreparedBy = value
            End Set
        End Property

        Private sAuthorizedBy As String
        Public Property authorized_by() As String
            Get
                Return sAuthorizedBy
            End Get
            Set(ByVal value As String)
                sAuthorizedBy = value
            End Set
        End Property

        Private sComments As String
        Public Property comments() As String
            Get
                Return sComments
            End Get
            Set(ByVal value As String)
                sComments = value
            End Set
        End Property

        Private sStatus As String
        Public Property status() As String
            Get
                Return sStatus
            End Get
            Set(ByVal value As String)
                sStatus = value
            End Set
        End Property
        Sub New()
            le_select_id = 0
            orig_customer_no = ""
            orig_cust_name = ""
            orig_cust_address = ""
            orig_cust_city = ""
            orig_cust_post_code = ""
            orig_cust_prov = ""
            new_customer_no = ""
            new_cust_name = ""
            new_cust_address = ""
            new_cust_city = ""
            new_cust_post_code = ""
            new_cust_prov = ""
            creation_dt = New DateTime
            expected_transfer_dt = New DateTime
            le_select_contact_name = ""
            le_select_corporate_email = ""
            le_select_return_no = ""
            le_select_order_no = ""
            prepared_by = ""
            authorized_by = ""
            comments = ""
            status = "NEW"
        End Sub

        Sub New(ByVal iLe_Select_ID As Integer)
            Me.New()
            Dim dr As DataRow = DALRoc.getLeSelectTransfer(iLe_Select_ID)
            SetInfo(dr)
        End Sub

        Sub SetInfo(ByVal dr As DataRow)
            Try
                le_select_id = dr("LE_SELECT_ID")
                If IsDBNull(dr("ORIG_CUSTOMER_NO")) = False Then
                    orig_customer_no = dr("ORIG_CUSTOMER_NO")
                End If

                If IsDBNull(dr("ORIG_CUST_NAME")) = False Then
                    orig_cust_name = dr("ORIG_CUST_NAME")
                End If

                If IsDBNull(dr("ORIG_CUST_ADDRESS")) = False Then
                    orig_cust_address = dr("ORIG_CUST_ADDRESS")
                End If

                If IsDBNull(dr("ORIG_CUST_CITY")) = False Then
                    orig_cust_city = dr("ORIG_CUST_CITY")
                End If

                If IsDBNull(dr("ORIG_CUST_POST_CODE")) = False Then
                    orig_cust_post_code = dr("ORIG_CUST_POST_CODE")
                End If

                If IsDBNull(dr("ORIG_CUST_PROV")) = False Then
                    orig_cust_prov = dr("ORIG_CUST_PROV")
                End If

                If IsDBNull(dr("ORIG_CUST_PHONE")) = False Then
                    orig_cust_phone = dr("ORIG_CUST_PHONE")
                End If

                If IsDBNull(dr("NEW_CUSTOMER_NO")) = False Then
                    new_customer_no = dr("NEW_CUSTOMER_NO")
                End If

                If IsDBNull(dr("NEW_CUST_NAME")) = False Then
                    new_cust_name = dr("NEW_CUST_NAME")
                End If

                If IsDBNull(dr("NEW_CUST_ADDRESS")) = False Then
                    new_cust_address = dr("NEW_CUST_ADDRESS")
                End If

                If IsDBNull(dr("NEW_CUST_CITY")) = False Then
                    new_cust_city = dr("NEW_CUST_CITY")
                End If

                If IsDBNull(dr("NEW_CUST_POST_CODE")) = False Then
                    new_cust_post_code = dr("NEW_CUST_POST_CODE")
                End If

                If IsDBNull(dr("NEW_CUST_PROV")) = False Then
                    new_cust_prov = dr("NEW_CUST_PROV")
                End If

                If IsDBNull(dr("CREATION_DT")) = False Then
                    creation_dt = dr("CREATION_DT")
                End If

                If IsDBNull(dr("EXPECTED_TRANSFER_DT")) = False Then
                    expected_transfer_dt = dr("EXPECTED_TRANSFER_DT")
                End If

                If IsDBNull(dr("LE_SELECT_CONTACT_NAME")) = False Then
                    le_select_contact_name = dr("LE_SELECT_CONTACT_NAME")
                End If

                If IsDBNull(dr("LE_SELECT_CORPORATE_EMAIL")) = False Then
                    le_select_corporate_email = dr("LE_SELECT_CORPORATE_EMAIL")
                End If

                If IsDBNull(dr("LE_SELECT_RETURN_NO")) = False Then
                    le_select_return_no = dr("LE_SELECT_RETURN_NO")
                End If

                If IsDBNull(dr("LE_SELECT_ORDER_NO")) = False Then
                    le_select_order_no = dr("LE_SELECT_ORDER_NO")
                End If

                If IsDBNull(dr("PREPARED_BY")) = False Then
                    prepared_by = dr("PREPARED_BY")
                End If

                If IsDBNull(dr("AUTHORIZED_BY")) = False Then
                    authorized_by = dr("AUTHORIZED_BY")
                End If

                If IsDBNull(dr("COMMENTS")) = False Then
                    comments = dr("COMMENTS")
                End If

                If IsDBNull(dr("STATUS")) = False Then
                    status = dr("STATUS")
                End If
            Catch ex As Exception
                Throw New Exception("REC_LCBO_LE_SELECT_TRANSFER.SetInfo(dr)->" & ex.Message.ToString)
            End Try

        End Sub

        Function validate() As Boolean
            Dim bValid As Boolean = True

            'bValid = bValid AndAlso IsNumeric(le_select_id)
            'bValid = bValid AndAlso le_select_id <> 0
            bValid = bValid AndAlso orig_customer_no.Trim <> ""
            bValid = bValid AndAlso IsNumeric(orig_customer_no.Trim)
            bValid = bValid AndAlso orig_cust_name.Trim <> ""
            bValid = bValid AndAlso le_select_contact_name.Trim <> ""
            bValid = bValid AndAlso le_select_corporate_email.Trim <> ""
            bValid = bValid AndAlso prepared_by.Trim <> ""
            bValid = bValid AndAlso status.Trim <> ""

            'VALIDATE OTHER FIELDS THAT ARE NOT MANDANTORY
            If bValid = True Then
                If new_customer_no.Trim <> "" Then
                    bValid = bValid And IsNumeric(new_customer_no.Trim)
                End If

                '    bValid = bValid And expected_transfer_dt >= Now

                If le_select_return_no.Trim <> "" Then
                    bValid = bValid And IsNumeric(le_select_return_no.Trim)
                End If

                If le_select_order_no.Trim <> "" Then
                    bValid = bValid And IsNumeric(le_select_order_no.Trim)
                End If

            End If

            Return bValid
        End Function

        Sub Insert(ByRef oracmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()
            Me.le_select_id = DALRoc.getNextLeSelectID

            Dim sQry As String = "INSERT INTO DBO.LCBO_LE_SELECT_TRANSFER (" & vbNewLine & _
                                 " LE_SELECT_ID        " & vbNewLine & _
                                 " , ORIG_CUSTOMER_NO          " & vbNewLine & _
                                " , ORIG_CUST_NAME            " & vbNewLine & _
                                " , ORIG_CUST_ADDRESS         " & vbNewLine & _
                                " , ORIG_CUST_CITY            " & vbNewLine & _
                                " , ORIG_CUST_POST_CODE       " & vbNewLine & _
                                " , ORIG_CUST_PROV            " & vbNewLine & _
                                " , ORIG_CUST_PHONE           " & vbNewLine & _
                                " , NEW_CUSTOMER_NO           " & vbNewLine & _
                                " , NEW_CUST_NAME             " & vbNewLine & _
                                " , NEW_CUST_ADDRESS          " & vbNewLine & _
                                " , NEW_CUST_CITY             " & vbNewLine & _
                                " , NEW_CUST_POST_CODE        " & vbNewLine & _
                                " , NEW_CUST_PROV             " & vbNewLine & _
                                " , CREATION_DT               " & vbNewLine & _
                                " , LE_SELECT_CONTACT_NAME    " & vbNewLine & _
                                " , LE_SELECT_CORPORATE_EMAIL " & vbNewLine & _
                                " , LE_SELECT_RETURN_NO       " & vbNewLine & _
                                " , LE_SELECT_ORDER_NO        " & vbNewLine & _
                                " , PREPARED_BY               " & vbNewLine & _
                                " , AUTHORIZED_BY             " & vbNewLine & _
                                " , COMMENTS                  " & vbNewLine & _
                                " , STATUS                    " & vbNewLine & _
                                " , LAST_UPDATE               " & vbNewLine & _
                                ") values (" & vbNewLine & _
                                le_select_id & vbNewLine & _
                                " ,'" & orig_customer_no.Trim & "'" & vbNewLine & _
                                " ,'" & orig_cust_name.Trim.Replace("'", "''") & "'" & vbNewLine & _
                                " ,'" & orig_cust_address.Trim.Replace("'", "''") & "'" & vbNewLine & _
                                " ,'" & orig_cust_city.Trim.Replace("'", "''") & "'" & vbNewLine & _
                                " ,'" & orig_cust_post_code.Trim & "'" & vbNewLine & _
                                " ,'" & orig_cust_prov.Trim & "'" & vbNewLine & _
                                " ,'" & orig_cust_phone.Trim & "'" & vbNewLine & _
                                " ,'" & new_customer_no.Trim & "'" & vbNewLine & _
                                " ,'" & new_cust_name.Trim.Replace("'", "''") & "'" & vbNewLine & _
                                " ,'" & new_cust_address.Trim.Replace("'", "''") & "'" & vbNewLine & _
                                " ,'" & new_cust_city.Trim.Replace("'", "''") & "'" & vbNewLine & _
                                " ,'" & new_cust_post_code.Trim & "'" & vbNewLine & _
                                " ,'" & new_cust_prov.Trim & "'" & vbNewLine & _
                                " ,SYSDATE " & vbNewLine & _
                                " ,'" & le_select_contact_name.Trim.Replace("'", "''") & "'" & vbNewLine & _
                                " ,'" & le_select_corporate_email.Trim.Replace("'", "''") & "'" & vbNewLine & _
                                " ,'" & le_select_return_no.Trim & "'" & vbNewLine & _
                                " ,'" & le_select_order_no.Trim & "'" & vbNewLine & _
                                " ,'" & prepared_by.Trim & "'" & vbNewLine & _
                                " ,'" & authorized_by & "'" & vbNewLine & _
                                " ,'" & comments & "'" & vbNewLine & _
                                " ,'" & status & "'" & vbNewLine & _
                                " , SYSDATE " & vbNewLine & _
                                ")"

            Try
                If bValid = False Then
                    Throw New Exception("Error validating LCBO_LE_SELECT_TRANSFER record")
                End If
                oracmd.CommandText = sQry
                oracmd.Parameters.Clear()

                If oracmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting LCBO_LE_SELECT_TRANSFER!")
                End If

                'GET THE LE_SELECT_ID

                If expected_transfer_dt.ToString("yyyy") > "2000" Then
                    sQry = "UPDATE DBO.LCBO_LE_SELECT_TRANSFER " & vbNewLine & _
                           " SET EXPECTED_TRANSFER_DT = TO_DATE('" & expected_transfer_dt.ToString("yyyyMMdd") & "','YYYYMMDD') " & _
                           ", LAST_UPDATE = SYSDATE " & vbNewLine & _
                           " WHERE LE_SELECT_ID = " & le_select_id

                    oracmd.CommandText = sQry
                    oracmd.Parameters.Clear()

                    If oracmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error UPDATING LCBO_LE_SELECT_TRANSFER NEW_CUSTOMER_NO " & new_customer_no)
                    End If
                End If
            Catch ex As Exception
                Throw New Exception("LCBO_LE_SELECT_TRANSFER.Insert->" & le_select_id & "," & orig_customer_no & vbNewLine & _
                                    ".Error Message:" & ex.Message.ToString)
            End Try

        End Sub

        Sub UPDATE(ByRef oracmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()

            Dim sQry As String = "UPDATE DBO.LCBO_LE_SELECT_TRANSFER " & vbNewLine & _
                                 "SET  ORIG_CUSTOMER_NO = '" & orig_customer_no.Trim & "'" & vbNewLine & _
                                " ,ORIG_CUST_NAME = '" & orig_cust_name.Trim.Replace("'", "''") & "'" & vbNewLine & _
                                " , NEW_CUSTOMER_NO = '" & new_customer_no.Trim & "'" & vbNewLine & _
                                " , NEW_CUST_NAME = '" & new_cust_name.Trim.Replace("'", "''") & "'" & vbNewLine & _
                                " , NEW_CUST_ADDRESS = '" & new_cust_address.Trim.Replace("'", "''") & "'" & vbNewLine & _
                                ", NEW_CUST_CITY = '" & new_cust_city.Trim.Replace("'", "''") & "'" & vbNewLine & _
                                ", NEW_CUST_POST_CODE = '" & new_cust_post_code.Trim & "'" & vbNewLine & _
                                ", NEW_CUST_PROV = '" & new_cust_prov & "'" & vbNewLine & _
                                " ,LE_SELECT_CONTACT_NAME = '" & le_select_contact_name.Trim.Replace("'", "''") & "'" & vbNewLine & _
                                " ,LE_SELECT_CORPORATE_EMAIL = '" & le_select_corporate_email.Trim.Replace("'", "''") & "'" & vbNewLine & _
                                " ,PREPARED_BY = '" & prepared_by & "'" & vbNewLine & _
                                " ,COMMENTS = '" & comments.Trim & "'" & vbNewLine & _
                                " ,STATUS  =  '" & status & "'" & vbNewLine & _
                                 ", LAST_UPDATE = SYSDATE " & vbNewLine & _
                                 " WHERE LE_SELECT_ID = " & le_select_id

            Try
                If bValid = False Then
                    Throw New Exception("Error validating LCBO_LE_SELECT_TRANSFER record")
                End If

                oracmd.CommandText = sQry
                oracmd.Parameters.Clear()

                If oracmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Updating LCBO_LE_SELECT_TRANSFER!")
                End If


                If le_select_return_no.Trim <> "" Then
                    sQry = "UPDATE DBO.LCBO_LE_SELECT_TRANSFER " & vbNewLine & _
                           " SET LE_SELECT_RETURN_NO = '" & le_select_return_no.Trim & "'" & _
                            ", LAST_UPDATE = SYSDATE " & vbNewLine & _
                           " WHERE LE_SELECT_ID = " & le_select_id

                    oracmd.CommandText = sQry
                    oracmd.Parameters.Clear()

                    If oracmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error UPDATING LCBO_LE_SELECT_TRANSFER LE_SELECT_RETURN_NO " & le_select_return_no)
                    End If
                End If

                If le_select_order_no.Trim <> "" Then
                    sQry = "UPDATE DBO.LCBO_LE_SELECT_TRANSFER " & vbNewLine & _
                           " SET LE_SELECT_ORDER_NO = '" & le_select_order_no.Trim & "'" & _
                            ", LAST_UPDATE = SYSDATE " & vbNewLine & _
                           " WHERE LE_SELECT_ID = " & le_select_id

                    oracmd.CommandText = sQry
                    oracmd.Parameters.Clear()

                    If oracmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error UPDATING LCBO_LE_SELECT_TRANSFER LE_SELECT_ORDER_NO " & le_select_order_no)
                    End If
                End If

                If authorized_by.Trim <> "" Then
                    sQry = "UPDATE DBO.LCBO_LE_SELECT_TRANSFER " & vbNewLine & _
                           " SET AUTHORIZED_BY = '" & authorized_by.Trim & "'" & _
                            ", LAST_UPDATE = SYSDATE " & vbNewLine & _
                           " WHERE LE_SELECT_ID = " & le_select_id

                    oracmd.CommandText = sQry
                    oracmd.Parameters.Clear()

                    If oracmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error UPDATING LCBO_LE_SELECT_TRANSFER AUTHORIZED_BY " & authorized_by)
                    End If
                End If

                If expected_transfer_dt.Year > 2000 Then
                    sQry = "UPDATE DBO.LCBO_LE_SELECT_TRANSFER " & vbNewLine & _
                                               " SET EXPECTED_TRANSFER_DT = TO_DATE('" & expected_transfer_dt.ToString("yyyy-MM-dd") & "','YYYY-MM-DD') " & vbNewLine & _
                                                ", LAST_UPDATE = SYSDATE " & vbNewLine & _
                                               " WHERE LE_SELECT_ID = " & le_select_id

                    oracmd.CommandText = sQry
                    oracmd.Parameters.Clear()

                    If oracmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error UPDATING LCBO_LE_SELECT_TRANSFER expected transfer date " & expected_transfer_dt)
                    End If
                End If

            Catch ex As Exception
                Throw New Exception("LCBO_LE_SELECT_TRANSFER.Insert->" & le_select_id & "," & orig_customer_no & vbNewLine & _
                                    "QRY: " & sQry & vbNewLine & _
                                    "Error Message:" & ex.Message.ToString)
            End Try

        End Sub

        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Dim b As REC_LCBO_LE_SELECT_TRANSFER = obj
            Return Me.le_select_id.CompareTo(b.le_select_id)
        End Function
    End Class
End Namespace
'LE_SELECT_ID	NUMBER	8		Y
'ORIG_CUSTOMER_NO	CHAR	10		Y
'ORIG_CUST_NAME	VARCHAR2	36		Y
'NEW_CUSTOMER_NO	CHAR	10		Y
'NEW_CUST_NAME	VARCHAR2	36		Y
'CREATION_DT	DATE	7		Y
'EXPECTED_TRANSFER_DT	DATE	7		Y
'LE_SELECT_CONTACT_NAME	VARCHAR2	41		Y
'LE_SELECT_CORPORATE_EMAIL	VARCHAR2	50		Y
'LE_SELECT_RETURN_NO	CHAR	16		Y
'LE_SELECT_ORDER_NO	CHAR	16		Y
'PREPARED_BY	CHAR	11		Y
'AUTHORIZED_BY	CHAR	11		Y
'COMMENTS	VARCHAR2	255		Y
'STATUS	VARCHAR2	10		Y