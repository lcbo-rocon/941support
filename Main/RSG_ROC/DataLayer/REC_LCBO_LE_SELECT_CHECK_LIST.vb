Imports Microsoft.VisualBasic
Imports System.Collections
Imports RSG_ROC.DataLayer

Namespace DataLayer
    Public Class REC_LCBO_LE_SELECT_CHECK_LIST
        Implements IComparable


        Private iTaskNum As Integer
        Public Property task_id() As Integer
            Get
                Return iTaskNum
            End Get
            Set(ByVal value As Integer)
                iTaskNum = value
            End Set
        End Property

        Private iLeSelectID As Integer
        Public Property le_select_id() As Integer
            Get
                Return iLeSelectID
            End Get
            Set(ByVal value As Integer)
                iLeSelectID = value
            End Set
        End Property

        Private iCheck_ID As Integer
        Public Property check_id() As Integer
            Get
                Return iCheck_ID
            End Get
            Set(ByVal value As Integer)
                iCheck_ID = value
            End Set
        End Property

        Private sCheckSequence As String
        Public Property check_sequence() As String
            Get
                Return sCheckSequence
            End Get
            Set(ByVal value As String)
                sCheckSequence = value
            End Set
        End Property

        Private iCheck_ID_Hdr As Integer
        Public Property check_id_hdr() As Integer
            Get
                Return iCheck_ID_Hdr
            End Get
            Set(ByVal value As Integer)
                iCheck_ID_Hdr = value
            End Set
        End Property


        Private sIsCheckHeader As String
        Public Property is_check_hdr() As String
            Get
                Return sIsCheckHeader
            End Get
            Set(ByVal value As String)
                sIsCheckHeader = value
            End Set
        End Property


        Private sCheck_Desc As String
        Public Property check_desc() As String
            Get
                Return sCheck_Desc
            End Get
            Set(ByVal value As String)
                sCheck_Desc = value
            End Set
        End Property

        Private sChecked As String
        Public Property checked() As String
            Get
                Return sChecked
            End Get
            Set(ByVal value As String)
                sChecked = value
            End Set
        End Property

        Private dtCheck_dt As DateTime
        Public Property check_dt() As DateTime
            Get
                Return dtCheck_dt
            End Get
            Set(ByVal value As DateTime)
                dtCheck_dt = value
            End Set
        End Property

        Private sComments As String
        Public Property comments() As String
            Get
                Return sComments
            End Get
            Set(ByVal value As String)
                sComments = value
            End Set
        End Property


        Private sAutomatedCheck As String
        Public Property automated_check() As String
            Get
                Return sAutomatedCheck
            End Get
            Set(ByVal value As String)
                sAutomatedCheck = value
            End Set
        End Property


        Private sAction As String
        Public Property action() As String
            Get
                Return sAction
            End Get
            Set(ByVal value As String)
                sAction = value
            End Set
        End Property


        Sub New()
            task_id = 0
            le_select_id = 0
            check_id = 0
            check_sequence = ""
            check_id_hdr = 0
            is_check_hdr = "Y"
            check_desc = ""
            checked = "N"
            check_dt = New DateTime
            comments = ""
            automated_check = "N"
            action = "A"
        End Sub

        Sub New(ByVal dr As DataRow)
            Me.New()
            SetInfo(dr)
        End Sub

        Sub SetInfo(ByVal dr As DataRow)
            Try

                le_select_id = dr("LE_SELECT_ID")
                check_id = dr("CHECK_ID")
                check_id_hdr = dr("CHECK_ID_HDR")
                is_check_hdr = dr("IS_CHECK_HDR")
                checked = dr("CHECKED")

                If IsDBNull(dr("CHECK_DESC")) = False Then
                    check_desc = dr("CHECK_DESC")
                End If

                If IsDBNull(dr("CHECK_DT")) = False Then
                    check_dt = DateTime.Parse(dr("CHECK_DT"))
                End If

                If IsDBNull(dr("COMMENTS")) = False Then
                    comments = dr("COMMENTS")
                End If

                If IsDBNull(dr("AUTOMATED_CHECK")) = False Then
                    automated_check = dr("AUTOMATED_CHECK")
                Else
                    automated_check = "N"
                End If

                If IsDBNull(dr("ACTION")) = False Then
                    action = dr("ACTION")
                Else
                    action = "N"
                End If


            Catch ex As Exception
                Throw New Exception("LE_SELECT_CHECK_LIST.SETINFO(DR)->" & ex.Message.ToString)
            End Try
        End Sub
        Function validate() As Boolean
            Dim bValid As Boolean = True

            bValid = bValid AndAlso IsNumeric(le_select_id)
            bValid = bValid AndAlso le_select_id <> 0
            bValid = bValid AndAlso check_id <> 0
            bValid = bValid AndAlso checked.Trim <> ""

            Return bValid
        End Function

        Sub UPDATE(ByRef oracmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()

            Dim sQry As String = ""

            If action = "A" Then
                sQry = "INSERT INTO DBO.LCBO_LE_SELECT_CHECK_LIST (" & vbNewLine & _
                                     " LE_SELECT_ID        " & vbNewLine & _
                                     " ,CHECK_ID " & vbNewLine & _
                                     " ,CHECKED " & vbNewLine & _
                                     " ,COMMENTS        " & vbNewLine & _
                                     ") values (" & vbNewLine & _
                                    le_select_id & vbNewLine & _
                                    "," & check_id & vbNewLine & _
                                    ",'" & checked & "'" & vbNewLine & _
                                    " ,'" & comments.Trim & "'" & vbNewLine & _
                                    ")"
            ElseIf action = "U" Then
                sQry = "UPDATE DBO.LCBO_LE_SELECT_CHECK_LIST " & vbNewLine & _
                       " SET CHECKED = '" & checked & "'" & vbNewLine & _
                       " ,COMMENTS = '" & comments & "'" & vbNewLine & _
                       " WHERE LE_SELECT_ID = " & le_select_id & vbNewLine & _
                       " AND CHECK_ID = " & check_id

            ElseIf action = "D" Then
                sQry = "DELETE FROM DBO.LCBO_LE_SELECT_CHECK_LIST " & vbNewLine & _
                      " WHERE LE_SELECT_ID = " & le_select_id & vbNewLine & _
                      " AND CHECK_ID = " & check_id

            Else
                Throw New Exception("LCBO_LE_SELECT_CHECK_LIST.Update ->" & le_select_id & "," & check_id & vbNewLine & _
                                    "Error Message: " & action & " is unknown action. expected ('A','U','D')")
            End If

            Try
                If bValid = False Then
                    Throw New Exception("Error validating LCBO_LE_SELECT_CHECK_LIST record")
                End If
                oracmd.CommandText = sQry
                oracmd.Parameters.Clear()

                If oracmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting LCBO_LE_SELECT_CHECK_LIST!")
                End If


                If check_dt.Year > 2000 Then
                    sQry = "UPDATE DBO.LCBO_LE_SELECT_CHECK_LIST " & vbNewLine & _
                           " SET CHECK_DT = TO_DATE('" & check_dt.ToString("yyyyMMddHHmmss") & "','YYYYMMDDHH24MISS')" & _
                          " WHERE LE_SELECT_ID = " & le_select_id & vbNewLine & _
                          " AND CHECK_ID = " & check_id

                    oracmd.CommandText = sQry
                    oracmd.Parameters.Clear()

                    If oracmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error UPDATING LCBO_LE_SELECT_CHECK_LIST CHECK_DT " & check_dt.ToString("yyyy-MM-dd HH:mm:ss"))
                    End If
                End If

            Catch ex As Exception
                Throw New Exception("LCBO_LE_SELECT_CHECK_LIST.UPDATE->" & le_select_id & "," & check_id & vbNewLine & _
                                    "QRY: " & sQry & vbNewLine & _
                                    "Error Message:" & ex.Message.ToString)
            End Try

        End Sub

        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            Dim objB As REC_LCBO_LE_SELECT_CHECK_LIST = obj
            Dim a As String = Right("00000000" & le_select_id, 8) & Right("000" & check_id, 3)
            Dim b As String = Right("00000000" & objB.le_select_id, 8) & Right("000" & objB.check_id, 3)
            Return a.CompareTo(b)
        End Function
    End Class
End Namespace