Imports Microsoft.VisualBasic

Namespace DataLayer
    Public Class REC_ON_ARTRAN
        ''' <summary>
        ''' AR_TRANS_NO,CHAR,6,,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sAr_Trans_No As String
        Public Property ar_trans_no() As String
            Get
                Return sAr_Trans_No
            End Get
            Set(ByVal value As String)
                sAr_Trans_No = value
            End Set
        End Property

        ''' <summary>
        ''' AR_DATE,DATE,7,,N,,
        ''' date in YYYY-MM-DD format
        ''' </summary>
        ''' <remarks></remarks>
        Private sAr_Date As String
        Public Property ar_date() As String
            Get
                Return sAr_Date
            End Get
            Set(ByVal value As String)
                sAr_Date = value
            End Set
        End Property

        ''' <summary>
        ''' AR_LINK_DATE,CHAR,7,,N,,
        ''' Link ardate in YYYYDDD format
        ''' </summary>
        ''' <remarks></remarks>
        Private sAr_Link_Date As String
        Public Property ar_link_date() As String
            Get
                Return sAr_Link_Date
            End Get
            Set(ByVal value As String)
                sAr_Link_Date = value
            End Set
        End Property

        ''' <summary>
        ''' AR_CUST_NO,CHAR,10,,Y,,
        ''' customer number
        ''' </summary>
        ''' <remarks></remarks>
        Private sAR_Cust_No As String
        Public Property ar_cust_no() As String
            Get
                Return sAR_Cust_No
            End Get
            Set(ByVal value As String)
                sAR_Cust_No = value
            End Set
        End Property

        ''' <summary>
        ''' AR_REC_NO,CHAR,2,,Y,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sAR_Rec_No As String
        Public Property ar_rec_no() As String
            Get
                Return sAR_Rec_No
            End Get
            Set(ByVal value As String)
                sAR_Rec_No = value
            End Set
        End Property

        ''' <summary>
        ''' AR_REF_NO,CHAR,6,,Y,,
        ''' The order number
        ''' </summary>
        ''' <remarks></remarks>
        Private sAR_REF_NO As String
        Public Property ar_ref_no() As String
            Get
                Return sAR_REF_NO
            End Get
            Set(ByVal value As String)
                sAR_REF_NO = value
            End Set
        End Property

        ''' <summary>
        ''' AR_DR_AMOUNT,NUMBER,14,2,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private dAR_DR_Amount As Decimal
        Public Property ar_dr_amount() As Decimal
            Get
                Return dAR_DR_Amount
            End Get
            Set(ByVal value As Decimal)
                dAR_DR_Amount = value
            End Set
        End Property

        ''' <summary>
        ''' AR_CR_AMOUNT,NUMBER,14,2,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private dAR_CR_Amount As Decimal
        Public Property ar_cr_amount() As Decimal
            Get
                Return dAR_CR_Amount
            End Get
            Set(ByVal value As Decimal)
                dAR_CR_Amount = value
            End Set
        End Property

        ''' <summary>
        ''' AR_TRANS_TYPE,CHAR,1,,N,,
        ''' I = Income
        ''' P = Payment
        ''' C = Credit
        ''' </summary>
        ''' <remarks></remarks>
        Private sAR_Trans_Type As String
        Public Property ar_trans_type() As String
            Get
                Return sAR_Trans_Type
            End Get
            Set(ByVal value As String)
                sAR_Trans_Type = value
            End Set
        End Property

        ''' <summary>
        ''' AR_USER,CHAR,3,,N,,
        ''' </summary>
        ''' <remarks></remarks>
        Private sAR_User As String
        Public Property ar_user() As String
            Get
                Return sAR_User
            End Get
            Set(ByVal value As String)
                sAR_User = value
            End Set
        End Property

        'AR_POST_DT,DATE,7,,Y,,


        Private sAR_Link_Trans_No As String
        Public Property ar_link_trans_no() As String
            Get
                Return sAR_Link_Trans_No
            End Get
            Set(ByVal value As String)
                sAR_Link_Trans_No = value
            End Set
        End Property

        ''' <summary>
        ''' AR_BALANCE,NUMBER,14,2,N,,0
        ''' </summary>
        ''' <remarks></remarks>
        Private dAR_Balance As Decimal
        Public Property ar_balance() As Decimal
            Get
                Return dAR_Balance
            End Get
            Set(ByVal value As Decimal)
                dAR_Balance = value
            End Set
        End Property

        Sub New()
            ar_trans_no = ""
            ar_date = ""
            ar_link_date = ""
            ar_cust_no = ""
            ar_rec_no = ""
            ar_ref_no = ""
            ar_dr_amount = 0D
            ar_cr_amount = 0D
            ar_trans_type = ""
            ar_user = ""
            ar_link_trans_no = ""
            ar_balance = 0D
        End Sub

        Sub SetInfo(ByVal dr As DataRow)
            ar_trans_no = dr("AR_TRANS_NO")
            ar_date = dr("AR_DATE")
            ar_link_date = dr("AR_LINK_DATE")
            ar_cust_no = dr("AR_CUST_NO")
            ar_rec_no = dr("AR_REC_NO")
            ar_ref_no = dr("AR_REF_NO")
            ar_dr_amount = dr("AR_DR_AMOUNT")
            ar_cr_amount = dr("AR_CR_AMOUNT")
            ar_trans_type = dr("AR_TRANS_TYPE")
            ar_user = dr("AR_USER")
            ar_link_trans_no = dr("AR_LINK_TRANS_NO")
            ar_balance = dr("AR_BALANCE")
        End Sub
        Sub SetInfo(ByVal dr As DataRow, ByVal sAR_Trans_No As String, ByVal sAR_Date As String, ByVal dCR_Amount As Decimal)
            ar_trans_no = sAR_Trans_No
            ar_date = sAR_Date
            ar_link_date = dr("AR_LINK_DATE")
            ar_cust_no = dr("AR_CUST_NO")
            ar_rec_no = dr("AR_REC_NO")
            ar_ref_no = dr("AR_REF_NO")
            ar_dr_amount = 0
            ar_cr_amount = dCR_Amount
            ar_trans_type = dr("AR_TRANS_TYPE")
            ar_user = dr("AR_USER")
            ar_link_trans_no = dr("AR_LINK_TRANS_NO")
            ar_balance = dr("AR_BALANCE")
        End Sub
        Function validate() As Boolean
            Dim bValid As Boolean = True

            bValid = bValid AndAlso (ar_trans_no.Trim <> "")
            bValid = bValid AndAlso (ar_date.Trim <> "")
            bValid = bValid AndAlso (ar_link_date.Trim <> "")
            bValid = bValid AndAlso (ar_cust_no.Trim <> "")
            bValid = bValid AndAlso (ar_rec_no.Trim <> "")
            bValid = bValid AndAlso (ar_ref_no.Trim <> "")
            bValid = bValid AndAlso (ar_dr_amount + ar_cr_amount <> 0)
            bValid = bValid AndAlso (ar_trans_type.Trim <> "")
            bValid = bValid AndAlso (ar_user.Trim <> "")
            bValid = bValid AndAlso (ar_link_trans_no.Trim <> "")
            bValid = bValid AndAlso (IsNumeric(ar_balance))
            bValid = bValid AndAlso (ar_link_date.Trim.Length <= 7)
            Dim TMPDATE As DateTime
            If bValid AndAlso DateTime.TryParse(ar_date, TMPDATE) Then
                ar_date = TMPDATE.ToString("yyyy-MM-dd")
            End If

            Return bValid
        End Function

        Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)
            Dim bValid As Boolean = validate()

            Dim sQry As String = "INSERT INTO DBO.ON_ARTRAN (" & vbNewLine & _
                                 " AR_TRANS_NO " & vbNewLine & _
                                 ",AR_DATE " & vbNewLine & _
                                 ",AR_LINK_DATE " & vbNewLine & _
                                 ",AR_CUST_NO " & vbNewLine & _
                                 ",AR_REC_NO " & vbNewLine & _
                                 ",AR_REF_NO " & vbNewLine & _
                                 ",AR_DR_AMOUNT " & vbNewLine & _
                                 ",AR_CR_AMOUNT " & vbNewLine & _
                                 ",AR_TRANS_TYPE " & vbNewLine & _
                                 ",AR_USER " & vbNewLine & _
                                 ",AR_LINK_TRANS_NO " & vbNewLine & _
                                 ",AR_BALANCE " & vbNewLine & _
                                 ") VALUES (" & vbNewLine & _
                                 "'" & ar_trans_no & "'" & vbNewLine & _
                                 ",TO_DATE('" & ar_date & "','YYYY-MM-DD')" & vbNewLine & _
                                 ",'" & ar_link_date & "'" & vbNewLine & _
                                 ",'" & ar_cust_no & "'" & vbNewLine & _
                                 ",'" & Right("00" & ar_rec_no, 2) & "'" & vbNewLine & _
                                 ",'" & Left(ar_ref_no.Trim, 6) & "'" & vbNewLine & _
                                 "," & ar_dr_amount & vbNewLine & _
                                 "," & ar_cr_amount & vbNewLine & _
                                 ",'" & ar_trans_type & "'" & vbNewLine & _
                                 ",'" & ar_user & "'" & vbNewLine & _
                                 ",'" & ar_link_trans_no & "'" & vbNewLine & _
                                 "," & ar_balance & vbNewLine & _
                                 ")"

            Try
                If bValid = False Then
                    Throw New Exception("Error validating ON_ARTRAN record")
                End If
                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting ON_ARTRAN record query: " & sQry & "!")
                End If

            Catch ex As Exception
                Throw New Exception("ON_ARTRAN.Insert->" & ar_ref_no & ".Error Message:" & ex.Message.ToString)
            End Try

        End Sub
    End Class
End Namespace
'AR_TRANS_NO,CHAR,6,,N,,
'AR_DATE,DATE,7,,N,,
'AR_LINK_DATE,CHAR,7,,N,,
'AR_CUST_NO,CHAR,10,,Y,,
'AR_REC_NO,CHAR,2,,Y,,
'AR_REF_NO,CHAR,6,,Y,,
'AR_DR_AMOUNT,NUMBER,14,2,N,,
'AR_CR_AMOUNT,NUMBER,14,2,N,,
'AR_TRANS_TYPE,CHAR,1,,N,,
'AR_USER,CHAR,3,,N,,
'AR_POST_DT,DATE,7,,Y,,
'AR_LINK_TRANS_NO,CHAR,6,,N,,
'AR_BALANCE,NUMBER,14,2,N,,0