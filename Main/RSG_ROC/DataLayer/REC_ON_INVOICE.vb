Imports Microsoft.VisualBasic

Namespace DataLayer
    Public Class REC_ON_INVOICE
        ''' <summary>
        ''' generated number
        ''' </summary>
        ''' <remarks></remarks>
        Private iInv_No As Integer
        Public Property inv_no() As Integer
            Get
                Return iInv_No
            End Get
            Set(ByVal value As Integer)
                iInv_No = value
            End Set
        End Property

        'INV_CLI,CHAR,10,,Y
        ''' <summary>
        ''' default to 3
        ''' </summary>
        ''' <remarks></remarks>
        Private iSite_Cd As Integer
        Public Property site_cd() As Integer
            Get
                Return iSite_Cd
            End Get
            Set(ByVal value As Integer)
                iSite_Cd = value
            End Set
        End Property

        ''' <summary>
        ''' Invoice Date
        ''' </summary>
        ''' <remarks></remarks>
        Private dtInv_Dt As DateTime
        Public Property inv_dt() As DateTime
            Get
                Return dtInv_Dt
            End Get
            Set(ByVal value As DateTime)
                dtInv_Dt = value
            End Set
        End Property

        ''' <summary>
        ''' O = open
        ''' P = paid
        ''' </summary>
        ''' <remarks></remarks>
        Private sInv_Status As String
        Public Property inv_status() As String
            Get
                Return sInv_Status
            End Get
            Set(ByVal value As String)
                sInv_Status = value
            End Set
        End Property

        ''' <summary>
        ''' always 'O' in rocon.
        ''' </summary>
        ''' <remarks></remarks>
        Private sInv_Type As String
        Public Property inv_type() As String
            Get
                Return sInv_Type
            End Get
            Set(ByVal value As String)
                sInv_Type = value
            End Set
        End Property

        ''' <summary>
        ''' Date in YYYY-MM-DD format.
        ''' NOTE: ROCON ON_INV_NO table has this column as a date field.
        ''' So when inserting/updating make sure to use TO_DATE function.
        ''' </summary>
        ''' <remarks></remarks>
        Private sTran_Dt As String
        Public Property tran_dt() As String
            Get
                Return sTran_Dt
            End Get
            Set(ByVal value As String)
                sTran_Dt = value
            End Set
        End Property

        ''' <summary>
        ''' In ROCON it is always 0
        ''' </summary>
        ''' <remarks></remarks>
        Private dAmount_Paid As Decimal
        Public Property amount_paid() As Decimal
            Get
                Return dAmount_Paid
            End Get
            Set(ByVal value As Decimal)
                dAmount_Paid = value
            End Set
        End Property

        ''' <summary>
        ''' N = Not fully paid
        ''' Y = Paid
        ''' </summary>
        ''' <remarks></remarks>
        Private sPaid_Fl As String
        Public Property paid_fl() As String
            Get
                Return sPaid_Fl
            End Get
            Set(ByVal value As String)
                sPaid_Fl = value
            End Set
        End Property

        Private sInv_Trans_No As String
        Public Property inv_trans_no() As String
            Get
                Return sInv_Trans_No
            End Get
            Set(ByVal value As String)
                sInv_Trans_No = value
            End Set
        End Property

        Private sInv_Cu_No As String
        Public Property inv_cu_no() As String
            Get
                Return sInv_Cu_No
            End Get
            Set(ByVal value As String)
                sInv_Cu_No = value
            End Set
        End Property


        Private sArchive_Dt As String
        Public Property archive_dt() As String
            Get
                Return sArchive_Dt
            End Get
            Set(ByVal value As String)
                sArchive_Dt = value
            End Set
        End Property

        Sub New()
            inv_no = 0
            site_cd = 3
            inv_dt = New DateTime
            inv_status = "O"
            inv_type = "O"
            tran_dt = ""
            amount_paid = 0D
            paid_fl = "N"
            inv_trans_no = ""
            inv_cu_no = ""
            archive_dt = ""
        End Sub

        Sub SetInfo(ByVal drInvoice As DataRow)
            inv_no = drInvoice("INV_NO")
            site_cd = drInvoice("SITE_CD")
            inv_dt = drInvoice("INV_DT")
            inv_status = drInvoice("INV_STATUS")
            inv_type = drInvoice("INV_TYPE")
            tran_dt = drInvoice("TRAN_DT")
            amount_paid = drInvoice("AMOUNT_PAID")
            paid_fl = drInvoice("PAID_FL")

            If IsDBNull(drInvoice("INV_TRANS_NO")) = False Then
                inv_trans_no = drInvoice("INV_TRANS_NO")
            End If

            inv_cu_no = drInvoice("INV_CU_NO")
            archive_dt = drInvoice("ARCHIVE_DT")
        End Sub

        Function validate() As Boolean
            Dim bRetval As Boolean = True
            'check inv_no must be > 0
            If inv_no < 0 Then
                bRetval = False
            End If

            If inv_status.Trim = "" Then
                bRetval = False
            End If

            If inv_type.Trim = "" Then
                bRetval = False
            End If

            If inv_cu_no.Trim = "" Then
                bRetval = False
            End If

            Return bRetval
        End Function
        Sub Insert(ByRef oraCmd As OracleClient.OracleCommand)

            Dim sQry As String = "INSERT INTO ON_INVOICE " & vbNewLine & _
                                             "(INV_NO " & vbNewLine & _
                                             ",SITE_CD " & vbNewLine & _
                                             ",INV_DT " & vbNewLine & _
                                             ",INV_STATUS " & vbNewLine & _
                                             ",INV_TYPE " & vbNewLine & _
                                             ",AMOUNT_PAID" & vbNewLine & _
                                             ",PAID_FL " & vbNewLine & _
                                             ",INV_TRANS_NO " & vbNewLine & _
                                             ",INV_CU_NO " & vbNewLine & _
                                             ") VALUES (" & vbNewLine & _
                                             inv_no & vbNewLine & _
                                             "," & site_cd & vbNewLine & _
                                             ", TO_DATE('" & inv_dt.ToString("yyyy-MM-dd HH:mm:ss") & "','YYYY-MM-DD HH24:MI:SS')" & vbNewLine & _
                                             ",'" & inv_status & "'" & vbNewLine & _
                                             ",'" & inv_type & "'" & vbNewLine & _
                                             "," & amount_paid & vbNewLine & _
                                             ",'" & paid_fl & "'" & vbNewLine & _
                                             ",'" & inv_trans_no & "'" & vbNewLine & _
                                             ",'" & inv_cu_no & "'" & vbNewLine & _
                                             ")"

            Try
                If validate() = False Then
                    Throw New Exception("Error validating ON_INVOICE record for inv_no: " & inv_no)
                End If

                oraCmd.CommandText = sQry
                oraCmd.Parameters.Clear()

                If oraCmd.ExecuteNonQuery <= 0 Then
                    Throw New Exception("Error Inserting ON_INVOICE record for inv_no: " & inv_no & "!")
                End If
            Catch ex As Exception
                Throw New Exception("ON_INVOICE.Insert->" & inv_no & ".Error Message:" & ex.Message.ToString)
            End Try


        End Sub

    End Class
End Namespace
'Table structure of ON_INVOICE TABLE
'INV_NO,NUMBER,10,,N
'INV_CLI,CHAR,10,,Y
'SITE_CD,NUMBER,10,,Y
'INV_DT,DATE,7,,N
'INV_STATUS,CHAR,1,,N
'INV_TYPE,CHAR,1,,N
'TRAN_DT,DATE,7,,Y
'AMOUNT_PAID,NUMBER,14,2,Y
'PAID_FL,CHAR,1,,Y
'INV_TRANS_NO,CHAR,6,,Y
'PAID_DT,DATE,7,,Y
'INV_CU_NO,CHAR,10,,Y
'ARCHIVE_DT,DATE,7,,Y