<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="TenderRestrictionOverride.aspx.vb" Inherits="_941Support.TenderRestrictionOverride" 
    title="Tender Restriction Override" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
 <table width="750" align="center" border="0">
        <tr>
            <td align="center">
                <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="Maroon"></asp:Label>&nbsp;
                <br />
            </td>
        </tr>
        <tr>
            <td class="lprompt" align="center" style="text-align: center" >
                Customer Number:             
                <asp:TextBox ID="txtCustomerNumber" runat="server" CssClass="textbox" Columns="20" maxlength="20"></asp:TextBox>&nbsp;
                <asp:Button ID="btnSubmit" runat="server" CssClass="BtnCss" Text="Search" />
                
            </td>
        </tr>
        <tr>
            <td align="center" class="lprompt">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center">
            <asp:Panel ID="pnlCustomerInfo" runat="server" Width="600" Visible="False" BorderStyle="None" GroupingText="Customer Information" Font-Bold="False">
                <table width="100%">
                    <tr>
                        <td class="rprompt" style="height: 21px">Customer Number:</td><td colspan="2" align="left" style="height: 21px"> <asp:Label ID="lblCustomerNumber" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="rprompt">Customer Name:</td><td colspan="2"  align="left"> <asp:Label ID="lblCustomerName" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="rprompt">
                        </td>
                        <td align="left" colspan="2">
                            <asp:CheckBox ID="ckActive" runat="server" Text="Active" />
                            <asp:Label ID="lblOldStatus" runat="server" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="rprompt">
                            InActive Reason:</td>
                        <td align="left" colspan="2">
                            <asp:Label ID="lblOldActivity" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="rprompt">Restricted Tender:</td><td colspan="2"  align="left"> <asp:DropDownList ID="drpTenderRestrict" runat="server">
                            <asp:ListItem Value="00 = No Tender Restriction"></asp:ListItem>
                            <asp:ListItem Value="20 = Restrict Personal Cheque"></asp:ListItem>
                            <asp:ListItem Value="40 = Restrict Head Office Charge"></asp:ListItem>
                            <asp:ListItem Value="80 = Restrict Personal Cheque"></asp:ListItem>
                            <asp:ListItem Value="FF = No Serve"></asp:ListItem>
                        </asp:DropDownList>
                            <asp:Label ID="lblOldTender" runat="server" Visible="False"></asp:Label></td>
                    </tr>
                    <tr>
                    <td class="rprompt">Comments:</td><td colspan="2"  align="left"><asp:TextBox ID="txtComments" runat="server" Columns="50" MaxLength="50" CssClass="textbox"></asp:TextBox></td>
                    </tr>
                    <tr><td colspan="3" style="height: 21px">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3"><asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="BtnCss" />
                        &nbsp;<asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="BtnCss" />
                        </td>
                    </tr>
                    
                </table>
            </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
