<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" Title="Product Search" CodeBehind="ProductSearch.aspx.vb" Inherits="_941Support.ProductSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">

<asp:Panel ID="pnl" runat="server" DefaultButton="btnSearch"> 
<table cellspacing="0" cellpadding="0" align="center" border="0" style="width: 80%">
<tr>
    <td colspan="4">
    </td>
</tr>
<tr>
    <td>
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblProduct" runat="server" Text="Product(sku): "  CssClass="LblCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtProduct" runat="server"></asp:TextBox>
    </td>
    <td>
    &nbsp;
    </td>
</tr>


<tr>
    <td colspan="4" align="right">
    <asp:Button ID="btnSearch" runat="server" CssClass="BtnCss" Text="Search" />
    <asp:Button ID="btnReset" runat="server" CssClass="BtnCss" Text="Reset" />
    &nbsp;
    </td>
</tr>

<tr>
    <td colspan="4">
    &nbsp;
    </td>
</tr>

<tr>
    <td colspan="4" align="center">
    <asp:Label ID="lblMessage" Text = "" runat="server" CssClass="LblCss"></asp:Label>
    </td>
</tr>

<tr>
    <td colspan="4" align="center">
        <asp:GridView ID="grdProduct" runat="server" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical" AutoGenerateColumns="False">
            <RowStyle BackColor="#F7F7DE" />
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="Maroon" />
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="Item" HeaderText="Item" SortExpression="Item" />
                <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                <asp:BoundField DataField="Case_Size" HeaderText="Case_Size" SortExpression="Case_Size" />
                <asp:BoundField DataField="Category" HeaderText="Category" SortExpression="Category" />
                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                <asp:BoundField DataField="Groupage" HeaderText="Groupage" SortExpression="Groupage" />
            </Columns>
        </asp:GridView>
        &nbsp;&nbsp;
    
    </td>
</tr>

<tr>
    <td colspan="4" align="center">
        <asp:GridView ID="grdRestriction" runat="server" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical" AutoGenerateColumns="False">
            <RowStyle BackColor="#F7F7DE" />
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="Maroon" />
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="Item" HeaderText="Item" SortExpression="Item" />
                <asp:BoundField DataField="Sold_To_Order_Type" HeaderText="Sold_To_Order_Type" SortExpression="Sold_To_Order_Type" />
            </Columns>
        </asp:GridView>
        &nbsp;&nbsp;
    
    </td>
</tr>

<tr>
    <td colspan="4" align="center">
    <asp:Label ID="lblMsg" Text = "" runat="server" CssClass="LblCss"></asp:Label>
    </td>
</tr>
<tr>
    <td colspan="4" align="center">
        <asp:GridView ID="grdQuantity" runat="server" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical" AutoGenerateColumns="False" DataKeyNames="Order_No">
            <RowStyle BackColor="#F7F7DE" />
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="Maroon" />
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                    <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" 
                    OnClientClick ="return confirm('Are you sure you want to delete this order?');"
                    CommandArgument=""></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Order_No" HeaderText="Order_No" SortExpression="Order_No" />
                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Customer" HeaderText="Customer" SortExpression="Customer" />
                <asp:BoundField DataField="Order_date_time" HeaderText="Order Date Time" />
                <asp:BoundField DataField="Order_reqd_date" HeaderText="Order Required Date" />
                <asp:BoundField DataField="Route_Code" HeaderText="Route_Code" SortExpression="Route_Code" />
                <asp:BoundField DataField="Item" HeaderText="Item" SortExpression="Item" />
                <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
            </Columns>
        </asp:GridView>
        &nbsp;&nbsp;
    
    </td>
</tr>



</table>
</asp:Panel>

</asp:Content>
