<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="RunEord.aspx.vb" Inherits="_941Support.RunEord" 
    title="Run EORD" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
<table cellspacing="0" cellpadding="0" align="center" border="0" style="width: 100%">
        <tr>
        <td align=center>
        <asp:Button ID="btnRunEord" runat="server" Text="Run Eord" Width="222px" CssClass="BtnCss" Height="30px" /></td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblMessage" runat="server" CssClass="BtnCss"></asp:Label></td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#CCCCCC"
                    BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="dataEORDLog" AllowSorting="True">
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                </asp:GridView>
                <asp:SqlDataSource ID="dataEORDLog" runat="server" ConnectionString="<%$ ConnectionStrings:TEST %>"
                    ProviderName="<%$ ConnectionStrings:TEST.ProviderName %>" SelectCommand="select timestamp as &quot;Last Run&quot;&#13;&#10;, osuser as &quot;Initiated by&quot;&#13;&#10;, program as &quot;Program&quot;&#13;&#10;FROM DBO.KILL_LOG&#13;&#10;WHERE program = 'EORD'&#13;&#10;and rownum <= 20&#13;&#10;order by Timestamp DESC">
                </asp:SqlDataSource>
                <br />
                <asp:Button ID="btnRefresh" runat="server" Text="Refresh " CssClass="BtnCss" />
            </td>
        </tr>
    </table>
</asp:Content>
