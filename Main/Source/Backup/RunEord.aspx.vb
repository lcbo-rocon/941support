Imports System.IO

Partial Class RunEord
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lblheading As Label = Master.FindControl("lblMainTitle")
        If Not lblHeading Is Nothing Then
            lblheading.Text = "Run EORD"
        End If

        lblMessage.Text = ""

        If Not IsPostBack Then
            Dim env As String = System.Configuration.ConfigurationManager.AppSettings("env")
            dataEORDLog.ConnectionString = ConfigurationManager.ConnectionStrings(env).ToString
        End If
    End Sub

    Protected Sub btnRunEord_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRunEord.Click
        Dim currentUser As String = User.Identity.Name.Substring(User.Identity.Name.IndexOf("\") + 1)

        Dim strSession As String = ""
        Dim instance As HttpSessionState = HttpContext.Current.Session

        Dim env As String = System.Configuration.ConfigurationManager.AppSettings("env")

        'Getting the current Session ID, this is used later to name the file with details to reset the user 
        strSession = instance.SessionID

        Dim fp As StreamWriter
        'Create a file in the \Data folder with the format sessionid_userid.pk
        'This file is checked by the perl script at whm001 every 3mins and if this file exists the
        'label print service is restarted.
        Try
            fp = File.CreateText(Server.MapPath(".\Data\" & strSession & "_" & currentUser.ToLower & ".eord"))
            fp.Close()
            Dim minutes As String = ConfigurationManager.AppSettings("JobTime")
            lblMessage.Text = "Eord will run in " & minutes & " minute(s)."
        Catch err As Exception
            lblMessage.Text = ConfigurationManager.AppSettings("GenericErrMsg")
        End Try

    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Response.Redirect("RunEord.aspx")
    End Sub
End Class
