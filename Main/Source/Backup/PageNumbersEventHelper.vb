Imports Microsoft.VisualBasic
Imports com.lowagie.text
Imports com.lowagie.text.pdf
Imports com.lowagie.text.Image

Imports System.IO

Public Class PageNumbersEventHelper
    Inherits PdfPageEventHelper

    Dim template As PdfTemplate
    Dim baseFont As BaseFont
    Dim table As PdfPTable
    Dim headerImage As Image
    Dim title As String
    Dim subtext As String = ""
    Dim logo As String
    Dim headerTable As PdfPTable = Nothing

    Public Sub New(ByVal reportname As String, ByVal strlogo As String)
        MyBase.New()
        title = reportname
        logo = strlogo
    End Sub
    Public Sub New(ByVal reportname As String, ByVal reporttext As String, ByVal strlogo As String)
        MyBase.New()
        title = reportname
        subtext = reporttext
        logo = strlogo
    End Sub

    Public Overrides Sub onOpenDocument(ByVal writer As PdfWriter, ByVal document As Document)
        Try
            headerImage = Image.getInstance(logo)
            Dim columns(1) As Single
            columns(0) = 1
            columns(1) = 3

            table = New PdfPTable(1)
            Dim p As New Phrase
            '            Dim ck As New Chunk(title, New Font(Font.TIMES_ROMAN, 8, Font.BOLD, Drawing.Color.FromArgb(74, 60, 140)))
            Dim ck As New Chunk(title, New Font(Font.TIMES_ROMAN, 8, Font.BOLD))
            p.add(ck)

            table.getDefaultCell.setBorderWidth(0)
            ''table.addCell(New Phrase(New Chunk(headerImage, 0, 0)))
            ''table.getDefaultCell.setHorizontalAlignment(Element.ALIGN_LEFT)
            table.getDefaultCell.setVerticalAlignment(Element.ALIGN_MIDDLE)
            ''If subtext <> "" Then
            ''    ck = New Chunk(vbNewLine & vbNewLine & subtext, New Font(Font.TIMES_ROMAN, 12, Font.BOLD, Drawing.Color.Maroon))
            ''    p.add(ck)
            ''End If
            table.addCell(p)


            template = writer.getDirectContent.createTemplate(1000, 1000)
            template.setBoundingBox(New Rectangle(0, 0, 1000, 1000))

            baseFont = pdf.BaseFont.createFont("Helvetica", baseFont.WINANSI, False)
        Catch ex As Exception
            Throw New Exception("onOpenDocument->" & ex.Message.ToString)
        End Try

    End Sub

    Public Overrides Sub onEndPage(ByVal writer As PdfWriter, ByVal document As Document)
        Dim cb As PdfContentByte = writer.getDirectContent
        cb.saveState()

        'write header table
        table.setTotalWidth(document.right - document.left)
        table.writeSelectedRows(0, -1, document.left, document.getPageSize.height - 20, cb)

        Dim text As String = "Page " & writer.getPageNumber & " of "
        Dim textsize As Double = baseFont.getWidthPoint(text, 8)
        Dim textbase As Double = document.bottom - 10
        Dim adjust As Double = baseFont.getWidthPoint("0", 8)

        cb.beginText()
        cb.setFontAndSize(baseFont, 8)
        'cb.setTextMatrix(document.left, textbase)
        cb.showTextAligned(PdfContentByte.ALIGN_LEFT, Now.ToString("D"), document.left, textbase, 0)
        cb.setTextMatrix(document.right() - textsize - adjust, textbase)
        cb.showText(text)
        cb.endText()
        cb.addTemplate(template, document.right() - adjust, textbase)

        cb.saveState()
    End Sub

    Public Overrides Sub onclosedocument(ByVal writer As PdfWriter, ByVal document As Document)
        template.beginText()
        template.setFontAndSize(baseFont, 8)
        template.setTextMatrix(0, 0)
        template.showText((writer.getPageNumber - 1).ToString)
        template.endText()

    End Sub
End Class
