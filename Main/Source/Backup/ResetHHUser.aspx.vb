'----------------------------------------------------------------------------------------------
' This page displays the form for the user to enter the User Id to be reset.
'----------------------------------------------------------------------------------------------
' Created By: Ria Bhatnagar, LCBO-RSG
' Date: March, 2008
'----------------------------------------------------------------------------------------------
' Updated By: Shirley Lam, LCBO-RSG
' Date: June 15, 2008
' Use MainTitle.ascx instead for the Heading and dynamically change the title in Page Load
' Changed message to read from web.config file for number of minutes to wait.
'----------------------------------------------------------------------------------------------

Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState


Partial Class ResetHHUser
    Inherits System.Web.UI.Page

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Response.Redirect("~\ResetHHUser.aspx")
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        'This is the main sub-routine that helps in resetting the User_Id. The first thing that is checked is whether
        'the User Id to be reset is a valid User Id. If this is a valid User ID, the session Id for this session is obtained,
        'the USer Id of the user resetting the other user is obtained.
        'Then all the details like the process id for the User Id to be reset are obtained.
        'Finally with all this information a file is created inside .\data\directory
        'The format of the file is SessionId_UserID1_UserId2.ks(UserId1: USer Id to be reset, UserId2: User resetting the other id)
        'The contents of the file is an instruction to kill the process id of the User Id being reset.
        'Eventually this subroutine creates this ks file with an instruction to kill the process.



        Dim strConnection As String
        Dim myConnection As OracleConnection = Nothing
        Dim myCommand As OracleCommand = Nothing
        Dim dr As OracleDataReader = Nothing
        Dim strTemp As String = ""
        Dim temp As Integer = 0
        Dim strReturn As Integer = 0
        Dim strUser As String = ""
        Dim strSid As String = ""
        Dim strPid As String = ""
        Dim strPgm As String = ""

        'Getting the current OS user(i.e. the user resetting someone's User Id, get the user as LCBO\UserId)
        'This would just get the User Id without the LCBO\
        Dim currentUser As String = User.Identity.Name.Substring(User.Identity.Name.IndexOf("\") + 1)

        Dim strSession As String = ""
        Dim instance As HttpSessionState = HttpContext.Current.Session

        Dim env As String = System.Configuration.ConfigurationManager.AppSettings("env")

        'Getting the current Session ID, this is used later to name the file with details to reset the user 
        strSession = instance.SessionID

        Dim grow As GridViewRow
        Dim chkReset As CheckBox
        Dim userReset As String = ""

        For Each grow In GridView1.Rows
            chkReset = grow.FindControl("chkResetUser")

            If chkReset.Checked = True Then
                userReset = grow.Cells(0).Text

                strConnection = System.Configuration.ConfigurationManager.AppSettings(env)
                myConnection = New OracleConnection(strConnection)
                Try



                    'strTemp = txtResetUser.Text.ToLower
                    'SQL statement to check if the User Id to be reset is a valid User Id
                    myCommand = New OracleCommand("SELECT COUNT(*) as Count FROM dbo.EMMAS WHERE lower(Trim(em_no)) = :resetUser", myConnection)
                    myCommand.Parameters.AddWithValue("resetUser", userReset)
                    If strConnection <> "" Then
                        myConnection.Open()
                    End If

                    dr = myCommand.ExecuteReader()

                    While dr.Read()
                        temp = dr(0)
                        If temp > 0 Then
                            'If the User Id to be reset is valid, select the details required to kill the user session from a sys view, V$Session
                            myCommand = New OracleCommand("SELECT username,sid,substr(process,1,instr(process,':')-1),program FROM v$session WHERE LOWER(osuser) = :tempUser OR LOWER(username) = :tempUser AND status='INACTIVE' AND program like '%hand%'AND rownum=1", myConnection)
                            myCommand.Parameters.AddWithValue("tempUser", userReset)
                            dr = myCommand.ExecuteReader()

                            If Not dr.HasRows Then
                                lblMessage.Text = "Please enter a correct User Id, the User Id: " & userReset & " does not have a hand held session."
                                'lblMessage.Text = "Please enter a correct User Id, the User Id: rebbb does not have a hand held session."
                            Else
                                While dr.Read()
                                    strUser = dr(0).ToString
                                    strSid = dr(1).ToString
                                    strPid = dr(2).ToString
                                    strPgm = dr(3).ToString

                                    Dim fp As StreamWriter
                                    'With the details of the User id to be rest obtained above, create a .cmd file with
                                    'the format SessionId_UserID To be Reset_UserID Resetting.cmd (example sessionid_reaaa_rebbb.cmd)
                                    'The content of the file is an instruction to kill the process id for the User Id to be reset
                                    Try
                                        fp = File.CreateText(Server.MapPath(".\Data\" & strSession & "_" & strUser.ToLower & "_" & currentUser.ToLower & ".ks"))
                                        fp.WriteLine("kill -f " & strPid)
                                        fp.Close()
                                    Catch err As Exception
                                        lblMessage.Text = ConfigurationManager.AppSettings("genericErrMsg")
                                    End Try

                                    Dim minutes As String = ConfigurationManager.AppSettings("JobTime")
                                    lblMessage.Text = "The User(s) will be reset in " & minutes & " minute(s)"
                                End While
                                dr.NextResult()
                            End If

                        End If
                    End While
                    dr.Close()
                    myConnection.Close()
                    myCommand.Dispose()
                Catch exc As Exception
                    If Not dr Is Nothing Then
                        dr.Close()
                    End If
                    If Not myConnection Is Nothing Then
                        If myConnection.State <> Data.ConnectionState.Closed Then
                            myConnection.Close()
                        End If
                    End If
                    If Not myCommand Is Nothing Then
                        myCommand.Dispose()
                    End If
                    lblMessage.Text = ConfigurationManager.AppSettings("genericErrMsg")
                End Try



            End If
        Next

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lblHeading As Label = Master.FindControl("lblMainTitle")
        If Not lblHeading Is Nothing Then
            lblHeading.Text = "Reset HH User Session"
        End If
        

        Dim env As String = ConfigurationManager.AppSettings("env")
        SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings(env).ToString
        SqlDataSource1.DataBind()

    End Sub
End Class
