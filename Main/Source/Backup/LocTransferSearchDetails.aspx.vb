Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Imports System.IO.Directory
Imports System.Data
Imports System.Security.Principal
Imports System.Threading
Imports RSG_ROC.DataLayer
Imports RSG_ROC.Exceptions
Imports RSG_ROC.OrderClasses
Imports RSG_ROC.LS_TRANSFERS
Imports System.Text.RegularExpressions
Partial Public Class LocTransferSearchDetails
    Inherits System.Web.UI.Page

    Private Sub LocTransferSearchDetails_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Dim ex As Exception = Server.GetLastError
        Session("ExceptionObj") = ex
        Response.Redirect("~\ErrorPage.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = "Details for Transfer ID: " & Session("LocTransferSelectId")
        Dim lblheading As Label = Master.FindControl("lblMainTitle")
        If Not lblheading Is Nothing Then
            lblheading.Text = "Licensee Location Transfer Details"
        End If

        If Not IsPostBack Then
            lnkLookUp.Attributes.Add("onclick", "showCustomerList('" & txtNewCustNo.UniqueID & "' );return false;")
            Dim transfer_id As String = Session("LocTransferSelectId")
            If transfer_id <> "" Then
                Dim roc_le_select_transfer As ROC_LE_SELECT_TRANSFER = New ROC_LE_SELECT_TRANSFER(transfer_id)

                If DALRoc.isTransferFinalized(transfer_id) Then

                    txtNewCustNo.Enabled = False
                    txtNewCustName.Enabled = False
                    txtNewCustAddress.Enabled = False
                    txtNewCustCity.Enabled = False
                    txtNewCustProvince.Enabled = False
                    txtNewCustPostal.Enabled = False

                    txtNewCustTransfer.Enabled = False
                    txtNewCustContact.Enabled = False
                    txtNewCustEmail.Enabled = False

                    btnSave.Enabled = False
                    Me.btnSetCustomer.Enabled = False
                    Me.lnkLookUp.Visible = False
                    Me.Img1.Visible = False


                    ' drpDocDescription.Enabled = False
                    'chkReplace.Enabled = False
                    'flupDocument.Enabled = False
                    'btnUpload.Enabled = False

                    ' grdPendingDocs.Enabled = False

                End If

                lblOrigCustNo.Text = roc_le_select_transfer.orig_customer_no
                lblOrigCustName.Text = roc_le_select_transfer.orig_cust_name
                lblOrigCustAddress.Text = roc_le_select_transfer.orig_cust_address
                lblOrigCustCity.Text = roc_le_select_transfer.orig_cust_city
                lblOrigCustProvince.Text = roc_le_select_transfer.orig_cust_prov
                lblOrigCustPostal.Text = roc_le_select_transfer.orig_cust_post_code
                lblOrigCustPhone.Text = roc_le_select_transfer.orig_cust_phone

                txtNewCustNo.Text = roc_le_select_transfer.new_customer_no
                txtNewCustName.Text = roc_le_select_transfer.new_cust_name
                txtNewCustAddress.Text = roc_le_select_transfer.new_cust_address
                txtNewCustCity.Text = roc_le_select_transfer.new_cust_city
                txtNewCustProvince.Text = roc_le_select_transfer.new_cust_prov
                txtNewCustPostal.Text = roc_le_select_transfer.new_cust_post_code

                txtNewCustTransfer.Text = IIf(roc_le_select_transfer.expected_transfer_dt.Year < 2000, "", roc_le_select_transfer.expected_transfer_dt.ToString("yyyy-MM-dd"))
                txtNewCustContact.Text = roc_le_select_transfer.le_select_contact_name
                txtNewCustEmail.Text = roc_le_select_transfer.le_select_corporate_email

                lblReturnNo.Text = roc_le_select_transfer.le_select_return_no
                lblTransferNo.Text = roc_le_select_transfer.le_select_order_no
                lblPreparedBy.Text = roc_le_select_transfer.prepared_by
                lblCurrentStatus.Text = roc_le_select_transfer.status

                drpDocDescription.DataSource = DALRoc.getDocumentNameList
                drpDocDescription.DataTextField = "Doc_Desc"
                drpDocDescription.DataValueField = "Doc_Type_Id"
                drpDocDescription.DataBind()

                grdCheckList.DataSource = roc_le_select_transfer.CheckList
                grdCheckList.DataBind()

                Session("LSUploadedDocs") = New DataView(roc_le_select_transfer.Uploaded_Documents)
                grdUploadedDocs.DataSource = Session("LSUploadedDocs")
                grdUploadedDocs.DataBind()

                grdPendingDocs.DataSource = roc_le_select_transfer.Pending_Documents
                grdPendingDocs.DataBind()


            End If
        End If
    End Sub

    Private Sub grdUploadedDocs_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdUploadedDocs.DataBound
        If grdUploadedDocs.Rows.Count > 0 Then
            For Each tcell As TableCell In grdUploadedDocs.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
        End If
        lblUpMsg.Text = "There are " & grdUploadedDocs.Rows.Count & " documents uploaded."
    End Sub

    Private Sub grdUploadedDocs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdUploadedDocs.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hypDocument As HyperLink = Nothing
            Dim lnkButton As LinkButton = Nothing
            Dim hypFilePath As String = ""
            Dim hypFiles() As String
            Dim cnt As Integer = 0

            lnkButton = e.Row.FindControl("lnkUploadDelete")
            hypDocument = e.Row.FindControl("hypDocFilePath")
            If Not hypDocument Is Nothing Then
                hypFilePath = hypDocument.NavigateUrl
                If hypFilePath.ToString <> "" Then
                    hypFiles = hypFilePath.Split("\")
                    cnt = hypFiles.Length
                    hypDocument.Text = hypFiles(cnt - 1)
                End If
            End If

            Dim transfer_id As String = Session("LocTransferSelectId")
            Dim roc_le_select_transfer As ROC_LE_SELECT_TRANSFER = New ROC_LE_SELECT_TRANSFER(transfer_id)

            If DALRoc.isTransferFinalized(transfer_id) Then
                lnkButton.Visible = False
            End If
        End If
    End Sub

    Private Sub grdCheckList_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdCheckList.DataBound
        If grdCheckList.Rows.Count > 0 Then
            For Each tcell As TableCell In grdCheckList.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
        End If
    End Sub

    Public Sub chkStatus_changed(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkbox As CheckBox = CType(sender, CheckBox)
        Dim checked As Boolean = False

        checked = chkbox.Checked
        Dim grow As GridViewRow = chkbox.Parent.Parent
        Dim lblCheck As Label = Nothing
        Dim check_id As String = ""

        lblCheck = grow.FindControl("lblCheckListCheck")
        check_id = lblCheck.Text

        Dim transfer_id As String = Session("LocTransferSelectId")
        Dim roc_le_select_transfer As ROC_LE_SELECT_TRANSFER = New ROC_LE_SELECT_TRANSFER(transfer_id)

        Dim conn As OracleConnection = Nothing
        conn = New OracleConnection(DALRoc.getConnectionstring(ConfigurationManager.AppSettings("env")))
        Dim cmd As OracleCommand = New OracleCommand
        Dim trans As OracleTransaction = Nothing

        Try
            conn.Open()
            trans = conn.BeginTransaction
            cmd.Connection = conn
            cmd.Transaction = trans

            roc_le_select_transfer.SetCheck(check_id, checked, cmd)

            cmd.Dispose()
            trans.Commit()
            conn.Close()

        Catch ex As Exception
            trans.Rollback()
            cmd.Dispose()
            conn.Close()
            Throw ex
        End Try

        grdCheckList.DataSource = roc_le_select_transfer.CheckList
        grdCheckList.DataBind()
    End Sub

    Private Sub grdCheckList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdCheckList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hypDocument As HyperLink = Nothing
            Dim check_id As Label = Nothing
            Dim hypFilePath As String = ""
            Dim chk As CheckBox = Nothing
            Dim cnt As Integer = 0

            e.Row.Cells(6).Text = e.Row.Cells(6).Text & "%"
            check_id = e.Row.FindControl("lblCheckListCheck")
            chk = e.Row.FindControl("chkCheckListStatus")

            If check_id.Text = 6 Then
                hypDocument = e.Row.FindControl("hypCheckListTask")
                'hypDocument.NavigateUrl = ConfigurationManager.AppSettings("LocationTransferDir") & hypDocument.Text
                hypDocument.NavigateUrl = "~\LocTransferCreate.aspx"
            End If
            If check_id.Text = 1 Then
                hypDocument = e.Row.FindControl("hypCheckListTask")
                hypDocument.NavigateUrl = ConfigurationManager.AppSettings.Item("LocationTransferDir") & "Physical_Inventory_Count_Sheet.pdf"
            End If
            Dim transfer_id As String = Session("LocTransferSelectId")
            Dim roc_le_select_transfer As ROC_LE_SELECT_TRANSFER = New ROC_LE_SELECT_TRANSFER(transfer_id)

            If DALRoc.isTransferFinalized(transfer_id) And check_id.Text = 6 Then
                hypDocument.Enabled = True
                chk.Enabled = False
            End If

            If DALRoc.isTransferFinalized(transfer_id) And check_id.Text = 6 Then
                hypDocument.Enabled = True
                chk.Enabled = False
            End If
           
        End If
    End Sub

    Private Sub grdPendingDocs_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdPendingDocs.DataBound
        If grdPendingDocs.Rows.Count > 0 Then
            For Each tcell As TableCell In grdPendingDocs.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
        End If
        lblPendMsg.Text = "There are " & grdPendingDocs.Rows.Count & " Pending Documents found!"
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim bValid As Boolean = True

        Dim transfer_id As String = Session("LocTransferSelectId")
        Dim roc_le_select_transfer As ROC_LE_SELECT_TRANSFER = New ROC_LE_SELECT_TRANSFER(transfer_id)

        If (Regex.IsMatch(txtNewCustNo.Text.Trim, "^[0-9]+$") And DALRoc.isValidCustomer(txtNewCustNo.Text.Trim)) = False Then
            bValid = False
            lblMsg.Text = "Invalid New Customer Number entered!"
        Else
            roc_le_select_transfer.new_customer_no = txtNewCustNo.Text.Trim
        End If

        If bValid AndAlso txtNewCustTransfer.Text.Trim <> "" Then
            If IsDate(txtNewCustTransfer.Text.ToString) = False Then
                bValid = False
                lblMsg.Text = "Invalid Expected Transfer Date Entered. Please enter date in YYYY-MM-DD format!"
            Else
                roc_le_select_transfer.expected_transfer_dt = txtNewCustTransfer.Text.Trim
            End If
        Else
            roc_le_select_transfer.expected_transfer_dt = New DateTime
        End If

        'Need to escape the single quotes with two single quotes
        roc_le_select_transfer.new_cust_name = txtNewCustName.Text.Trim.Replace("'", "|").Replace("|", "''")
        roc_le_select_transfer.new_cust_address = txtNewCustAddress.Text.Trim.Replace("'", "|").Replace("|", "''")
        roc_le_select_transfer.new_cust_city = txtNewCustCity.Text.Trim.Replace("'", "|").Replace("|", "''")
        roc_le_select_transfer.new_cust_prov = txtNewCustProvince.Text.Trim.Replace("'", "|").Replace("|", "''")
        roc_le_select_transfer.new_cust_post_code = txtNewCustPostal.Text.Trim

        roc_le_select_transfer.le_select_contact_name = txtNewCustContact.Text.Trim.Replace("'", "|").Replace("|", "''")
        roc_le_select_transfer.le_select_corporate_email = txtNewCustEmail.Text.Trim.Replace("'", "|").Replace("|", "''")

        If bValid = True Then

            DALRoc.setEnv(ConfigurationManager.AppSettings("env"))
            Dim conn As OracleConnection = Nothing
            conn = New OracleConnection(DALRoc.getConnectionstring(ConfigurationManager.AppSettings("env")))
            Dim cmd As OracleCommand = New OracleCommand
            Dim trans As OracleTransaction = Nothing

            Try
                conn.Open()
                trans = conn.BeginTransaction
                cmd.Connection = conn
                cmd.Transaction = trans

                roc_le_select_transfer.UPDATE(cmd)

                cmd.Dispose()
                trans.Commit()
                conn.Close()

            Catch ex As Exception
                trans.Rollback()
                cmd.Dispose()
                conn.Close()
                Me.lblMsg.Text = "Error Saving Transfer information!"
            End Try
        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("~\LocTransferSearch.aspx")
    End Sub

    Private Sub grdUploadedDocs_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdUploadedDocs.RowDeleting
        Dim currIndex As Integer = e.RowIndex

        Dim transfer_id As String = Session("LocTransferSelectId")
        Dim roc_le_select_transfer As ROC_LE_SELECT_TRANSFER = New ROC_LE_SELECT_TRANSFER(transfer_id)
        Dim doc_id As String = ""
        doc_id = grdUploadedDocs.DataKeys(currIndex).Value


        Dim conn As OracleConnection = Nothing
        conn = New OracleConnection(DALRoc.getConnectionstring(ConfigurationManager.AppSettings("env")))
        Dim cmd As OracleCommand = New OracleCommand
        Dim trans As OracleTransaction = Nothing

        Try
            conn.Open()
            trans = conn.BeginTransaction
            cmd.Connection = conn
            cmd.Transaction = trans

            roc_le_select_transfer.DeleteLeSelectDocument(doc_id, cmd)

            cmd.Dispose()
            trans.Commit()
            conn.Close()

        Catch ex As Exception
            trans.Rollback()
            cmd.Dispose()
            conn.Close()
            Throw ex
        End Try
        grdUploadedDocs.DataSource = roc_le_select_transfer.Uploaded_Documents
        grdUploadedDocs.DataBind()

    End Sub

    Private Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim transfer_id As String = Session("LocTransferSelectId")
        Dim roc_le_select_transfer As ROC_LE_SELECT_TRANSFER = New ROC_LE_SELECT_TRANSFER(transfer_id)
        Dim current_user As String = Session("CurrentUserId")

        Dim conn As OracleConnection = Nothing
        conn = New OracleConnection(DALRoc.getConnectionstring(ConfigurationManager.AppSettings("env")))
        Dim cmd As OracleCommand = New OracleCommand
        Dim trans As OracleTransaction = Nothing
        Dim rdr As OracleDataReader = Nothing
        Dim cnt As Integer = 0
        Dim file_path As String = ""
        Dim file_name As String = ""
        Dim uploadFileName As String = ""
        Dim doc_type_id As String = ""
        Dim replace_doc As Boolean = False
        replace_doc = chkReplace.Checked

        Try
            conn.Open()
            trans = conn.BeginTransaction
            cmd.Connection = conn
            cmd.Transaction = trans

            cmd.CommandText = "SELECT count(*) from dbo.LCBO_LE_SELECT_DOCUMENTS WHERE LE_SELECT_ID='" & transfer_id & "' " & _
                            " AND DOC_TYPE_ID='" & drpDocDescription.SelectedValue & "' "

            rdr = cmd.ExecuteReader

            While rdr.Read
                cnt = rdr.Item(0)
            End While

            If (cnt > 0 And replace_doc = True) Or (cnt = 0) Then
                doc_type_id = drpDocDescription.SelectedValue
                file_path = ConfigurationManager.AppSettings("TransferUploadDir")
                file_name = flupDocument.FileName
                uploadFileName = roc_le_select_transfer.AddLeSelectDocument(doc_type_id, file_path, file_name, current_user, cmd)

                flupDocument.SaveAs(uploadFileName)
            End If

            cmd.Dispose()
            trans.Commit()
            conn.Close()

        Catch ex As Exception
            trans.Rollback()
            cmd.Dispose()
            conn.Close()
            Throw ex
        End Try
        grdUploadedDocs.DataSource = roc_le_select_transfer.Uploaded_Documents
        grdUploadedDocs.DataBind()

        Me.grdCheckList.DataSource = roc_le_select_transfer.CheckList
        Me.grdCheckList.DataBind()

        Me.grdPendingDocs.DataSource = roc_le_select_transfer.Pending_Documents
        Me.grdPendingDocs.DataBind()


    End Sub

    Private Sub btnSetCustomer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSetCustomer.Click
        Dim valid_cust As Boolean = False
        Dim dr As DataRow = Nothing

        dr = DALRoc.getCustInfo(txtNewCustNo.Text.Trim)

        txtNewCustName.Text = dr("CU_NAME")
        txtNewCustAddress.Text = dr("ADD1_SH") & " " & dr("ADD2_SH")
        txtNewCustCity.Text = dr("CITY_SH")
        txtNewCustProvince.Text = dr("PROV_SH")
        txtNewCustPostal.Text = dr("POST_CODE")
    End Sub

    Private Sub grdUploadedDocs_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdUploadedDocs.Sorting
        Dim dv As DataView = Session("LSUploadedDocs")
        Dim order As String = ""
        If Not ViewState("SSOrder") Is Nothing Then
            order = ViewState("SSOrder")
        End If

        If order.StartsWith(e.SortExpression) Then
            If order.ToString.EndsWith("ASC") Then
                order = "DESC"
            Else
                order = "ASC"
            End If
        Else
            order = "ASC"
        End If

        ViewState("SSOrder") = e.SortExpression + " " & order
        dv.Sort = ViewState("SSOrder")
        grdUploadedDocs.DataSource = dv
        grdUploadedDocs.DataBind()
    End Sub
End Class