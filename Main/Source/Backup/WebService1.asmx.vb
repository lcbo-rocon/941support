Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Net
Imports System.Data.OracleClient
Imports System.IO

<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<System.Web.Script.Services.ScriptService()> _
<ToolboxItem(False)> _
Public Class WebService1
    Inherits System.Web.Services.WebService

    Shared Function getOraConnection() As OracleConnection
        Dim conn As OracleConnection = Nothing
        Dim env As String = ConfigurationManager.AppSettings("env")
        conn = New OracleConnection(ConfigurationManager.ConnectionStrings(env).ToString)
        Return conn
    End Function
    <WebMethod()> _
       Public Function getRouteCodes(ByVal prefixText As String, ByVal count As Integer) As String()
        Dim conn As OracleConnection = getOraConnection()
        Dim dt As New DataTable
        Dim items As New List(Of String)
        Dim counter As Integer = 0
        If (count = 0) Then
            count = 10
        End If
        Dim cmd As New OracleCommand("select distinct trim(route_code) from dbo.route_code_mas where lower(route_code) like '" & prefixText.ToLower & "%'" & _
                        "order by 1", conn)
        Dim reader As OracleDataReader = Nothing
        conn.Open()
        Try
            reader = cmd.ExecuteReader
            counter = 0
            While reader.Read And counter < count
                items.Add(reader(0))
                counter = counter + 1
            End While
        Catch ex As Exception
            reader.Close()
            cmd.Dispose()
            conn.Close()
        End Try
        reader.Close()
        cmd.Dispose()
        conn.Close()

        Return items.ToArray()
    End Function

    <WebMethod()> _
       Public Function getStopNos(ByVal prefixText As String, ByVal count As Integer) As String()
        Dim conn As OracleConnection = getOraConnection()
        Dim dt As New DataTable
        Dim items As New List(Of String)
        Dim counter As Integer = 0
        If (count = 0) Then
            count = 10
        End If
        Dim cmd As New OracleCommand("select distinct trim(route_stop) from dbo.on_codtl where route_stop like '" & prefixText & "%'" & _
                        "order by 1", conn)
        Dim reader As OracleDataReader = Nothing
        conn.Open()
        Try
            reader = cmd.ExecuteReader
            counter = 0
            While reader.Read And counter < count
                items.Add(reader(0))
                counter = counter + 1
            End While
        Catch ex As Exception
            reader.Close()
            cmd.Dispose()
            conn.Close()
        End Try
        reader.Close()
        cmd.Dispose()
        conn.Close()

        Return items.ToArray()
    End Function
    <WebMethod()> _
       Public Function getCustomerNos(ByVal prefixText As String, ByVal count As Integer) As String()
        Dim conn As OracleConnection = getOraConnection()
        Dim dt As New DataTable
        Dim items As New List(Of String)
        Dim counter As Integer = 0
        If (count = 0) Then
            count = 10
        End If
        Dim cmd As New OracleCommand("select distinct cu_no from dbo.on_cohdr where cu_no like '" & prefixText & "%'" & _
                        "order by 1", conn)
        Dim reader As OracleDataReader = Nothing
        conn.Open()
        Try
            reader = cmd.ExecuteReader
            counter = 0
            While reader.Read And counter < count
                items.Add(reader(0))
                counter = counter + 1
            End While
        Catch ex As Exception
            reader.Close()
            cmd.Dispose()
            conn.Close()
        End Try
        reader.Close()
        cmd.Dispose()
        conn.Close()

        Return items.ToArray()
    End Function
    <WebMethod()> _
      Public Function getOrderNos(ByVal prefixText As String, ByVal count As Integer) As String()
        Dim conn As OracleConnection = getOraConnection()
        Dim dt As New DataTable
        Dim items As New List(Of String)
        Dim counter As Integer = 0
        If (count = 0) Then
            count = 10
        End If
        Dim cmd As New OracleCommand("select distinct co_odno from dbo.on_cohdr where co_odno like '" & prefixText & "%' and co_status='O'" & _
                        "order by 1", conn)
        Dim reader As OracleDataReader = Nothing
        conn.Open()
        Try
            reader = cmd.ExecuteReader
            counter = 0
            While reader.Read And counter < count
                items.Add(reader(0))
                counter = counter + 1
            End While
        Catch ex As Exception
            reader.Close()
            cmd.Dispose()
            conn.Close()
        End Try
        reader.Close()
        cmd.Dispose()
        conn.Close()

        Return items.ToArray()
    End Function

    <WebMethod()> _
      Public Function getCarrierNos(ByVal prefixText As String, ByVal count As Integer) As String()
        Dim conn As OracleConnection = getOraConnection()
        Dim dt As New DataTable
        Dim items As New List(Of String)
        Dim counter As Integer = 0
        If (count = 0) Then
            count = 10
        End If
        Dim cmd As New OracleCommand("Select distinct CARRIER_NAME from DBO.CARRIER_MAS where lower(CARRIER_NAME) like '" & prefixText.ToLower & "%'" & _
                        "order by 1", conn)
        Dim reader As OracleDataReader = Nothing
        conn.Open()
        Try
            reader = cmd.ExecuteReader
            counter = 0
            While reader.Read And counter < count
                items.Add(reader(0))
                counter = counter + 1
            End While
        Catch ex As Exception
            reader.Close()
            cmd.Dispose()
            conn.Close()
        End Try
        reader.Close()
        cmd.Dispose()
        conn.Close()

        Return items.ToArray()
    End Function

    <WebMethod()> _
      Public Function getOrderTypes(ByVal prefixText As String, ByVal count As Integer) As String()
        Dim conn As OracleConnection = getOraConnection()
        Dim dt As New DataTable
        Dim items As New List(Of String)
        Dim counter As Integer = 0
        If (count = 0) Then
            count = 10
        End If
        Dim cmd As New OracleCommand("Select distinct ORDER_TYPE from DBO.ON_CUST_ORDER_TYPE where lower(ORDER_TYPE) like '" & prefixText.ToLower & "%'" & _
                        "order by 1", conn)
        Dim reader As OracleDataReader = Nothing
        conn.Open()
        Try
            reader = cmd.ExecuteReader
            counter = 0
            While reader.Read And counter < count
                items.Add(reader(0))
                counter = counter + 1
            End While
        Catch ex As Exception
            reader.Close()
            cmd.Dispose()
            conn.Close()
        End Try
        reader.Close()
        cmd.Dispose()
        conn.Close()

        Return items.ToArray()
    End Function
End Class