Imports System.IO

Partial Public Class ViewConveyorLogs
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lblheading As Label = Master.FindControl("lblMainTitle")
        If Not lblheading Is Nothing Then
            lblheading.Text = "View Conveyor Logs"
        End If

        If Not IsPostBack Then
            Dim dt As New DataTable
            Dim drow As DataRow = Nothing

            dt.Columns.Add("FileLastUpdate")
            dt.Columns.Add("FileFullName")
            dt.Columns.Add("FileName")

            Dim LogDir As String = ConfigurationManager.AppSettings("DematicLogDir").ToString
            Dim logs As String() = Directory.GetFiles(LogDir, "ConvCtr*.log")


            For Each logfile As String In (logs)
                drow = dt.NewRow
                drow("FileLastUpdate") = DateTime.Parse(File.GetLastWriteTime(logfile)).ToString("yyyy-MM-dd hh:mm:ss")
                drow("FileFullName") = logfile
                drow("FileName") = logfile.Replace(LogDir, "")
                dt.Rows.Add(drow)
            Next

            Dim dv As New DataView(dt)
            dv.Sort = "FileFullName ASC"
            Session("tblConveyor") = dv

            grdConveyorLogs.DataSource = Session("tblConveyor")
            grdConveyorLogs.DataBind()
        End If

    End Sub

    Private Sub grdConveyorLogs_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdConveyorLogs.DataBound
        If grdConveyorLogs.Rows.Count > 0 Then
            For Each tcell As TableCell In grdConveyorLogs.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
        End If
    End Sub


    Private Sub grdConveyorLogs_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdConveyorLogs.Sorting
        Dim dv As DataView = Session("tblConveyor")
        Dim order As String = ""
        If Not ViewState("SSOrder") Is Nothing Then
            order = ViewState("SSOrder")
        End If

        If order.StartsWith(e.SortExpression) Then
            If order.ToString.EndsWith("ASC") Then
                order = "DESC"
            Else
                order = "ASC"
            End If
        Else
            order = "ASC"
        End If

        ViewState("SSOrder") = e.SortExpression + " " & order
        dv.Sort = ViewState("SSOrder")
        grdConveyorLogs.DataSource = dv
        grdConveyorLogs.DataBind()
    End Sub
End Class