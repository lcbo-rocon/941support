Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Imports System.IO.Directory
Imports System.Data
Imports System.Security.Principal
Imports System.Threading
'----------------------------------------------------------------------------------------------
' This web interface allows the user to create an ASN from this utility that will be sent to 
' 1709 from Rocon.
'----------------------------------------------------------------------------------------------
' Author: Omran Chaudhry
' Date: July 2010
'----------------------------------------------------------------------------------------------
' Perfected by (Code Audit): Ria Bhatnagar 
' Date: August 2010
'----------------------------------------------------------------------------------------------
' Changed by: Ria Bhatnagar 
' Date: November 2010
' Change to give 0941 users ability to send the IST to all stores, instead of just 1709
'----------------------------------------------------------------------------------------------
Partial Public Class CreateIST
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'MOD: 2010-08-24 - request to limit access to CreateIST User group to this page.
        'Dim id As IIdentity = Thread.CurrentPrincipal.Identity 'HttpContext.Current.User.Identity
        'Dim userid As String = UCase(id.Name).Replace("LCBO\", "")
        'Dim access As Boolean = False
        'Dim CreateISTUsers As String() = ConfigurationManager.AppSettings("CreateISTUsers").Split(",")
        'For Each TOUser As String In CreateISTUsers
        '    If UCase("lcbo\" & userid).Equals(UCase(TOUser)) Then
        '        'show menu item
        '        access = True
        '    End If
        'Next

        'If access = False Then
        '    Response.Redirect("Default.aspx")
        'End If

        If Not IsPostBack Then

            'txtStoreNo.Text = ConfigurationManager.AppSettings("DefaultStore")

            txtOrderDateFrom.Text = Format$(Now, "yyyy-MM-dd")
            txtOrderDateTo.Text = Format$(Now, "yyyy-MM-dd")

            Dim lblHeading As Label = Master.FindControl("lblMainTitle")
            If Not lblHeading Is Nothing Then
                lblHeading.Text = "Create IST"
            End If

            grdSummary.DataSourceID = Nothing
            grdSummary.DataSource = Nothing
            grdSummary.DataBind()

            lblMsg.Text = ""
            lblDetailMsg.Text = ""


        End If
    End Sub

    Private Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Response.Redirect("~\CreateIST.aspx")
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim myConnection As OracleConnection = Nothing
        Dim myCommand As OracleCommand = Nothing
        Dim dr As OracleDataReader = Nothing

        Dim valid As Boolean = True
        Dim tmpInt As Integer = 0
        Dim tmpDateFrom As DateTime = Nothing
        Dim tmpDateTo As DateTime = Nothing
        Dim dtSummary As DataTable = Nothing
        Dim where As String = ""

        lblMsg.Text = ""
        pnlDetails.Visible = False

        'check if dates are valid
        If txtOrderDateFrom.Text.Trim <> "" Then
            If DateTime.TryParse(txtOrderDateFrom.Text.Trim, tmpDateFrom) = False Then
                valid = False
                lblMsg.Text = lblMsg.Text & "Please enter a valid Required Date From.<br/>"
                txtOrderDateFrom.CssClass = "etextbox"
            Else
                If where <> "" Then
                    where = where & " AND "
                End If
                where = where & " TRUNC(COH.ORDER_REQD_DT) >= TO_DATE('" & tmpDateFrom.ToString("yyyy-MM-dd") & "','YYYY-MM-DD') "
            End If
        Else
            valid = False
            lblMsg.Text = lblMsg.Text & "Please enter a valid Required Date From.<br/>"
        End If

        If txtOrderDateTo.Text.Trim <> "" Then
            If DateTime.TryParse(txtOrderDateTo.Text.Trim, tmpDateTo) = False Then
                valid = False
                lblMsg.Text = lblMsg.Text & "Please enter a valid Required Date To.<br/>"
                txtOrderDateTo.CssClass = "etextbox"
            Else
                If where <> "" Then
                    where = where & " AND "
                End If
                where = where & " TRUNC(COH.ORDER_REQD_DT) <= TO_DATE('" & tmpDateTo.ToString("yyyy-MM-dd") & "','YYYY-MM-DD') "
            End If
        End If

        If txtOrderNo.Text.Trim <> "" Then
            If Integer.TryParse(txtOrderNo.Text, tmpInt) = False Then
                valid = False
                lblMsg.Text = lblMsg.Text & "Please enter a valid numeric order number.<br/>"
                txtOrderNo.CssClass = "etextbox"
            Else
                If where <> "" Then
                    where = where & " AND "
                End If
                where = where & " COH.CO_ODNO = " & txtOrderNo.Text
            End If

        End If

        'Check if Date To is < than Date from 
        If valid = True Then
            If txtOrderDateTo.Text.Trim <> "" Then
                If tmpDateFrom > tmpDateTo Then
                    valid = False
                    lblMsg.Text = lblMsg.Text & "Error - From DATE is greater than To DATE<br/>"
                    txtOrderDateFrom.CssClass = "etextbox"
                    txtOrderDateTo.CssClass = "etextbox"
                End If
            End If
        End If

        'Check the store number to send the IST to
        Dim storenum As String = ""
        If txtStoreNo.Text <> "" Then
            storenum = txtStoreNo.Text.Replace("s", "")
            storenum = txtStoreNo.Text.Replace("S", "")
            storenum = Trim(storenum)

            If Regex.IsMatch(storenum, "^\d+$") And storenum.Length < 5 Then
                storenum = "S" & Right(("0000" & storenum), 4)
            Else
                valid = False
                lblMsg.Text = lblMsg.Text & "Error - Please enter a valid store number<br/>"
            End If
        End If

        Dim qrySummary As String = ""
        If valid = True Then
            If storenum = "" Then
                qrySummary = "SELECT COH.CO_ODNO AS OrderNo, COH.CU_NO AS StoreNo, TO_CHAR(COH.ORDER_REQD_DT,'YYYY-MM-DD') AS OrderRequiredDate, SUM(COD.PICKED_QTY) AS UnitsPicked " & _
                                   "FROM DBO.CODTL COD, DBO.COHDR COH " & _
                                   "WHERE(COD.CO_ODNO = COH.CO_ODNO) " & _
                                   "AND COH.CO_STATUS = 'C' " & _
                                   "AND COH.ORDER_TYPE = 'IST' "
            Else

                qrySummary = "SELECT COH.CO_ODNO AS OrderNo, COH.CU_NO AS StoreNo, TO_CHAR(COH.ORDER_REQD_DT,'YYYY-MM-DD') AS OrderRequiredDate, SUM(COD.PICKED_QTY) AS UnitsPicked " & _
                                    "FROM DBO.CODTL COD, DBO.COHDR COH " & _
                                    "WHERE(COD.CO_ODNO = COH.CO_ODNO) " & _
                                    "AND COH.CO_STATUS = 'C' " & _
                                    "AND COH.ORDER_TYPE = 'IST' " & _
                                    "AND COH.CU_NO = '" & storenum & "'"
            End If

            If where <> "" Then
                qrySummary = qrySummary & " and " & where
                where = " WHERE " & where
            End If

            qrySummary = qrySummary & " GROUP BY COH.CO_ODNO,COH.ORDER_REQD_DT,COH.CU_NO ORDER BY COH.CO_ODNO, COH.ORDER_REQD_DT ASC "

            dtSummary = SPURDataControl.getData(qrySummary)
            Session("SUMMARY") = dtSummary
            grdSummary.DataSource = Session("SUMMARY")
            grdSummary.DataBind()
        Else
            grdSummary.DataSource = Nothing
            grdSummary.DataBind()
        End If

    End Sub

    Private Sub btnExportSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportSummary.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=SummaryExport.xls")
        Response.Charset = "UTF-8"
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.ms-excel"
        Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-1")

        EnableViewState = False
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New HtmlTextWriter(stringWrite)

        Dim newReport As New GridView
        newReport.BorderStyle = grdSummary.BorderStyle
        newReport.BorderWidth = grdSummary.BorderWidth
        newReport.CellPadding = grdSummary.CellPadding
        newReport.HeaderStyle.ForeColor = Drawing.Color.White
        newReport.HeaderStyle.BackColor = Drawing.Color.FromArgb(617231)


        Dim dv As DataView
        dv = New DataView(Session("SUMMARY"))
        newReport.DataSource = dv
        newReport.DataBind()

        newReport.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString)
        Response.End()

    End Sub

    Private Sub btnExportDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportDetails.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=DetailsExport.xls")
        Response.Charset = "UTF-8"
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.ms-excel"
        Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-1")

        EnableViewState = False
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New HtmlTextWriter(stringWrite)

        Dim newReport As New GridView
        newReport.BorderStyle = grdSummary.BorderStyle
        newReport.BorderWidth = grdSummary.BorderWidth
        newReport.CellPadding = grdSummary.CellPadding
        newReport.HeaderStyle.ForeColor = Drawing.Color.White
        newReport.HeaderStyle.BackColor = Drawing.Color.FromArgb(617231)


        Dim dv As DataView
        dv = New DataView(Session("DETAILS"))
        newReport.DataSource = dv
        newReport.DataBind()

        newReport.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString)
        Response.End()
    End Sub

    Private Sub grdSummary_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdSummary.DataBound
        If grdSummary.Rows.Count > 0 Then
            For Each tcell As TableCell In grdSummary.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
            lblMsg.Text = ""
            txtOrderDateFrom.CssClass = ""
            txtOrderDateTo.CssClass = ""
            txtOrderNo.CssClass = ""
            pnlSummary.Visible = True
            lblMsg.Text = grdSummary.Rows.Count & " order(s) match your search criteria."

            'MOD: August 18Disable the Create ASN Button if the order_required_date is greater than ASN_LIMIT.

            Dim lblOrderRequiredDate As Label = Nothing
            Dim lnkCreateASN As LinkButton = Nothing
            Dim DateLimit As Date = Now.Subtract(System.TimeSpan.FromDays(ConfigurationManager.AppSettings("ASN_LIMIT")))


            For Each grow As GridViewRow In Me.grdSummary.Rows
                lblOrderRequiredDate = grow.FindControl("lblOrderRequiredDate")
                lnkCreateASN = grow.FindControl("lnkCreateASN")

                lnkCreateASN.Visible = lblOrderRequiredDate.Text > DateLimit.ToString("yyyy-MM-dd")

            Next

        Else
            lblMsg.Text = "There are 0 orders that match your search criteria."
            pnlSummary.Visible = False
            pnlDetails.Visible = False
        End If
    End Sub


    Private Sub grdSummary_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdSummary.RowCommand

        'If Order number is clicked.
        If e.CommandName = "GetOrderDetails" Then

            Dim OrderNo As String = e.CommandArgument

            Dim dtDetails As DataTable = Nothing


            Dim change As Int16 = 0
            Dim rowsaffected As Int16 = 0

            Try
                lblDetailMsg.Text = ""


                Dim qryDetails As String = "SELECT COD.CO_ODNO AS OrderNo, COD.ITEM AS SkuNo, COD.COD_LINE AS OrderlineNo, COD.COD_QTY AS OrderedQty, COD.PICKED_QTY AS PickedQty " & _
                      "FROM DBO.CODTL COD " & _
                      "WHERE COD.CO_ODNO = '" & Trim(OrderNo) & "' ORDER by COD.COD_LINE "

                dtDetails = SPURDataControl.getData(qryDetails)
                Session("DETAILS") = dtDetails
                grdDetails.DataSource = Session("DETAILS")
                grdDetails.DataBind()
                grdDetails.Visible = True
                pnlDetails.Visible = True

            Catch ex As Exception
                lblMsg.Text = ConfigurationManager.AppSettings("genericErrMsg")
            End Try
        End If

        'If the ACN link is clicked
        If e.CommandName = "GetCreateASN" Then
            Dim arg As String = e.CommandArgument
            Dim args() As String 
            Dim ordernum As String = ""
            Dim OrderNo As String = ""
            Dim grow As GridViewRow = Nothing
            Dim StoreNo As String = ""
            Dim txtStoreNumberFrom As String = "0941"

            args = arg.Split(",")
            OrderNo = Trim(args(0))
            ordernum = Right$(Trim(OrderNo).PadLeft(8, "00000000"), 8)
            StoreNo = Trim(args(1)).Substring(1)

            Dim dtACN As DataTable = Nothing
            Dim aryACN As New ArrayList
            Dim txtStoreNumberTo As String = ""
            txtStoreNumberTo = StoreNo

            'If (ConfigurationManager.AppSettings("env")) <> "PROD" Then
            '    txtStoreNumberTo = ConfigurationManager.AppSettings("DefaultStore")
            '    txtStoreNumberTo = txtStoreNumberTo.Substring(1)
            'Else
            'txtStoreNumberTo = StoreNo
            'End If

            Dim txtDateFile As String = Format$(Now, "yyMMddhhmm")
            Dim txtDateStamp As String = Format$(Now, "yyMMdd")

            Dim change As Int16 = 0
            Dim rowsaffected As Int16 = 0
            Try
                lblMsg.Text = "ASN file created for " & Trim(OrderNo)

                Dim txtFileName As String = txtStoreNumberTo & "." & txtStoreNumberFrom & "." & "s4" & "." & ordernum & "." & txtDateFile.ToString & ".sts"

                ' Dim qryDetails As String = "SELECT OrderNo AS COD.CO_ODNO, SkuNo As COD.ITEM, OrderlineNo AS COD.COD_LINE, OrderedQty AS COD.COD_QTY, PickedQty AS COD.PICKED_UNITS" & _
                '      "FROM DBO.CODTL COD" & _
                '     "WHERE Trim(lblOrderNo) = :OrderNo "


                Dim qryACN As String = "SELECT COD.ITEM As SkuNo, COD.PICKED_QTY AS PickedQty " & _
                      "FROM DBO.CODTL COD " & _
                      "WHERE COD.CO_ODNO = '" & Trim(OrderNo) & "' Order By COD.COD_LINE "


                dtACN = SPURDataControl.getData(qryACN)
                Session("DETAILSACN") = dtACN
                Dim recordcount As Int16
                For Each drow As DataRow In dtACN.Rows
                    aryACN.Add("1" & "," & Trim(drow("SkuNo")) & "," & "0" & "," & Trim(drow("PickedQty")))
                    recordcount = recordcount + 1
                Next
                Dim strACN As String = Join(aryACN.ToArray, vbNewLine)
                recordcount = recordcount + 1
                'From here create a file of IST/K52 format.
                'The format will be #Header Record#



                'MsgBox("The ASN file will be placed in " & CurDir() & "\Work\")

                Dim ACNDirectory As String = Server.MapPath(".\Work")

                If Directory.Exists(ACNDirectory) <> True Then

                    Directory.CreateDirectory(ACNDirectory)

                End If


                Dim sw As StreamWriter = New StreamWriter(ACNDirectory & "\" & txtFileName)
                ' Add the relavent text to the file.
                sw.WriteLine(Chr(34) & "#HEADER#" & Chr(34) & "," & Chr(34) & "I7" & Chr(34) & "," & txtDateStamp.ToString & "," & _
                         Chr(34) & "0" & Chr(34) & "," & Chr(34) & txtStoreNumberTo & Chr(34) & "," & Chr(34) & Chr(34) & _
                         "," & Chr(34) & CInt(txtStoreNumberFrom) & Chr(34) & "," & Chr(34) & Chr(34) & "," & Chr(34) & "##LAST##" & Chr(34) & _
                         "," & Chr(34) & "#INLIST#" & Chr(34))
                sw.WriteLine("0" & "," & Chr(34) & Trim(OrderNo) & Chr(34) & "," & _
                            Chr(34) & CInt(txtStoreNumberFrom) & Chr(34) & "," & txtDateStamp.ToString & "," & Chr(34) & Chr(34))
                sw.WriteLine(strACN)
                sw.Write(Chr(34) & "#TRAILER#" & Chr(34) & "," & Chr(34) & Chr(34) & "," & recordcount.ToString & "," & "0")
                sw.Close()
                Dim path1 As String = ACNDirectory & "\" & txtFileName
                Dim path2 As String = Server.MapPath(".\Data\") & txtFileName
                File.Move(path1, path2)
            Catch ex As Exception
                ' lblMsg.Text = ConfigurationManager.AppSettings("An Error occurred. Please open magic ticket with Retail Help Desk and assign to RSG HOST support.")
                lblMsg.Text = "Error " & ex.Message.ToString
            End Try

        End If
    End Sub
   
    Private Sub grdDetails_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdDetails.DataBound
        If grdDetails.Rows.Count > 0 Then
            For Each tcell As TableCell In grdDetails.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
            lblMsg.Text = ""
            pnlDetails.Visible = True
            lblDetailMsg.Text = grdDetails.Rows.Count & " line item(s) belong to this order."
        Else
            lblDetailMsg.Text = "There are 0 line items in this order."
            pnlDetails.Visible = False
        End If
    End Sub

    Private Sub grdSummary_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdSummary.Sorting
        Dim d As Integer = 0
        d = grdSummary.Columns.Count
        GridViewSortExpression = e.SortExpression
        Dim pageIndex As Integer = grdSummary.PageIndex

        grdSummary.DataSource = SortingTable(Session("SUMMARY"), False)
        grdSummary.DataBind()
        grdSummary.PageIndex = pageIndex

    End Sub
    Private Property GridViewSortDirection() As String
        Get
            If ViewState("SortDirection") Is Nothing Then
                ViewState("SortDirection") = SortDirection.Ascending
            End If
            Return ViewState("SortDirection")
        End Get
        Set(ByVal value As String)
            ViewState("SortDirection") = value
        End Set
    End Property

    Private Property GridViewSortExpression() As String
        Get
            If ViewState("SortExpression") Is Nothing Then
                ViewState("SortExpression") = ""
            End If
            Return ViewState("SortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("SortExpression") = value
        End Set
    End Property

    Private Function GetSortDirection() As String
        Select Case GridViewSortDirection
            Case "0"
                GridViewSortDirection = "1"
            Case "1"
                GridViewSortDirection = "0"
        End Select

        Return GridViewSortDirection
    End Function

    Protected Function SortingTable(ByVal dt As DataTable, ByVal isPageIndexChanging As Boolean) As DataView
        Dim dv As DataView
        Dim order As String = ""

        dv = New DataView(dt)
        If GridViewSortExpression <> "" Then
            If isPageIndexChanging Then
                If GridViewSortDirection = "0" Then
                    order = "ASC"
                Else
                    order = "DESC"
                End If
                dv.Sort = GridViewSortExpression & " " & order

            Else
                If GetSortDirection() = "0" Then
                    order = "ASC"
                Else
                    order = "DESC"
                End If
                dv.Sort = GridViewSortExpression & " " & order

            End If

        End If

        Return dv
    End Function

    Private Sub grdDetails_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdDetails.Sorting

        Dim d As Integer = 0
        d = grdDetails.Columns.Count
        GridViewSortExpression = e.SortExpression
        Dim pageIndex As Integer = grdDetails.PageIndex

        grdDetails.DataSource = SortingTable(Session("DETAILS"), False)
        grdDetails.DataBind()
        grdDetails.PageIndex = pageIndex
    End Sub
End Class