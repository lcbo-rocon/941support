Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Imports System.IO.Directory
Imports System.Data
Imports System.Security.Principal
Imports System.Threading
Imports RSG_ROC.DataLayer
Imports RSG_ROC.Exceptions
Imports RSG_ROC.OrderClasses
Imports RSG_ROC.LS_TRANSFERS
Imports System.Text.RegularExpressions
Partial Public Class LocTransferCustomer
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        DALRoc.setEnv(ConfigurationManager.AppSettings("env"))

        If Not IsPostBack Then
            Dim cu_no As String = ""
            If Request.QueryString("custno") Is Nothing = False Then
                cu_no = Request.QueryString("custno")
                Me.txtCustNo.Text = cu_no
                Me.btnSearch_Click(sender, e)
            End If
        End If
       


    End Sub

    Private Sub grdCustomer_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdCustomer.DataBound
        If grdCustomer.Rows.Count = 0 Then
            lblMsg.Text = "There were no records that match your search criteria"
        ElseIf grdCustomer.Rows.Count > 0 Then
            lblMsg.Text = grdCustomer.Rows.Count & " record(s) match your search criteria"
            For Each tcell As TableCell In grdCustomer.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
            Dim lnkCustomer As LinkButton = Nothing
            For Each grow As GridViewRow In grdCustomer.Rows
                lnkCustomer = grow.FindControl("lnkSelect")
                If lnkCustomer Is Nothing = False Then
                    lnkCustomer.Attributes.Add("onclick", "closeDialog('" & lnkCustomer.CommandArgument & "')")
                End If
            Next
        End If
    End Sub

    'Private Sub grdCustomer_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdCustomer.RowCommand
    '    If e.CommandName = "Select" Then
    '        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    '        Dim row As GridViewRow = grdCustomer.Rows(index)
    '        Dim lnkSelect As LinkButton = Nothing
    '        Dim custno As String = ""
    '        Dim custname As String = ""
    '        Dim address As String = ""
    '        Dim city As String = ""
    '        Dim province As String = ""
    '        Dim postalcode As String = ""
    '        Dim phoneno As String = ""

    '        lnkSelect = grdCustomer.Rows(index).FindControl("lnkSelect")
    '        custno = grdCustomer.DataKeys.Item(index).Values(0).ToString.Trim

    '        custname = grdCustomer.Rows(index).Cells(2).Text.Trim
    '        address = grdCustomer.Rows(index).Cells(3).Text.Trim
    '        city = grdCustomer.Rows(index).Cells(4).Text.Trim
    '        province = grdCustomer.Rows(index).Cells(5).Text.Trim
    '        postalcode = grdCustomer.Rows(index).Cells(6).Text.Trim
    '        phoneno = grdCustomer.Rows(index).Cells(7).Text.Trim

    '        Session("LocTransferCustomerNo") = custno
    '        Session("LocTransferCustomerName") = custname
    '        Session("LocTransferCustomerAdd") = address
    '        Session("LocTransferCustomerCity") = city
    '        Session("LocTransferCustomerProv") = province
    '        Session("LocTransferCustomerPost") = postalcode
    '        Session("LocTransferCustomerPhn") = phoneno

    '        ClientScript.RegisterStartupScript(Me.GetType(), "RefreshParent", "<script language=JavaScript>window.opener.document.forms[0].submit();window.close();</script>", False)
    '    End If
    'End Sub

    Protected Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim dt As DataTable = Nothing
        dt = DALRoc.getLicenseeList(Me.txtCustNo.Text, Me.txtCustName.Text.ToUpper)

        grdCustomer.DataSource = dt
        grdCustomer.DataBind()
    End Sub

    Protected Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Me.txtCustNo.Text = ""
        Me.txtCustName.Text = ""
        Me.grdCustomer.DataSource = Nothing
        Me.grdCustomer.DataBind()
    End Sub
End Class