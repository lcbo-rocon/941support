'Class FixLocation
'----------------------------------------------------------------------------------------------
' This page displays the form for the user to enter the Location Id to be fixed.
'----------------------------------------------------------------------------------------------
' Created By: Ria Bhatnagar, LCBO-RSG
' Date: May, 2008
'----------------------------------------------------------------------------------------------
' Updated By: Shirley Lam, LCBO-RSG
' Date: June 15, 2008
' Use MainTitle.ascx instead for the Heading and dynamically change the title in Page Load
' Changed message to read from web.config file for number of minutes to wait.
'----------------------------------------------------------------------------------------------

Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Partial Class FixLocation
    Inherits System.Web.UI.Page

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtLocationId.Text = ""
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strConnection As String
        Dim myConnection As OracleConnection = Nothing
        Dim myCommand As OracleCommand = Nothing
        Dim dr As OracleDataReader = Nothing


        If txtLocationId.Text = "" Then
            lblMessage.Text = "Please enter the Location Id, it can not be blank."
        Else

            Dim loc_id As String = ""
            loc_id = txtLocationId.Text

            Dim location_id As String = ""

            Dim currentUser As String = User.Identity.Name.Substring(User.Identity.Name.IndexOf("\") + 1)

            Dim env As String = System.Configuration.ConfigurationManager.AppSettings("env")

            Dim strSession As String = ""
            Dim instance As HttpSessionState = HttpContext.Current.Session
            strSession = instance.SessionID

            strConnection = System.Configuration.ConfigurationManager.AppSettings(env)
            myConnection = New OracleConnection(strConnection)


            Try
                'Query to make sure that the location id entered to be fixed is a valid location
                myCommand = New OracleCommand("SELECT COUNT(*) FROM dbo.LOMAS WHERE UPPER(TRIM(lo_loca)) = UPPER(TRIM(:LocationId))", myConnection)
                myCommand.Parameters.Clear()
                myCommand.Parameters.AddWithValue("LocationId", loc_id)

                If strConnection <> "" Then
                    myConnection.Open()
                End If

                dr = myCommand.ExecuteReader()

                While dr.Read()
                    location_id = dr(0)
                End While

                If location_id <= 0 Then
                    lblMessage.Text = "The location id: " & txtLocationId.Text & " is not a valid location, please re-enter a valid Location Id"
                Else
                    'If the location entered to be fixed is a valid location then create a file in the \Data folder 
                    'with the format sessionid_userid.ln with the location to be fixed as the file content 
                    Dim fp As StreamWriter
                    Try
                        fp = File.CreateText(Server.MapPath(".\Data\" & strSession & "_" & currentUser.ToLower & ".ln"))
                        fp.WriteLine(txtLocationId.Text)
                        fp.Close()
                    Catch err As Exception
                        lblMessage.Text = ConfigurationManager.AppSettings("genericErrMsg")
                    End Try
                    Dim minutes As String = ConfigurationManager.AppSettings("JobTime")
                    lblMessage.Text = "The location id: " & txtLocationId.Text & " will be fixed in " & minutes & " minute(s)"
                End If


                dr.Close()
                myConnection.Close()
                myCommand.Dispose()


            Catch exc As Exception
                If Not dr Is Nothing Then
                    dr.Close()
                End If
                If Not myConnection Is Nothing Then
                    If myConnection.State <> Data.ConnectionState.Closed Then
                        myConnection.Close()
                    End If
                End If
                If Not myCommand Is Nothing Then
                    myCommand.Dispose()
                End If
                lblMessage.Text = ConfigurationManager.AppSettings("genericErrMsg")
            End Try
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Text = ""
        Dim lblHeading As Label = Master.FindControl("lblMainTitle")
        If Not lblHeading Is Nothing Then
            lblHeading.Text = "Fixing Location"
        End If
    End Sub
End Class
