<%@ Page Language="vb" AutoEventWireup="false" Title="Freight Forwarding" CodeBehind="FreightForwarding.aspx.vb" MasterPageFile="~/Main.Master" Inherits="_941Support.FreightForwarding" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true" ShowSummary="false" />   
<table cellspacing="0" cellpadding="0" align="center" border="0" style="width: 90%;">

<tr>
    <td colspan="4" align="left">
    &nbsp;
    <asp:Label ID="lblNote" Text="Note: Mandatory Fields (*)" runat="server" CssClass="LblCss"></asp:Label>
    </td>
</tr>

<tr>
<td colspan="4"><br/></td>
</tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblCustomer" runat="server" Text="Customer#:"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtCustomer" runat="server" Width="100px"></asp:TextBox>
    <asp:Label ID="lblStar" runat="server" Text="*" CssClass="LabelCss"></asp:Label>
    <asp:RequiredFieldValidator ID="CustomerReqValidator" runat="server" ControlToValidate="txtCustomer" ErrorMessage="Customer# is a Mandatory Field" Display="None"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="CustomerCmpValidator" runat="server" ControlToValidate="txtCustomer"
    Operator="DataTypeCheck" Type="Integer" ErrorMessage="Customer# should be numerical" Display="None"></asp:CompareValidator>
    </td>
    <td>
    &nbsp;
    </td>
</tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblOrder" runat="server" Text="Order#:"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtOrder" runat="server" Width="80px"></asp:TextBox>
    <asp:CompareValidator ID="OrderValidator" runat="server" ControlToValidate="txtOrder"
    Operator="DataTypeCheck" Type="Integer" ErrorMessage="Order# should be numerical" Display="None"></asp:CompareValidator>
    </td>
    <td>
    &nbsp;
    </td>
</tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblDate" runat="server" Text="Invoice Date(YYYY-MM-DD):"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtInvoiceDate" runat="server" Width="100px"></asp:TextBox>
    <asp:RegularExpressionValidator ID="DateRegValidator" runat="server" ControlToValidate="txtInvoiceDate" 
    ErrorMessage="Invalid Invoice Date Format" Display="None" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator>
    <asp:CompareValidator ID="DateCmpValidator" runat="server" ControlToValidate="txtInvoiceDate"
    Operator="DataTypeCheck" Type="Date" ErrorMessage="Invalid Invoice Date" Display="None"></asp:CompareValidator>
    <a id="a1" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtInvoiceDate, 'a1', 'yyyy-MM-dd');return false;"
	href="#" name="a1"><img id="Img1" height="21" src="Images/calendar.gif" width="34" align="top" border="0"></a>
    </td>
    <td>
    &nbsp;
    </td>
</tr>

<tr>
    <td colspan="2">&nbsp;</td>
    <td colspan="2" align="right">
    <asp:Button ID="btnSearch" runat="server" CssClass="BtnCss" Text="Search" />
    <asp:Button ID="btnReset" runat="server" CssClass="BtnCss" Text="Reset" />
    &nbsp;
    </td>
</tr>

<tr>
    <td colspan="4" align="center">
    &nbsp;
    </td>
</tr>

<tr>
    <td colspan="4" align="center">
    <asp:Label ID="lblMessage" Text="" runat="server" CssClass="LblCss"></asp:Label>
    </td>
</tr>

<tr>
    <td colspan="4" align="center" >
        <div style="z-index: 10; overflow: auto;height:250px">
            <asp:GridView ID="grdForwarding" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            BackColor="White" BorderColor="#CBD49E" BorderStyle="None" BorderWidth="1px" 
            CellPadding="4" Font-Names="Arial" GridLines="Vertical" ForeColor="Black" DataKeyNames="cu_no,co_odno"   >
            <FooterStyle BackColor="#CCCC99" />
            <RowStyle BackColor="#F7F7DE" Font-Size="Small" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="White" BorderStyle="None" />
            <HeaderStyle BackColor="#CBD49E" Font-Size="Small" ForeColor="Maroon" Font-Bold="True" />            
                <Columns>                                   
                    <asp:BoundField DataField="Cu_No" HeaderText="Customer#" SortExpression="Cu_No" />
                    <asp:BoundField DataField="Cu_Name" HeaderText="Customer Name" SortExpression="Cu_Name" />
                    <asp:BoundField DataField="Co_odno" HeaderText="Order#" SortExpression="Co_odno" />
                    <asp:BoundField DataField="Inv_Dt" DataFormatString="{0:d}" HeaderText="Date" SortExpression="Inv_Dt" />
                    <asp:BoundField DataField="Order_Type" HeaderText="Order Type" SortExpression="Order_Type" />
                    <asp:BoundField DataField="Inv_Amt" HeaderText="Invoice Amount" SortExpression="Inv_Amt" />
                    <asp:BoundField DataField="Paid_Amount" HeaderText="Paid Amount" SortExpression="Paid_Amount" />
                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                    <asp:TemplateField>
                    <ItemTemplate>
                       <asp:LinkButton ID="lnkForward" runat="server" Text="Forward" 
                       CommandName="Forward" CommandArgument='<%# Bind("Co_odno") %>'></asp:LinkButton>
                    </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        </div>       
    </td>
</tr>

<tr>
    <td colspan="4" align="center">
    &nbsp;
    </td>
</tr>

<tr>
    <td colspan="4" align="center">
    <asp:Label ID="lblMsg" Text="" runat="server" CssClass="LabelCss"></asp:Label>
    </td>
</tr>

<tr>
    <td colspan="4" align="center">
    &nbsp;
    </td>
</tr>

</table>

</asp:Content>

