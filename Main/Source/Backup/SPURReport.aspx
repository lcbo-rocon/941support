<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="SPURReport.aspx.vb" Inherits="_941Support.SPURReport" 
    title="SPUR Report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
<table width="900">
<tr>
    <td></td>
    <td></td>
</tr>
    <tr>
        <td class="rprompt">Delivery Date:</td>
        <td>
            <asp:TextBox ID="txtDeliveryDate" runat="server" Columns="10" CssClass="textbox"
                MaxLength="10"></asp:TextBox>
        <a id="a1" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtDeliveryDate, 'a1', 'yyyy-MM-dd');return false;"
								href="#" name="a1"><img id="Img1" height="21" src="Images/calendar.gif" width="34" align="top" border="0"></a>
        </td>
    </tr>
    <tr>
        <td class="rprompt" style="width:50%">
            Pick Plan From:</td>
        <td class="lprompt">
            <asp:TextBox ID="txtPickPlanFrom" runat="server" Columns="6" MaxLength="6" CssClass="textbox"></asp:TextBox>
            To:
            <asp:TextBox ID="txtPickPlanTo" runat="server" Columns="6" MaxLength="6" CssClass="textbox"></asp:TextBox>&nbsp;<asp:Button
                ID="btnSubmit" runat="server" Text="Submit" CssClass="BtnCss" /></td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;</td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:Label ID="lblMsg" runat="server" CssClass="cprompt"></asp:Label></td>
    </tr>
<tr>
    <td colspan="2" align="center">
    <asp:Panel ID="pnlSummary" runat="server" Visible="false">
         <table width="600" cellpadding="0" cellspacing="0">
            <tr>
                <td><asp:Label ID="lblSummaryMsg" runat="server" CssClass="cprompt"></asp:Label></td>
            </tr>
            <tr>
                <td style="background-color:#1C5E55; font-family:Arial; font-size:larger; font-weight:bold; color:White; height: 40px;">
                    SPUR Summary
                </td>
            </tr>
            <tr>
                <td>
                    <div style="height:150px; overflow:auto; z-index:10;">
                    <asp:GridView ID="grdSummary" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        ForeColor="#333333" GridLines="None" AllowSorting="True" CellSpacing="2" Font-Names="Arial" BorderColor="#004000" BorderStyle="Solid" BorderWidth="1px">
                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#E3EAEB" />
                        <Columns>
                            <asp:BoundField DataField="pick_plan_nu" HeaderText="Pick Plan" SortExpression="Pick_Plan_nu" >
                                <ItemStyle Width="50px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="1" SortExpression="LANE1">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Label1" runat="server" Text='<%# Bind("LANE1") %>' CommandName="Details" CommandArgument='<%# Bind("pick_plan_nu") %>'></asp:LinkButton>
                                </ItemTemplate>
                                 <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="2" SortExpression="LANE2">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Label2" runat="server" Text='<%# Bind("LANE2") %>' CommandName="Details" CommandArgument='<%# Bind("pick_plan_nu") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="3" SortExpression="LANE3">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Label3" runat="server" Text='<%# Bind("LANE3") %>' CommandName="Details" CommandArgument='<%# Bind("pick_plan_nu") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="4" SortExpression="LANE4">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Label4" runat="server" Text='<%# Bind("LANE4") %>' CommandName="Details" CommandArgument='<%# Bind("pick_plan_nu") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="5" SortExpression="LANE5">
                                 <ItemTemplate>
                                    <asp:LinkButton ID="Label5" runat="server" Text='<%# Bind("LANE5") %>' CommandName="Details" CommandArgument='<%# Bind("pick_plan_nu") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="6" SortExpression="LANE6">
                                 <ItemTemplate>
                                    <asp:LinkButton ID="Label6" runat="server" Text='<%# Bind("LANE6") %>' CommandName="Details" CommandArgument='<%# Bind("pick_plan_nu") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="7" SortExpression="LANE7">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Label7" runat="server" Text='<%# Bind("LANE7") %>' CommandName="Details" CommandArgument='<%# Bind("pick_plan_nu") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="8" SortExpression="LANE8">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Label8" runat="server" Text='<%# Bind("LANE8") %>' CommandName="Details" CommandArgument='<%# Bind("pick_plan_nu") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="9" SortExpression="LANE9">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Label9" runat="server" Text='<%# Bind("LANE9") %>' CommandName="Details" CommandArgument='<%# Bind("pick_plan_nu") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="10" SortExpression="LANE10">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Label10" runat="server" Text='<%# Bind("LANE10") %>' CommandName="Details" CommandArgument='<%# Bind("pick_plan_nu") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Null" SortExpression="NA">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LabelNA" runat="server" Text='<%# Bind("LANENA") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total" SortExpression="Total">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotal" runat="server" Text='<%# Bind("Total") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#7C6F57" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
       
    </td>
</tr>
    <tr>
        <td align="center" colspan="2">
            &nbsp;</td>
    </tr>
    <tr>
        <td align="center" colspan="2">
        <asp:Panel ID="pnlDetails" runat="server" Visible="false" >
        <table width="650" cellpadding="0" cellspacing="0">
        <tr>
                <td width="70%" align="right"><asp:Label ID="lblDetailsMsg" runat="server" CssClass="rtitle"></asp:Label></td>
                <td align="right"><asp:Button id="btnExportPDF" runat="server" CssClass="BtnCss" Text="Export to PDF" Width="150px"></asp:Button></td>
            </tr>
        <tr>
            <td style="background-color: #336666; font-weight:bold; color:White; font-family:Arial; font-size:larger; height: 40px;" align="center" colspan="2">SPUR Details</td>
        </tr>
        <tr>
            <td style="background-color: #336666;" align="center"><asp:Label ID="lblLaneNo" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="White"></asp:Label></td>
            <td style="background-color: #336666;" align="center"><asp:Label ID="lblPickPlanNu" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="White"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="2"> 
            <div style="height:250px; width:650px; overflow:auto; z-index:20;">
            <asp:GridView ID="grdDetails" runat="server" BackColor="White" BorderColor="#336666" Width="630px"
                BorderStyle="Solid" BorderWidth="3px" CellPadding="4" GridLines="None" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="2" Font-Names="Arial">
                <FooterStyle BackColor="White" ForeColor="#333333" />
                <RowStyle BackColor="White" ForeColor="#333333" />
                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                <Columns>
                    <asp:BoundField DataField="load_order_seq" HeaderText="Pallet" SortExpression="load_order_seq" />
                    <asp:BoundField DataField="pick_cont_group_label_id" HeaderText="Case ID" SortExpression="pick_cont_group_label_id" />
                    <asp:BoundField DataField="stop_seq_no" HeaderText="Stop " SortExpression="stop_seq_no" />
                    <asp:BoundField DataField="PartNo" HeaderText="Item/Part" SortExpression="PartNo" >
                        <ItemStyle Width="200px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="im_desc" HeaderText="Item Description" SortExpression="im_desc" >
                        <ItemStyle Font-Size="Smaller" HorizontalAlign="Left" Width="250px" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
        </div>
        </td>
        </tr>
        </table>
        </asp:Panel>
        </td>
    </tr>
</table>
</asp:Content>
