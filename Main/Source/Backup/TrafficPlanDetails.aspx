<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="TrafficPlanDetails.aspx.vb" Inherits="_941Support.TrafficPlanDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
 <script type="text/javascript">
        function SelectAll(id)
        {
            //get reference of GridView control
            var grid = document.getElementById("<%= grdTrafficPlan.ClientID %>");
            //variable to contain the cell of the grid
            var cell;
            
            if (grid.rows.length > 0)
            {
                //loop starts from 1. rows[0] points to the header.
                for (i=1; i<grid.rows.length; i++)
                {
                    //get the reference of first column
                    cell = grid.rows[i].cells[0];
                    
                    //loop according to the number of childNodes in the cell
                    for (j=0; j<cell.childNodes.length; j++)
                    {           
                        //if childNode type is CheckBox                 
                        if (cell.childNodes[j].type =="checkbox")
                        {
                        //assign the status of the Select All checkbox to the cell checkbox within the grid
                            cell.childNodes[j].checked = document.getElementById(id).checked;
                        }
                    }
                }
            }
        }
 </script>	
 
 <asp:Panel ID="pnl" runat="server" DefaultButton="btnSave">  
<table cellspacing="0" cellpadding="0" align="center" border="0" style="width: 90%">

<tr>
    <td colspan="5" align="right">
    
    <asp:Button ID="btnPrev" runat="server" CssClass="BtnCss" Text="Previous" /> 
    <asp:Button ID="btnSave" runat="server" CssClass="BtnCss" Text="Save" />
    <asp:Button ID="btnSend" runat="server" CssClass="BtnCss" Text="Send Orders" />
    <asp:Button ID="btnReset" runat="server" CssClass="BtnCss" Text="Reset" />
    &nbsp;    
    </td>
</tr>

<tr><td colspan="5"><br /></td></tr>

<tr>
    <td colspan="2">&nbsp;</td>
    <td colspan="3" align="center">
    <asp:Label ID="lblMessage" Text = "" runat="server" CssClass="LblCss"></asp:Label>
    </td>    
</tr>

<tr>    
    <td valign="top">
    <div style="z-index: 10; overflow: auto;height:450px">
    <asp:GridView ID="grdRoute" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" 
            CellPadding="3" CellSpacing="1" Font-Names="Arial" GridLines="None" DataKeyNames="Route_Code,Route" >
            <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
            <RowStyle BackColor="White" ForeColor="Black" Font-Size="Small" />
            <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#CBD49E" Font-Size="Small" ForeColor="Maroon" />
            <Columns>
                <asp:BoundField DataField="Route" HeaderText="Route" Visible="False" />
            <asp:TemplateField HeaderText="Route">
                <ItemTemplate>
                  <asp:LinkButton ID="lnkRoute" runat="server" Text='<%# Bind("Route_Code") %>' ></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Orders">
                <ItemTemplate>
                  <asp:Label ID="lblOrders" runat="server" Text='<%# Bind("Orders") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cases">
                <ItemTemplate>
                   <asp:Label ID="lblCases" runat="server" Text='<%# Bind("Cases") %>' ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>            
        </Columns>
    </asp:GridView>
    </div>
    </td>
    
    <td valign="top">
    &nbsp;&nbsp;      
    </td>
    
    <td colspan="3" align="left" valign="top">
        <div style="z-index: 10; overflow: auto;height:550px">
            <asp:GridView ID="grdTrafficPlan" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="1px" 
            CellPadding="0" CellSpacing="0" Font-Names="Arial" GridLines="Horizontal" Width="635px"  >
            <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
            <RowStyle BackColor="White" ForeColor="Black" Font-Size="Smaller" />
            <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#CBD49E" Font-Size="Small" ForeColor="Maroon" />            
                <Columns>
                    <asp:TemplateField HeaderText="Send">
                    <HeaderTemplate>
                    <asp:Checkbox ID="chkSelectAll" runat="server" checked="false"/>
                    <asp:Label ID="lblSendAll" runat="server" CssClass="labelCss" Text="Send"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                    <asp:Checkbox ID="chkSend" runat="server" checked="false"/>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Route Code" SortExpression="Route_Code">                    
                    <ItemTemplate>
                    <asp:TextBox ID="txtRt" runat="server" Width="50px"></asp:TextBox>
                      <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" 
                        TargetControlID="txtRt"
                        ServicePath="WebService1.asmx"
                        ServiceMethod="getRouteCodes"
                        MinimumPrefixLength="1" 
                        CompletionSetCount="10" 
                        runat="server">
                        </cc1:AutoCompleteExtender>
                    <asp:dropdownList id="drpRt" runat="server" Visible="false" />
                    <asp:Label ID="lblRt" Visible="false" runat="server" Text='<%# Bind("Route_Code") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Stop" SortExpression="StopNo">
                    <ItemTemplate>
                    <asp:TextBox ID="txtSn"   runat="server" Width="30px"></asp:TextBox>
                    <asp:Label ID="lblSn"  Visible="false" runat="server" Text='<%# Bind("StopNo") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Order No" SortExpression="OrderNo">                        
                        <ItemTemplate>
                            <asp:Label ID="lblOrder" runat="server" Text='<%# Bind("OrderNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Order Type" SortExpression="OrderType">                        
                        <ItemTemplate>
                            <asp:Label ID="lblOrderType" runat="server" Text='<%# Bind("OrderType") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                    
                    <asp:BoundField DataField="CustomerNo" HeaderText="Customer No" SortExpression="CustomerNo" />
                    <asp:BoundField DataField="Cases" HeaderText="Cases" SortExpression="Cases" />
                    <asp:BoundField DataField="ReqdDate" HeaderText="Required Date" SortExpression="ReqdDate" />
                </Columns>
                <AlternatingRowStyle BackColor="LightGoldenrodYellow" />
            </asp:GridView>
        </div>       
    </td>
</tr>

<tr><td colspan="5">&nbsp;</td></tr>



<tr>
    <td colspan="2">&nbsp;</td>
    <td colspan="3" align="center">
    <asp:Label ID="lblMsg" Text = "" runat="server" CssClass="LblCss"></asp:Label>
    </td>    
</tr>

</table>
</asp:Panel>
</asp:Content>
