Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Imports System.IO.Directory
Imports System.Data
Imports System.Security.Principal
Imports System.Threading
Imports RSG_ROC.DataLayer
Imports RSG_ROC.Exceptions
Imports RSG_ROC.OrderClasses
Imports RSG_ROC.GLClasses

Partial Public Class FreightForwarding
    Inherits System.Web.UI.Page

    Private Sub FreightForwarding_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Dim ex As Exception = Server.GetLastError
        Session("ExceptionObj") = ex
        Response.Redirect("~\ErrorPage.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lblheading As Label = Master.FindControl("lblMainTitle")
        If Not lblheading Is Nothing Then
            lblheading.Text = "Freight Forwarding"
        End If
        If Not IsPostBack Then
            Dim whereclause As String = ""
            Dim dt As DataTable = Nothing

            If Session("FreightFwdCustomer") <> "" Then
                txtCustomer.Text = Session("FreightFwdCustomer")
            End If
            If Session("FreightFwdOrder") <> "" Then
                txtOrder.Text = Session("FreightFwdOrder")
            End If
            If Session("FreightFwdDate") <> "" Then
                txtInvoiceDate.Text = Session("FreightFwdDate")
            End If

            If Session("FreightFwdWhere") <> "" Then
                whereclause = Session("FreightFwdWhere")
            End If

            If Session("FreightFwdCustomer") <> "" Then
                dt = DALRoc.getCustomerOrderList(Session("FreightFwdCustomer"))
                Dim dv As New DataView(dt, whereclause, "cu_no,co_odno", DataViewRowState.CurrentRows)
                grdForwarding.DataSource = dv
                grdForwarding.DataBind()
            End If
            lblMsg.Text = Session("FreightFwdResult")
        End If
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Session("FreighFwdCustomer") = ""
        Session("FreighFwdOrder") = ""
        Session("FreighFwdDate") = ""
        Session("FreighFwdWhere") = ""
        Response.Redirect("~\FreightForwarding.aspx")
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblMsg.Text = ""
        lblMessage.Text = ""
        Dim valid_cust As Boolean = False
        If txtCustomer.Text.Trim <> "" Then
            valid_cust = DALRoc.isValidCustomer(txtCustomer.Text)
        End If
        If Not valid_cust Then
            lblMessage.Text = "Please enter a valid customer number"
        Else
            Dim cu_no As String = txtCustomer.Text.Trim
            Dim dt As DataTable = Nothing
            Dim whereclause As String = ""

            If txtOrder.Text.Trim <> "" And txtInvoiceDate.Text.Trim <> "" Then
                whereclause = "co_odno='" & txtOrder.Text.Trim & "' AND inv_dt>#" & txtInvoiceDate.Text.Trim & " 00:00:00# and inv_dt<#" & txtInvoiceDate.Text.Trim & " 23:59:59#"
            ElseIf txtOrder.Text.Trim <> "" Then
                whereclause = "co_odno='" & txtOrder.Text.Trim & "'"
            ElseIf txtInvoiceDate.Text.Trim <> "" Then
                whereclause = "inv_dt>#" & txtInvoiceDate.Text.Trim & " 00:00:00# and inv_dt<#" & txtInvoiceDate.Text.Trim & " 23:59:59#"
            End If

            Session("FreightFwdCustomer") = txtCustomer.Text.Trim
            Session("FreightFwdOrder") = txtOrder.Text.Trim
            Session("FreightFwdDate") = txtInvoiceDate.Text.Trim
            Session("FreightFwdWhere") = whereclause
            dt = DALRoc.getCustomerOrderList(cu_no)
            Dim dv As New DataView(dt, whereclause, "inv_dt desc", DataViewRowState.CurrentRows)
            Session("FreightFwdSummary") = dv
            grdForwarding.DataSource = Session("FreightFwdSummary")
            grdForwarding.DataBind()
        End If
    End Sub

    Private Sub grdForwarding_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdForwarding.DataBound
        If grdForwarding.Rows.Count = 0 Then
            lblMessage.Text = "There were no records that match your search criteria"
        ElseIf grdForwarding.Rows.Count > 0 Then
            lblMessage.Text = grdForwarding.Rows.Count & " record(s) match your search criteria"
            For Each tcell As TableCell In grdForwarding.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
            Dim drow As GridViewRow
            Dim lnkForward As LinkButton = Nothing
            For Each drow In grdForwarding.Rows
                'If the order is forwarded already, remove the link
                lnkForward = drow.FindControl("lnkForward")
                If DALRoc.isFreightForward(drow.Cells(2).Text, drow.Cells(0).Text) Then
                    lnkForward.Visible = False
                End If
            Next
        End If
    End Sub

    Private Sub grdForwarding_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdForwarding.RowCommand
        If e.CommandName = "Forward" Then
            Dim co_odno As String = ""
            co_odno = e.CommandArgument.ToString.Trim
            Session("FreightFwdOrder") = co_odno

            Response.Redirect("~\FreightForwardDetails.aspx")
        End If
    End Sub
   
    Private Sub grdForwarding_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdForwarding.Sorting
        Dim dv As DataView = Session("FreightFwdSummary")
        Dim order As String = ""
        If Not ViewState("SSOrder") Is Nothing Then
            order = ViewState("SSOrder")
        End If

        If order.StartsWith(e.SortExpression) Then
            If order.ToString.EndsWith("ASC") Then
                order = "DESC"
            Else
                order = "ASC"
            End If
        Else
            order = "ASC"
        End If

        ViewState("SSOrder") = e.SortExpression + " " & order
        dv.Sort = ViewState("SSOrder")
        grdForwarding.DataSource = dv
        grdForwarding.DataBind()
    End Sub
End Class