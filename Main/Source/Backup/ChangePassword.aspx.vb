Imports System.Data.OracleClient
Imports System.Data

Partial Public Class ChangePassword
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.pnlPassword.Visible = False
            Me.txtUserID.Focus()
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim where As String = ""
        Dim qry As String = " SELECT EM_NO AS USERID, EM_NAME AS USERNAME FROM DBO.EMMAS " & _
                            " WHERE EM_NO NOT IN (    " & _
                            " 		Select em_no from DBO.EMMAS		 	" & _
                            " 		where upper(em_no) like 'EMP%'	" & _
                            " 		OR upper(em_no) = 'LCBO'		" & _
                            " 		OR UPPER(EM_NO) = 'DBO'		" & _
                            "       OR UPPER(EM_NO) LIKE 'IT%' " & _
                            " 		OR UPPER(TRIM(EM_NO)) LIKE '%HH'" & _
                            " 		OR upper(em_no) = 'ROC8'				" & _
                            " 		OR upper(em_no) = 'ADMN'				" & _
                            " 		OR UPPER(EM_no) = 'ROOT'				" & _
                            "       OR UPPER(EM_NO) = 'ARCHIVE') " & _
                            " AND EM_NAME IS NOT NULL							" & _
                            " AND MENU_ACCESS_GRP_DESKTOP <> 'N'	" & _
                            " AND EM_NAME NOT LIKE '%INVENTORY%'	" & _
                            " AND UPPER(TRIM(em_no)) in (SELECT UPPER(TRIM(USERNAME)) FROM SYS.DBA_USERS) "

        Dim qry2 As String = " SELECT EM_NO AS USERID, EM_NAME AS USERNAME FROM DBO.ON_EMP_MAS " & _
                            " WHERE EM_NO NOT IN (    " & _
                            " 		Select em_no from DBO.ON_EMP_MAS	 	" & _
                            " 		where upper(em_no) like 'EMP%'	" & _
                            " 		OR upper(em_no) = 'LCBO'		" & _
                            " 		OR UPPER(EM_NO) = 'DBO'		" & _
                            "       OR UPPER(EM_NO) LIKE 'IT%' " & _
                            " 		OR UPPER(TRIM(EM_NO)) LIKE '%HH'" & _
                            " 		OR upper(em_no) = 'ROC8'				" & _
                            " 		OR upper(em_no) = 'ADMN'				" & _
                            " 		OR UPPER(EM_no) = 'ROOT'				" & _
                            "       OR UPPER(EM_NO) = 'ARCHIVE') " & _
                            " AND EM_NAME IS NOT NULL							" & _
                            " AND MENU_ACCESS_GRP_DESKTOP <> 'N'	" & _
                            " AND EM_NAME NOT LIKE '%INVENTORY%'	" & _
                            " AND UPPER(TRIM(em_no)) in (SELECT UPPER(TRIM(USERNAME)) FROM SYS.DBA_USERS) "
        Dim dt As DataTable = Nothing

        If Me.txtUserID.Text.Trim <> "" Then
            where = " upper(trim(em_no)) = '" & txtUserID.Text.Trim.ToUpper & "' "
        End If
        If Me.txtUsername.Text.Trim <> "" Then
            If where <> "" Then
                where = where & " AND "
            End If
            where = " upper(trim(em_name)) LIKE '%" & txtUsername.Text.Trim.ToUpper & "%'"
        End If

        If where <> "" Then
            qry = qry & " AND " & where
            qry2 = qry2 & " AND " & where
        End If

        qry = qry & " UNION " & qry2 & " ORDER BY USERID ASC "

        dt = SPURDataControl.getData(qry)
        Session("tblUsers") = dt
        grdUsers.DataSource = Session("tblUsers")
        grdUsers.DataBind()

        ClearPassword()
        pnlPassword.Visible = False
    End Sub

    Private Sub grdUsers_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdUsers.DataBound
        If grdUsers.Rows.Count > 0 Then
            For Each tcell As TableCell In grdUsers.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
        End If
        lblMsg.Text = "There are " & grdUsers.Rows.Count & " record(s) found that matches your search."
    End Sub

    Private Sub grdUsers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdUsers.RowCommand
        If e.CommandName.ToUpper = "CHANGEPASSWORD" Then
            ClearPassword()
            Dim grow As GridViewRow = grdUsers.Rows(e.CommandArgument)
            Dim lblSelectedUserID As Label = grow.FindControl("lblChangeUserID")
            Dim lblSelectedUserName As Label = grow.FindControl("lblChangeUserName")

            lblUserID.Text = lblSelectedUserID.Text
            lblUserName.Text = lblSelectedUserName.Text


            Me.pnlPassword.Visible = True
            Me.txtNewPassword.Focus()
        End If
    End Sub

    Private Sub ClearPassword()
        lblUserID.Text = ""
        lblUserName.Text = ""
        txtNewPassword.Text = ""
        txtConfirm.Text = ""
    End Sub

    Protected Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Response.Redirect("ChangePassword.aspx")
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ClearPassword()
        Me.pnlPassword.Visible = False
    End Sub

    Protected Sub btnChangePassword_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangePassword.Click
        'Validate if confirm and new password are the same
        Dim valid As Boolean = True
        If txtNewPassword.Text.Trim = "" Then
            valid = False
            lblMsg.Text = "ERROR: Password cannot be blank!"
            txtNewPassword.CssClass = "etextbox"
            txtNewPassword.Focus()
        End If
        'check if password contains
        If Regex.IsMatch(txtNewPassword.Text, "^[A-Za-z][0-9A-Za-z]+$") = False Then
            valid = False
            lblMsg.Text = "ERROR: Password must start with an alphabetic letter and contains only letters and numbers!"
            txtNewPassword.CssClass = "etextbox"
            txtNewPassword.Focus()
        End If
        If valid = True Then
            If txtNewPassword.Text = txtConfirm.Text Then
                If SPURDataControl.changeROCONPassword(User.Identity.Name, lblUserID.Text, txtNewPassword.Text) = True Then
                    lblMsg.Text = "User: " & lblUserID.Text & " password changed successfully!<br>NOTE: the new password won't take in effect until the next login!"
                Else
                    lblMsg.Text = "Error changing password for user: " & lblUserID.Text & "!!!"
                End If
            Else
                lblMsg.Text = "The New Password entered does not match confirmed password entered! Please re-enter."
                txtNewPassword.CssClass = "etextbox"
                txtConfirm.CssClass = "etextbox"
            End If
        End If

    End Sub

    Private Sub btnCancel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Load
        Dim lblheading As Label = Master.FindControl("lblMainTitle")
        If Not lblheading Is Nothing Then
            lblheading.Text = "ROC/ROCON Password Change"
        End If

        txtNewPassword.CssClass = "textbox"
        txtConfirm.CssClass = "textbox"
        lblMsg.Text = ""
    End Sub

    Private Sub grdUsers_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdUsers.Sorting
        Dim dv As DataView = New DataView(Session("tblUsers"))
        Dim order As String = ""
        If Not ViewState("UsrOrder") Is Nothing Then
            order = ViewState("UsrOrder")
        End If

        If order.StartsWith(e.SortExpression) Then
            If order.ToString.EndsWith("ASC") Then
                order = "DESC"
            Else
                order = "ASC"
            End If
        Else
            order = "ASC"
        End If

        ViewState("UsrOrder") = e.SortExpression + " " & order
        dv.Sort = ViewState("UsrOrder")
        grdUsers.DataSource = dv
        grdUsers.DataBind()
    End Sub
End Class