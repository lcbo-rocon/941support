Public Partial Class ErrorPage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ex As Exception = Session("ExceptionObj")
        If Not ex Is Nothing Then
            lblMessage.Text = "An error has occured: <br/>" & ex.Message.ToString & "<br/> Please contact RSG-HOST support."
        End If
    End Sub

End Class