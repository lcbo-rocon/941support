Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Imports System.IO.Directory
Imports System.Data
Imports System.Security.Principal
Imports System.Threading
Imports RSG_ROC.DataLayer
Imports RSG_ROC.Exceptions
Imports RSG_ROC.OrderClasses
Imports RSG_ROC.LS_TRANSFERS
Imports System.Text.RegularExpressions
Partial Public Class CatchAllSkus
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        DALRoc.setEnv(ConfigurationManager.AppSettings("env"))
        grdCatchAllSkus.DataSource = DALRoc.getCatchAllSkus
        grdCatchAllSkus.DataBind()
    End Sub

    'Private Sub grdCatchAllSkus_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdCatchAllSkus.RowCommand

    'Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    'Dim row As GridViewRow = grdCatchAllSkus.Rows(index)
    'Dim lnkSelect As LinkButton = Nothing
    'Dim itemno As Integer = 0
    'Dim itemprice As Decimal = Nothing

    'lnkSelect = grdCatchAllSkus.Rows(index).FindControl("lnkSelect")
    'itemno = grdCatchAllSkus.DataKeys.Item(index).Values(0).ToString.Trim
    'itemprice = grdCatchAllSkus.DataKeys.Item(index).Values(1)

    ''Session("CatchAllItem") = itemno
    ''Session("CatchAllPrice") = itemprice
    ''ClientScript.RegisterStartupScript(Me.GetType(), "RefreshParent", "<script language=JavaScript>window.opener.document.forms[0].submit();window.close();</script>", False)


    'End Sub

    Private Sub grdCatchAllSkus_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdCatchAllSkus.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnkSelect As LinkButton = Nothing
            Dim item_no As Integer = 0
            Dim unit_price As Decimal = 0
            item_no = grdCatchAllSkus.DataKeys(e.Row.RowIndex).Values.Item(0).trim
            unit_price = grdCatchAllSkus.DataKeys(e.Row.RowIndex).Values.Item(1)
            lnkSelect = e.Row.FindControl("lnkSelect")
            lnkSelect.Attributes.Add("onclick", "closeDialog(" & item_no & "," & unit_price & ")")
        End If
    End Sub
End Class