'Class RestartRRPS
'----------------------------------------------------------------------------------------------
' This page displays the interface for user to restart the print label service
'----------------------------------------------------------------------------------------------
' Created By: Ria Bhatnagar, LCBO-RSG
' Date: May, 2008
'----------------------------------------------------------------------------------------------
' Updated By: Shirley Lam, LCBO-RSG
' Date: June 15, 2008
' Use MainTitle.ascx instead for the Heading and dynamically change the title in Page Load
' Changed message to read from web.config file for number of minutes to wait.
'----------------------------------------------------------------------------------------------

Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState

Partial Class RestartRRPS
    Inherits System.Web.UI.Page

    Protected Sub btnResetLabel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetLabel.Click
        Dim currentUser As String = User.Identity.Name.Substring(User.Identity.Name.IndexOf("\") + 1)

        Dim strSession As String = ""
        Dim instance As HttpSessionState = HttpContext.Current.Session

        Dim env As String = System.Configuration.ConfigurationManager.AppSettings("env")

        'Getting the current Session ID, this is used later to name the file with details to reset the user 
        strSession = instance.SessionID

        Dim fp As StreamWriter
        'Create a file in the \Data folder with the format sessionid_userid.pk
        'This file is checked by the perl script at whm001 every 3mins and if this file exists the
        'label print service is restarted.
        Try
            fp = File.CreateText(Server.MapPath(".\Data\" & strSession & "_" & currentUser.ToLower & ".pk"))
            fp.Close()
            Dim minutes As String = ConfigurationManager.AppSettings("JobTime")
            lblMessage.Text = "The Service to resolve the label printing issue will restart in " & minutes & " minute(s)."
        Catch err As Exception
            lblMessage.Text = ConfigurationManager.AppSettings("GenericErrMsg")
        End Try




    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Text = ""
        Dim lblHeading As Label = Master.FindControl("lblMainTitle")
        If Not lblHeading Is Nothing Then
            lblHeading.Text = "Restart Label Service"
        End If
    End Sub
End Class
