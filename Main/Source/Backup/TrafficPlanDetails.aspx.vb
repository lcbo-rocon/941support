Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Imports System.IO.Directory
Imports System.Data
Imports System.Security.Principal
Imports System.Threading

Partial Public Class TrafficPlanDetails
    Inherits System.Web.UI.Page

    Private Sub TrafficPlanDetails_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Dim ex As Exception = Server.GetLastError
        Session("ExceptionObj") = ex
        Response.Redirect("~\ErrorPage.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim lblheading As Label = Master.FindControl("lblMainTitle")
        If Not lblheading Is Nothing Then
            lblheading.Text = "Traffic Planning"
        End If
        If Not IsPostBack Then
            Dim dt As DataTable = Nothing
            Dim route As String = ""
            Dim stopNo As String = ""
            Dim customerNo As String = ""
            Dim orderNo As String = ""
            Dim carrier As String = ""
            Dim shipDate As String = ""
            Dim orderType As String = ""
            Dim whereclause As String = ""

            dt = Session("TrafficPlanTbl")

            route = dt.Rows(0).Item("Route")
            stopNo = dt.Rows(0).Item("StopNo")
            customerNo = dt.Rows(0).Item("CustomerNo")
            orderNo = dt.Rows(0).Item("OrderNo")
            carrier = dt.Rows(0).Item("Carrier")
            shipDate = dt.Rows(0).Item("ShipDate")
            orderType = dt.Rows(0).Item("OrderType")

            'Code to create the whereclause
            For i As Integer = 0 To dt.Columns.Count - 1
                If dt.Rows(0).Item(i) <> "" Then
                    whereclause = whereclause & " AND " & dt.Rows(0).Item(i)
                End If
            Next
            lblMessage.Text = ""
            lblMsg.Text = ""
            Session("TrafficPlanDet") = whereclause
            'Load the gridviews with Routes and the corresponding orders
            'the first time the orders shown should be for the first route only
            getGrdTrafficPlan()
            getGrdRoute()
        End If
    End Sub

    Protected Function getOraConnection() As OracleConnection
        Dim conn As OracleConnection = Nothing
        Dim env As String = ConfigurationManager.AppSettings("env")
        conn = New OracleConnection(ConfigurationManager.ConnectionStrings(env).ToString)
        Return conn
    End Function
    'If there is no route that matches the search criteria then just display the grid header
    Private Sub grdRoute_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdRoute.DataBound
        If grdRoute.Rows.Count > 0 Then
            For Each tcell As TableCell In grdRoute.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
        Else
            'In case there are no rows that match the search criteria
            'below code will populate the Route gridview so that only the header is visible
            Dim connection As OracleConnection = getOraConnection()
            Dim tblEmpty As New DataTable
            Session("RowCountTrafficPlan") = 1
            Dim Adp = New OracleDataAdapter("Select 1 as Route, 1 as Route_Code, 1 As Orders, 1 As Cases from dual ", connection)
            Try
                Adp.Fill(tblEmpty)
            Catch ex As Exception
                Adp.Dispose()
                Throw ex
            End Try
            Adp.Dispose()
            grdRoute.DataSource = tblEmpty
            grdRoute.DataBind()
            grdRoute.Rows(0).Visible = False
        End If
    End Sub
    'Everytime a new Route is clicked three things happen
    '1. The checked orders in the previous routes need to be recorded in a string
    '2. All the stops and routes changed for any order in the previous route needs to be saved
    '3. The number of orders and cases in the last route need to be calculated, recorded and displayed
    Private Sub grdRoute_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdRoute.RowCommand
        Session("TPDFinalOrders") = Nothing
        setCheckedOrders()
        setRouteStop()
        getSelectedOrders()
        Dim route_code As String = ""
        Dim route As String = ""
        Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
        route_code = grdRoute.DataKeys(row.RowIndex).Values(0)
        route = grdRoute.DataKeys(row.RowIndex).Values(1)
        Session("TPDLastRouteClicked") = route

        getGrdRoute()
        getGrdTrafficPlan(route)
        
    End Sub
    'Display the route codes as a drop down list in the Traffic Planning Grid
    Private Sub grdTrafficPlan_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdTrafficPlan.DataBound
        Dim conn As OracleConnection = getOraConnection()
        Dim tblRC As New DataTable
        Dim drpRoute As DropDownList = Nothing
        Dim lblRoute As Label = Nothing
        Dim txtStop As TextBox = Nothing
        Dim txtRt As TextBox = Nothing
        Dim lblStop As Label = Nothing
        Dim whereclause As String = ""
        whereclause = Session("TrafficPlanDet")

        ''Get the list of Route Codes Dropdownlist to choose from, incase Route code needs to be changed
        'Dim Adapter = New OracleDataAdapter("Select distinct trim(ROUTE_CODE) as Route, trim(ROUTE_CODE) As Route_Code " & _
        '"from DBO.ROUTE_CODE_MAS WHERE DELETE_Y_OR_N IS NULL OR  DELETE_Y_OR_N = 'N' order by 1", conn)
        'Try
        '    Adapter.Fill(tblRC)
        'Catch ex As Exception
        '    Adapter.Dispose()
        'End Try
        'Adapter.Dispose()

        'populate the dropdown from the routes obtained from the above query
        For Each drow As GridViewRow In grdTrafficPlan.Rows
            'drpRoute = drow.FindControl("drpRt")
            lblRoute = drow.FindControl("lblRt")
            txtStop = drow.FindControl("txtSn")
            txtRt = drow.FindControl("txtRt")
            lblStop = drow.FindControl("lblSn")
            txtRt.Text = lblRoute.Text
            txtStop.Text = lblStop.Text
            'drpRoute.DataSource = tblRC
            'drpRoute.DataTextField = tblRC.Columns("route_code").ToString
            'drpRoute.DataValueField = tblRC.Columns("route_code").ToString
            'drpRoute.SelectedValue = lblRoute.Text
            'drpRoute.DataBind()
        Next

        If Not Session("TPDFinalOrders") Is Nothing Then
            Dim orderstring = Session("TPDFinalOrders")
            lblMsg.Text = "The following order(s) have been Traffic Planned: <br/>" & orderstring
        Else
            lblMsg.Text = ""
        End If

        If grdTrafficPlan.Rows.Count > 0 Then
            For Each tcell As TableCell In grdTrafficPlan.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
            If Not Session("RowCountTrafficPlan") Is Nothing And Session("RowCountTrafficPlan") = "1" Then
                lblMessage.Text = grdTrafficPlan.Rows.Count - 1 & " row(s) match your search criteria"
            Else
                lblMessage.Text = grdTrafficPlan.Rows.Count & " row(s) match your search criteria"
            End If
            Session("RowCountTrafficPlan") = 0

        Else
            'In case there are no rows that match the search criteria
            'below code will populate the Traffic Planning gridview so that only the header is visible
            Dim connection As OracleConnection = getOraConnection()
            Dim tblEmpty As New DataTable
            Session("RowCountTrafficPlan") = 1
            Dim Adp = New OracleDataAdapter("Select trim(ROUTE_CODE) as Route_Code, '1' As StopNo, '1' as OrderNo, '1' as OrderType, " & _
                                            "'1' as CustomerNo, '1' as Cases, '1' as ReqdDate from DBO.ROUTE_CODE_MAS " & _
                                            "WHERE (DELETE_Y_OR_N IS NULL OR  DELETE_Y_OR_N = 'N') and rownum=1", connection)
            Try
                Adp.Fill(tblEmpty)
            Catch ex As Exception
                Adp.Dispose()
                Throw ex
            End Try
            Adp.Dispose()
            grdTrafficPlan.DataSource = tblEmpty
            grdTrafficPlan.DataBind()
            grdTrafficPlan.Rows(0).Visible = False
        End If
    End Sub
    'checking for any product or tender restrictions and displaying the selected orders as checked
    Private Sub grdTrafficPlan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdTrafficPlan.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            Dim chkAll As CheckBox = Nothing
            chkAll = e.Row.FindControl("chkSelectAll")
            chkAll.Attributes.Add("onclick", "javascript:SelectAll('" & chkAll.ClientID & "')")
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblOrder As Label = Nothing
            Dim chkSend As CheckBox = Nothing
            Dim drpRoute As DropDownList = Nothing
            Dim lblOrderType As Label = Nothing
            Dim OrderNo As String = ""
            Dim tempOrders As String = ""
            Dim ords As String() = Nothing
            chkSend = e.Row.FindControl("chkSend")
            drpRoute = e.Row.FindControl("drpRT")
            lblOrder = e.Row.FindControl("lblOrder")
            lblOrderType = e.Row.FindControl("lblOrderType")

            If Not lblOrder Is Nothing Then
                OrderNo = lblOrder.Text.Trim
                Dim productCtr As Boolean = True
                Dim tenderCtr As Boolean = True
                If lblOrderType.Text.Trim.ToUpper = "AGY" Then
                    productCtr = SPURDataControl.validateOrderProductRestriction(OrderNo, lblOrderType.Text.Trim, "E")
                    tenderCtr = SPURDataControl.validateTenderRestriction(OrderNo, lblOrderType.Text.Trim, "")
                    If productCtr = False Or tenderCtr = False Then
                        e.Row.BackColor = Drawing.Color.Red
                        chkSend.Enabled = False
                        drpRoute.Enabled = False
                    End If
                End If
                If Not Session("TPDOrderString") Is Nothing Then
                    tempOrders = Session("TPDOrderString")
                    ords = tempOrders.Split(",")
                    For Each ord As String In ords
                        If ord.Trim = OrderNo Then
                            chkSend.Checked = True
                        End If
                    Next
                End If
            End If
        End If
    End Sub
    'Private Sub setSavedStops()
    '    Dim drow As GridViewRow = Nothing
    '    Dim route_code As String = ""
    '    Dim route As DropDownList = Nothing
    '    Dim txtStop As TextBox = Nothing
    '    Dim stop_no As String = ""
    '    Dim order_number As String = ""
    '    Dim order As Label = Nothing
    '    Dim stmt As String = ""

    '    For Each drow In grdTrafficPlan.Rows
    '        route = drow.FindControl("drpRt")
    '        route_code = route.SelectedValue

    '        txtStop = drow.FindControl("txtSn")
    '        stop_no = txtStop.Text

    '        order = drow.FindControl("lblOrder")
    '        order_number = order.Text.Trim

    '        stmt = "Update dbo.on_cohdr set route_code='" & route_code & "', route_stop='" & stop_no & "' " & _
    '                "where co_odno='" & order_number.Trim & "'"
    '        processStatement(stmt)
    '    Next

    '    getGrdTrafficPlan(route_code)
    '    getGrdRoute()
    'End Sub

    'Private Sub processStatement(ByVal stmt As String)
    '    Dim conn As OracleConnection = getOraConnection()
    '    Dim cmd As OracleCommand = Nothing
    '    Try
    '        conn.Open()
    '        cmd = New OracleCommand(stmt, conn)
    '        cmd.ExecuteNonQuery()
    '    Catch ex As Exception
    '        cmd.Dispose()
    '        conn.Close()
    '    End Try
    '    cmd.Dispose()
    '    conn.Close()
    'End Sub
    Private Sub getGrdTrafficPlan()
        Dim whereclause As String = ""
        Dim conn As OracleConnection = getOraConnection()
        Dim dataTbl As New DataTable
        Dim tblRoute As New DataTable
        whereclause = Session("TrafficPlanDet")

        Dim adapter = New OracleDataAdapter("SELECT trim(MAX(NVL(ON_COHDR.ROUTE_CODE, CUMAS.ROUTE_CODE))) As Route_Code, " & _
         "CASE Trim(ON_COHDR.ROUTE_STOP) WHEN '0' THEN TO_NUMBER('') ELSE TO_NUMBER(Trim(ON_COHDR.ROUTE_STOP))  END As StopNo, TRIM(ON_COHDR.CO_ODNO) As OrderNo, ON_COHDR.ORDER_TYPE as OrderType,  MAX(NVL(ON_COHDR.SHIP_TO, ON_COHDR.CU_NO)) As CustomerNo, " & _
          "CEIL(SUM(ON_CODTL.TOTAL_ALLOCATED_QTY / DECODE(NVL(glob_item.CASE_SZ, 1), 0, 1, NVL(glob_item.CASE_SZ, 1)))) As Cases," & _
         "TO_CHAR(ON_COHDR.order_reqd_dt,'YYYY-MM-DD') As ReqdDate " & _
         "FROM ON_COHDR, CUMAS, ON_CODTL, glob_item, ON_SITE_MAS  " & _
         "WHERE ON_CODTL.CO_ODNO = ON_COHDR.CO_ODNO AND ON_CODTL.DELETED_FL != 'R' AND glob_item.ITEM = ON_CODTL.ITEM AND ON_COHDR.CO_STATUS = 'O' " & _
         "AND glob_item.PACKAGING_CODE = ON_CODTL.PACKAGING_CODE  AND NVL(ON_COHDR.SHIP_TO, ON_COHDR.CU_NO) = CUMAS.CU_NO " & _
         "AND ON_SITE_MAS.SITE=ON_COHDR.SITE AND ON_SITE_MAS.TYPE_FL='I'  " & whereclause & " " & _
           " AND on_COHDR.ROUTE_CODE = (select min(NVL(ON_COHDR.ROUTE_CODE, CUMAS.ROUTE_CODE)) " & _
        "FROM ON_COHDR, CUMAS, ON_CODTL, IMMAS, ON_SITE_MAS  WHERE ON_CODTL.CO_ODNO = ON_COHDR.CO_ODNO " & _
        "AND ON_CODTL.DELETED_FL != 'R' AND IMMAS.ITEM = ON_CODTL.ITEM AND ON_COHDR.CO_STATUS = 'O' " & _
        "AND IMMAS.PACKAGING_CODE = ON_CODTL.PACKAGING_CODE  AND NVL(ON_COHDR.SHIP_TO, ON_COHDR.CU_NO) = CUMAS.CU_NO AND ON_SITE_MAS.SITE=ON_COHDR.SITE " & _
        "AND ON_SITE_MAS.TYPE_FL='I' " & whereclause & ")  " & _
        "GROUP BY ON_COHDR.CO_ODNO, ON_COHDR.ORDER_TYPE, ON_COHDR.order_reqd_dt,ON_COHDR.ROUTE_STOP  ORDER BY 4 ASC, 3 ASC ", conn)
        Try
            adapter.Fill(dataTbl)
        Catch ex As Exception
            adapter.Dispose()
            Throw ex
        End Try
        adapter.Dispose()
        grdTrafficPlan.DataSource = dataTbl
        grdTrafficPlan.DataBind()
        Session("TrafficPlanSort") = dataTbl

    End Sub
    Private Sub getGrdTrafficPlan(ByVal route_code As String)
        Dim whereclause As String = ""
        Dim spid As String = ""
        Dim conn As OracleConnection = getOraConnection()
        Dim dataTbl As New DataTable
        Dim tblRoute As New DataTable
        whereclause = Session("TrafficPlanDet")
        spid = Session("TrafficPlanDetSPID")
        Session("TrafficPlanDetRoute") = route_code
        Dim adapter As OracleDataAdapter = Nothing

        If route_code = "0" Then
            adapter = New OracleDataAdapter("SELECT trim(MAX(NVL(ON_COHDR.ROUTE_CODE, CUMAS.ROUTE_CODE))) As Route_Code, " & _
                    "CASE Trim(ON_COHDR.ROUTE_STOP) WHEN '0' THEN TO_NUMBER('') ELSE TO_NUMBER(Trim(ON_COHDR.ROUTE_STOP))  END As StopNo, TRIM(ON_COHDR.CO_ODNO) As OrderNo, ON_COHDR.ORDER_TYPE as OrderType,  MAX(NVL(ON_COHDR.SHIP_TO, ON_COHDR.CU_NO)) As CustomerNo, " & _
                     "CEIL(SUM(ON_CODTL.TOTAL_ALLOCATED_QTY / DECODE(NVL(glob_item.CASE_SZ, 1), 0, 1, NVL(glob_item.CASE_SZ, 1)))) As Cases," & _
                    "TO_CHAR(ON_COHDR.order_reqd_dt,'YYYY-MM-DD') As ReqdDate " & _
                    "FROM ON_COHDR, CUMAS, ON_CODTL, glob_item, ON_SITE_MAS  " & _
                    "WHERE ON_CODTL.CO_ODNO = ON_COHDR.CO_ODNO AND ON_CODTL.DELETED_FL != 'R' AND glob_item.ITEM = ON_CODTL.ITEM AND ON_COHDR.CO_STATUS = 'O' " & _
                    "AND glob_item.PACKAGING_CODE = ON_CODTL.PACKAGING_CODE  AND NVL(ON_COHDR.SHIP_TO, ON_COHDR.CU_NO) = CUMAS.CU_NO " & _
                    "AND ON_SITE_MAS.SITE=ON_COHDR.SITE AND ON_SITE_MAS.TYPE_FL='I'  " & whereclause & " " & _
                    "GROUP BY ON_COHDR.CO_ODNO, ON_COHDR.ORDER_TYPE, ON_COHDR.order_reqd_dt,ON_COHDR.ROUTE_STOP  ORDER BY 4 ASC, 3 ASC ", conn)

        Else
            adapter = New OracleDataAdapter("SELECT trim(MAX(NVL(ON_COHDR.ROUTE_CODE, CUMAS.ROUTE_CODE))) As Route_Code, " & _
                    "CASE Trim(ON_COHDR.ROUTE_STOP) WHEN '0' THEN TO_NUMBER('') ELSE TO_NUMBER(Trim(ON_COHDR.ROUTE_STOP))  END As StopNo, TRIM(ON_COHDR.CO_ODNO) As OrderNo, ON_COHDR.ORDER_TYPE as OrderType,  MAX(NVL(ON_COHDR.SHIP_TO, ON_COHDR.CU_NO)) As CustomerNo, " & _
                    "CEIL(SUM(ON_CODTL.TOTAL_ALLOCATED_QTY / DECODE(NVL(glob_item.CASE_SZ, 1), 0, 1, NVL(glob_item.CASE_SZ, 1)))) As Cases," & _
                    "TO_CHAR(ON_COHDR.order_reqd_dt,'YYYY-MM-DD') As ReqdDate " & _
                    "FROM ON_COHDR, CUMAS, ON_CODTL, glob_item, ON_SITE_MAS  " & _
                    "WHERE ON_CODTL.CO_ODNO = ON_COHDR.CO_ODNO AND ON_CODTL.DELETED_FL != 'R' AND glob_item.ITEM = ON_CODTL.ITEM AND ON_COHDR.CO_STATUS = 'O' " & _
                    "AND glob_item.PACKAGING_CODE = ON_CODTL.PACKAGING_CODE  AND NVL(ON_COHDR.SHIP_TO, ON_COHDR.CU_NO) = CUMAS.CU_NO  and ON_COHDR.route_code ='" & route_code & "' " & _
                    "AND ON_SITE_MAS.SITE=ON_COHDR.SITE AND ON_SITE_MAS.TYPE_FL='I'  " & whereclause & " " & _
                    "GROUP BY ON_COHDR.CO_ODNO, ON_COHDR.ORDER_TYPE, ON_COHDR.order_reqd_dt,ON_COHDR.ROUTE_STOP  ORDER BY 4 ASC, 3 ASC ", conn)


        End If
        Try
            adapter.Fill(dataTbl)
        Catch ex As Exception
            adapter.Dispose()
            Throw ex
        End Try
        adapter.Dispose()
        grdTrafficPlan.DataSource = dataTbl
        grdTrafficPlan.DataBind()
        Session("TrafficPlanSort") = dataTbl
    End Sub
    Private Sub getGrdRoute()
        Dim whereclause As String = ""
        Dim spid As String = ""
        Dim conn As OracleConnection = getOraConnection()
        Dim dataTbl As New DataTable
        Dim tblRoute As New DataTable
        whereclause = Session("TrafficPlanDet")
        spid = Session("TrafficPlanDetSPID")

        Dim adapter = New OracleDataAdapter("SELECT DISTINCT NVL(ON_CODTL.ROUTE_CODE, ON_COHDR.ROUTE_CODE) AS Route, NVL(ON_CODTL.ROUTE_CODE, ON_COHDR.ROUTE_CODE) AS Route_Code  " & _
       ", 0 As Orders, 0 As Cases FROM ON_COHDR, CUMAS, ON_CODTL, glob_item, ON_SITE_MAS   " & _
       "WHERE ON_CODTL.CO_ODNO = ON_COHDR.CO_ODNO AND ON_CODTL.DELETED_FL != 'R'  " & _
       "AND glob_item.ITEM = ON_CODTL.ITEM AND ON_COHDR.CO_STATUS = 'O'   " & _
       "AND glob_item.PACKAGING_CODE = ON_CODTL.PACKAGING_CODE  AND NVL(ON_COHDR.SHIP_TO, ON_COHDR.CU_NO) = CUMAS.CU_NO   " & _
       "AND ON_SITE_MAS.SITE=ON_COHDR.SITE AND ON_SITE_MAS.TYPE_FL='I'  " & whereclause & " " & _
       "ORDER BY 1", conn)

        Try
            adapter.Fill(tblRoute)
        Catch ex As Exception
            adapter.Dispose()
            Throw ex
        End Try
        adapter.Dispose()
        grdRoute.DataSource = tblRoute
        grdRoute.DataBind()
        Session("RouteSort") = tblRoute

    End Sub
    Private Sub grdTrafficPlan_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdTrafficPlan.Sorting
        Dim d As Integer = 0
        d = grdTrafficPlan.Columns.Count
        'Dim dv As DataView = New DataView(Session("myTable"))
        GridViewSortExpression = e.SortExpression
        Dim pageIndex As Integer = grdTrafficPlan.PageIndex

        grdTrafficPlan.DataSource = SortDataTable(Session("TrafficPlanSort"), False)
        grdTrafficPlan.DataBind()
        grdTrafficPlan.PageIndex = pageIndex
    End Sub
    'Display the correct number of cases and orders selected for a given route
    Private Sub grdRoute_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdRoute.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblRoute As LinkButton = Nothing
            Dim lblOrders As Label = Nothing
            Dim lblCases As Label = Nothing
            Dim route As String = ""
            Dim route_compare As String = ""
            Dim dt As New DataTable

            lblRoute = e.Row.FindControl("lnkRoute")
            lblOrders = e.Row.FindControl("lblOrders")
            lblCases = e.Row.FindControl("lblCases")
            route = lblRoute.Text

            If Not Session("TPDTbl") Is Nothing Then
                dt = Session("TPDTbl")
                For i As Integer = 0 To dt.Rows.Count - 1
                    route_compare = dt.Rows(i).Item("Route")
                    If route.ToString.Trim.ToLower = route_compare.ToString.Trim.ToLower Then
                        lblOrders.Text = dt.Rows(i).Item("Orders")
                        lblCases.Text = dt.Rows(i).Item("Cases")
                    End If
                Next
            End If
        End If
    End Sub
    Private Sub grdRoute_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdRoute.Sorting
        Dim d As Integer = 0
        d = grdRoute.Columns.Count
        GridViewSortExpression = e.SortExpression
        Dim pageIndex As Integer = grdRoute.PageIndex

        grdRoute.DataSource = SortDataTable(Session("RouteSort"), False)
        grdRoute.DataBind()
        grdRoute.PageIndex = pageIndex
    End Sub
    Private Property GridViewSortDirection() As String
        Get
            If ViewState("SortDirection") Is Nothing Then
                ViewState("SortDirection") = SortDirection.Ascending
            End If
            Return ViewState("SortDirection")
        End Get
        Set(ByVal value As String)
            ViewState("SortDirection") = value
        End Set
    End Property
    Private Property GridViewSortExpression() As String
        Get
            If ViewState("SortExpression") Is Nothing Then
                ViewState("SortExpression") = ""
            End If
            Return ViewState("SortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("SortExpression") = value
        End Set
    End Property

    Private Function GetSortDirection() As String
        Select Case GridViewSortDirection
            Case "0"
                GridViewSortDirection = "1"
            Case "1"
                GridViewSortDirection = "0"
        End Select

        Return GridViewSortDirection
    End Function

    Protected Function SortDataTable(ByVal dt As DataTable, ByVal isPageIndexChanging As Boolean) As DataView
        Dim dv As DataView
        Dim order As String = ""

        dv = New DataView(dt)
        If GridViewSortExpression <> "" Then
            If isPageIndexChanging Then
                If GridViewSortDirection = "0" Then
                    order = "ASC"
                Else
                    order = "DESC"
                End If
                dv.Sort = GridViewSortExpression & " " & order

            Else
                If GetSortDirection() = "0" Then
                    order = "ASC"
                Else
                    order = "DESC"
                End If
                dv.Sort = GridViewSortExpression & " " & order

            End If

        End If
        Return dv
    End Function

    Private Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        'Traffic Planning, sending the selected orders from ROCON to ROC
        Dim drow As GridViewRow = Nothing
        Dim chkSelect As CheckBox = Nothing
        Dim order_number As String = ""
        Dim order As Label = Nothing
        Dim err_order As String = ""
        Dim route_code As String = ""

        Dim temporders As String = ""
        Dim ords As String() = Nothing

        Dim conn As OracleConnection = getOraConnection()
        Dim cmd As OracleCommand = Nothing
        Dim rdr As OracleDataReader = Nothing
        Dim reader As OracleDataReader = Nothing
        Dim stmt As String = ""
        Dim count As Integer = 0

        setCheckedOrders()
        setRouteStop()

        If Not Session("TPDOrderString") Is Nothing Then
            temporders = Session("TPDOrderString")
            ords = temporders.Split(",")
            For Each ord As String In ords
                order_number = ord
                Try
                    conn.Open()
                    'Checking first if the Stop Number is null then default it to zero
                    stmt = "SELECT count(*) from ON_COHDR WHERE ON_COHDR.CO_ODNO =  '" & order_number & "' and route_stop is null "

                    cmd = New OracleCommand(stmt, conn)
                    rdr = cmd.ExecuteReader
                    While rdr.Read
                        count = rdr(0)
                    End While

                    If count > 0 Then
                        stmt = "Update ON_COHDR set route_stop=0 WHERE ON_COHDR.CO_ODNO =  '" & order_number & "' "

                        cmd = New OracleCommand(stmt, conn)
                        cmd.ExecuteNonQuery()
                    End If

                    'get the last route_code clicked to display it 
                    'cmd.Dispose()
                    'stmt = "SELECT route_code from ON_COHDR WHERE ON_COHDR.CO_ODNO =  '" & order_number & "' "

                    'cmd = New OracleCommand(stmt, conn)
                    'reader = cmd.ExecuteReader
                    'While reader.Read
                    '    route_code = reader(0)
                    'End While
                    cmd.Dispose()
                    conn.Close()
                Catch ex As Exception
                    cmd.Dispose()
                    Throw ex
                End Try

                sendTrafficPlanOrders(order_number)
            Next

        End If
        If Not Session("TPDLastRouteClicked") Is Nothing Then
            route_code = Session("TPDLastRouteClicked")
        Else
            route_code = Session("TPDLastRouteSaved")
        End If
        getGrdTrafficPlan(route_code)
        getGrdRoute()
        If err_order <> "" Then
            lblMessage.Text = "The following order(s) could not be Traffic Planned because the Stop Number is empty: <br/> " & err_order
        End If
    End Sub
    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Session("TPDTbl") = Nothing
        Session("TPDOrderString") = Nothing
        Response.Redirect("~\TrafficPlanDetails.aspx")
    End Sub
    'Sub routine to calculate the total number of orders and cases selected/checked in a given route
    Private Sub getSelectedOrders()
        Dim drow As GridViewRow = Nothing
        Dim chkSelect As CheckBox = Nothing
        Dim count As Integer = 0
        Dim cases As Integer = 0
        Dim ctr As Integer = 0
        Dim order_number As String = ""
        Dim order_string As String = ""
        Dim order As Label = Nothing
        Dim route As Label = Nothing
        Dim route_no As String = ""
        Dim route_check As String = ""
        Dim stmt As String = ""

        Dim conn As OracleConnection = getOraConnection()
        Dim cmd As OracleCommand = Nothing
        Dim rdr As OracleDataReader = Nothing
        Dim trans As OracleTransaction = Nothing

        'Update the Total Number of Orders and Cases for the Selected Orders
        For Each drow In grdTrafficPlan.Rows
            chkSelect = drow.FindControl("chkSend")
            route = drow.FindControl("lblRt")
            route_no = route.Text
            order = drow.FindControl("lblOrder")
            order_number = order.Text.Trim

            If chkSelect.Checked Then
                count += 1
                Try
                    conn.Open()
                    trans = conn.BeginTransaction
                    stmt = "SELECT ceil(SUM(ON_CODTL.TOTAL_ALLOCATED_QTY / DECODE(NVL(glob_item.CASE_SZ, 1), 0, 1, NVL(glob_item.CASE_SZ, 1)))) As Cases " & _
                            "FROM ON_COHDR, CUMAS, ON_CODTL, glob_item, ON_SITE_MAS  " & _
                            "WHERE ON_CODTL.CO_ODNO = ON_COHDR.CO_ODNO AND ON_CODTL.DELETED_FL != 'R'  " & _
                            "AND glob_item.ITEM = ON_CODTL.ITEM AND ON_COHDR.CO_STATUS = 'O' " & _
                            "AND glob_item.PACKAGING_CODE = ON_CODTL.PACKAGING_CODE  AND NVL(ON_COHDR.SHIP_TO, ON_COHDR.CU_NO) = CUMAS.CU_NO  " & _
                            "AND ON_SITE_MAS.SITE=ON_COHDR.SITE AND ON_SITE_MAS.TYPE_FL='I'  " & _
                            "AND ON_CODTL.CO_ODNO =  '" & order_number & "' " & _
                            "GROUP BY ON_COHDR.CO_ODNO,ON_COHDR.ORDER_TYPE,ON_COHDR.order_reqd_dt "

                    cmd = New OracleCommand(stmt, conn, trans)
                    rdr = cmd.ExecuteReader
                    While rdr.Read
                        cases = cases + rdr(0)
                    End While

                    cmd.Dispose()
                    trans.Commit()
                    conn.Close()
                Catch ex As Exception
                    trans.Rollback()
                    cmd.Dispose()
                    conn.Close()
                    Throw ex
                End Try
                
            End If
        Next

        Dim dt As New DataTable
        Dim dt_compare As New DataTable
        Dim dr As DataRow
        Dim dc As New DataColumn
        Dim route_compare As String = ""
        Dim count_dup As Integer = 0

        'Compare if the Order total and case total for this route exist already
        'If they do then we need to delete the entry and add the new details to the Session("TPDTbl")
        If Session("TPDTbl") Is Nothing And Not Session("TPDOrderString") Is Nothing Then
            dt.Columns.Add("Route")
            dt.Columns.Add("Orders")
            dt.Columns.Add("Cases")

            dr = dt.NewRow
            dr.Item("Route") = route_no
            dr.Item("Orders") = count
            dr.Item("Cases") = cases

            dt.Rows.Add(dr)
            Session("TPDTbl") = dt
        ElseIf Session("TPDTbl") Is Nothing Then
            dt.Columns.Add("Route")
            dt.Columns.Add("Orders")
            dt.Columns.Add("Cases")

            dr = dt.NewRow
            dr.Item("Route") = route_no
            dr.Item("Orders") = count
            dr.Item("Cases") = cases

            dt.Rows.Add(dr)
            Session("TPDTbl") = dt
        Else
            dt_compare = Session("TPDTbl")
            For i As Integer = dt_compare.Rows.Count - 1 To 0 Step -1
                route_compare = dt_compare.Rows(i).Item("Route")
                If route_no.ToString.Trim.ToLower = route_compare.ToString.Trim.ToLower Then
                    dt_compare.Rows(i).Delete()
                End If
            Next
            dr = dt_compare.NewRow
            dr.Item("Route") = route_no
            dr.Item("Orders") = count
            dr.Item("Cases") = cases
            dt_compare.Rows.Add(dr)

            Session("TPDTbl") = dt_compare
        End If
        
    End Sub
    Private Sub sendTrafficPlanOrders(ByVal order_number As String)
        Dim conn As OracleConnection = getOraConnection()
        Dim cmd As OracleCommand = Nothing
        Dim rdr As OracleDataReader = Nothing
        Dim trans As OracleTransaction = Nothing
        Dim stmt As String = ""
        Dim route_number As String = ""
        Dim caseqty As Integer = 0
        Dim orderString As String = ""

        Try
            conn.Open()
            trans = conn.BeginTransaction
            'Making sure that the order to be traffic planned is still in open status
            stmt = "UPDATE ON_COHDR SET CO_STATUS = CO_STATUS WHERE CO_ODNO ='" & order_number.Trim & "'"
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            stmt = "SELECT a.ROUTE_CODE, ceil(SUM(b.TOTAL_ALLOCATED_QTY / DECODE(NVL(d.CASE_SZ, 1), 0, 1, NVL(d.CASE_SZ, 1)))) " & _
            "FROM ON_COHDR a, CUMAS c, ON_CODTL b, glob_item d WHERE b.CO_ODNO = a.CO_ODNO AND b.DELETED_FL != 'R' AND d.ITEM = b.ITEM " & _
            "AND a.co_odno ='" & order_number & "' AND d.PACKAGING_CODE = b.PACKAGING_CODE AND NVL(a.SHIP_TO, a.CU_NO) = c.CU_NO GROUP BY a.CO_ODNO,a.ROUTE_CODE "
            cmd = New OracleCommand(stmt, conn, trans)
            rdr = cmd.ExecuteReader
            While rdr.Read
                route_number = rdr(0)
                caseqty = rdr(1)
            End While
            cmd.Dispose()

            'Insert the traffic planned order into the COHDR table
            stmt = "INSERT INTO COHDR(CO_ODNO, CO_STATUS, ROUTE_CODE, STOP_SEQ_NO, PRIOR_NAME, CU_NO, SHIP_TO, ORDER_DT_TM, " & _
            "ORDER_REQD_DT, ORDER_TYPE, TRANSPORT_MODE, CARRIER, CLI_NO, TRAFFIC_REF_NO, PU_ORDER_FL, PAY_METHOD, " & _
            "EMPLOYEE_NUMBER, DELIVERY_FL, ENTRY_FL ) " & _
            "SELECT '" & order_number & "' , ON_COHDR.CO_STATUS, ON_COHDR.ROUTE_CODE, DECODE(LENGTH(TO_CHAR(ON_COHDR.ROUTE_STOP)),  " & _
            "1, '000' || to_char(ON_COHDR.route_stop), 2, '00'  || to_char(ON_COHDR.route_stop), 3, '0'   || to_char(ON_COHDR.route_stop),  " & _
            "to_char(ON_COHDR.route_stop)), RTRIM(ON_COHDR.PRIOR_NAME,' '), RTRIM(ON_COHDR.CU_NO,' '), RTRIM(ON_COHDR.SHIP_TO,' '),  " & _
            "ON_COHDR.ORDER_DT_TM, ON_COHDR.ORDER_REQD_DT, RTRIM(ON_COHDR.ORDER_TYPE,' '), ' ', RTRIM(ON_COHDR.CARRIER,' '), RTRIM(ON_COHDR.CLI_NO,' '),  " & _
            "RTRIM(ON_COHDR.TRAFFIC_REF_NO,' '), NVL(ON_COHDR.PU_ORDER_FL, 'N'), RTRIM(ON_COHDR.PAY_METHOD,' '), ON_COHDR.EMPLOYEE_NUMBER,  " & _
            "NVL(ON_COHDR.DELIVERY_FL, 'N'), NVL(ON_COHDR.ENTRY_FL, 'N')  " & _
            "FROM ON_COHDR ON_COHDR WHERE ON_COHDR.CO_ODNO = '" & order_number & "' "
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            'Insert the traffic planned order into the CODTL table
            stmt = "INSERT INTO CODTL  ( CO_ODNO, COD_LINE, ITEM, PACKAGING_CODE, COD_QTY, TOTAL_ALLOCATED_QTY, PLANNED_QTY, PICKED_QTY,  " & _
            "SHIPPED_QTY, LOT_NUMBER, USE_BLOCKED_STOCK_FL, EXPIRY_DT, DELETED_FL, LINE_CANCELED_Y_OR_N )  " & _
            "SELECT '" & order_number & "' ,  t3283.COD_LINE, t3283.ITEM, t3283.PACKAGING_CODE, t3283.COD_QTY, t3283.TOTAL_ALLOCATED_QTY,  " & _
            "0, 0, 0, t3283.LOT_NUMBER, t3283.USE_BLOCKED_STOCK_FL, t3283.EXPIRY_DT, NVL(t3283.DELETED_FL, 'N'),  " & _
            "DECODE(NVL(t3283.DELETED_FL, 'N'), 'N', 'N', 'Y')  " & _
            "FROM ON_CODTL t3283 WHERE t3283.CO_ODNO =  '" & order_number & "' "
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            'Insert the traffic planned order into the COCUS table, it it exists in ON_COCUS
            stmt = "INSERT INTO COCUS  ( CO_ODNO, SHIP_TO_NAME, SHIP_TO_ADD1, SHIP_TO_ADD2, SHIP_TO_CITY, SHIP_TO_PROV, SHIP_TO_POST_CD,   " & _
            "SHIP_TO_PHONE, SHIP_TO_FAX, SHIP_TO_CONTACT, BILL_TO_NAME, BILL_TO_ADD1, BILL_TO_ADD2, BILL_TO_CITY, BILL_TO_PROV,   " & _
            "BILL_TO_POST_CD, BILL_TO_PHONE, BILL_TO_FAX, SHIP_TO, SHIP_TO_COUNTRY, BILL_TO_COUNTRY )   " & _
            "SELECT '" & order_number & "', t3335.SHIP_TO_NAME, t3335.SHIP_TO_ADD1, t3335.SHIP_TO_ADD2, t3335.SHIP_TO_CITY, t3335.SHIP_TO_PROV,  " & _
            "t3335.SHIP_TO_POST_CD, t3335.SHIP_TO_PHONE, t3335.SHIP_TO_FAX, t3335.SHIP_TO_CONTACT, t3335.BILL_TO_NAME, t3335.BILL_TO_ADD1,   " & _
            "t3335.BILL_TO_ADD2, t3335.BILL_TO_CITY, t3335.BILL_TO_PROV, t3335.BILL_TO_POST_CD, t3335.BILL_TO_PHONE, t3335.BILL_TO_FAX,  " & _
            "t3335.SHIP_TO, t3335.SHIP_TO_COUNTRY, t3335.BILL_TO_COUNTRY FROM ON_COCUS t3335 WHERE t3335.CO_ODNO = '" & order_number & "' "
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            'Insert the traffic planned order into the ODCMT table, it it exists in ON_ODCMT
            stmt = "INSERT INTO ODCMT  ( CO_ODNO, OC_SEQN, OC_LINE, DOWNLOAD_CNT, ODCMT_COMMENT, OC_TYPE )   " & _
            "SELECT '" & order_number & "', t3370.OC_SEQN, t3370.OC_LINE, t3370.DOWNLOAD_CNT, RTRIM(t3370.ODCMT_COMMENT,' '), t3370.OC_TYPE   " & _
            "FROM ON_ODCMT t3370 WHERE t3370.CO_ODNO =  '" & order_number & "' "
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            'Insert the traffic planned order into the COD_SUBSTITUTION table, it it exists in ON_COD_SUBSTITUTION
            stmt = "INSERT INTO COD_SUBSTITUTION  ( CO_ODNO, COD_LINE, PRIORITY, ITEM, PACKAGING_CODE )  " & _
            "SELECT '" & order_number & "', t3386.COD_LINE, t3386.PRIORITY, t3386.ITEM, t3386.PACKAGING_CODE  " & _
            "FROM ON_COD_SUBSTITUTION t3386 WHERE t3386.CO_ODNO =  '" & order_number & "' "
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()


            'Insert the traffic planned order into the COH_LOG table, it it exists in ON_COH_LOG
            stmt = "INSERT INTO COH_LOG  ( CO_ODNO, TYPE, DEST, EM_NO, ENTRY_FL, SOURCE ) " & _
            "SELECT '" & order_number & "', t3404.TYPE, t3404.DEST, t3404.EM_NO, t3404.ENTRY_FL, t3404.SOURCE  " & _
            "FROM ON_COH_LOG t3404 WHERE t3404.CO_ODNO =  '" & order_number & "' "
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            'Insert the traffic planned order into the COD_LOG table, it it exists in ON_COD_LOG
            stmt = "INSERT INTO COD_LOG  ( CO_ODNO, COD_LINE, TYPE, ITEM, PACKAGING_CODE, QTY, EM_NO, SOURCE ) " & _
            "SELECT '" & order_number & "', t3427.COD_LINE, t3427.TYPE, t3427.ITEM, t3427.PACKAGING_CODE, t3427.QTY, t3427.EM_NO, t3427.SOURCE " & _
            "FROM ON_COD_LOG t3427 WHERE t3427.CO_ODNO =  '" & order_number & "' "
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            'Delete the order from the ROCON tables
            stmt = "DELETE ON_COD_LOG WHERE CO_ODNO =  '" & order_number & "' "
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            stmt = "DELETE ON_COH_LOG WHERE CO_ODNO =  '" & order_number & "' "
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            stmt = "DELETE ON_COD_SUBSTITUTION WHERE CO_ODNO =  '" & order_number & "' "
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            stmt = "DELETE ON_ODCMT WHERE CO_ODNO =  '" & order_number & "' "
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            stmt = "DELETE ON_CODTL WHERE CO_ODNO =  '" & order_number & "' "
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            stmt = "DELETE ON_COCUS WHERE CO_ODNO =  '" & order_number & "' "
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            stmt = "DELETE ON_COHDR WHERE CO_ODNO =  '" & order_number & "' "
            cmd = New OracleCommand(stmt, conn, trans)
            cmd.ExecuteNonQuery()

            trans.Commit()
            cmd.Dispose()
            conn.Close()

            orderString = Session("TPDFinalOrders")
            If orderString = "" Then
                orderString = order_number
            Else
                orderString = orderString & ", " & order_number
            End If
            Session("TPDFinalOrders") = orderString

            'Update the Order Count and Case Totals
            Dim dt As New DataTable
            dt = Session("TPDTbl")
            Dim route_compare As String = ""
            Dim ordercount As Integer = 0
            Dim casecount As Integer = 0

            If Not Session("TPDTbl") Is Nothing Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    route_compare = dt.Rows(i).Item("Route")
                    If route_compare.Trim = route_number.Trim Then
                        ordercount = dt.Rows(i).Item("Orders")
                        casecount = dt.Rows(i).Item("Cases")
                        dt.Rows(i).Item("Orders") = ordercount - 1
                        dt.Rows(i).Item("Cases") = casecount - caseqty
                    End If
                Next
                Session("TPDTbl") = dt
            End If

        Catch ex As Exception
            trans.Rollback()
            cmd.Dispose()
            conn.Close()
            Throw ex
        End Try

    End Sub
    'SubRoutine to create a comma separated string with all the orders that are checked for various routes to Traffic Plan
    Private Sub setCheckedOrders()
        Dim drow As GridViewRow = Nothing
        Dim chkSelect As CheckBox = Nothing
        Dim order_number As String = ""
        Dim order_string As String = ""
        Dim order As Label = Nothing
        
        For Each drow In grdTrafficPlan.Rows
            chkSelect = drow.FindControl("chkSend")
            order = drow.FindControl("lblOrder")
            order_number = order.Text.Trim
           
            order_string = Session("TPDOrderString")
            'If the order is checked and the initial order string is empty then add the order to the order string
            If order_string = "" And chkSelect.Checked Then
                order_string = order_number
                Session("TPDOrderString") = order_string
                'If the order is checked and the initial order string is not empty then add the order to the order string
                'If the order number is already a part of the exisiting order string then dont add it
            ElseIf chkSelect.Checked Then
                If order_string.Contains(order_number) Then
                    order_string = order_string
                Else
                    order_string = order_string & "," & order_number
                    Session("TPDOrderString") = order_string
                End If
                'In case the order is a part of order string and is now unchecked then remove it from the string
            ElseIf Not Session("TPDOrderString") Is Nothing Then
                If order_string.Contains(",") Then
                    Session("TPDOrderString") = order_string.Replace(order_number & ",", "")
                Else
                    Session("TPDOrderString") = order_string.Replace(order_number, "")
                End If
            End If
        Next
    End Sub
    'SubRoutine to save the route codes and stop numbers for all the orders
    Private Sub setRouteStop()
        Dim drow As GridViewRow = Nothing
        Dim order_number As String = ""
        Dim order As Label = Nothing
        Dim txtStop As TextBox = Nothing
        Dim txtRt As TextBox = Nothing
        'Dim drpRoute As DropDownList = Nothing
        Dim route As String = ""
        Dim conn As OracleConnection = getOraConnection()
        Dim cmd As New OracleCommand()
        Dim rdr As OracleDataReader = Nothing
        Dim stmt As String = ""
        Dim stop_number As String = ""
        Dim dt_compare As DataTable = Nothing
        Dim drow_compare As DataRow = Nothing

        dt_compare = Session("TrafficPlanSort")

        For Each drow In grdTrafficPlan.Rows

            Try
                cmd.Connection = conn
                conn.Open()

                txtStop = drow.FindControl("txtSn")
                txtRt = drow.FindControl("txtRt")
                order = drow.FindControl("lblOrder")
                'drpRoute = drow.FindControl("drpRt")
                order_number = order.Text.Trim
                stop_number = txtStop.Text.Trim
                route = txtRt.Text.Trim

                For Each drow_compare In dt_compare.Rows
                    If drow_compare("OrderNo").ToString.Trim = order_number Then
                        If route <> drow_compare("Route_Code").ToString.Trim And getRouteStatus(route) = True Then
                            stmt = "SELECT route_code FROM ON_COHDR " & _
                                   "WHERE ON_COHDR.CO_ODNO = '" & order_number & "' for update "
                            cmd.CommandText = stmt
                            rdr = cmd.ExecuteReader

                            stmt = "Update ON_COHDR set route_code='" & route & "' WHERE " & _
                                    "ON_COHDR.CO_ODNO = '" & order_number & "' "
                            cmd.CommandText = stmt
                            cmd.ExecuteNonQuery()

                            stmt = "commit"
                            cmd.CommandText = stmt
                            cmd.ExecuteNonQuery()

                        End If
                        If stop_number <> drow_compare("StopNo").ToString And (Regex.IsMatch(stop_number, "^[0-9]+$") Or stop_number.Trim = "") Then
                            stmt = "SELECT route_stop FROM ON_COHDR " & _
                                  "WHERE ON_COHDR.CO_ODNO = '" & order_number & "' for update "
                            cmd.CommandText = stmt
                            rdr = cmd.ExecuteReader

                            stmt = "Update ON_COHDR set route_stop='" & stop_number & "' WHERE " & _
                                   "ON_COHDR.CO_ODNO = '" & order_number & "' "
                            cmd.CommandText = stmt
                            cmd.ExecuteNonQuery()

                            stmt = "commit"
                            cmd.CommandText = stmt
                            cmd.ExecuteNonQuery()
                        End If
                    End If
                Next
                cmd.Dispose()
                conn.Close()
            Catch ex As Exception
                cmd.Dispose()
                conn.Close()
                Throw ex
            End Try
        Next
        Session("TPDLastRouteSaved") = route
    End Sub
    'Everytime Save is clicked three things happen
    '1. The checked orders in the current route needs to be added to the orders string
    '2. All the stops and routes changed for any order in the current route needs to be saved
    '3. The number of orders and cases in the current route need to be addded to any existing cases/order totals and displayed
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        setCheckedOrders()
        setRouteStop()
        getSelectedOrders()
        Dim route As String = ""
        'Show the route code that was last clicked on the Route Grid
        'If all the route codes in the last clicked route were saved to another route code, then display the route code
        'that was last saved
        If Not Session("TPDLastRouteClicked") Is Nothing Then
            route = Session("TPDLastRouteClicked")
        Else
            route = Session("TPDLastRouteSaved")
        End If
        getGrdTrafficPlan(route)
        getGrdRoute()
    End Sub

    Private Sub btnPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrev.Click
        Response.Redirect("~/TrafficPlanning.aspx")
    End Sub
    Private Function getRouteStatus(ByVal route_code As String) As Boolean

        Dim routes As String = ""
        Dim conn As OracleConnection = getOraConnection()
        Dim cmd As New OracleCommand()
        Dim rdr As OracleDataReader = Nothing
        Dim stmt As String = ""
        Dim result As Boolean = False

        Try
            cmd.Connection = conn
            conn.Open()

            stmt = "SELECT DISTINCT TRIM(ROUTE_CODE) FROM dbo.ROUTE_CODE_MAS "

            cmd.CommandText = stmt
            rdr = cmd.ExecuteReader

            While rdr.Read
                routes = rdr(0)
                If routes.ToLower = route_code.ToLower Then
                    result = True
                End If
            End While

            cmd.Dispose()
            conn.Close()
        Catch ex As Exception
            cmd.Dispose()
            conn.Close()
            Throw ex
        End Try
        Return result
    End Function
End Class