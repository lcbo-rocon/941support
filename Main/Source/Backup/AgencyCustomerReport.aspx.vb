Imports com.lowagie.text.pdf
Imports com.lowagie.text
Imports com.lowagie.text.pdf.PdfTemplate
Imports com.lowagie.text.pdf.PdfPageEventHelper
Imports com.lowagie.text.Font
Imports System.IO
Imports System.Globalization

Partial Public Class AgencyCustomerReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lblHeading As Label = Master.FindControl("lblMainTitle")
        If Not lblHeading Is Nothing Then
            lblHeading.Text = "Agency Orders Reporting"
        End If
        lblErrorMsg.Text = ""
        txtOrderDate.CssClass = "textbox"
        txtDeliveryDate.CssClass = "textbox"
        txtOrderNo.CssClass = "textbox"

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim bValid As Boolean = True
        Dim sQry As String = "SELECT *  FROM                            " & vbNewLine & _
            "(Select HDR.ORDER_DT_TM AS ORDER_DATE    " & vbNewLine & _
            ", HDR.CU_NO       AS CUSTOMER            " & vbNewLine & _
            ", CUST.CU_NAME AS CUSTOMER_NAME          " & vbNewLine & _
            ", HDR.ORDER_TYPE                         " & vbNewLine & _
            ", INV.INV_NO AS INVNUM                   " & vbNewLine & _
            ", HDR.CO_ODNO AS ORDERNUM               " & vbNewLine & _
            ", COUNT(DTL.COD_LINE) AS ORDERLINES      " & vbNewLine & _
            ", INV.INV_AMT AS ORDERTOTAL              " & vbNewLine & _
            ", TRUNC(HDR.ORDER_REQD_DT) AS EXPECTED_DELIVERY " & vbNewLine & _
            ", INV.EM_NO AS EM_NO " & vbNewLine & _
            ", 'INVOICED' AS STATUS                   " & vbNewLine & _
            "FROM COHDR HDR INNER JOIN CODTL DTL      " & vbNewLine & _
            "ON HDR.CO_ODNO = DTL.CO_ODNO             " & vbNewLine & _
            "INNER JOIN ON_INV_CO INV                 " & vbNewLine & _
            "ON HDR.CO_ODNO = INV.CO_ODNO             " & vbNewLine & _
            " INNER JOIN ON_CUST_MAS CUST ON HDR.CU_NO = CUST.CU_NO " & vbNewLine & _
            "WHERE DTL.DELETED_FL = 'N'  AND INV.CO_INV_TYPE = 'A' " & vbNewLine & _
            " AND DTL.COD_QTY > 0 " & vbNewLine & _
            "GROUP BY HDR.ORDER_DT_TM, HDR.ORDER_TYPE, HDR.CO_ODNO, INV.INV_AMT , HDR.ORDER_REQD_DT " & vbNewLine & _
            " ,INV.INV_NO, HDR.CU_NO, INV.EM_NO, CUST.CU_NAME " & vbNewLine & _
            "UNION                                      " & vbNewLine & _
            "Select HDR.ORDER_DT_TM AS ORDER_DATE       " & vbNewLine & _
            ", HDR.CU_NO       AS CUSTOMER            " & vbNewLine & _
            ", CUST.CU_NAME AS CUSTOMER_NAME          " & vbNewLine & _
            ", HDR.ORDER_TYPE                           " & vbNewLine & _
            ", INV.INV_NO AS INVNUM                   " & vbNewLine & _
            ", HDR.CO_ODNO AS ORDERNUM                 " & vbNewLine & _
            ", COUNT(DTL.COD_LINE) AS ORDERLINES        " & vbNewLine & _
            ", INV.INV_AMT AS ORDERTOTAL                " & vbNewLine & _
            ", TRUNC(HDR.ORDER_REQD_DT) AS EXPECTED_DELIVERY   " & vbNewLine & _
            ", INV.EM_NO AS EM_NO " & vbNewLine & _
            ",'SUBMITTED' AS STATUS FROM ON_COHDR HDR INNER JOIN ON_CODTL DTL " & vbNewLine & _
            "ON HDR.CO_ODNO = DTL.CO_ODNO INNER JOIN ON_INV_CO INV ON HDR.CO_ODNO = INV.CO_ODNO    " & vbNewLine & _
             "  INNER JOIN ON_CUST_MAS CUST ON HDR.CU_NO = CUST.CU_NO " & vbNewLine & _
            "WHERE DTL.DELETED_FL = 'N'  AND INV.CO_INV_TYPE = 'E' AND DTL.COD_QTY > 0 " & vbNewLine & _
            "GROUP BY HDR.ORDER_DT_TM, HDR.ORDER_TYPE, HDR.CO_ODNO, INV.INV_AMT, HDR.ORDER_REQD_DT " & vbNewLine & _
            " ,INV.INV_NO, HDR.CU_NO, INV.EM_NO, CUST.CU_NAME " & vbNewLine & _
            "UNION                                       " & vbNewLine & _
            "Select HDR.ORDER_DT_TM AS ORDER_DATE        " & vbNewLine & _
            ", HDR.CU_NO       AS CUSTOMER            " & vbNewLine & _
            ", CUST.CU_NAME AS CUSTOMER_NAME          " & vbNewLine & _
            ", HDR.ORDER_TYPE                            " & vbNewLine & _
            ", INV.INV_NO AS INVNUM                      " & vbNewLine & _
            ",  HDR.CO_ODNO AS ORDERNUM                  " & vbNewLine & _
            ", COUNT(DTL.COD_LINE) AS ORDERLINES         " & vbNewLine & _
            ", INV.INV_AMT AS ORDERTOTAL                 " & vbNewLine & _
            ", TRUNC(HDR.ORDER_REQD_DT) AS EXPECTED_DELIVERY    " & vbNewLine & _
            ", INV.EM_NO AS EM_NO " & vbNewLine & _
            ",'PLANNED' AS STATUS FROM COHDR HDR INNER JOIN CODTL DTL ON HDR.CO_ODNO = DTL.CO_ODNO " & _
            "  INNER JOIN ON_INV_CO INV ON HDR.CO_ODNO = INV.CO_ODNO  " & vbNewLine & _
            "  INNER JOIN ON_CUST_MAS CUST ON HDR.CU_NO = CUST.CU_NO " & vbNewLine & _
            "WHERE DTL.DELETED_FL = 'N'  AND INV.CO_INV_TYPE = 'E' AND DTL.COD_QTY > 0   " & vbNewLine & _
            "AND NOT EXISTS(SELECT 1 FROM ON_INV_CO T1 WHERE HDR.CO_ODNO=T1.CO_ODNO AND T1.CO_INV_TYPE = 'A') " & vbNewLine & _
            "GROUP BY HDR.ORDER_DT_TM, HDR.ORDER_TYPE, HDR.CO_ODNO, INV.INV_AMT, HDR.ORDER_REQD_DT " & vbNewLine & _
            " ,INV.INV_NO, HDR.CU_NO, INV.EM_NO, CUST.CU_NAME) " & vbNewLine

        Dim iInvalid As Integer = 0
        Dim tmpDate As DateTime = Nothing
        'validate the search fields
        'create select statements
        Dim where As String = ""
        If Me.txtOrderNo.Text.Trim <> "" Then
            'validate order number if numeric
            If IsNumeric(txtOrderNo.Text.Trim) = False Then
                bVAlid = False
                lblErrorMsg.Text = "Invalid Order number entered!"
            End If
            If where <> "" Then
                where = where & " AND "
            End If
            where = where & " TRIM(ORDERNUM) = '" & Me.txtOrderNo.Text.Trim & "'"
        End If

        'ORDER TYPES
        If Me.drpOrderTypes.SelectedValue <> "ALL" Then
            If where <> "" Then
                where = where & " AND "
            End If
            where = where & " TRIM(ORDER_TYPE) = '" & Me.drpOrderTypes.SelectedValue.Trim & "'"
        End If

        'ORDER DATE
        If Me.txtOrderDate.Text <> "" Then
            If DateTime.TryParse(txtOrderDate.Text, tmpDate) = False Then
                bValid = False
                txtOrderDate.CssClass = "etextbox"
                lblErrorMsg.Text = "Invalid Order Date entered!"
            Else
                Me.txtOrderDate.Text = tmpDate.ToString("yyyy-MM-dd")
                If where <> "" Then
                    where = where & " AND "
                End If
                where = where & " TRUNC(ORDER_DATE) = TO_DATE('" & txtOrderDate.Text & "','YYYY-MM-DD')"
            End If
        End If

        'DELIVERY DATE
        If Me.txtDeliveryDate.Text <> "" Then
            If DateTime.TryParse(txtDeliveryDate.Text, tmpDate) = False Then
                bValid = False
                txtDeliveryDate.CssClass = "etextbox"
                lblErrorMsg.Text = "Invalid Ship Date entered!"
            Else
                Me.txtDeliveryDate.Text = tmpDate.ToString("yyyy-MM-dd")
                If where <> "" Then
                    where = where & " AND "
                End If
                where = where & " TRUNC(EXPECTED_DELIVERY) = TO_DATE('" & txtDeliveryDate.Text & "','YYYY-MM-DD')"
            End If
        End If

        'CUSTOMER NUMBER
        If Me.txtCustomerNo.Text <> "" Then
            'validate customer number if numeric
            If IsNumeric(txtCustomerNo.Text.Trim) = False Then
                bValid = False
                lblErrorMsg.Text = "Invalid CUSTOMER number entered!"
            End If
            If where <> "" Then
                where = where & " AND "
            End If
            where = where & " TRIM(CUSTOMER) = '" & Me.txtCustomerNo.Text.Trim & "'"
        End If

        'CUSTOMER NAME
        If Me.txtCustomerName.Text.Trim <> "" Then
            If where <> "" Then
                where = where & " AND "
            End If
            where = where & " UPPER(CUSTOMER_NAME) LIKE '%" & Me.txtCustomerName.Text.Trim & "%'"
        End If
        If bVAlid = True Then

            If where <> "" Then
                sQry = sQry & " WHERE " & where
            End If

            Dim dt As DataTable = SPURDataControl.getData(sQry)
            dt.Columns.Add("ITEM_VALID")
            dt.Columns.Add("TENDER_VALID")
            Dim sStatus As String = ""
            For Each dr As DataRow In dt.Rows
                'validate product restriction
                Select Case (dr("STATUS"))
                    Case "INVOICED" : sStatus = "A"
                    Case Else
                        sStatus = "E"
                End Select
                If SPURDataControl.validateOrderProductRestriction(dr("ORDERNUM"), dr("ORDER_TYPE"), sStatus) = True Then
                    dr("ITEM_VALID") = "OK"
                Else
                    dr("ITEM_VALID") = "INVALID"
                End If

                If SPURDataControl.validateTenderRestriction(dr("ORDERNUM"), dr("ORDER_TYPE"), dr("CUSTOMER")) = True Then
                    dr("TENDER_VALID") = "OK"
                Else
                    dr("TENDER_VALID") = "INVALID"
                End If

                If dr("TENDER_VALID") = "INVALID" Or dr("ITEM_VALID") = "INVALID" Then
                    iInvalid = iInvalid + 1
                End If
            Next

            Session("SUMMARY") = dt
            grdSummary.DataSource = Session("SUMMARY")
            grdSummary.DataBind()

            Session("DETAILS") = Nothing
            Session("TENDERS") = Nothing

            grdDetails.DataSource = Nothing
            grdTenders.DataSource = Nothing
            grdDetails.DataBind()
            grdTenders.DataBind()

            lblMsg.Text = "There are " & dt.Rows.Count & " orders found with " & iInvalid & " of them are invalid!"
        End If

    End Sub

    Protected Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Response.Redirect("~\AgencyCustomerReport.aspx")
    End Sub

    Private Sub grdSummary_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdSummary.DataBound
        If grdSummary.Rows.Count > 0 Then
            For Each tcell As TableCell In grdSummary.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
            '            lblMsg.Text = ""
            txtOrderDate.CssClass = ""
            txtDeliveryDate.CssClass = ""
            txtOrderNo.CssClass = ""
            pnlSummary.Visible = True
            '           lblMsg.Text = grdSummary.Rows.Count & " order(s) match your search criteria."


            Dim lblValidItems As Label = Nothing
            Dim lblValidTenders As Label = Nothing

            For Each grow As GridViewRow In Me.grdSummary.Rows
                lblValidItems = grow.FindControl("lblValidItems")
                lblValidTenders = grow.FindControl("lblValidTenders")

                If lblValidItems.Text = "INVALID" Then
                    grow.BackColor = Drawing.Color.Red
                    grow.Font.Bold = True
                End If

                If lblValidTenders.Text = "INVALID" Then
                    grow.BackColor = Drawing.Color.Red
                    grow.Font.Bold = True
                End If
            Next

        Else
            ' lblMsg.Text = "There are 0 orders that match your search criteria."
            pnlSummary.Visible = False
            pnlDetails.Visible = False
        End If
    End Sub

    Private Sub grdSummary_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdSummary.RowCommand

        'If Order number is clicked.
        If e.CommandName = "GetOrderDetails" Then

            Dim sParams As String() = e.CommandArgument.ToString.Split(",")
            Dim sOrderNum As String = sParams(0)
            Dim sOrderStatus As String = sParams(1)
            Dim sValidItems As String = sParams(2)
            Dim sValidTenders As String = sParams(3)
            Dim sOrderType As String = sParams(4)
            Dim sCustNum As String = sParams(5)
            Dim sCOInvType As String = ""
            Dim dtDetails As DataTable = Nothing
            Dim dtTenders As DataTable = Nothing

            Select Case (sOrderStatus)
                Case "INVOICED"
                    sCOInvType = "A"
                Case Else
                    sCOInvType = "E"
            End Select

            Try
                Dim sQry As String = " SELECT     " & vbNewLine & _
                                    "  T1.CO_ODNO    " & vbNewLine & _
                                    " ,T1.COD_LINE  " & vbNewLine & _
                                    " ,TO_NUMBER(T1.ITEM) as Item     " & vbNewLine & _
                                    " ,T2.IM_DESC AS PROD_DESCRIPTION " & vbNewLine & _
                                    " ,T1.PRICE AS BASIC_PRICE " & vbNewLine & _
                                    " ,T1.DEPOSIT_PRICE AS BOTTLE_DEP " & vbNewLine & _
                                    " ,T1.QTY " & vbNewLine & _
                                    " ,T1.ITEM_CATEGORY " & vbNewLine & _
                                    " ,NVL(T1.RETAIL_PRICE,0) AS RETAIL_PRICE  " & vbNewLine & _
                                    " ,T1.EXTD_PRICE AS EXTENDED_PRICE " & vbNewLine & _
                                    " ,ROUND(T2.ITEM_SIZE,0) AS VOLUME " & vbNewLine & _
                                    " ,'OK' AS COMMENTS " & vbNewLine & _
                                    " FROM DBO.ON_INV_CODTL T1 INNER JOIN DBO.IMMAS T2 ON T1.ITEM = T2.ITEM " & vbNewLine & _
                                    " WHERE CO_ODNO = '" & sOrderNum & "' " & vbNewLine & _
                                    " AND CO_INV_TYPE = '" & sCOInvType & "' " & vbNewLine & _
                                    " ORDER BY ITEM ASC "


                dtDetails = SPURDataControl.getData(sQry)

                'check which item is valid

                For Each drItems As DataRow In dtDetails.Rows
                    drItems("RETAIL_PRICE") = SPURDataControl.calcRetailPrice(drItems("BASIC_PRICE"), drItems("BOTTLE_DEP"), drItems("ITEM_CATEGORY"))
                Next

                Session("DETAILS") = dtDetails
                If sValidItems = "INVALID" Then
                    Dim dtInvalid As DataTable = SPURDataControl.getOrderProductRestrictionItems(sOrderNum, sOrderType, sCOInvType)

                    For Each drInvalid As DataRow In dtInvalid.Rows
                        For Each drDetails As DataRow In dtDetails.Select("ITEM = '" & drInvalid("ITEM") & "'")
                            drDetails("COMMENTS") = "INVALID"
                        Next

                    Next
                End If
                grdDetails.DataSource = Session("DETAILS")
                grdDetails.DataBind()
                grdDetails.Visible = True
                pnlDetails.Visible = True

                'Get the tender info

                sQry = "select T2.CO_ODNO AS ORDERNUM " & vbNewLine & _
                            ",T1.INV_NO                    " & vbNewLine & _
                            ",T1.REF_SEQ                   " & vbNewLine & _
                            ",T1.TIMESTAMP                 " & vbNewLine & _
                            ",T1.EMP_NO                    " & vbNewLine & _
                            ",T1.TENDER_CD                 " & vbNewLine & _
                            ",T1.TENDER_TYPE               " & vbNewLine & _
                            ",T1.PAY_AMT                   " & vbNewLine & _
                            ",T1.CREDIT_AUTH               " & vbNewLine & _
                            ",T1.PAY_COMMENT               " & vbNewLine & _
                            ",T1.PAY_DT                    " & vbNewLine & _
                            ",'OK' as COMMENTS " & vbNewLine & _
                            " from on_inv_pay T1 INNER JOIN ON_INV_CO T2 " & vbNewLine & _
                            " ON T1.INV_NO = T2.INV_NO " & vbNewLine & _
                            " WHERE CO_INV_TYPE = 'E' " & vbNewLine & _
                            " AND T2.CO_ODNO = '" & sOrderNum & "'" & vbNewLine & _
                            " ORDER BY REF_SEQ ASC "

                dtTenders = SPURDataControl.getData(sQry)
                Session("TENDERS") = dtTenders
                If sValidTenders = "INVALID" Then
                    Dim dtInvalid As DataTable = SPURDataControl.getTenderRestriction(sOrderNum, sOrderType, sCustNum)

                    For Each drInvalid As DataRow In dtInvalid.Rows
                        For Each drTenders As DataRow In dtTenders.Select("TENDER_CD = " & drInvalid("TENDER_CD"))
                            drTenders("COMMENTS") = "INVALID"
                        Next
                    Next
                End If
                grdTenders.DataSource = Session("TENDERS")
                grdTenders.DataBind()
                grdTenders.Visible = True

            Catch ex As Exception
                lblMsg.Text = ConfigurationManager.AppSettings("genericErrMsg")
            End Try
        End If


    End Sub
    Private Sub grdSummary_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdSummary.Sorting
        Dim dv As DataView = New DataView(Session("SUMMARY"))
        Dim order As String = ""
        If Not ViewState("SUMOrder") Is Nothing Then
            order = ViewState("SUMOrder")
        End If

        If order.StartsWith(e.SortExpression) Then
            If order.ToString.EndsWith("ASC") Then
                order = "DESC"
            Else
                order = "ASC"
            End If
        Else
            order = "ASC"
        End If

        ViewState("SUMOrder") = e.SortExpression + " " & order
        dv.Sort = ViewState("SUMOrder")
        grdSummary.DataSource = dv
        grdSummary.DataBind()

    End Sub
    Private Sub grdDetails_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdDetails.DataBound
        Dim iInvalidcount As Integer = 0
        If grdDetails.Rows.Count > 0 Then
            For Each tcell As TableCell In grdDetails.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
            ' lblMsg.Text = ""
            pnlDetails.Visible = True
            Dim lblComments As Label = Nothing
            For Each grow As GridViewRow In grdDetails.Rows
                lblComments = grow.FindControl("lblComments")
                If lblComments.Text = "INVALID" Then
                    grow.BackColor = Drawing.Color.Red
                    grow.Font.Bold = True
                    iInvalidcount = iInvalidcount + 1
                End If
            Next

            lblDetailMsg.Text = "There are " & grdDetails.Rows.Count & " item(s) in this order with " & iInvalidcount & " invalid item(s)."
        Else
            lblDetailMsg.Text = "There are 0 line items in this order."
            pnlDetails.Visible = False
        End If
    End Sub
    Private Sub grdDetails_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdDetails.Sorting
        Dim dv As DataView = New DataView(Session("Details"))
        Dim order As String = ""
        If Not ViewState("DTLOrder") Is Nothing Then
            order = ViewState("DTLOrder")
        End If

        If order.StartsWith(e.SortExpression) Then
            If order.ToString.EndsWith("ASC") Then
                order = "DESC"
            Else
                order = "ASC"
            End If
        Else
            order = "ASC"
        End If

        ViewState("DTLOrder") = e.SortExpression + " " & order
        dv.Sort = ViewState("DTLOrder")
        grdDetails.DataSource = dv
        grdDetails.DataBind()
    End Sub

    Protected Sub btnExportDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportDetails.Click
        Dim sReport As String = Server.MapPath("Temp\" & Session.SessionID & ".pdf")
        Dim dv As DataView = New DataView(Session("DETAILS"))
        Dim dvDTLRow As DataRowView = dv.Item(0)

        Dim dvSum As DataView = New DataView(Session("SUMMARY"))
        dvSum.RowFilter = "ORDERNUM = '" & dvDTLRow("CO_ODNO") & "'"
        Dim dvSUMRow As DataRowView = dvSum.Item(0)

        dv.Sort = ViewState("DTLOrder")

        Dim dvTnd As DataView = New DataView(Session("TENDERS"))
        dvTnd.Sort = "REF_SEQ ASC"

        Dim document As New Document(PageSize.LETTER, 50, 50, 40, 40)
        Dim write As PdfWriter = PdfWriter.getInstance(document, New FileStream(sReport, FileMode.Create))

        Dim reportTitle As String = ""
        If dvSUMRow("ITEM_VALID") = "INVALID" Then
            reportTitle = "THIS ORDER CONTAINS 1 OR MORE RESTRICTED PRODUCT FOR ORDER TYPE: " & dvSUMRow("ORDER_TYPE")
        End If

        If dvSUMRow("TENDER_VALID") = "INVALID" Then
            If reportTitle <> "" Then
                reportTitle = reportTitle & " AND 1 OR MORE RESTRICTED TENDER!"
            Else
                reportTitle = "THIS ORDER CONTAINS 1 OR MORE RESTRICTED TENDER "
            End If
        End If


        write.setPageEvent(New PageNumbersEventHelper(reportTitle, " as of " & Now.ToLongDateString & " " & Now.ToLongTimeString, Server.MapPath("./Images/logo.jpg")))

        Try
            document.open()
        Catch ex As Exception
            Throw New Exception("open crashed" & ex.Message.ToString)
        End Try

        Dim tbl As com.lowagie.text.Table = New Table(12)

        tbl.cellsFitPage = True
        tbl.setWidth(100)
        tbl.setPadding(3)


        ' tbl.setDefaultCellBackgroundColor(Drawing.Color.FromArgb(28, 94, 95))
        Dim headerfont As Font
        Dim rptfont As Font
        Dim rptBoldfont As Font

        Try
            headerfont = New Font(Font.HELVETICA, 8, Font.BOLD, Drawing.Color.Black)
            rptfont = New Font(Font.HELVETICA, 6, Font.NORMAL, Drawing.Color.Black)
            rptBoldfont = New Font(Font.HELVETICA, 6, Font.BOLD, Drawing.Color.Black)

        Catch ex As Exception
            Throw New Exception("document.head crashed.->" & ex.Message.ToString)
        End Try

        With tbl
            .setBorder(0)
            .addCell(SPURDataControl.makeCell("Order Number: " & dvSUMRow("ORDERNUM"), headerfont, tbl, True, 4, Element.ALIGN_LEFT))
            .addCell(SPURDataControl.makeCell("Order Type :" & dvSUMRow("ORDER_TYPE"), headerfont, tbl, True, 4))
            .addCell(SPURDataControl.makeCell("Customer Number: " & dvSUMRow("CUSTOMER").ToString.Trim, headerfont, tbl, True, 4, Element.ALIGN_RIGHT))
            .addCell(SPURDataControl.makeCell("Order Date: " & dvSUMRow("ORDER_DATE"), headerfont, tbl, True, 4, Element.ALIGN_LEFT))
            .addCell(SPURDataControl.makeCell("Ship Date :" & dvSUMRow("EXPECTED_DELIVERY"), headerfont, tbl, True, 4))
            .addCell(SPURDataControl.makeCell("STATUS:" & dvSUMRow("STATUS"), headerfont, tbl, True, 4, Element.ALIGN_RIGHT))
        End With
        With tbl
            .setBorder(3)
            .addCell(SPURDataControl.makeCell("Product Number", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Order Qty", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Product Description", headerfont, tbl, True, 3))
            .addCell(SPURDataControl.makeCell("DEP", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Size ml", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Prod. Type", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Unit Price", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Extended Price", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Retail Price", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell(" ", headerfont, tbl, True))
            .endHeaders()
        End With

        tbl.setDefaultCellBackgroundColor(Drawing.Color.White)

        Dim rowFont As Font

        For Each drow As DataRowView In dv
            If drow("COMMENTS") = "INVALID" Then
                tbl.setDefaultCellBackgroundColor(Drawing.Color.Red)
                rowFont = rptBoldfont
            Else
                tbl.setDefaultCellBackgroundColor(Drawing.Color.White)
                rowFont = rptfont
            End If

            With tbl
                .addCell(SPURDataControl.makeCell(drow("ITEM").ToString.Trim, rowFont, tbl, False))
                .addCell(SPURDataControl.makeCell(drow("QTY").ToString.Trim, rowFont, tbl, False))
                .addCell(SPURDataControl.makeCell(drow("PROD_DESCRIPTION").ToString.Trim, rowFont, tbl, False, 3, Element.ALIGN_LEFT))
                .addCell(SPURDataControl.makeCell(drow("BOTTLE_DEP").ToString.Trim, rowFont, tbl, False, 1, Element.ALIGN_RIGHT))
                .addCell(SPURDataControl.makeCell(drow("VOLUME").ToString.Trim, rowFont, tbl, False, 1))
                .addCell(SPURDataControl.makeCell(drow("ITEM_CATEGORY").ToString.Trim, rowFont, tbl, False))
                .addCell(SPURDataControl.makeCell(drow("BASIC_PRICE").ToString.Trim, rowFont, tbl, False, 1, Element.ALIGN_RIGHT))
                .addCell(SPURDataControl.makeCell(drow("EXTENDED_PRICE").ToString.Trim, rowFont, tbl, False, 1, Element.ALIGN_RIGHT))
                .addCell(SPURDataControl.makeCell(drow("RETAIL_PRICE").ToString.Trim, rowFont, tbl, False, 1, Element.ALIGN_RIGHT))
                .addCell(SPURDataControl.makeCell(drow("COMMENTS").ToString.Trim, rowFont, tbl, False))
            End With
        Next
        document.add(tbl)
        document.add(New Chunk(" "))
        tbl = New Table(9)
        tbl.cellsFitPage = True
        tbl.setWidth(100)
        tbl.setPadding(3)

        With tbl
            .setBorder(0)
            .addCell(SPURDataControl.makeCell("Tender Payments For Order: " & dvSUMRow("ORDERNUM"), headerfont, tbl, True, 9, Element.ALIGN_LEFT))
        End With

        tbl.setDefaultCellBackgroundColor(Drawing.Color.White)
        If dvTnd.Count > 0 Then
            With tbl
                .setBorder(3)
                .addCell(SPURDataControl.makeCell("Payment#", headerfont, tbl, True))
                .addCell(SPURDataControl.makeCell("Timestamp", headerfont, tbl, True))
                .addCell(SPURDataControl.makeCell("EMP", headerfont, tbl, True))
                .addCell(SPURDataControl.makeCell("Tender", headerfont, tbl, True))
                .addCell(SPURDataControl.makeCell("Pay Amt", headerfont, tbl, True))
                .addCell(SPURDataControl.makeCell("Auth#", headerfont, tbl, True))
                .addCell(SPURDataControl.makeCell("Pay Comment", headerfont, tbl, True))
                .addCell(SPURDataControl.makeCell("Pay Date", headerfont, tbl, True))
                .addCell(SPURDataControl.makeCell("Remarks", headerfont, tbl, True))
                .endHeaders()
            End With

            For Each drow As DataRowView In dvTnd

                If drow("COMMENTS") = "INVALID" Then
                    tbl.setDefaultCellBackgroundColor(Drawing.Color.Red)
                    rowFont = rptBoldfont
                Else
                    tbl.setDefaultCellBackgroundColor(Drawing.Color.White)
                    rowFont = rptfont
                End If

                With tbl
                    .addCell(SPURDataControl.makeCell(drow("REF_SEQ").ToString.Trim, rowFont, tbl, False))
                    .addCell(SPURDataControl.makeCell(drow("TIMESTAMP").ToString.Trim, rowFont, tbl, False))
                    .addCell(SPURDataControl.makeCell(drow("EMP_NO").ToString.Trim, rowFont, tbl, False))
                    .addCell(SPURDataControl.makeCell(drow("TENDER_TYPE").ToString.Trim, rowFont, tbl, False))
                    .addCell(SPURDataControl.makeCell(drow("PAY_AMT").ToString.Trim, rowFont, tbl, False))
                    .addCell(SPURDataControl.makeCell(drow("CREDIT_AUTH").ToString.Trim, rowFont, tbl, False))
                    .addCell(SPURDataControl.makeCell(drow("PAY_COMMENT").ToString.Trim, rowFont, tbl, False))
                    .addCell(SPURDataControl.makeCell(drow("PAY_DT").ToString.Trim, rowFont, tbl, False))
                    .addCell(SPURDataControl.makeCell(drow("COMMENTS").ToString.Trim, rowFont, tbl, False))
                End With


            Next
        Else
            tbl.endHeaders()
            tbl.addCell(SPURDataControl.makeCell("There are no payments for this order yet.", rptfont, tbl, False, 9, Element.ALIGN_LEFT))
        End If

        document.add(tbl)
        document.close()

        SPURDataControl.ExportPDF(sReport, dvSUMRow("ORDERNUM").ToString.Trim, Me)
    End Sub

    Protected Sub btnPriceList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPriceList.Click
        Dim sReport As String = Server.MapPath("Temp\" & Session.SessionID & ".pdf")
        Dim dv As DataView = New DataView(Session("DETAILS"))
        Dim dvDTLRow As DataRowView = dv.Item(0)

        Dim dvSum As DataView = New DataView(Session("SUMMARY"))
        dvSum.RowFilter = "ORDERNUM = '" & dvDTLRow("CO_ODNO") & "'"
        Dim dvSUMRow As DataRowView = dvSum.Item(0)

        dv.Sort = ViewState("DTLOrder")

        Dim document As New Document(PageSize.LETTER, 50, 50, 40, 40)
        Dim write As PdfWriter = PdfWriter.getInstance(document, New FileStream(sReport, FileMode.Create))
        Dim reportTitle As String = "RETAIL PRICE LIST"

        write.setPageEvent(New PageNumbersEventHelper(reportTitle, " as of " & Now.ToLongDateString & " " & Now.ToLongTimeString, Server.MapPath("./Images/logo.jpg")))

        Try
            document.open()
        Catch ex As Exception
            Throw New Exception("open crashed" & ex.Message.ToString)
        End Try

        Dim tbl As com.lowagie.text.Table = New Table(12)

        tbl.cellsFitPage = True
        tbl.setWidth(100)
        tbl.setPadding(3)

        ' tbl.setDefaultCellBackgroundColor(Drawing.Color.FromArgb(28, 94, 95))
        Dim headerfont As Font
        Dim rptfont As Font
        Dim rptBoldfont As Font

        Try
            headerfont = New Font(Font.HELVETICA, 8, Font.BOLD, Drawing.Color.Black)
            rptfont = New Font(Font.HELVETICA, 6, Font.NORMAL, Drawing.Color.Black)
            rptBoldfont = New Font(Font.HELVETICA, 7, Font.BOLD, Drawing.Color.Black)

        Catch ex As Exception
            Throw New Exception("document.head crashed.->" & ex.Message.ToString)
        End Try

        Try
            headerfont = New Font(Font.HELVETICA, 8, Font.BOLD, Drawing.Color.Black)
            rptfont = New Font(Font.HELVETICA, 6, Font.NORMAL, Drawing.Color.Black)

        Catch ex As Exception
            Throw New Exception("document.head crashed.->" & ex.Message.ToString)
        End Try


        With tbl
            .setBorder(0)
            .addCell(SPURDataControl.makeCell("Order Number: " & dvSUMRow("ORDERNUM"), headerfont, tbl, True, 4, Element.ALIGN_LEFT))
            .addCell(SPURDataControl.makeCell("Sale Type :" & dvSUMRow("ORDER_TYPE"), headerfont, tbl, True, 4))
            .addCell(SPURDataControl.makeCell("Customer Number: " & dvSUMRow("CUSTOMER").ToString.Trim, headerfont, tbl, True, 4, Element.ALIGN_RIGHT))
            .addCell(SPURDataControl.makeCell("Order Date: " & dvSUMRow("ORDER_DATE"), headerfont, tbl, True, 4, Element.ALIGN_LEFT))
            .addCell(SPURDataControl.makeCell("Ship Date :" & dvSUMRow("EXPECTED_DELIVERY"), headerfont, tbl, True, 4))
            .addCell(SPURDataControl.makeCell("STATUS:" & dvSUMRow("STATUS"), headerfont, tbl, True, 4, Element.ALIGN_RIGHT))
        End With
        With tbl
            .setBorder(3)
            .addCell(SPURDataControl.makeCell("Product Number", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Order Qty", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Product Description", headerfont, tbl, True, 4))
            .addCell(SPURDataControl.makeCell("DEP", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Size ml", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Prod. Type", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Unit Price", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Extended Price", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Retail Price", headerfont, tbl, True))
            .endHeaders()
        End With


        tbl.setDefaultCellBackgroundColor(Drawing.Color.White)

        For Each drow As DataRowView In dv
            With tbl
                .addCell(SPURDataControl.makeCell(drow("ITEM").ToString.Trim, rptfont, tbl, False))
                .addCell(SPURDataControl.makeCell(drow("QTY").ToString.Trim & " ", rptfont, tbl, False, 1, Element.ALIGN_RIGHT))
                .addCell(SPURDataControl.makeCell(drow("PROD_DESCRIPTION").ToString.Trim, rptfont, tbl, False, 4, Element.ALIGN_LEFT))
                .addCell(SPURDataControl.makeCell(drow("BOTTLE_DEP").ToString.Trim, rptfont, tbl, False, 1, Element.ALIGN_RIGHT))
                .addCell(SPURDataControl.makeCell(drow("VOLUME").ToString.Trim, rptfont, tbl, False, 1))
                .addCell(SPURDataControl.makeCell(drow("ITEM_CATEGORY").ToString.Trim, rptfont, tbl, False))
                .addCell(SPURDataControl.makeCell(drow("BASIC_PRICE").ToString.Trim, rptfont, tbl, False, 1, Element.ALIGN_RIGHT))
                .addCell(SPURDataControl.makeCell(drow("EXTENDED_PRICE").ToString.Trim, rptfont, tbl, False, 1, Element.ALIGN_RIGHT))
                .addCell(SPURDataControl.makeCell(drow("RETAIL_PRICE").ToString.Trim, rptfont, tbl, False, 1, Element.ALIGN_RIGHT))
            End With
        Next

        If dv.Count > 0 Then
            'add summary information
            Dim iTotalOrdered As Integer = 0
            iTotalOrdered = dv.ToTable.Compute("SUM(QTY)", "1=1")
            With tbl
                .setBorder(0)
                .addCell(SPURDataControl.makeCell("Total Qty", headerfont, tbl, False))
                .addCell(SPURDataControl.makeCell(iTotalOrdered, headerfont, tbl, False, 1, Element.ALIGN_RIGHT))
                .addCell(SPURDataControl.makeCell(" ", rptfont, tbl, False, 10, Element.ALIGN_LEFT))
            End With
        End If
        document.add(tbl)

        document.close()

        SPURDataControl.ExportPDF(sReport, "Retail.Pricelist." & dvSUMRow("ORDERNUM").ToString.Trim, Me)
    End Sub

    Private Sub grdTenders_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdTenders.DataBound
        Dim iInvalidcount As Integer = 0
        If grdTenders.Rows.Count > 0 Then
            
            Dim lblComments As Label = Nothing
            For Each grow As GridViewRow In grdTenders.Rows
                lblComments = grow.FindControl("lblComments")
                If lblComments.Text = "INVALID" Then
                    grow.BackColor = Drawing.Color.Red
                    grow.Font.Bold = True
                    iInvalidcount = iInvalidcount + 1
                End If
            Next

            lblTenderMsg.Text = "There are " & grdTenders.Rows.Count & " payment(s) in this order with " & iInvalidcount & " invalid payment(s)."
        Else
            lblTenderMsg.Text = "There are No Payments made for this order yet."

        End If
    End Sub

   
End Class