<%@ Page Language="vb" AutoEventWireup="false" Title="Create Location Transfer" MasterPageFile="~/Main.Master" CodeBehind="LocTransferCreate.aspx.vb" Inherits="_941Support.LocTransferCreate" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">

<script type="text/javascript">
function popuplist(){
window.open("CatchAllSkus.aspx","_blank","width=750,height=450,top=200");
}

function showStoreSearch(txtItemNo, txtUnitPrice)
   {
        var WinSettings = "center:yes;resizable:no;dialogHeight:450px;dialogWidth:800px"
        var myValue = window.showModalDialog("CatchAllSkus.aspx","",WinSettings);
       
          if (myValue == null || myValue == '')
        {
            //alert("Nothing selected");
        }
        else
        {
            var partOfStr  = myValue.split(',');
            document.getElementById(txtItemNo).value = partOfStr[0];
            document.getElementById(txtUnitPrice).value = partOfStr[1];
        }
        return false;        
   }

  function showCustomerList(txtCustID)
   { 
   //alert("OK");
        var WinSettings = "center:yes;resizable:no;dialogHeight:500px;dialogWidth:900px"
        var myCust = window.showModalDialog("LocTransferCustomer.aspx?custno=" + document.getElementById(txtCustID).value ,"",WinSettings);
      
          if (myCust == null || myCust == '')
        {
            //alert("Nothing selected");
        }
        else
        {
            
            document.getElementById(txtCustID).value = myCust;
            
        }
        return false;        
   }

</script>

<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true" ShowSummary="false" />   
<table cellspacing="0" cellpadding="0" align="center" border="0" style="width: 90%;">

<tr>
    <td>&nbsp;</td>
    <td colspan="6">
    <asp:Label ID="lblMsg" Text="" runat="server" CssClass="WelcomeCss"></asp:Label>
    <asp:label ID="txtCatchAll" runat="server"  ></asp:label>
    </td>
</tr>

<tr><td colspan="7">&nbsp;</td></tr>



<tr><td colspan="7">&nbsp;</td></tr>

<tr>
    <td colspan="1">
    &nbsp;
    </td>
    <td colspan="2" valign="top">
        <table border="1" align="left">
        <tr>
            <td colspan="2" style="background-color:#CBD49E">
            <asp:Label ID="Label1" runat="server" Text="Original Customer Information:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl1" runat="server" Text="Customer #:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" style="white-space:nowrap;">
            <asp:Label ID="lblOrigCustNo" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl2" runat="server" Text="Customer Name:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" style="white-space:nowrap;">
            <asp:Label ID="lblOrigCustName" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl3" runat="server" Text="Address:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:Label ID="lblOrigCustAddress" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl4" runat="server" Text="City:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:Label ID="lblOrigCustCity" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl5" runat="server" Text="Province:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" style="white-space:nowrap;">
            <asp:Label ID="lblOrigCustProvince" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl6" runat="server" Text="Postal Code:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" style="white-space:nowrap;">
            <asp:Label ID="lblOrigCustPostal" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl7" runat="server" Text="Phone:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" style="white-space:nowrap;">
            <asp:Label ID="lblOrigCustPhone" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        </table>
    </td>
    
    <td>
    &nbsp;
    </td>
    
    <td colspan="2" valign="top">
        <table border="1" align="left">
        <tr>
            <td colspan="2" style="background-color:#CBD49E">
            <asp:Label ID="label2" runat="server" Text="New Customer Information:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl11" runat="server" Text="Customer #:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" style="white-space:nowrap;">
            <asp:TextBox ID="txtNewCustNo" runat="server" Width="50px"></asp:TextBox>&nbsp;
            <asp:LinkButton ID="lnkLookUp"  runat="server" Text="Search" CausesValidation="False"></asp:LinkButton>&nbsp;
            <asp:Button ID="btnSetCustomer" runat="server" CssClass="BtnCss" Text="Set" Width="40px"  />&nbsp;  
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space: nowrap;">
            <asp:Label ID="lbl12" runat="server" Text="Customer Name:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" style="height: 28px">
            <asp:TextBox ID="txtNewCustName" runat="server" ></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl13" runat="server" Text="Address:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:TextBox ID="txtNewCustAddress" runat="server" ></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl14" runat="server" Text="City:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" style="height: 28px">
            <asp:TextBox ID="txtNewCustCity" runat="server" ></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl15" runat="server" Text="Province:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:TextBox ID="txtNewCustProvince" runat="server"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl16" runat="server" Text="Postal Code:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:TextBox ID="txtNewCustPostal" runat="server" ></asp:TextBox>&nbsp;
            </td>
        </tr>
        </table>
    </td>
      
    <td>
    &nbsp;
    </td>  
</tr>


<tr><td colspan="7"><br /></td></tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td colspan="2" align="left">
    <asp:Label ID="lbl21" runat="server" Text="Expected Transfer Date:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:TextBox ID="txtNewCustTransfer" runat="server" width="80"></asp:TextBox>
     <a id="a1" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtNewCustTransfer, 'a1', 'yyyy-MM-dd');return false;"
		href="#" name="a1"><img id="Img1" runat="server" height="21" alt="Calendar"  src="Images/calendar.gif" width="34" align="top" border="0"/></a>
    
    &nbsp;
    </td>
    <td></td>
    <td colspan="2" align="left">
    <asp:Label ID="lbl22" runat="server" Text="Return Order#:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblReturnNo" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
    </td>
    <td>
    &nbsp;
    </td>
</tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td colspan="2" align="left">
    <asp:Label ID="lbl23" runat="server" Text="Contact Name:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:TextBox ID="txtNewCustContact" runat="server" ></asp:TextBox>&nbsp;
    </td>
    <td></td>
    <td colspan="2" align="left">
    <asp:Label ID="lbl24" runat="server" Text="Transfer Order#:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblTransferNo" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
    </td>
    <td>
    &nbsp;
    </td>
</tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td colspan="2" align="left">
    <asp:Label ID="lbl25" runat="server" Text="Corporate Email:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:TextBox ID="txtNewCustEmail" runat="server" ></asp:TextBox>&nbsp;
    </td>
    <td></td>
    <td colspan="2" align="left">
    <asp:Label ID="lbl26" runat="server" Text="Prepared By:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblPreparedBy" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
    </td>
    <td>
    &nbsp;
    </td>
</tr>


<tr>
    <td colspan="7" align="right">
    <asp:Button ID="btnSave" runat="server" CssClass="BtnCss" Text="Save" />    <asp:Button ID="btnFinalize" runat="server" CssClass="BtnCss" Text="Finalize" 
    OnClientClick ="return confirm('Are you sure you want to finalize the order?');" />
        <asp:Button ID="btnBack" runat="server" CssClass="BtnCss" Text="Back" />
    &nbsp;&nbsp;&nbsp;
    </td>
</tr>

<tr><td colspan="7">&nbsp;</td></tr>

<tr>
    <td colspan="7" align="center">
    <asp:Label ID="lblMessage" Text="" runat="server" CssClass="MsgCss"></asp:Label>
    </td>
</tr>

<tr>
    <td>&nbsp;</td>
    <td colspan="6" align="center">
        <div style="z-index: 10; overflow: auto;">
            <asp:GridView ID="grdItemList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            BackColor="White" BorderColor="#CBD49E" BorderStyle="Inset" BorderWidth="2px" DataKeyNames="cod_line" 
            CellPadding="4" Font-Size="Small" Font-Names="Arial" GridLines="Vertical" ForeColor="Black" >
            <FooterStyle BackColor="#CCCC99" />
            <RowStyle BackColor="#F7F7DE" Font-Size="Small" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="White" BorderStyle="None" />
            <HeaderStyle BackColor="#CBD49E" Font-Size="Small" ForeColor="Maroon" Font-Bold="True" />            
                <Columns>  
                    <asp:TemplateField HeaderText="Line" SortExpression="cod_line">
                        <ItemTemplate>
                            <asp:Label ID="lblItemListLine" runat="server" Text='<%# Bind("cod_line") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="item" HeaderText="Item No" SortExpression="item" />
                    <asp:BoundField DataField="item_desc" HeaderText="Item Description" SortExpression="item_desc" />
                    <asp:BoundField DataField="catchall_desc" HeaderText="(Special Desc - for Catch All)" SortExpression="catchall_desc" />
                    <asp:BoundField DataField="item_category" HeaderText="Item Category" SortExpression="item_category" />
                    <asp:TemplateField HeaderText="Quantity" SortExpression="qty">
                        <ItemTemplate>
                            <asp:TextBox ID="txtItemListQty" Width="50px" OnTextChanged="txtItemQty_Changed" 
                            AutoPostBack="true" runat="server" Text='<%# Bind("qty") %>'></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="price" HeaderText="Unit Price" SortExpression="price" DataFormatString="{0:F2}" />
                    <asp:BoundField DataField="extd_price" HeaderText="Extended Price" SortExpression="extd_price" DataFormatString="{0:F2}" />
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkItemListDelete" runat="server" CausesValidation="False"
                            OnClientClick ="return confirm('Are you sure you want to delete this line?');" 
                            CommandName="Delete" Text="Delete"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        </div>       
    </td>
</tr>

<tr><td colspan="7">&nbsp;</td></tr>

<tr>
    <td colspan="7">
    <asp:Panel runat="server" ID="pnlAddtoolbar" DefaultButton="btnAddItem">
    <table border="1">
    <tr>
        <td align="left" style="background-color:#CBD49E">
        <asp:Label id="lbl30" runat="server" Text="Item No(*)" CssClass="HeadCss"></asp:Label>
        </td>
        
        <td align="left" style="background-color:#CBD49E">
        <asp:Label id="lbl31" runat="server" Text="(Special Desc - For Catch All)" CssClass="HeadCss"></asp:Label>
        </td>
        
        <td align="left" style="background-color:#CBD49E">
        <asp:Label id="lbl32" runat="server" Text="Quantity(*)" CssClass="HeadCss"></asp:Label>
        </td>
        
        <td align="left" style="background-color:#CBD49E">
        <asp:Label id="lbl33" runat="server" Text="Unit Price" CssClass="HeadCss"></asp:Label>
        </td>
        
        <td align="left" style="background-color:#CBD49E">
        &nbsp;
        </td>        
    </tr>
    
    <tr>
        <td align="left">
        <asp:TextBox ID="txtAddItemNo" runat="server" Width="80"></asp:TextBox>
        <asp:CompareValidator ID="cmpItemNo" runat="server" ControlToValidate="txtAddItemNo"
        Operator="DataTypeCheck" Type="Integer" ErrorMessage="Item No should be numeric" Display="None"></asp:CompareValidator>
         </td>
        
        <td align="left">
        <asp:TextBox ID="txtAddCatchAll" runat="server" Width="180"></asp:TextBox>
        </td>
        
        <td align="left">
        <asp:TextBox ID="txtAddQty" runat="server" Width="80"></asp:TextBox>
        <asp:CompareValidator ID="cmpQty" runat="server" ControlToValidate="txtAddQty"
        Operator="DataTypeCheck" Type="Integer" ErrorMessage="Quantity should be numeric" Display="None"></asp:CompareValidator>
        </td>
        
        <td align="left">
        <asp:TextBox ID="txtAddUnitPrice" runat="server" Width="80"></asp:TextBox>
        </td>
        
        <td align="center">
        &nbsp;
        <asp:Button ID="btnAddItem" runat="server" CssClass="BtnCss" Text="Add Item" />
        &nbsp;
        </td>        
    </tr>
    
    </table>
    </asp:Panel>
    </td>
</tr>

<tr><td colspan="7">&nbsp;</td></tr>

<tr>
    <td colspan="7" align="right" style="height: 22px"><asp:Button ID="btnSave2" runat="server" CssClass="BtnCss" Text="Save" />
    <asp:Button ID="btnFinalize2" runat="server" CssClass="BtnCss" Text="Finalize" 
    OnClientClick ="return confirm('Are you sure you want to finalize the order?');" />    
    <asp:Button ID="btnBack2" runat="server" CssClass="BtnCss" Text="Back" />
    &nbsp;&nbsp;&nbsp;
    </td>
</tr>

<tr><td colspan="7">&nbsp;</td></tr>

<tr>
    <td colspan="7" align="left">
    <asp:LinkButton ID="lnkCatchAll" runat="server" Text="List of Catch All SKUs"></asp:LinkButton>
    </td>
</tr>

<tr><td colspan="7">&nbsp;</td></tr>
<tr><td colspan="7">&nbsp;</td></tr>

</table>

</asp:Content>