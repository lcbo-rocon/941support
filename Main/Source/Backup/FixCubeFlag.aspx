<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="FixCubeFlag.aspx.vb" Inherits="_941Support.FixCubeFlag" 
    title="Fix Location Cube Item Flag" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
<table width="750" align="center" border="0">
        <tr>
            <td align="center">
                <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="Maroon"></asp:Label>&nbsp;
                <br />
            </td>
        </tr>
        <tr>
            <td class="lprompt" align="center" style="text-align: center" >Enter the Item Number:             
                <asp:TextBox ID="txtItem" runat="server" CssClass="textbox" Columns="8" maxlength="8"></asp:TextBox>&nbsp;
                <asp:Button ID="btnSubmit" runat="server" CssClass="BtnCss" Text="Submit" />&nbsp;
                <asp:Button ID="btnReset" runat="server" CssClass="BtnCss" Text="Clear" />
            </td>
        </tr>
        <tr>
            <td align="center" class="lprompt">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center">
            <asp:Panel ID="pnlLocationResults" runat="server" Width="700" Visible="false">
                <table width="100%">
                    <tr>
                        <td align="center"><asp:Label ID="lblLocationResults" runat="server" CssClass="lprompt" Font-Bold="True" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="grdLocationResults" runat="server" AutoGenerateColumns="False"
                                BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                                CellPadding="4" ForeColor="Black" GridLines="Horizontal" AllowSorting="True" DataSourceID="dataMismatch">
                                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                                <Columns>
                                    <asp:TemplateField HeaderText="ITEM">
                                        <ItemTemplate>
                                            <asp:Label ID="lblItem" runat="server" Text='<%# Bind("ITEM") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TICKET ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTicket" runat="server" Text='<%# Bind("TICKET_ID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="LOCATION">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLo_Loca" runat="server" Text='<%# Bind("LO_LOCA") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="LOCA_PALLET">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblLoca_pallet" runat="server" Text='<%# Bind("LOCA_PALLET") %>' Visible="false"></asp:Label>
                                            <asp:DropDownList ID="drpLoca_pallet" runat="server">
                                                <asp:ListItem Text="Y" Value="Y"></asp:ListItem>
                                                <asp:ListItem Text="N" Value="N"></asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblLoca_pallet" runat="server" Text='<%# Bind("LOCA_PALLET") %>'></asp:Label>
                                            <asp:DropDownList ID="drpLoca_pallet" runat="server" Visible="false">
                                                <asp:ListItem Text="Y" Value="Y"></asp:ListItem>
                                                <asp:ListItem Text="N" Value="N"></asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="LOCATION_QUANT">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblQuant" runat="server" Text='<%# Bind("LOCATION_QUANT") %>' Visible="false"></asp:Label>
                                            <asp:DropDownList ID="drpQuant" runat="server">
                                                <asp:ListItem Text="Y" Value="Y"></asp:ListItem>
                                                <asp:ListItem Text="N" Value="N"></asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblQuant" runat="server" Text='<%# Bind("LOCATION_QUANT") %>'></asp:Label>
                                            <asp:DropDownList ID="drpQuant" runat="server" Visible="false">
                                                <asp:ListItem Text="Y" Value="Y"></asp:ListItem>
                                                <asp:ListItem Text="N" Value="N"></asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="MATCH" HeaderText="MATCH" ReadOnly="True" />
                                    <asp:TemplateField ShowHeader="False">
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lnkUpdate" runat="server" CausesValidation="True" CommandName="UpdateLoca"
                                                Text="Update" CommandArgument='<%# Bind("LO_LOCA") %>'></asp:LinkButton>
                                            <asp:LinkButton ID="lnkCancel" runat="server" CausesValidation="False" CommandName="Cancel"
                                                Text="Cancel"></asp:LinkButton>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                                Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="dataMismatch" runat="server" ConnectionString="<%$ ConnectionStrings:TEST %>"
                                ProviderName="<%$ ConnectionStrings:TEST.ProviderName %>" SelectCommand="select t1.item as ITEM, T1.ticket_id as ticket_id, t1.lo_loca as LO_LOCA, t1.cube_item_fl as LOCA_PALLET, t2.cube_item_fl as LOCATION_QUANT,&#13;&#10;CASE WHEN T1.cube_item_fl <> t2.cube_item_fl then 'MISMATCH!!!' else 'OK' end as &quot;MATCH&quot;&#13;&#10;from loca_pallet t1&#13;&#10;inner join location_quant t2 on t1.lo_loca = t2.lo_loca&#13;&#10;where ticket_type <> 'DEL'&#13;&#10;and t1.cube_item_fl <> t2.cube_item_fl&#13;&#10;and to_number(t1.item) = :ITEM">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="txtItem" DefaultValue="0" Name="ITEM" PropertyName="Text" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
