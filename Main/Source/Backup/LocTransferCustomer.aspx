<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LocTransferCustomer.aspx.vb" Inherits="_941Support.LocTransferCustomer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Customer Information</title>
    <link href="Stylesheet.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">window.name = "DefaultPage"</script>
    <script language="javascript" type="text/javascript">
    <!--
    function closeDialog(custno)
    {
     if (!(custno == null || custno == ''))
     {
        //alert(store)
        window.returnValue = custno; 
     }
     window.close();
    }
// -->
</script>
</head>
<body>
    <form id="form1" runat="server" target="DefaultPage">
    <div>
    
    <table>
    
    <tr><td colspan="5">&nbsp;</td></tr>
        
    <tr><td colspan="5">&nbsp;</td></tr>
    
    <tr>
        <td colspan="5">
            <table>
                <tr>
                    <td class="HeadCss">Customer No:</td>
                    <td><asp:TextBox runat="server" ID="txtCustNo" Columns="10" MaxLength="10"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="HeadCss">Customer Name:</td>
                    <td><asp:TextBox runat="server" ID="txtCustName" Columns="35" MaxLength="35"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <asp:Button runat="server" ID="btnSearch" Text="Search" CssClass="BtnCss" />
                        &nbsp;
                        <asp:Button runat="server" ID="btnReset" Text="Reset" CssClass="BtnCss" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
        <tr>
            <td align="center" colspan="5">
    <asp:Label ID="lblMsg" runat="server" CssClass="MsgCss"></asp:Label></td>
        </tr>
    <tr>
        <td colspan="5">
        <div style="z-index: 10; overflow: auto;height:350px">
            <asp:GridView ID="grdCustomer" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            BackColor="White" BorderColor="#CBD49E" BorderStyle="None" BorderWidth="1px" 
            CellPadding="4" Font-Size="Small" Font-Names="Arial" GridLines="Vertical" ForeColor="Black" DataKeyNames="cu_no"   >
            <FooterStyle BackColor="#CCCC99" />
            <RowStyle BackColor="#F7F7DE" Font-Size="Small" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="White" BorderStyle="None" />
            <HeaderStyle BackColor="#CBD49E" Font-Size="Small" ForeColor="Maroon" Font-Bold="True" />            
                <Columns>                                   
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                        <asp:LinkButton ID="lnkSelect" runat="server" CausesValidation="False" CommandName="SelectCust"
                        CommandArgument='<%# Bind("CU_NO") %>' Text="Select"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="cu_no" HeaderText="Customer#" SortExpression="cu_no" />
                    <asp:BoundField DataField="cu_name" HeaderText="Customer Name" SortExpression="cu_name" />
                    <asp:BoundField DataField="add1_sh" HeaderText="Address" SortExpression="add1_sh" />
                    <asp:BoundField DataField="city_sh" HeaderText="City" SortExpression="city_sh" />
                    <asp:BoundField DataField="prov_sh" HeaderText="Province" SortExpression="prov_sh" />
                    <asp:BoundField DataField="post_code" HeaderText="Postal Code" SortExpression="post_code" />
                    <asp:BoundField DataField="phone_nu" HeaderText="Phone" SortExpression="phone_nu" />
                </Columns>
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        </div>       
    </td>    
    </tr>
    
    <tr><td colspan="5">
    <asp:Button ID="btnRefresh" runat="server" Visible="false" />
    &nbsp;</td></tr>
    <tr><td colspan="5">&nbsp;</td></tr>
    <tr><td colspan="5">&nbsp;</td></tr>
    
    </table>
    
    
    </div>
    </form>
</body>
</html>
