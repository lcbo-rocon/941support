Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Imports System.IO.Directory
Imports System.Data
Imports System.Security.Principal
Imports System.Threading
Imports RSG_ROC.DataLayer
Imports RSG_ROC.Exceptions
Imports RSG_ROC.OrderClasses
Imports RSG_ROC.GLClasses
Imports System.Text.RegularExpressions
Partial Public Class FreightForwardDetails
    Inherits System.Web.UI.Page

    Private Sub FreightForwardDetails_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Dim ex As Exception = Server.GetLastError
        Session("ExceptionObj") = ex
        Response.Redirect("~\ErrorPage.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lblheading As Label = Master.FindControl("lblMainTitle")
        If Not lblheading Is Nothing Then
            lblheading.Text = "Freight Forwarding"
        End If

        If Not IsPostBack Then
            Dim cu_no As String = ""
            Dim co_odno As String = ""
            Dim inv_dt As String = ""
            Dim del_chrg As Double = 0
            Dim del_tax As Double = 0
            Dim del_total As Double = 0
            Dim item_lst As DataTable = Nothing
            Dim sub_total As Double = 0
            Dim disc As Double = 0
            Dim markup As Double = 0
            Dim levy As Double = 0
            Dim hst As Double = 0
            Dim hst_rounding As Double = 0
            Dim total As Double = 0
            Dim total_plus_del As Double = 0


            co_odno = Session("FreightFwdOrder")
            Dim roc_order As ROC_ORDER = New ROC_ORDER(co_odno, "A")
            cu_no = roc_order.customer_no
            inv_dt = roc_order.inv_dt.ToString("yyyy-MM-dd")
            del_chrg = roc_order.delivery_charge
            del_tax = roc_order.delivery_tax2
            del_total = del_chrg + del_tax
            sub_total = roc_order.Extended_Amount_Total
            disc = roc_order.Discount_Total
            markup = roc_order.Markup_Total
            levy = roc_order.Levy_Total
            hst = roc_order.HST_Total
            hst_rounding = roc_order.HST_Rounding_Total
            total = sub_total + disc + markup + levy + hst + hst_rounding
            total_plus_del = total + del_total

            lblCuNo.Text = cu_no
            lblOrderNo.Text = co_odno
            lblInvDt.Text = inv_dt
            lblDelChrg.Text = del_chrg
            lblDelTax.Text = del_tax
            lblDelTotal.Text = del_total

            lblSubTotal.Text = sub_total
            lblDiscount.Text = disc
            lblMarkup.Text = markup
            lblLevy.Text = levy
            lblHST.Text = hst
            lblTotal.Text = total
            lblTotalPlusDel.text = total_plus_del

            item_lst = roc_order.OrderedItems

            grdFreightFwdDetail.DataSource = item_lst
            grdFreightFwdDetail.DataBind()

        End If
    End Sub

    Private Sub grdFreightFwdDetail_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdFreightFwdDetail.DataBound
        If grdFreightFwdDetail.Rows.Count > 0 Then
            For Each tcell As TableCell In grdFreightFwdDetail.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("~\FreightForwarding.aspx")
    End Sub

    ''' <summary>
    ''' Handles the Click event of the btnForward control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub btnForward_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnForward.Click
        Dim valid_cust As Boolean = False
        Dim return_code As Integer = 0
        Dim return_message As String = ""
        'DALRoc.setEnv(ConfigurationManager.AppSettings("env"))
        Dim conn As OracleConnection = Nothing
        conn = New OracleConnection(DALRoc.getConnectionstring(ConfigurationManager.AppSettings("env")))
        Dim cmd As OracleCommand = New OracleCommand
        Dim trans As OracleTransaction = Nothing
        Dim strError As StringBuilder = New StringBuilder
        Dim old_order As String = ""
        Dim new_cust As String = ""
        Dim comments As String = ""

        If txtNewCustomer.Text.Trim <> "" Then
            valid_cust = DALRoc.isValidCustomer(txtNewCustomer.Text.Trim)
        End If

        lblMessage.Text = ""

        If txtNewCustomer.Text.Trim = "" Or txtComments.Text.Trim = "" Then
            return_code = 1
            strError.Append("--The customer number and the comments field are mandatory fields.<br/>")
        ElseIf Not valid_cust Then
            return_code = 1
            strError.Append("--The customer number entered is not in a valid state(not active/no serve/not setup).<br/>")
        End If
        If Not Regex.IsMatch(txtNewCustomer.Text.Trim, "^[0-9]+$") Then
            return_code = 1
            strError.Append("--The customer number entered should be numeric only.<br/>")
        End If
        If txtNewCustomer.Text.Trim = lblCuNo.Text.Trim Then
            return_code = 1
            strError.Append("--The customer number entered is the same as the old customer number.<br/>")
        End If

        If return_code = 1 Then
            lblMessage.Text = strError.ToString
        ElseIf return_code = 0 Then
            Dim roc_trans_orders As ROC_TRANSFERRED_ORDERS = Nothing
            Try
                old_order = lblOrderNo.Text.Trim
                new_cust = txtNewCustomer.Text.Trim
                comments = txtComments.Text.Trim.Replace("'", "''")

                conn.Open()
                trans = conn.BeginTransaction
                cmd.Connection = conn
                cmd.Transaction = trans
                roc_trans_orders = DALRoc.ProcessFreightForward(old_order, new_cust, Session("CurrentUserId"), comments, cmd)

                cmd.Dispose()
                trans.Commit()
                conn.Close()

            Catch ex As Exception
                trans.Rollback()
                cmd.Dispose()
                conn.Close()
                Throw ex
            End Try

            Dim old_je As String = ""
            Dim new_je As String = ""
            Dim i As Integer = 0

            For i = 0 To roc_trans_orders.Orig_JE_Payments.Rows.Count - 1
                If old_je = "" Then
                    old_je = roc_trans_orders.Orig_JE_Payments.Rows(i).Item("trans_no")
                Else
                    old_je = old_je & ", " & roc_trans_orders.Orig_JE_Payments.Rows(i).Item("trans_no")
                End If
            Next

            For i = 0 To roc_trans_orders.Orig_JE_Payments.Rows.Count - 1
                If new_je = "" Then
                    new_je = roc_trans_orders.New_Order_JE_Payments.Rows(i).Item("trans_no")
                Else
                    new_je = new_je & ", " & roc_trans_orders.New_Order_JE_Payments.Rows(i).Item("trans_no")
                End If
            Next

            Dim strBuilder As StringBuilder = New StringBuilder
            Dim iCount As Integer = 1
            strBuilder.Append("Freight forwarding was successful. Please see the results below:")
            strBuilder.Append("<br/><br/>")
            strBuilder.Append(iCount.ToString & ". Order#: " & roc_trans_orders.orig_order_no.Trim & " has been freight fowarded.")
            iCount = iCount + 1

            strBuilder.Append("<br/>")
            strBuilder.Append(iCount.ToString & ". Return#: " & roc_trans_orders.cr_order_no.Trim & " was created to offset the sales.")
            iCount = iCount + 1

            If roc_trans_orders.delivery_chg_trans.Trim <> "" Then
                strBuilder.Append("<br/>")
                strBuilder.Append(iCount.ToString & ". Journal Entry: " & roc_trans_orders.delivery_chg_trans & " done to for the Delivery charge return.")
                iCount = iCount + 1
            End If

            strBuilder.Append("<br/>")
            strBuilder.Append(iCount.ToString & ". Journal Entry: " & old_je & " done to offset the payment.")
            iCount = iCount + 1

            strBuilder.Append("<br/>")
            strBuilder.Append(iCount.ToString & ". New Order#: " & roc_trans_orders.order_no.Trim & " was created and billed to the correct customer(" & roc_trans_orders.cu_no & ").")
            iCount = iCount + 1

            strBuilder.Append("<br/>")
            strBuilder.Append(iCount.ToString & ". Journal Entry: " & new_je & " done for new order payment.")
            iCount = iCount + 1

            strBuilder.Append("<br/>")
            If roc_trans_orders.orig_credit_pay_total > 0 Then
                strBuilder.Append("***Note: There were credit note tenders in the original order for the amount of $" & roc_trans_orders.orig_credit_pay_total & ". Please arrange and confirm if it needs to be updated.")
                strBuilder.Append("<br/>")
            End If

            Session("FreightFwdResult") = strBuilder.ToString

            Response.Redirect("~\FreightForwarding.aspx")
            End If
    End Sub

    Private Sub grdFreightFwdDetail_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdFreightFwdDetail.Sorting

    End Sub
End Class