Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Imports System.IO.Directory
Imports System.Data
Imports System.Security.Principal
Imports System.Threading
Imports System.Text.RegularExpressions
Partial Public Class UserAccess
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dt As DataTable = Nothing
        Dim qry As String = ""
        Dim access_type As Integer = 0
        Dim menu_group As String = ""

        access_type = Session("UserListType")
        menu_group = Session("UserListMenuGroup")
        '1 is ROCON Access and 2 is ROC Access
        If access_type = 1 Then
            qry = "SELECT distinct a.group_id As Access_Group, a.caption As Menu_Header, b.caption As Menu_Detail from " & _
                  "ON_MENU_ACCESS a, ON_MENU_ACCESS b WHERE a.group_id=b.group_id " & _
                  "and a.menu_id=b.parent_id and a.enable='Y' and b.enable='Y' and b.caption is not null " & _
                  "and trim(a.group_id)='" & menu_group.Trim & "' "
        ElseIf access_type = 2 Then
            qry = "SELECT distinct a.group_id As Access_Group, a.caption As Menu_Header, b.caption As Menu_Detail from " & _
                  "MENU_ACCESS a, MENU_ACCESS b WHERE a.group_id=b.group_id " & _
                  "and a.menu_id=b.parent_id and a.enable='Y' and b.enable='Y' and b.caption is not null " & _
                  "and trim(a.group_id)='" & menu_group.Trim & "' "
        End If

        dt = SPURDataControl.getData(qry)
        grdUserAccess.DataSource = dt
        grdUserAccess.DataBind()
    End Sub

    Private Sub grdUserAccess_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdUserAccess.DataBound
        If grdUserAccess.Rows.Count > 0 Then
            For Each tcell As TableCell In grdUserAccess.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
        End If
    End Sub
End Class