<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="AgencyCustomerReport.aspx.vb" Inherits="_941Support.AgencyCustomerReport" 
    title="Agency Customer Report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
    &nbsp;
    <table>
        <tr>
            <td align="center" colspan="6">
                <asp:Label ID="lblErrorMsg" runat="server" BackColor="Yellow" Font-Bold="True"
        ForeColor="Maroon"></asp:Label></td>
        </tr>
        <tr>
        <td colspan="6"></td>
        </tr>
       <%-- <tr>            
            <td colspan="6" align="left">
            <asp:Label ID="lblHeading" runat="server" CssClass="HeadCss" Text="Create IST"></asp:Label> 
            </td>            
        </tr>--%>
        <tr>
            <td colspan="2" style="width: 119px" ></td>
            <td class="rprompt" > 
                Order#</td>
            <td align="left">
                <asp:TextBox ID="txtOrderNo" runat="server" CssClass="textbox" MaxLength="6" Width="73px"></asp:TextBox></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2" style="width: 119px">
            </td>
            <td class="rprompt">
                Order Type:</td>
            <td align="left">
                <asp:DropDownList ID="drpOrderTypes" runat="server">
                    <asp:ListItem Selected="True" Value="AGY">Agency</asp:ListItem>
                </asp:DropDownList></td>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width: 119px">
            </td>
            <td class="rprompt">
                Customer Number:</td>
            <td align="left">
                <asp:TextBox ID="txtCustomerNo" runat="server" Columns="16" CssClass="textbox" MaxLength="6"></asp:TextBox></td>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width: 119px">
            </td>
            <td class="rprompt">
                Customer Name:</td>
            <td align="left">
                <asp:TextBox ID="txtCustomerName" runat="server" Columns="36" CssClass="textbox"
                    MaxLength="36"></asp:TextBox></td>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width: 119px" ></td>
            <td class="rprompt" >Order Date :</td>
            <td align="left">
                <asp:TextBox ID="txtOrderDate" runat="server" Columns="10" CssClass="textbox"
                    MaxLength="10"></asp:TextBox>
                 <a id="a1" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtOrderDate, 'a1', 'yyyy-MM-dd');return false;"
								href="#" name="a1"><img id="Img1" height="21" src="Images/calendar.gif" width="34" align="top" border="0"></a>&nbsp;
         
			</td>
			<td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2" style="width: 119px" ></td>
            <td class="rprompt" >
                Ship Date :</td>
            <td align="left">
            <asp:TextBox ID="txtDeliveryDate" runat="server" Columns="10" CssClass="textbox"
                MaxLength="10"></asp:TextBox>
            <a id="a2" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtDeliveryDate, 'a2', 'yyyy-MM-dd');return false;"
								href="#" name="a2"><img id="Img2" height="21" src="Images/calendar.gif" width="34" align="top" border="0"></a>
            </td>
            <td colspan="2"></td>
        </tr>
        <tr>
           <td colspan="4" align="right">
                <asp:Button ID="btnSearch" runat="server" CssClass="BtnCss" Text="Search" /></td>
            <td colspan="2" align="left">
                &nbsp;<asp:Button ID="btnClear" runat="server" CssClass="BtnCss" Text="Reset" />&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                </td>
        </tr>
        <tr>
            <td align="center" colspan="6" >
                <asp:Label ID="lblMsg" runat="server" CssClass="LblCss" ></asp:Label></td>
        </tr>    
         <tr>
            <td align="center" colspan="6" >
                <asp:Panel ID="pnlSummary" runat="server" Visible="false">
                    <table>
                        
                        
                        <tr>
                            <td>
                                <div style="z-index: 10; overflow: auto; height: 200px">
                                    <asp:GridView ID="grdSummary" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                        BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px"
                                        CellPadding="3" CellSpacing="1" Font-Names="Arial" GridLines="None" Width="850px">
                                        <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                        <RowStyle BackColor="#DEDFDE" ForeColor="Black" Font-Size="Small" />
                                        <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                                        <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="Maroon" />   
                                        <Columns>
                                           <asp:TemplateField HeaderText="Order#" SortExpression="ORDERNUM">
                                                <ItemTemplate>
                                                   <asp:LinkButton ID="lblOrderNum" runat="server" Text='<%# Bind("ORDERNUM") %>' CommandArgument='<%# Eval("ORDERNUM") + "," + Eval("STATUS") + "," + Eval("ITEM_VALID") + "," + Eval("TENDER_VALID") + "," + Eval("ORDER_TYPE") + "," + Eval("CUSTOMER") %>' CommandName="GetOrderDetails"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Customer#" SortExpression="CUSTOMER">
                                                <ItemTemplate>
                                                   <asp:Label ID="lblCustomer" runat="server" Text='<%# Bind("CUSTOMER") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Customer Name" SortExpression="CUSTOMER_NAME">
                                                <ItemTemplate>
                                                   <asp:Label ID="lblCustomerName" runat="server" Text='<%# Bind("CUSTOMER_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Order Type" SortExpression="ORDER_TYPE" >
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOrderType" runat="server" Text='<%# Bind("ORDER_TYPE") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Order Date" SortExpression="ORDER_DATE" >
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOrderDate" runat="server" Text='<%# Bind("ORDER_DATE") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Order Lines" SortExpression="ORDERLINES" >
                                                 <ItemTemplate>
                                                    <asp:Label ID="lblOrderLines" runat="server" Text='<%# Bind("ORDERLINES") %>' ></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Order Total" SortExpression="ORDERTOTAL" >
                                                 <ItemTemplate>
                                                    <asp:Label ID="lblOrderTotal" runat="server" Text='<%# Bind("ORDERTOTAL") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Ship Date" SortExpression="EXPECTED_DELIVER" >
                                                 <ItemTemplate>
                                                    <asp:Label ID="lblExpectedDelivery" runat="server" Text='<%# Bind("EXPECTED_DELIVERY") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valid Items" SortExpression="ITEM_VALID" >
                                                 <ItemTemplate>
                                                    <asp:Label ID="lblValidItems" runat="server" Text='<%# Bind("ITEM_VALID") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Valid Tenders" SortExpression="TENDER_VALID" >
                                                 <ItemTemplate>
                                                    <asp:Label ID="lblValidTenders" runat="server" Text='<%# Bind("TENDER_VALID") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Last Update By" SortExpression="EM_NO" >
                                                 <ItemTemplate>
                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Bind("EM_NO") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Status" SortExpression="STATUS" >
                                                 <ItemTemplate>
                                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("STATUS") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                          <%-- <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkCreateASN" runat="server" Text="i don't know" CommandArgument='<%# Eval("ORDERNUM") + "," + Eval("StoreNo") %>' CommandName="GetCreateASN"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>
                                   
                                </div></td>                                                   
                         </tr>
                         <tr>
                            <td align="right" >  &nbsp;</td>                          
                         </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        
        <tr>
                 <td align="center" colspan="6" >
                            <asp:Panel ID="pnlDetails" runat="server" Visible="false">
                                <table cellpadding="0" cellspacing="0" >
                                    <tr>
                                        <td align="center" style="height: 19px">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="height: 19px" >
                                            <asp:Label ID="lblTenderMsg" runat="server" CssClass="LblCss" ></asp:Label>&nbsp;</td>
                                        
                                    </tr>
                                    <tr>
                                        <td align="center">
                                         <asp:GridView ID="grdTenders" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                         Font-Names="Arial" Width="900px" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CellSpacing="1" GridLines="None" >
                                                    <RowStyle Font-Size="Small" BackColor="#DEDFDE" ForeColor="Black" />
                                                    <HeaderStyle BackColor="#CBD49E" ForeColor="Maroon" Font-Size="Medium" Font-Bold="True" />
                                                    <Columns>
                                                        <asp:BoundField DataField="ORDERNUM" HeaderText="Order#" SortExpression="ORDERNUM" />
                                                        <asp:BoundField DataField="REF_SEQ" HeaderText="Payment#" SortExpression="REF_SEQ"/>
                                                        <asp:BoundField DataField="TIMESTAMP" HeaderText="Timestamp" SortExpression="TIMESTAMP" />
                                                        <asp:BoundField DataField="EMP_NO" HeaderText="EMP" SortExpression="EMP_NO"/>
                                                        <asp:BoundField DataField="TENDER_TYPE" HeaderText="Tender" SortExpression="TENDER_TYPE"/>
                                                        <asp:BoundField DataField="PAY_AMT" HeaderText="Pay Amt" SortExpression="PAY_AMT" />
                                                        <asp:BoundField DataField="CREDIT_AUTH" HeaderText="Auth#" SortExpression="CREDIT_AUTH" />
                                                        <asp:BoundField DataField="PAY_COMMENT" HeaderText="Pay Comment" SortExpression="PAY_COMMENT" />
                                                        <asp:BoundField DataField="PAY_DT" HeaderText="Pay Date" SortExpression="PAY_DT"/>
                                                        <asp:TemplateField HeaderText="REMARKS" SortExpression="COMMENTS">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblComments" runat="server" Text='<%# Bind("COMMENTS") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                                                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>       
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblDetailMsg" runat="server" CssClass="LblCss"></asp:Label></td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            <div style="z-index: 10; overflow: auto;  height: 250px">
                                                <asp:GridView ID="grdDetails" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                         Font-Names="Arial" Width="900px" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CellSpacing="1" GridLines="None" >
                                                    <RowStyle Font-Size="Small" BackColor="#DEDFDE" ForeColor="Black" />
                                                    <HeaderStyle BackColor="#CBD49E" ForeColor="Maroon" Font-Size="Medium" Font-Bold="True" />
                                                    <Columns>
                                                        <asp:BoundField DataField="CO_ODNO" HeaderText="Order#" SortExpression="CO_ODNO" />
                                                        <asp:BoundField DataField="ITEM" HeaderText="Product Number" SortExpression="ITEM" />
                                                        <asp:BoundField DataField="QTY" HeaderText="Order Qty" SortExpression="QTY"/>
                                                        <asp:BoundField DataField="PROD_DESCRIPTION" HeaderText="Product Description" SortExpression="PROD_DESCRIPTION" />
                                                        <asp:BoundField DataField="BOTTLE_DEP" HeaderText="DEP" SortExpression="BOTTLE_DEP"/>
                                                        <asp:BoundField DataField="VOLUME" HeaderText="Size (ml)" SortExpression="VOLUME"/>
                                                        <asp:BoundField DataField="ITEM_CATEGORY" HeaderText="Prod. Type" SortExpression="ITEM_CATEGORY" />
                                                        <asp:BoundField DataField="BASIC_PRICE" HeaderText="Unit Price" SortExpression="BASIC_PRICE" />
                                                        <asp:BoundField DataField="EXTENDED_PRICE" HeaderText="Extended Price" SortExpression="EXTENDED_PRICE" />
                                                        <asp:BoundField DataField="RETAIL_PRICE" HeaderText="Retail Price" SortExpression="RETAIL_PRICE"/>
                                                        <asp:TemplateField HeaderText="Comments" SortExpression="COMMENTS">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblComments" runat="server" Text='<%# Bind("COMMENTS") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                                                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>                                               
                                            </div>
                                         </td>
                                    </tr>
                                    <tr><td>
                                        &nbsp;</td></tr>
                                    <tr>                                       
                                       <td align="right" style="white-space:nowrap">
                                        <asp:Button ID="btnExportDetails" runat="server" CssClass="BtnCss" Text="Export Details"  Width="144px"  />
                                        <asp:Button ID="btnPriceList" runat="server" CssClass="BtnCss" Text="Export Price List"  Width="144px" ToolTip="Export Price List for Agency"  />
                                    
                                    </td>
                                    </tr>
                                </table>
                          </asp:Panel>
                </td>
        </tr>
  </table>
    
</asp:Content>
