<%@ Page Language="vb" EnableViewState="true" AutoEventWireup="false" MasterPageFile="~/Main.Master" Title="Traffic Planning" CodeBehind="TrafficPlanning.aspx.vb" Inherits="_941Support.TrafficPlanning" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
    
<table cellspacing="0" cellpadding="0" align="center" border="1" style="width: 80%; background-color:#CBD49E">
<tr>
    <td>
    &nbsp;
    </td>
    <td align="center">
    <asp:Label ID="lbl2" runat="server" Text="From"  CssClass="LblCss"></asp:Label>
    </td>
    <td align="center">
    <asp:Label ID="lbl3" runat="server" Text="To"  CssClass="LblCss"></asp:Label>
    </td>
    <td align="center">
    <asp:Label ID="lbl4" runat="server" Text="Or Comma Separated"  CssClass="LblCss"></asp:Label>
    </td>
</tr>
<tr>
    <td align="right">
    <asp:Label ID="lbl5" runat="server" Text="Route Code"  CssClass="LblCss"></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txtFromRoute" runat="server"></asp:TextBox>
      <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" 
        TargetControlID="txtFromRoute"
        ServicePath="WebService1.asmx"
        ServiceMethod="getRouteCodes"
        MinimumPrefixLength="1" 
        CompletionSetCount="10" 
        runat="server">
        </cc1:AutoCompleteExtender>
    </td>
    <td>
        <asp:TextBox ID="txtToRoute" runat="server"></asp:TextBox>
      <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" 
        TargetControlID="txtToRoute"
        ServicePath="WebService1.asmx"
        ServiceMethod="getRouteCodes"
        MinimumPrefixLength="1" 
        CompletionSetCount="10" 
        runat="server">
        </cc1:AutoCompleteExtender>
    </td>
    <td>
    <asp:TextBox ID="txtRoute" runat="server"></asp:TextBox>
    </td>
    
</tr>

<tr>
    <td align="right">
    <asp:Label ID="lbl6" runat="server" Text="Stop No"  CssClass="LblCss"></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="txtFromStop" runat="server"></asp:TextBox>
      <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" 
        TargetControlID="txtFromStop"
        ServicePath="WebService1.asmx"
        ServiceMethod="getStopNos"
        MinimumPrefixLength="1" 
        CompletionSetCount="10" 
        runat="server">
        </cc1:AutoCompleteExtender>
    </td>
    <td>
        <asp:TextBox ID="txtToStop" runat="server"></asp:TextBox>
      <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" 
        TargetControlID="txtToStop"
        ServicePath="WebService1.asmx"
        ServiceMethod="getStopNos"
        MinimumPrefixLength="1" 
        CompletionSetCount="10" 
        runat="server">
        </cc1:AutoCompleteExtender>
    </td>
    <td>
    <asp:TextBox ID="txtStop" runat="server"></asp:TextBox>
    </td>    
</tr>

<tr>
    <td align="right">
    <asp:Label ID="lbl7" runat="server" Text="Customer No"  CssClass="LblCss"></asp:Label>
    </td>
    <td>
       <asp:TextBox ID="txtFromCustomer" runat="server"></asp:TextBox>
      <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" 
        TargetControlID="txtFromCustomer"
        ServicePath="WebService1.asmx"
        ServiceMethod="getCustomerNos"
        MinimumPrefixLength="1" 
        CompletionSetCount="10" 
        runat="server">
        </cc1:AutoCompleteExtender>
    </td>
    <td>
        <asp:TextBox ID="txtToCustomer" runat="server"></asp:TextBox>
      <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" 
        TargetControlID="txtToCustomer"
        ServicePath="WebService1.asmx"
        ServiceMethod="getCustomerNos"
        MinimumPrefixLength="1" 
        CompletionSetCount="10" 
        runat="server">
        </cc1:AutoCompleteExtender>
    </td>
    <td>
    <asp:TextBox ID="txtCustomer" runat="server"></asp:TextBox>
    </td>    
</tr>

<tr>
    <td align="right">
    <asp:Label ID="lbl8" runat="server" Text="Order No"  CssClass="LblCss"></asp:Label>
    </td>
    <td>
       <asp:TextBox ID="txtFromOrder" runat="server"></asp:TextBox>
      <cc1:AutoCompleteExtender ID="AutoCompleteExtender7" 
        TargetControlID="txtFromOrder"
        ServicePath="WebService1.asmx"
        ServiceMethod="getOrderNos"
        MinimumPrefixLength="1" 
        CompletionSetCount="10" 
        runat="server">
        </cc1:AutoCompleteExtender>
    </td>
    <td>
        <asp:TextBox ID="txtToOrder" runat="server"></asp:TextBox>
      <cc1:AutoCompleteExtender ID="AutoCompleteExtender8" 
        TargetControlID="txtToOrder"
        ServicePath="WebService1.asmx"
        ServiceMethod="getOrderNos"
        MinimumPrefixLength="1" 
        CompletionSetCount="10" 
        runat="server">
        </cc1:AutoCompleteExtender>
    </td>
    <td>
    <asp:TextBox ID="txtOrder" runat="server"></asp:TextBox>
    </td>    
</tr>

<tr>
    <td align="right">
    <asp:Label ID="lbl9" runat="server" Text="Carrier"  CssClass="LblCss"></asp:Label>
    </td>
    <td>
       <asp:TextBox ID="txtFromCarrier" runat="server"></asp:TextBox>
      <cc1:AutoCompleteExtender ID="AutoCompleteExtender9" 
        TargetControlID="txtFromCarrier"
        ServicePath="WebService1.asmx"
        ServiceMethod="getCarrierNos"
        MinimumPrefixLength="1" 
        CompletionSetCount="10" 
        runat="server">
        </cc1:AutoCompleteExtender>
    </td>
    <td>
        <asp:TextBox ID="txtToCarrier" runat="server"></asp:TextBox>
      <cc1:AutoCompleteExtender ID="AutoCompleteExtender10" 
        TargetControlID="txtToCarrier"
        ServicePath="WebService1.asmx"
        ServiceMethod="getCarrierNos"
        MinimumPrefixLength="1" 
        CompletionSetCount="10" 
        runat="server">
        </cc1:AutoCompleteExtender>
    </td>
    <td>
    <asp:TextBox ID="txtCarrier" runat="server"></asp:TextBox>
    </td>    
</tr>

<tr>
    <td align="right">
    <asp:Label ID="lbl10" runat="server" Text="Ship Date(YYYY-MM-DD)"  CssClass="LblCss"></asp:Label>
    </td>
    <td>
       <asp:TextBox ID="txtFromShip" runat="server" Columns="10" Width="110px" ></asp:TextBox>
       <a id="a1" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtFromShip, 'a1', 'yyyy-MM-dd');return false;"
		href="#" name="a1"><img id="Img1" height="21" alt="Calendar"  src="Images/calendar.gif" width="34" align="top" border="0"/></a>
     </td>
    <td>
        <asp:TextBox ID="txtToShip" runat="server" Columns="10" Width="110px" EnableViewState="true"></asp:TextBox>
        <a id="a2" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtToShip, 'a2', 'yyyy-MM-dd');return false;"
		href="#" name="a2"><img id="Img2" height="21" alt="Calendar"  src="Images/calendar.gif" width="34" align="top" border="0"/></a>
    </td>
    <td>
    <asp:TextBox ID="txtShip" runat="server"></asp:TextBox>
    </td>    
</tr>

<tr>
    <td align="right">
    <asp:Label ID="lbl11" runat="server" Text="Order Type"  CssClass="LblCss"></asp:Label>
    </td>
    <td>
       <asp:TextBox ID="txtFromOrderType" runat="server"></asp:TextBox>
      <cc1:AutoCompleteExtender ID="AutoCompleteExtender11" 
        TargetControlID="txtFromOrderType"
        ServicePath="WebService1.asmx"
        ServiceMethod="getOrderTypes"
        MinimumPrefixLength="1" 
        CompletionSetCount="10" 
        runat="server">
        </cc1:AutoCompleteExtender>
    </td>
    <td>
        <asp:TextBox ID="txtToOrderType" runat="server"></asp:TextBox>
      <cc1:AutoCompleteExtender ID="AutoCompleteExtender12" 
        TargetControlID="txtToOrderType"
        ServicePath="WebService1.asmx"
        ServiceMethod="getOrderTypes"
        MinimumPrefixLength="1" 
        CompletionSetCount="10" 
        runat="server">
        </cc1:AutoCompleteExtender>
    </td>
    <td>
    <asp:TextBox ID="txtOrderType" runat="server"></asp:TextBox>
    </td>    
</tr>

<tr>
    <td colspan="4" align="center">
    <asp:Label ID="lblMessage" Text="" runat="server"></asp:Label>
    </td>
</tr>

<tr valign="middle" style="height:30px">
    <td colspan="4" align="right">
    <asp:Button ID="btnNext" runat="server" CssClass="BtnCss" Text="Next" />
    <asp:Button ID="btnReset" runat="server" CssClass="BtnCss" Text="Reset" />
    &nbsp;
    </td>
</tr>
</table>

</asp:Content>
