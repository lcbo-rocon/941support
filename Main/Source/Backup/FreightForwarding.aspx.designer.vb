'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.3603
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On



'''<summary>
'''FreightForwarding class.
'''</summary>
'''<remarks>
'''Auto-generated class.
'''</remarks>
Partial Public Class FreightForwarding

    '''<summary>
    '''ValidationSummary1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ValidationSummary1 As Global.System.Web.UI.WebControls.ValidationSummary

    '''<summary>
    '''lblNote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNote As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCustomer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCustomer As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtCustomer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCustomer As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblStar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblStar As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''CustomerReqValidator control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CustomerReqValidator As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''CustomerCmpValidator control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CustomerCmpValidator As Global.System.Web.UI.WebControls.CompareValidator

    '''<summary>
    '''lblOrder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOrder As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtOrder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOrder As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''OrderValidator control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents OrderValidator As Global.System.Web.UI.WebControls.CompareValidator

    '''<summary>
    '''lblDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtInvoiceDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtInvoiceDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''DateRegValidator control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DateRegValidator As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''DateCmpValidator control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DateCmpValidator As Global.System.Web.UI.WebControls.CompareValidator

    '''<summary>
    '''btnSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSearch As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnReset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnReset As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''grdForwarding control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdForwarding As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''lblMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMsg As Global.System.Web.UI.WebControls.Label
End Class
