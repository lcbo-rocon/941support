Imports System.Web.Services.Protocols
Imports System.Data
Imports System.DirectoryServices
Imports System.Security.Principal
Imports System.Threading
Imports System.Data.OracleClient

Partial Public Class Main
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Dim id As IIdentity = Thread.CurrentPrincipal.Identity 'HttpContext.Current.User.Identity
        ' Dim userid As String = id.Name
        Dim userid As String = UCase(id.Name).Replace("LCBO\", "")
        Dim first_name As String = ""
        Dim last_name As String = ""
        Dim currentPage As String = Request.AppRelativeCurrentExecutionFilePath.Trim.Replace("~", "")
        currentPage = currentPage.Replace("/", "")
        currentPage = currentPage.Replace(".aspx", "")

        Dim menuitm As MenuItem = Nothing

        userid = userid.Substring(userid.IndexOf("\") + 1)

        first_name = User.GetUserInfo(userid, "givenName")
        last_name = User.GetUserInfo(userid, "sn")

        lblWelcome.Text = "Welcome " & first_name & " " & last_name
        Session("CurrentUserId") = userid
        lblEnv.Text = System.Configuration.ConfigurationManager.AppSettings("env")
        lblVersion.Text = "V." & System.Configuration.ConfigurationManager.AppSettings("VersionNumber")

        'Disable menu items for Operation users
        menuitm = MainMenu.FindItem("UserList")
        menuitm.Enabled = False

        menuitm = MainMenu.FindItem("ResetHHUser")
        menuitm.Enabled = False

        menuitm = MainMenu.FindItem("RestartRRPS")
        menuitm.Enabled = False

        menuitm = MainMenu.FindItem("UnlockCarton")
        menuitm.Enabled = False

        menuitm = MainMenu.FindItem("FixLocation")
        menuitm.Enabled = False

        menuitm = MainMenu.FindItem("FixCubeFlag")
        menuitm.Enabled = False

        menuitm = MainMenu.FindItem("ArchiveInvoice")
        menuitm.Enabled = False

        menuitm = MainMenu.FindItem("RunEord")
        menuitm.Enabled = False

        menuitm = MainMenu.FindItem("SPURReport")
        menuitm.Enabled = False

        menuitm = MainMenu.FindItem("ViewConveyorLogs")
        menuitm.Enabled = False

        menuitm = MainMenu.FindItem("ChangePassword")
        menuitm.Enabled = False

        'Disable Tender Override Option 
        menuitm = MainMenu.FindItem("TenderRestrictionOverride")
        menuitm.Enabled = False

        'Disable Create IST Option
        menuitm = MainMenu.FindItem("CreateIST")
        menuitm.Enabled = False

        'Disable Reboot Conveyor Option
        menuitm = MainMenu.FindItem("RebootConveyor")
        menuitm.Enabled = False


        'Disable Traffic Planning
        menuitm = MainMenu.FindItem("TrafficPlanning")
        menuitm.Enabled = False

        'Disable Product Search
        menuitm = MainMenu.FindItem("ProductSearch")
        menuitm.Enabled = False

        'Disable Freight Forwarding
        menuitm = MainMenu.FindItem("FreightForwarding")
        menuitm.Enabled = False

        'Disable Location Transfer
        menuitm = MainMenu.FindItem("LocationTransfer")
        menuitm.Enabled = False

        'Show menu items for user with access

        Dim OperationUsers As String() = ConfigurationManager.AppSettings("OperationsUser").Split(",")
        For Each touser As String In OperationUsers
            'show menu item
            If UCase("lcbo\" & userid).Equals(UCase(touser)) Then
                'show menu item
                menuitm = MainMenu.FindItem("UserList")
                menuitm.Enabled = True

                menuitm = MainMenu.FindItem("ResetHHUser")
                menuitm.Enabled = True

                menuitm = MainMenu.FindItem("RestartRRPS")
                menuitm.Enabled = True

                menuitm = MainMenu.FindItem("UnlockCarton")
                menuitm.Enabled = True

                menuitm = MainMenu.FindItem("FixLocation")
                menuitm.Enabled = True

                menuitm = MainMenu.FindItem("FixCubeFlag")
                menuitm.Enabled = True

                menuitm = MainMenu.FindItem("ArchiveInvoice")
                menuitm.Enabled = True

                menuitm = MainMenu.FindItem("RunEord")
                menuitm.Enabled = True
                
                menuitm = MainMenu.FindItem("SPURReport")
                menuitm.Enabled = True

                menuitm = MainMenu.FindItem("ViewConveyorLogs")
                menuitm.Enabled = True

                menuitm = MainMenu.FindItem("ChangePassword")
                menuitm.Enabled = True

                menuitm = MainMenu.FindItem("TrafficPlanning")
                menuitm.Enabled = True

                menuitm = MainMenu.FindItem("ProductSearch")
                menuitm.Enabled = True

                menuitm = MainMenu.FindItem("FreightForwarding")
                menuitm.Enabled = True

                menuitm = MainMenu.FindItem("LocationTransfer")
                menuitm.Enabled = True
            End If

        Next
        Dim TenderOverrideUsers As String() = ConfigurationManager.AppSettings("TenderOverrideUsers").Split(",")
        For Each TOUser As String In TenderOverrideUsers
            If UCase("lcbo\" & userid).Equals(UCase(TOUser)) Then
                'show menu item
                menuitm = MainMenu.FindItem("TenderRestrictionOverride")
                menuitm.Enabled = True
            End If
        Next

        Dim CreateISTUsers As String() = ConfigurationManager.AppSettings("CreateISTUsers").Split(",")
        For Each TOUser As String In CreateISTUsers
            If UCase("lcbo\" & userid).Equals(UCase(TOUser)) Then
                'show menu item
                menuitm = MainMenu.FindItem("CreateIST")
                menuitm.Enabled = True
            End If
        Next

        Dim ConveyorRebootUsers As String() = ConfigurationManager.AppSettings("ConveyorRebootUsers").Split(",")
        For Each TOUser As String In ConveyorRebootUsers
            If UCase("lcbo\" & userid).Equals(UCase(TOUser)) Then
                'show menu item
                menuitm = MainMenu.FindItem("RebootConveyor")
                menuitm.Enabled = True
            End If
        Next

        Try

            If currentPage.ToLower = ("agencycustomerreport") Then
                MainMenu.FindItem("AgencyCustomerReport").Selected = True
            End If

            If currentPage.ToLower = ("changepassword") Then
                MainMenu.FindItem("ChangePassword").Selected = True
            End If

            If currentPage.ToLower = ("createist") Then
                MainMenu.FindItem("CreateIST").Selected = True
            End If

            If currentPage.ToLower = ("fixcubeflag") Then
                MainMenu.FindItem("FixCubeFlag").Selected = True
            End If

            If currentPage.ToLower = ("fixlocation") Then
                MainMenu.FindItem("FixLocation").Selected = True
            End If

            If currentPage.ToLower = ("freightforwarding") Then
                MainMenu.FindItem("FreightForwarding").Selected = True
            End If

            If currentPage.ToLower = "freightforwarddetails" Then
                MainMenu.FindItem("FreightForwarding").Selected = True
            End If

            If currentPage.ToLower = ("restartrrps") Then
                MainMenu.FindItem("RestartRRPS").Selected = True
            End If

            If currentPage.ToLower = ("locationtransfer") Then
                MainMenu.FindItem("LocationTransfer").Selected = True
            End If

            If currentPage.ToLower = ("productsearch") Then
                MainMenu.FindItem("ProductSearch").Selected = True
            End If

            If currentPage.ToLower = ("rebootconveyor") Then
                MainMenu.FindItem("RebootConveyor").Selected = True
            End If

            If currentPage.ToLower = ("resethhuser") Then
                MainMenu.FindItem("ResetHHUser").Selected = True
            End If

            If currentPage.ToLower = ("Runeord") Then
                MainMenu.FindItem("RunEord").Selected = True
            End If

            If currentPage.ToLower = ("spurReport") Then
                MainMenu.FindItem("SPURReport").Selected = True
            End If

            If currentPage.ToLower = ("tenderrestrictionoverride") Then
                MainMenu.FindItem("TenderRestrictionOverride").Selected = True
            End If

            If currentPage.ToLower = ("trafficplanning") Then
                MainMenu.FindItem("TrafficPlanning").Selected = True
            End If

            If currentPage.ToLower = ("trafficplandetails") Then
                MainMenu.FindItem("TrafficPlanning").Selected = True
            End If

            If currentPage.ToLower = "unlockcarton" Then
                MainMenu.FindItem("UnlockCarton").Selected = True
            End If

            If currentPage.ToLower = "archiveinvoice" Then
                MainMenu.FindItem("ArchiveInvoice").Selected = True
            End If

            If currentPage.ToLower = "viewconveyorlogs" Then
                MainMenu.FindItem("ViewConveyorLogs").Selected = True
            End If

        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try
    End Sub

  
End Class