Imports com.lowagie.text.pdf
Imports com.lowagie.text
Imports com.lowagie.text.pdf.PdfTemplate
Imports com.lowagie.text.pdf.PdfPageEventHelper
Imports com.lowagie.text.Font
Imports System.IO
Imports System.Globalization

Partial Class SPURReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtPickPlanFrom.CssClass = "textbox"
        txtPickPlanTo.CssClass = "texbox"
        txtDeliveryDate.CssClass = "textbox"
        lblMsg.Text = ""

        Dim lblheading As Label = Master.FindControl("lblMainTitle")
        If Not lblheading Is Nothing Then
            lblheading.Text = "SPUR Report"
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'validate pick plan number is numeric
        Dim valid As Boolean = True
        Dim tmpInt1 As Integer = 0
        Dim tmpInt2 As Integer = 0
        Dim tmpDate As DateTime = Nothing
        Dim where As String = ""

        lblSummaryMsg.Text = ""
        lblDetailsMsg.Text = ""
        pnlSummary.Visible = False
        pnlDetails.Visible = False

        'check if delivery date is valid
        If txtDeliveryDate.Text.Trim <> "" Then
            If DateTime.TryParse(txtDeliveryDate.Text.Trim, tmpDate) = False Then
                valid = False
                lblMsg.Text = lblMsg.Text & "Invalid Delivery Date entered!<br/>"
                txtDeliveryDate.CssClass = "etextbox"
            Else
                If where <> "" Then
                    where = where & " AND "
                End If
                where = where & " TRUNC(T6.ORDER_REQD_DT) = TO_DATE('" & tmpDate.ToString("yyyy-MM-dd") & "','YYYY-MM-DD') "
            End If
        End If
        If txtPickPlanFrom.Text.Trim <> "" Then
            If Integer.TryParse(txtPickPlanFrom.Text, tmpInt1) = False Then
                valid = False
                lblMsg.Text = lblMsg.Text & "Invalid Pick plan number entered! Must be numeric!<br/>"
                txtPickPlanFrom.CssClass = "etextbox"
            Else
                If where <> "" Then
                    where = where & " AND "
                End If
                where = where & " T4.PICK_PLAN_NU >= " & txtPickPlanFrom.Text
            End If

        End If

        If txtPickPlanTo.Text.Trim <> "" Then
            If Integer.TryParse(txtPickPlanTo.Text, tmpInt2) = False Then
                valid = False
                lblMsg.Text = lblMsg.Text & "Invalid Pick plan number entered! Must be numeric!<br/>"
                txtPickPlanTo.CssClass = "etextbox"
            Else
                If where <> "" Then
                    where = where & " AND "
                End If
                where = where & " T4.PICK_PLAN_NU <= " & txtPickPlanTo.Text
            End If
        End If

        'Check if pick plan from is < than pick plan to 
        If valid = True Then
            If tmpInt1 > tmpInt2 Then
                valid = False
                lblMsg.Text = lblMsg.Text & "Error From Pick Plan Number is greater than To Pick Plan Number!<br/>"
                txtPickPlanFrom.CssClass = "etextbox"
                txtPickPlanTo.CssClass = "etextbox"
            End If
        End If

        Dim qry As String = ""
        ' Dim spurlist As String = SPURDataControl.getCartonList
        If valid = True Then
            If where <> "" Then
                'qry = qry & " WHERE " & where
                where = " WHERE " & where
            End If

            Dim dt As DataTable = SPURDataControl.getSPURInfo(where, Session.SessionID)
            dt.Columns.Add("Total")
            For Each drow As DataRow In dt.Rows
                drow("Total") = 0
                For Each col As DataColumn In dt.Columns
                    If col.ColumnName <> "PICK_PLAN_NU" Then
                        If col.ColumnName <> "Total" Then
                            If IsDBNull(drow(col.ColumnName)) = False Then
                                drow("Total") = drow("Total") + drow(col.ColumnName)
                            End If
                        End If
                    End If
                Next
            Next
            Session("SUMMARY") = dt
            grdSummary.DataSource = dt
            grdSummary.DataBind()
        End If

    End Sub

    Private Sub grdSummary_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdSummary.DataBound
        If grdSummary.Rows.Count > 0 Then
            For Each tcell As TableCell In grdSummary.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
            Dim lblTotal As Label = Nothing
            Dim lane As LinkButton = Nothing

            Dim reccount As Integer = 0
            For Each grow As GridViewRow In grdSummary.Rows
                lblTotal = grow.FindControl("lblTotal")
                If lblTotal.Text = "0" Then
                    grow.Visible = False
                Else
                    reccount = reccount + 1
                End If
                For i As Integer = 1 To 10
                    lane = grow.FindControl("Label" & i.ToString)
                    If Not lane Is Nothing Then
                        If lane.Text = "0" Then
                            lane.Visible = False
                        End If
                    End If
                Next
                lane = grow.FindControl("LabelNA")
                If Not lane Is Nothing Then
                    If lane.Text = "0" Then
                        lane.Visible = False
                    End If
                End If
            Next
            If reccount > 0 Then
                pnlSummary.Visible = True
                lblSummaryMsg.Text = "There are " & reccount & " records found!"
            Else
                pnlSummary.Visible = False
                lblMsg.Text = "Your search criteria returned no records. Please try again!"
            End If
        Else
            pnlSummary.Visible = False
            lblMsg.Text = "Your search criteria returned no records. Please try again!"
        End If
    End Sub

    Private Sub grdSummary_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdSummary.RowCommand
        If e.CommandName = "Details" Then
            Dim pick_plan As String = e.CommandArgument
            Dim lane_no As String = e.CommandSource.ID.ToString.Replace("Label", "")

            Session("Details") = SPURDataControl.getSPURDetails(pick_plan, lane_no, Session.SessionID)
            grdDetails.DataSource = Session("Details")
            grdDetails.DataBind()
            lblPickPlanNu.Text = "Pick Plan: " & pick_plan
            lblLaneNo.Text = "Lane No: " & lane_no
        End If

    End Sub

    Private Sub grdDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdDetails.RowDataBound
        If grdDetails.Rows.Count > 0 Then
            For Each tcell As TableCell In grdDetails.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
            pnlDetails.Visible = True
            lblDetailsMsg.Text = "There are " & grdDetails.Rows.Count & " records found!"
        Else
            pnlDetails.Visible = False
        End If
    End Sub

    Private Sub grdDetails_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdDetails.Sorting
        Dim dv As DataView = New DataView(Session("Details"))
        Dim order As String = ""
        If Not ViewState("DTLOrder") Is Nothing Then
            order = ViewState("DTLOrder")
        End If

        If order.StartsWith(e.SortExpression) Then
            If order.ToString.EndsWith("ASC") Then
                order = "DESC"
            Else
                order = "ASC"
            End If
        Else
            order = "ASC"
        End If

        ViewState("DTLOrder") = e.SortExpression + " " & order
        dv.Sort = ViewState("DTLOrder")
        grdDetails.DataSource = dv
        grdDetails.DataBind()
    End Sub

    Private Sub grdSummary_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdSummary.Sorting
        Dim dv As DataView = New DataView(Session("SUMMARY"))
        Dim order As String = ""
        If Not ViewState("SUMOrder") Is Nothing Then
            order = ViewState("SUMOrder")
        End If

        If order.StartsWith(e.SortExpression) Then
            If order.ToString.EndsWith("ASC") Then
                order = "DESC"
            Else
                order = "ASC"
            End If
        Else
            order = "ASC"
        End If

        ViewState("SUMOrder") = e.SortExpression + " " & order
        dv.Sort = ViewState("SUMOrder")
        grdSummary.DataSource = dv
        grdSummary.DataBind()

    End Sub

    Private Sub btnExportPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportPDF.Click
        Dim report As String = Server.MapPath("Temp\" & Session.SessionID & ".pdf")
        Dim dv As DataView = New DataView(Session("Details"))
        dv.Sort = ViewState("SUMOrder")

        Dim document As New Document(PageSize.LETTER, 50, 50, 40, 40)
        Dim write As PdfWriter = PdfWriter.getInstance(document, New FileStream(report, FileMode.Create))
        Dim reportTitle As String = "SPUR Details for " & lblPickPlanNu.Text & "  " & lblLaneNo.Text

        write.setPageEvent(New PageNumbersEventHelper(reportTitle, " as of " & Now.ToLongDateString & " " & Now.ToLongTimeString, Server.MapPath("./Images/logo.jpg")))

        Try
            document.open()
        Catch ex As Exception
            Throw New Exception("open crashed" & ex.Message.ToString)
        End Try

        Dim tbl As com.lowagie.text.Table = New Table(8)
        tbl.cellsFitPage = True
        tbl.setWidth(100)
        tbl.setPadding(3)

        tbl.setDefaultCellBackgroundColor(Drawing.Color.FromArgb(28, 94, 95))
        Dim headerfont As Font
        Dim rptfont As Font
        Try
            headerfont = New Font(Font.HELVETICA, 8, Font.BOLD, Drawing.Color.White)
            rptfont = New Font(Font.HELVETICA, 6, Font.NORMAL, Drawing.Color.Black)
        Catch ex As Exception
            Throw New Exception("document.head crashed.->" & ex.Message.ToString)
        End Try
       

        With tbl
            .addCell(SPURDataControl.makeCell("SPUR Details", headerfont, tbl, True, 8))
            .addCell(SPURDataControl.makeCell(lblLaneNo.Text & " " & lblPickPlanNu.Text, headerfont, tbl, True, 8))
            .addCell(SPURDataControl.makeCell("Pallet", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Case ID", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Stop", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Item/Part", headerfont, tbl, True))
            .addCell(SPURDataControl.makeCell("Item Description", headerfont, tbl, True, 4))
            .endHeaders()
        End With

        tbl.setDefaultCellBackgroundColor(Drawing.Color.White)

        For Each drow As DataRowView In dv
            With tbl
                .addCell(SPURDataControl.makeCell(drow("load_order_seq").ToString.Trim, rptfont, tbl, False))
                .addCell(SPURDataControl.makeCell(drow("pick_cont_group_label_id").ToString.Trim, rptfont, tbl, False))
                .addCell(SPURDataControl.makeCell(drow("stop_seq_no").ToString.Trim, rptfont, tbl, False))
                .addCell(SPURDataControl.makeCell(drow("Partno").ToString.Trim, rptfont, tbl, False))
                .addCell(SPURDataControl.makeCell(IIf(IsDBNull(drow("im_desc")), "", drow("im_desc").ToString.Trim), rptfont, tbl, False, 4, Element.ALIGN_LEFT))
            End With
        Next


        document.add(tbl)

        document.close()

        SPURDataControl.ExportPDF(report, "SPURDetails", Me)

    End Sub
End Class