<%@ Page Language="vb" AutoEventWireup="false" Title="New Location Transfer" MasterPageFile="~/Main.Master" CodeBehind="LocTransferNew.aspx.vb" Inherits="_941Support.LocTransferNew" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">

<%--<script type="text/javascript">
    function popuplist(){
    window.open("LocTransferCustomer.aspx","_blank","width=750,height=450,top=200");
    }
</script>   --%>
<script language="javascript" type="text/javascript">
    function showCustomerList(txtCustID)
   { 
   //alert("OK");
        var WinSettings = "center:yes;resizable:no;dialogHeight:500px;dialogWidth:900px"
        var myCust = window.showModalDialog("LocTransferCustomer.aspx?custno=" + document.getElementById(txtCustID).value ,"",WinSettings);
      
          if (myCust == null || myCust == '')
        {
            //alert("Nothing selected");
        }
        else
        {
            
            document.getElementById(txtCustID).value = myCust;
            
        }
        return false;
        
   }
  </script>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true" ShowSummary="false" />   
<table cellspacing="0" cellpadding="0" align="center" border="0" style="width: 90%;">

<tr>
    <td colspan="4" align="left">
    &nbsp;
    <asp:LinkButton ID="lnkSearch" runat="server" Text="Search" CssClass="HeadCss"></asp:LinkButton>
    &nbsp;
    <asp:LinkButton ID="lnkNewTransfer" runat="server" Text="New Transfer" CssClass="HeadCss"></asp:LinkButton>
    </td>
</tr>

<tr><td colspan="4">&nbsp;</td></tr>

<tr>
    <td>&nbsp;</td>
    <td colspan="3">
    <asp:Label ID="lblMsg" Text="New Le Select Transfer" runat="server" CssClass="WelcomeCss"></asp:Label>
    </td>
</tr>

<tr><td colspan="4">&nbsp;</td></tr>

<tr>
    <td colspan="1">
    &nbsp;
    </td>
    <td colspan="3" valign="top">
        <table border="1" align="center">       
        <tr>
            <td colspan="4" style="background-color:#CBD49E" align="left">
            <asp:Label ID="Label1" runat="server" Text="Original Customer Information:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
            <asp:Label ID="lbl1" runat="server" Text="Customer #:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" colspan="3">
            <asp:TextBox ID="txtOrigCustNo" AutoPostBack="false" runat="server" width="80" CssClass="ValueCss"></asp:TextBox>&nbsp;
            &nbsp;
            <asp:LinkButton ID="lnkLookUp"  runat="server" Text="Look Up" CausesValidation="False"></asp:LinkButton>&nbsp;
            &nbsp;
            <asp:Button ID="btnSetCustomer" runat="server" CssClass="BtnCss" Text="Set Customer" Width="113px" />&nbsp; 
            </td>
        </tr>
        <tr>
            <td align="right">
            <asp:Label ID="lbl2" runat="server" Text="Customer Name:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" colspan="3">
            <asp:Label ID="lblOrigCustName" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
            <asp:Label ID="lbl3" runat="server" Text="Address:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" colspan="3">
            <asp:Label ID="lblOrigCustAddress" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
            <asp:Label ID="lbl4" runat="server" Text="City:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" colspan="3">
            <asp:Label ID="lblOrigCustCity" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
            <asp:Label ID="lbl5" runat="server" Text="Province:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" colspan="3">
            <asp:Label ID="lblOrigCustProvince" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
            <asp:Label ID="lbl6" runat="server" Text="Postal Code:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" colspan="3">
            <asp:Label ID="lblOrigCustPostal" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
            <asp:Label ID="lbl7" runat="server" Text="Phone:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" colspan="3">
            <asp:Label ID="lblOrigCustPhone" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="Label7" runat="server" CssClass="HeadCss" Text="Status:"></asp:Label></td>
                <td align="left" colspan="3">
                    <asp:Label ID="lblStatus" runat="server" CssClass="ValueCss" Text=""></asp:Label></td>
            </tr>
        </table>
    </td>      
</tr>

<tr><td colspan="4">&nbsp;</td></tr>

<tr>
    <td colspan="1">
    &nbsp;
    </td>
    <td colspan="3" align="center">
    <asp:Panel ID="pnlNewCustomer" runat="server" Visible="false">
        <table border="1" >
       
        <tr>
            <td colspan="4" style="background-color:#CBD49E" align="left">
            <asp:Label ID="Label2" runat="server" Text="New Customer Information:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="Label3" runat="server" Text="Customer #:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" colspan="3">
            <asp:TextBox ID="txtNewCustNo" runat="server" width="80" CssClass="ValueCss"></asp:TextBox>&nbsp;            
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="Label4" runat="server" Text="Customer Name:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" colspan="3">
             <asp:TextBox ID="txtNewCustName" runat="server" width="80" CssClass="ValueCss"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
            <asp:Label ID="Label6" runat="server" Text="Address:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" colspan="3">
            <asp:TextBox ID="txtNewCustAddress" runat="server" Text=""  CssClass="ValueCss"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="Label8" runat="server" Text="City:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" colspan="3">
            <asp:TextBox ID="txtNewCustCity" runat="server" Text=""  CssClass="ValueCss"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="Label10" runat="server" Text="Province:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" colspan="3">
            <asp:TextBox ID="txtNewCustProvince" runat="server" Text=""  CssClass="ValueCss"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="Label12" runat="server" Text="Postal Code:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" colspan="3">
            <asp:TextBox ID="txtNewCustPostal" runat="server" Text=""  CssClass="ValueCss"></asp:TextBox>&nbsp;
            </td>
        </tr>
        
        </table>
      </asp:Panel>  
    </td>      
</tr>

<tr><td colspan="4">&nbsp;</td></tr>

<tr>
    <td>
    &nbsp;
    </td>    
    <td colspan="3" align="center">
    <asp:Panel ID="pnlNewInfo" runat="server" Visible="false">
    <table>
    <tr>
        <td align="right">
        <asp:Label ID="lbl21" runat="server" Text="Expected Transfer Date(YYYY-MM-DD):"  CssClass="HeadCss"></asp:Label>&nbsp;
        </td>
        <td align="left" style="white-space:nowrap;">    
        <asp:TextBox ID="txtNewCustTransfer" runat="server" Width="80"></asp:TextBox>
        <a id="a1" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtNewCustTransfer, 'a1', 'yyyy-MM-dd');return false;"
		href="#" name="a1"><img id="Img1" height="21" alt="Calendar"  src="Images/calendar.gif" width="34" align="top" border="0"/></a>
    
        &nbsp;
        </td>
    </tr>
    
     <tr>
        <td align="right">
        <asp:Label ID="Label5" runat="server" Text="Contact Name:"  CssClass="HeadCss"></asp:Label>&nbsp;
        </td>
        <td align="left">    
        <asp:TextBox ID="txtNewCustContact" runat="server" Width="80"></asp:TextBox>&nbsp;  
        </td>
    </tr>
    
      <tr>
        <td align="right">
        <asp:Label ID="Label9" runat="server" Text="Corporate Email:"  CssClass="HeadCss"></asp:Label>&nbsp;
        </td>
        <td align="left">    
        <asp:TextBox ID="txtNewCustEmail" runat="server" Width="80"></asp:TextBox>&nbsp;
        </td>
    </tr>
    </table>
    </asp:Panel>
    </td>
</tr>

<tr><td colspan="4">&nbsp;</td></tr>

<tr>
    <td colspan="4" align="center">
    <asp:Label ID="lblMessage" Text="" runat="server" CssClass="MsgCss"></asp:Label>
    </td>
</tr>

<tr><td colspan="4">&nbsp;</td></tr>

<tr>
    <td colspan="4" align="right">
    <asp:Button ID="btnSave" runat="server" CssClass="BtnCss" Text="Save"
    OnClientClick ="return confirm('Are you sure you want to create the new transfer?');" Visible="False"
     />&nbsp;
    <asp:Button ID="btnCancel" runat="server" CssClass="BtnCss" Text="Cancel" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </td>
</tr>

<tr><td colspan="4">&nbsp;</td></tr>
<tr><td colspan="4">&nbsp;</td></tr>

</table>

</asp:Content>