<%@ Page Language="vb"  AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="UserList.aspx.vb" Inherits="_941Support.UserList" Title="ROC/ROCON Users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">

<script type="text/javascript">
function popuplist(){
window.open("UserAccess.aspx","_blank","width=750,height=450,top=200");
}
</script>
   
    <table cellspacing="0" cellpadding="0" align="center" border="0" >
        
              
          <tr> 
            <td colspan="4" align="center" >&nbsp;</td>
          </tr>
        
        <tr>
            <td colspan="4" align="center" >
            <asp:Label ID="lblSystem" runat="server" Text="System:" CssClass="LabelCss"></asp:Label>
            <asp:DropDownList ID="drpSystem" runat="server" AutoPostBack="true">
                <asp:ListItem Selected="True" Value="0">--None--</asp:ListItem>
                <asp:ListItem Value="1">ROCON</asp:ListItem>
                <asp:ListItem Value="2">ROC</asp:ListItem>
                <asp:ListItem Value="3">SCSGL-View</asp:ListItem>
                <asp:ListItem Value="4">SCSGL-Update</asp:ListItem>
            </asp:DropDownList>
           
            &nbsp;
             <asp:Button ID="btnExport" runat="server" Text="Export Report" Width="121px" CssClass="BtnCss" />
            &nbsp;
            <asp:Button ID="btnExportROCON" runat="server" Text="Export ROCON Access" Width="165px" CssClass="BtnCss" />
            &nbsp;
            <asp:Button ID="btnExportROC" runat="server" Text="Export ROC Access" Width="145px" CssClass="BtnCss" />
            &nbsp;
            
            </td>           
        </tr>
        
                
        <tr> 
            <td colspan="4" align="center" >&nbsp;</td>
        </tr>
        
        <tr> 
            <td colspan="4" align="center"  >             
             <asp:Label ID="lblMsg" runat="server" CssClass="LblCss" ></asp:Label></td>
        </tr>
          
        <tr> 
            <td colspan="4" align="center" rowspan="3" valign="top">
             <asp:Panel ID="pnlSummary" runat="server" >
              <div style="z-index: 10; overflow: auto; height: 350px; width: 295px;">
                <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" DataKeyNames="ACCESS_GROUP"  AllowSorting="True" Height="88px">
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" BorderColor="#617231" />
                    <FooterStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#CBD49E" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#999999" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:BoundField DataField="EMP_ID" HeaderText="Emp_Id" ReadOnly="True" SortExpression="EMP_ID" />
                        <asp:BoundField DataField="EMPLOYEE" HeaderText="Employee" SortExpression="EMPLOYEE" />
                        <asp:TemplateField HeaderText="Access" SortExpression="ACCESS_GROUP">
                             <ItemTemplate>
                                <asp:LinkButton ID="lnkGroupId" runat="server" CommandArgument='<%# CType(Container, GridViewRow).RowIndex %>'  Text='<%# Bind("ACCESS_GROUP") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>                       
                    </Columns>
                </asp:GridView>
              &nbsp;
            </div>
            </asp:Panel>
            </td>
        </tr>
        
        <tr> 
            <td colspan="4" align="center">&nbsp;</td>
        </tr>    
      
      
       
    
    
  </table>    
    
     
</asp:Content>
