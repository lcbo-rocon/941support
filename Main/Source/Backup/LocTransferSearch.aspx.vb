Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Imports System.IO.Directory
Imports System.Data
Imports System.Security.Principal
Imports System.Threading
Imports RSG_ROC.DataLayer
Imports RSG_ROC.Exceptions
Imports RSG_ROC.OrderClasses
Imports RSG_ROC.LS_TRANSFERS
Partial Public Class LocTransferSearch
    Inherits System.Web.UI.Page

    Private Sub LocTransferSearch_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Dim ex As Exception = Server.GetLastError
        Session("ExceptionObj") = ex
        Response.Redirect("~\ErrorPage.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lblheading As Label = Master.FindControl("lblMainTitle")
        If Not lblheading Is Nothing Then
            lblheading.Text = "Licensee Location Transfer"
        End If
        If Not IsPostBack Then
            lblMessage.Text = ""
        End If
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("~\LocTransferSearch.aspx")
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblMessage.Text = ""
        DALRoc.setEnv(ConfigurationManager.AppSettings("env"))
        Dim conn As OracleConnection = Nothing
        conn = New OracleConnection(DALRoc.getConnectionstring(ConfigurationManager.AppSettings("env")))
        Dim cmd As OracleCommand = New OracleCommand
        Dim trans As OracleTransaction = Nothing
        Dim roc_transfer_orders As DataTable = Nothing
        Dim current_user As String = Session("CurrentUserId")

        Dim transfer_id As String = ""
        Dim orig_cust_no As String = ""
        Dim orig_cust_name As String = ""
        Dim new_cust_no As String = ""
        Dim new_cust_name As String = ""
        Dim contact_name As String = ""
        Dim corporate_email As String = ""
        Dim creation_date_from As String = ""
        Dim creation_date_to As String = ""
        Dim transfer_date_from As String = ""
        Dim transfer_date_to As String = ""
        Dim return_order As String = ""
        Dim transfer_order As String = ""
        Dim prepared_by As String = ""
        Dim curr_status As String = ""

        transfer_id = txtTransferId.Text.Trim
        orig_cust_no = txtOrigCustomerNo.Text.Trim
        orig_cust_name = txtOrigCustomerName.Text.Trim
        new_cust_no = txtNewCustomerNo.Text.Trim
        new_cust_name = txtNewCustomerName.Text.Trim
        contact_name = txtContactName.Text.Trim
        corporate_email = txtCorpEmail.Text.Trim
        creation_date_from = txtCreationFrom.Text.Trim
        creation_date_to = txtCreationTo.Text.Trim
        transfer_date_from = txtTransferFrom.Text.Trim
        transfer_date_to = txtTransferTo.Text.Trim
        return_order = txtReturnOrder.Text.Trim
        transfer_order = txtTransferOrder.Text.Trim
        prepared_by = txtPreparedBy.Text.Trim
        curr_status = drpStatus.SelectedValue

        If curr_status = "0" Then
            curr_status = ""
        End If

        Try
            conn.Open()
            trans = conn.BeginTransaction
            cmd.Connection = conn
            cmd.Transaction = trans
            roc_transfer_orders = DALRoc.getLeSelectTransfer(transfer_id, orig_cust_no, orig_cust_name, new_cust_no, new_cust_name, contact_name, corporate_email, creation_date_from, creation_date_to, transfer_date_from, transfer_date_to, return_order, transfer_order, prepared_by, curr_status)

            cmd.Dispose()
            trans.Commit()
            conn.Close()

        Catch ex As Exception
            trans.Rollback()
            cmd.Dispose()
            conn.Close()
            Throw ex
        End Try

        Session("LSTransfers") = New DataView(roc_transfer_orders)
        grdForwarding.DataSource = Session("LSTransfers")
        grdForwarding.DataBind()

    End Sub

    Private Sub grdForwarding_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdForwarding.DataBound
        If grdForwarding.Rows.Count = 0 Then
            lblMessage.Text = "There were no records that match your search criteria"
        ElseIf grdForwarding.Rows.Count > 0 Then
            lblMessage.Text = grdForwarding.Rows.Count & " record(s) match your search criteria"
            For Each tcell As TableCell In grdForwarding.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
        End If
    End Sub

    Private Sub grdForwarding_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdForwarding.RowCommand
        If e.CommandName = "SelectLS" Then
            Session("LocTransferSelectId") = e.CommandArgument
            Response.Redirect("~\LocTransferSearchDetails.aspx")
        End If
    End Sub

    Private Sub lnkNewTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNewTransfer.Click
        Response.Redirect("~\LocTransferNew.aspx")
    End Sub

    Private Sub grdForwarding_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdForwarding.Sorting
        Dim dv As DataView = Session("LSTransfers")
        Dim order As String = ""
        If Not ViewState("SSOrder") Is Nothing Then
            order = ViewState("SSOrder")
        End If

        If order.StartsWith(e.SortExpression) Then
            If order.ToString.EndsWith("ASC") Then
                order = "DESC"
            Else
                order = "ASC"
            End If
        Else
            order = "ASC"
        End If

        ViewState("SSOrder") = e.SortExpression + " " & order
        dv.Sort = ViewState("SSOrder")
        grdForwarding.DataSource = dv
        grdForwarding.DataBind()
    End Sub
End Class