<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CatchAllSkus.aspx.vb" Inherits="_941Support.CatchAllSkus" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Catch All Skus</title>
 <script language="Javascript" type="text/javascript">
function RefreshParent()
{    
    window.close();
}
</script> 
<script language="javascript" type="text/javascript">window.name = "DefaultPage"</script>
<script language="javascript" type="text/javascript">
<!--
function closeDialog(itemno,unitprice)
{
  window.returnValue = itemno + "," + unitprice; 
  window.close();
}
// -->
</script> 
<base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <table>
        
    <tr><td colspan="5">&nbsp;</td></tr>
    
    <tr>
        <td colspan="5">
        <div style="z-index: 10; overflow: auto;height:350px">
            <asp:GridView ID="grdCatchAllSkus" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            BackColor="White" BorderColor="#CBD49E" BorderStyle="None" BorderWidth="1px" 
            CellPadding="4" Font-Size="Small" Font-Names="Arial" GridLines="Vertical" ForeColor="Black" DataKeyNames="item,retail_price"   >
            <FooterStyle BackColor="#CCCC99" />
            <RowStyle BackColor="#F7F7DE" Font-Size="Small" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="White" BorderStyle="None" />
            <HeaderStyle BackColor="#CBD49E" Font-Size="Small" ForeColor="Maroon" Font-Bold="True" />            
                <Columns>      
                    <asp:TemplateField HeaderText="Item No" SortExpression="item">
                      <ItemTemplate>
                        <asp:LinkButton ID="lnkSelect" runat="server" Text='<%# Bind("item") %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="retail_price" HeaderText="Retail Price" SortExpression="retail_price" DataFormatString="{0:F2}" />
                    <asp:BoundField DataField="prod_desc" HeaderText="Product Desc" SortExpression="prod_desc" />
                    <asp:BoundField DataField="prod_size" HeaderText="Product Size" SortExpression="prod_size" />
                    <asp:BoundField DataField="bottle_deposit" HeaderText="Bottle Deposit" SortExpression="bottle_deposit" DataFormatString="{0:F2}" />
                </Columns>
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        </div>       
    </td>    
    </tr>
    
    <tr><td colspan="5">
    <asp:Button ID="btnRefresh" runat="server" Visible="false" />
    &nbsp;</td></tr>
    <tr><td colspan="5">&nbsp;</td></tr>
    <tr><td colspan="5">&nbsp;</td></tr>
    
    </table>
    
    </div>
    </form>
</body>
</html>
