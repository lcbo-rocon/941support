<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UserAccess.aspx.vb" Inherits="_941Support.UserAccess" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>User Access</title>
 <script language="Javascript" type="text/javascript">
function RefreshParent()
{    
    window.close();
}
</script> 
<script language="javascript" type="text/javascript">window.name = "DefaultPage"</script>
<base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <table>
        
    <tr><td colspan="5">&nbsp;</td></tr>
    
    <tr>
        <td colspan="5">
        <div style="z-index: 10; overflow: auto;height:350px">
            <asp:GridView ID="grdUserAccess" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            BackColor="White" BorderColor="#CBD49E" BorderStyle="None" BorderWidth="1px" 
            CellPadding="4" Font-Size="Small" Font-Names="Arial" GridLines="Vertical" ForeColor="Black" DataKeyNames="access_group"   >
            <FooterStyle BackColor="#CCCC99" />
            <RowStyle BackColor="#F7F7DE" Font-Size="Small" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="White" BorderStyle="None" />
            <HeaderStyle BackColor="#CBD49E" Font-Size="Small" ForeColor="Maroon" Font-Bold="True" />            
                <Columns>      
                    <asp:BoundField DataField="access_group" HeaderText="Access_Group" SortExpression="access_group" />
                    <asp:BoundField DataField="menu_header" HeaderText="Menu_Header" SortExpression="menu_header" />
                    <asp:BoundField DataField="menu_detail" HeaderText="Menu_Detail" SortExpression="menu_detail" />                    
                </Columns>
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        </div>       
    </td>    
    </tr>
    
    <tr><td colspan="5">
    <asp:Button ID="btnRefresh" runat="server" Visible="false" />
    &nbsp;</td></tr>
    <tr><td colspan="5">&nbsp;</td></tr>
    <tr><td colspan="5">&nbsp;</td></tr>
    
    </table>
    
    </div>
    </form>
</body>
</html>
