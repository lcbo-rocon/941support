Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Imports System.IO.Directory
Imports System.Data
Imports System.Security.Principal
Imports System.Threading

Partial Public Class TrafficPlanning
    Inherits System.Web.UI.Page

    Private Sub TrafficPlanning_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Dim ex As Exception = Server.GetLastError
        Session("ExceptionObj") = ex
        Response.Redirect("~\ErrorPage.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lblheading As Label = Master.FindControl("lblMainTitle")
        If Not lblheading Is Nothing Then
            lblheading.Text = "Traffic Planning"
        End If
        If Not IsPostBack Then
            If Not Session("TrafficPlanTbl") Is Nothing Then
                Dim dt As DataTable = Nothing
                Dim route As String = ""
                Dim to_routes() As String = Nothing
                Dim from_routes() As String = Nothing
                Dim stopNo As String = ""
                Dim to_stops() As String = Nothing
                Dim from_stops() As String = Nothing
                Dim customerNo As String = ""
                Dim to_custs() As String = Nothing
                Dim from_custs() As String = Nothing
                Dim orderNo As String = ""
                Dim to_orders() As String = Nothing
                Dim from_orders() As String = Nothing
                Dim carrier As String = ""
                Dim to_cs() As String = Nothing
                Dim from_cs() As String = Nothing
                Dim shipDate As String = ""
                Dim to_ship() As String = Nothing
                Dim from_ship() As String = Nothing
                Dim orderType As String = ""
                Dim to_ot() As String = Nothing
                Dim from_ot() As String = Nothing


                dt = Session("TrafficPlanTbl")

                route = dt.Rows(0).Item("Route")
                stopNo = dt.Rows(0).Item("StopNo")
                customerNo = dt.Rows(0).Item("CustomerNo")
                orderNo = dt.Rows(0).Item("OrderNo")
                carrier = dt.Rows(0).Item("Carrier")
                shipDate = dt.Rows(0).Item("ShipDate")
                orderType = dt.Rows(0).Item("OrderType")

                If route.Contains(",") Then
                    to_routes = Regex.Split(route, "\(")
                    txtRoute.Text = Trim((to_routes(1).Replace("'", "")).Replace(")", ""))
                ElseIf route.Contains(" and ") Then
                    to_routes = Regex.Split(route, " and ")
                    txtToRoute.Text = Trim(to_routes(1).Replace("'", ""))
                    from_routes = Regex.Split(to_routes(0), " between ")
                    txtFromRoute.Text = Trim(from_routes(1).Replace("'", ""))
                ElseIf route.Contains(" like ") Then
                    from_routes = Regex.Split(route, " like ")
                    txtFromRoute.Text = Trim(from_routes(1).Replace("'", ""))
                ElseIf route.Contains(">") Then
                    from_routes = Regex.Split(route, " >= ")
                    txtFromRoute.Text = Trim(from_routes(1).Replace("'", ""))
                ElseIf route.Contains("<") Then
                    to_routes = Regex.Split(route, " <= ")
                    txtToRoute.Text = Trim(to_routes(1).Replace("'", ""))
                End If

                If stopNo.Contains(",") Then
                    to_stops = Regex.Split(stopNo, "\(")
                    txtStop.Text = Trim((to_stops(1).Replace("'", "")).Replace(")", ""))
                ElseIf stopNo.Contains(" and ") Then
                    to_stops = Regex.Split(stopNo, " and ")
                    txtToStop.Text = Trim(to_stops(1).Replace("'", ""))
                    from_stops = Regex.Split(to_stops(0), " between ")
                    txtFromStop.Text = Trim(from_stops(1).Replace("'", ""))
                ElseIf stopNo.Contains(">") Then
                    from_stops = Regex.Split(stopNo, " >= ")
                    txtFromStop.Text = Trim(from_stops(1).Replace("'", ""))
                ElseIf stopNo.Contains("<") Then
                    to_stops = Regex.Split(stopNo, " <= ")
                    txtToStop.Text = Trim(to_stops(1).Replace("'", ""))
                End If

                If customerNo.Contains(",") Then
                    to_custs = Regex.Split(customerNo, "\(")
                    txtCustomer.Text = Trim((to_custs(1).Replace("'", "")).Replace(")", ""))
                ElseIf customerNo.Contains(" and ") Then
                    to_custs = Regex.Split(customerNo, " and ")
                    txtToCustomer.Text = Trim(to_custs(1).Replace("'", ""))
                    from_custs = Regex.Split(to_custs(0), " between ")
                    txtFromCustomer.Text = Trim(from_custs(1).Replace("'", ""))
                ElseIf customerNo.Contains(">") Then
                    from_custs = Regex.Split(customerNo, " >= ")
                    txtFromCustomer.Text = Trim(from_custs(1).Replace("'", ""))
                ElseIf customerNo.Contains("<") Then
                    to_custs = Regex.Split(customerNo, " <= ")
                    txtToCustomer.Text = Trim(to_custs(1).Replace("'", ""))
                End If

                If orderNo.Contains(",") Then
                    to_orders = Regex.Split(orderNo, "\(")
                    txtOrder.Text = Trim((to_orders(1).Replace("'", "")).Replace(")", ""))
                ElseIf orderNo.Contains(" and ") Then
                    to_orders = Regex.Split(orderNo, " and ")
                    txtToOrder.Text = Trim(to_orders(1).Replace("'", ""))
                    from_orders = Regex.Split(to_orders(0), " between ")
                    txtFromOrder.Text = Trim(from_orders(1).Replace("'", ""))
                ElseIf orderNo.Contains(">") Then
                    from_orders = Regex.Split(orderNo, " >= ")
                    txtFromOrder.Text = Trim(from_orders(1).Replace("'", ""))
                ElseIf orderNo.Contains("<") Then
                    to_orders = Regex.Split(orderNo, " <= ")
                    txtToOrder.Text = Trim(to_orders(1).Replace("'", ""))
                End If

                If carrier.Contains(",") Then
                    to_cs = Regex.Split(carrier, "\(")
                    txtCarrier.Text = Trim((to_cs(1).Replace("'", "")).Replace(")", ""))
                ElseIf carrier.Contains(" and ") Then
                    to_cs = Regex.Split(carrier, " and ")
                    txtToCarrier.Text = Trim(to_cs(1).Replace("'", ""))
                    from_cs = Regex.Split(to_cs(0), " between ")
                    txtFromCarrier.Text = Trim(from_cs(1).Replace("'", ""))
                ElseIf carrier.Contains("like") Then
                    from_cs = Regex.Split(carrier, " like ")
                    txtFromCarrier.Text = Trim(from_cs(1).Replace("'", ""))
                ElseIf carrier.Contains(">") Then
                    from_cs = Regex.Split(carrier, " >= ")
                    txtFromCarrier.Text = Trim(from_cs(1).Replace("'", ""))
                ElseIf carrier.Contains("<") Then
                    to_cs = Regex.Split(carrier, " <= ")
                    txtToCarrier.Text = Trim(to_cs(1).Replace("'", ""))
                End If


                If shipDate.Contains(" and ") Then
                    to_ship = Regex.Split(shipDate, " and ")
                    txtToShip.Text = Trim(to_ship(1).Replace("'", ""))
                    from_ship = Regex.Split(to_ship(0), " between ")
                    txtFromShip.Text = Trim(from_ship(1).Replace("'", ""))
                ElseIf shipDate.Contains("like") Then
                    from_ship = Regex.Split(shipDate, " like ")
                    txtFromShip.Text = Trim(from_ship(1).Replace("'", ""))
                ElseIf shipDate.Contains(">") Then
                    from_ship = Regex.Split(shipDate, ">")
                    txtFromShip.Text = Trim(from_ship(1).Replace("'", ""))
                ElseIf shipDate.Contains("<") Then
                    to_ship = Regex.Split(shipDate, "<")
                    txtToShip.Text = Trim(to_ship(1).Replace("'", ""))
                ElseIf shipDate.Contains(",") Then
                    to_ship = Regex.Split(shipDate, "in")
                    to_ship = Regex.Split(to_ship(1), "\(")
                    txtShip.Text = Trim((to_ship(1).Replace("'", "")).Replace(")", ""))
                End If


                If orderType.Contains(",") Then
                    to_ot = Regex.Split(orderType, "\(")
                    txtOrderType.Text = Trim((to_ot(1).Replace("'", "")).Replace(")", ""))
                ElseIf orderType.Contains(" and ") Then
                    to_ot = Regex.Split(orderType, " and ")
                    txtToOrderType.Text = Trim(to_ot(1).Replace("'", ""))
                    from_ot = Regex.Split(to_ot(0), " between ")
                    txtFromOrderType.Text = Trim(from_ot(1).Replace("'", ""))
                ElseIf orderType.Contains("like") Then
                    from_ot = Regex.Split(orderType, " like ")
                    txtFromOrderType.Text = Trim(from_ot(1).Replace("'", ""))
                ElseIf orderType.Contains(">") Then
                    from_ot = Regex.Split(orderType, " >= ")
                    txtFromOrderType.Text = Trim(from_ot(1).Replace("'", ""))
                ElseIf orderType.Contains("<") Then
                    to_ot = Regex.Split(orderType, " <= ")
                    txtToOrderType.Text = Trim(to_ot(1).Replace("'", ""))
                ElseIf orderType.Contains("lower") Then
                    from_ot = Regex.Split(orderType, "=")
                    txtFromOrderType.Text = Trim(from_ot(1).Replace("'", ""))
                End If


            Else
            End If
        Else
        End If
        'txtFromShip.Text = "2011-06-16"
        'txtToShip.Text = "2011-06-16"
    End Sub
    
    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("~\TrafficPlanning.aspx")
    End Sub
    ' Based on the criteria chosen by the user to run Traffic Planning, create the whereclause for the select statement
    Private Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Dim dt As New DataTable
        Dim drow As DataRow
        Dim route As String = ""
        Dim stopNo As String = ""
        Dim customerNo As String = ""
        Dim orderNo As String = ""
        Dim carrier As String = ""
        Dim shipDate As String = ""
        Dim orderType As String = ""
        Dim temp() As String = Nothing
        Dim tempItem As String = ""
        Dim tempString As String = ""

        If Trim(txtFromRoute.Text) <> "" And Trim(txtToRoute.Text) <> "" Then
            route = " on_cohdr.route_code between '" & Trim(txtFromRoute.Text) & "' and '" & Trim(txtToRoute.Text) & "' "
        ElseIf Trim(txtFromRoute.Text) <> "" And IsNumeric(Trim(txtFromRoute.Text)) Then
            route = " on_cohdr.route_code >= '" & Trim(txtFromRoute.Text) & "' "
        ElseIf Trim(txtFromRoute.Text) <> "" Then
            route = " lower(on_cohdr.route_code) like '%" & Trim(txtFromRoute.Text).ToLower & "%' "
        ElseIf Trim(txtToRoute.Text) <> "" And IsNumeric(Trim(txtFromRoute.Text)) Then
            route = " on_cohdr.route_code <= '" & Trim(txtToRoute.Text) & "' "
        ElseIf Trim(txtToRoute.Text) <> "" Then
            route = " lower(on_cohdr.route_code) like '%" & Trim(txtFromRoute.Text).ToLower & "%' "
        ElseIf Trim(txtRoute.Text) <> "" Then
            temp = Trim(txtRoute.Text).Split(",")
            For Each tempItem In temp
                If tempString = "" Then
                    tempString = "'" & Trim(tempItem) & "'"
                Else
                    tempString = tempString & ",'" & Trim(tempItem) & "'"
                End If
            Next
            route = " on_cohdr.route_code in(" & tempString & ")"
            temp = Nothing
            tempItem = ""
            tempString = ""
        End If

        If Trim(txtFromStop.Text) <> "" And Trim(txtToStop.Text) <> "" Then
            stopNo = " on_cohdr.route_stop between '" & Trim(txtFromStop.Text) & "' and '" & Trim(txtToStop.Text) & "'"
        ElseIf Trim(txtFromStop.Text) <> "" Then
            stopNo = " on_cohdr.route_stop >= '" & Trim(txtFromStop.Text) & "'"
        ElseIf Trim(txtToStop.Text) <> "" Then
            stopNo = " on_cohdr.route_stop <= '" & Trim(txtToStop.Text) & "'"
        ElseIf Trim(txtStop.Text) <> "" Then
            temp = Trim(txtStop.Text).Split(",")
            For Each tempItem In temp
                If tempString = "" Then
                    tempString = "'" & Trim(tempItem) & "'"
                Else
                    tempString = tempString & ",'" & Trim(tempItem) & "'"
                End If
            Next
            stopNo = " on_cohdr.route_stop in(" & tempString & ")"
            temp = Nothing
            tempItem = ""
            tempString = ""
        End If

        If Trim(txtFromCustomer.Text) <> "" And Trim(txtToCustomer.Text) <> "" Then
            customerNo = " on_cohdr.cu_no between '" & Trim(txtFromCustomer.Text) & "' and '" & Trim(txtToCustomer.Text) & "'"
        ElseIf Trim(txtFromCustomer.Text) <> "" Then
            customerNo = " on_cohdr.cu_no >= '" & Trim(txtFromCustomer.Text) & "'"
        ElseIf Trim(txtToCustomer.Text) <> "" Then
            customerNo = " on_cohdr.cu_no <= '" & Trim(txtToCustomer.Text) & "'"
        ElseIf Trim(txtCustomer.Text) <> "" Then
            temp = Trim(txtCustomer.Text).Split(",")
            For Each tempItem In temp
                If tempString = "" Then
                    tempString = "'" & Trim(tempItem) & "'"
                Else
                    tempString = tempString & ",'" & Trim(tempItem) & "'"
                End If
            Next
            customerNo = " on_cohdr.cu_no in(" & tempString & ")"
            temp = Nothing
            tempItem = ""
            tempString = ""
        End If

        If Trim(txtFromOrder.Text) <> "" And Trim(txtToOrder.Text) <> "" Then
            orderNo = " on_cohdr.co_odno between '" & Trim(txtFromOrder.Text) & "' and '" & Trim(txtToOrder.Text) & "'"
        ElseIf Trim(txtFromOrder.Text) <> "" Then
            orderNo = " on_cohdr.co_odno >= '" & Trim(txtFromOrder.Text) & "'"
        ElseIf Trim(txtToOrder.Text) <> "" Then
            orderNo = " on_cohdr.co_odno <= '" & Trim(txtToOrder.Text) & "'"
        ElseIf Trim(txtOrder.Text) <> "" Then
            temp = Trim(txtOrder.Text).Split(",")
            For Each tempItem In temp
                If tempString = "" Then
                    tempString = "" & Trim(tempItem) & "'"
                Else
                    tempString = tempString & ",'" & Trim(tempItem) & "'"
                End If
            Next
            orderNo = " on_cohdr.co_odno in(" & tempString & ")"
            temp = Nothing
            tempItem = ""
            tempString = ""
        End If

        If Trim(txtFromCarrier.Text) <> "" And Trim(txtToCarrier.Text) <> "" Then
            carrier = " on_cohdr.carrier between '" & Trim(txtFromCarrier.Text) & "' and '" & Trim(txtToCarrier.Text) & "' "
        ElseIf Trim(txtFromCarrier.Text) <> "" Then
            carrier = " on_cohdr.carrier='" & Trim(txtFromCarrier.Text) & "' "
        ElseIf Trim(txtToCarrier.Text) <> "" Then
            carrier = " on_cohdr.carrier='" & Trim(txtToCarrier.Text) & "' "
        ElseIf Trim(txtCarrier.Text) <> "" Then
            temp = Trim(txtCarrier.Text).Split(",")
            For Each tempItem In temp
                If tempString = "" Then
                    tempString = "'" & Trim(tempItem) & "'"
                Else
                    tempString = tempString & ",'" & Trim(tempItem) & "'"
                End If
            Next
            carrier = " on_cohdr.carrier in(" & tempString & ")"
            temp = Nothing
            tempItem = ""
            tempString = ""
        End If

        If Trim(txtFromShip.Text) <> "" And Trim(txtToShip.Text) <> "" Then
            shipDate = " To_char(Trunc(on_cohdr.order_reqd_dt),'YYYY-MM-DD') between '" & Trim(txtFromShip.Text) & "' and '" & Trim(txtToShip.Text) & "' "
        ElseIf Trim(txtFromShip.Text) <> "" Then
            shipDate = " To_char(Trunc(on_cohdr.order_reqd_dt),'YYYY-MM-DD')>'" & Trim(txtFromShip.Text) & "' "
        ElseIf Trim(txtToShip.Text) <> "" Then
            shipDate = " To_char(Trunc(on_cohdr.order_reqd_dt),'YYYY-MM-DD')<'" & Trim(txtToShip.Text) & "' "
        ElseIf Trim(txtShip.Text) <> "" Then
            temp = Trim(txtShip.Text).Split(",")
            For Each tempItem In temp
                If tempString = "" Then
                    tempString = "'" & Trim(tempItem) & "'"
                Else
                    tempString = tempString & ",'" & Trim(tempItem) & "'"
                End If
            Next
            shipDate = " To_char(Trunc(on_cohdr.order_reqd_dt),'YYYY-MM-DD') in(" & tempString & ")"
            temp = Nothing
            tempItem = ""
            tempString = ""
        End If

        If Trim(txtFromOrderType.Text) <> "" Or Trim(txtToOrderType.Text) <> "" Then
            orderType = " lower(on_cohdr.order_type)='" & Trim(txtFromOrderType.Text.ToLower) & "' "
        ElseIf Trim(txtOrderType.Text) <> "" Then
            temp = Trim(txtOrderType.Text).Split(",")
            For Each tempItem In temp
                If tempString = "" Then
                    tempString = "'" & Trim(tempItem).ToLower & "'"
                Else
                    tempString = tempString & ",'" & Trim(tempItem).ToLower & "'"
                End If
            Next
            orderType = " lower(on_cohdr.order_type) in(" & tempString & ")"
            temp = Nothing
            tempItem = ""
            tempString = ""
        End If


        dt.Columns.Add("Route")
        dt.Columns.Add("StopNo")
        dt.Columns.Add("CustomerNo")
        dt.Columns.Add("OrderNo")
        dt.Columns.Add("Carrier")
        dt.Columns.Add("ShipDate")
        dt.Columns.Add("OrderType")

        drow = dt.NewRow
        drow.Item("Route") = route
        drow.Item("StopNo") = stopNo
        drow.Item("CustomerNo") = customerNo
        drow.Item("OrderNo") = orderNo
        drow.Item("Carrier") = carrier
        drow.Item("ShipDate") = shipDate
        drow.Item("OrderType") = orderType
        
        dt.Rows.Add(drow)
        Session("TrafficPlanTbl") = dt
        Response.Redirect("~\TrafficPlanDetails.aspx")
    End Sub
End Class