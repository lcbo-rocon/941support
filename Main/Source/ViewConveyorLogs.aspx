<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="ViewConveyorLogs.aspx.vb" Inherits="_941Support.ViewConveyorLogs" 
    title="View Conveyor Logs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
    <table width="90%">
    <tr>
        <td align="center">
            <div style="height:400px; overflow:auto; z-index:10; width:320px">
             <asp:GridView ID="grdConveyorLogs" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                CellPadding="4" ForeColor="#333333" GridLines="None" Width="300px" Font-Names="Ariel">
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                    <asp:BoundField DataField="FileLastUpdate" HeaderText="Last Update Date Time" SortExpression="FileLastUpdate" />
                    <asp:TemplateField HeaderText="Log File" SortExpression="FileName">
                       <ItemTemplate>
                            <asp:HyperLink ID="lnkFileName" runat="server" Text='<%# Bind("FileName") %>' NavigateUrl='<%# Bind("FileFullName") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#7C6F57" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
            </div>
        </td>
    </tr>
    </table>
   
</asp:Content>
