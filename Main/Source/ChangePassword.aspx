<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="ChangePassword.aspx.vb" Inherits="_941Support.ChangePassword" 
    title="Change Password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
<table width="100%">
    <tr>
        <td align="right" class="rprompt">ROC/ROCON Userid:</td>
        <td align="left"><asp:TextBox ID="txtUserID" runat="server" CssClass="textbox" Columns="10" MaxLength="10"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right" class="rprompt">User Name:</td>
        <td align="left"><asp:TextBox ID="txtUsername" runat="server" CssClass="textbox" Columns="50" MaxLength="50"></asp:TextBox></td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <asp:Button ID="btnSearch" runat="server" CssClass="BtnCss" Text="Search" />
            <asp:Button ID="btnClear" runat="server" CssClass="BtnCss" Text="Reset" />
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            &nbsp;</td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <asp:Label ID="lblMsg" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <div style="height:400px; width:400px; overflow:auto; z-index:10;">
            <asp:GridView ID="grdUsers" runat="server" AutoGenerateColumns="False" CellPadding="4"
                ForeColor="#333333" GridLines="None" Width="380px" AllowSorting="True">
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#E3EAEB" />
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                           <asp:LinkButton ID="lnkEdit" runat="server" Text="Change Password" CommandArgument='<%# Container.DataItemIndex %>' CommandName="ChangePassword"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="User ID" SortExpression="UserID">
                        <ItemTemplate>
                            <asp:Label ID="lblChangeUserID" runat="server" Text='<%# Bind("UserID") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="User Name" SortExpression="UserName">
                         <ItemTemplate>
                            <asp:Label ID="lblChangeUserName" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#7C6F57" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
            </div>
        </td>
        <td valign="top">
            <asp:Panel ID="pnlPassword" runat="server" Visible="false">
            <table class="Frame">
                <tr>
                    <td align="right" class="rprompt">User ID:</td>
                    <td align="left"><asp:Label ID="lblUserID" runat="server" CssClass="lprompt" ForeColor="Black" ></asp:Label></td>
                </tr>
                <tr>
                    <td align="right" style="white-space:nowrap" class="rprompt">User Name:</td>
                    <td align="left"><asp:Label ID="lblUserName" runat="server" CssClass="lprompt" ForeColor="Black"></asp:Label></td>
                </tr>
                <tr>
                    <td align="right" style="white-space:nowrap" class="rprompt">New Password:</td>
                    <td align="left"><asp:TextBox id="txtNewPassword" runat="server" CssClass="textbox" Columns="10" MaxLength="10" TextMode="Password"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" style="white-space:nowrap" class="rprompt">Confirm New Password:</td>
                    <td align="left"><asp:TextBox ID="txtConfirm" runat="server" Columns="10" MaxLength="10" TextMode="Password"></asp:TextBox></td>
                </tr>
                <tr>
                    <td align="right" colspan="2" style="white-space: nowrap">
                        <asp:Button ID="btnChangePassword" runat="server" CssClass="BtnCss" Text="Change Password" Width="175px" />
                        <asp:Button ID="btnCancel" runat="server" CssClass="BtnCss" Text="Cancel" /></td>
                </tr>
            </table>
            </asp:Panel>
        </td>
    </tr>
</table>
</asp:Content>
