Imports Microsoft.VisualBasic
Imports System.Security.Principal
Imports System.Threading
Imports System.DirectoryServices
Imports System.Data


Public Class User
    Inherits DataTable
    Dim _Userid As String
    Dim _firstname As String
    Dim _lastname As String
    Dim _email As String

    ReadOnly Property userid() As String
        Get
            Return _Userid
        End Get
    End Property
    ReadOnly Property firstname() As String
        Get
            Return _firstname
        End Get
    End Property
    ReadOnly Property lastname() As String
        Get
            Return _lastname
        End Get
    End Property
    ReadOnly Property email() As String
        Get
            Return _email
        End Get
    End Property

    
    Public Shared Function GetUserInfo(ByVal inSAM As String, ByVal inType As String) As String


        Dim sPath As String = "LDAP://lcbo.com/DC=lcbo,DC=com"
        Dim SamAccount As String = Right(inSAM, Len(inSAM) - InStr(inSAM, "\"))
        Dim myDirectory As New DirectoryEntry(sPath)
        Dim mySearcher As New DirectorySearcher(myDirectory)
        Dim mySearchResultColl As SearchResultCollection
        Dim mySearchResult As SearchResult
        Dim myResultPropColl As ResultPropertyCollection
        Dim myResultPropValueColl As ResultPropertyValueCollection
        Dim retval As String = ""
        Dim group As String = ""
        Dim groups As String()
        'Build LDAP query
        Try
            mySearcher.Filter = ("(&(objectClass=user)(samaccountname=" & SamAccount & "))")
            'mySearcher.PropertiesToLoad.Add("memberOf")
            mySearchResultColl = mySearcher.FindAll()
            For Each mySearchResult In mySearchResultColl
                mySearchResult = mySearchResultColl.Item(0)
                myResultPropColl = mySearchResult.Properties
                myResultPropValueColl = myResultPropColl.Item(inType)

                For Each group In myResultPropValueColl
                    groups = group.Split(",")
                    retval = retval & group
                    '      retval = retval & Right(groups(0), Len(groups(0)) - InStr(groups(0), "=")) & "<BR>"
                Next

            Next

        Catch ex As Exception
            retval = ""
        End Try

        Return retval
    End Function

    
End Class


