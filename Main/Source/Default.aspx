<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="_941Support._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head id="Head1" runat="server">
    <title>941 Support Utility</title>
    <link href="Stylesheet.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="./Scripts/AnchorPosition.js"></script>
	<script type="text/javascript" src="./Scripts/PopupWindow.js"></script>
	<script type="text/javascript" src="./Scripts/date.js"></script>
	<script type="text/javascript" src="./Scripts/selectbox.js"></script>
	<script type="text/javascript" src="./Scripts/CalendarPopup.js"></script>
	<script type="text/javascript">document.write(CalendarPopup_getStyles());</script>
</head>
<body>
 <form id="form2" runat="server">
    <table class="Frame" width="1000" cellpadding="0" cellspacing="0">
    <tr style="background-color:#CBD49E;">
		    <td align="center" colspan="3" valign="middle" style="height:30px">
		    	<asp:Label ID="lblMainTitle" runat="server" CssClass="Heading">941 Support Utility</asp:Label>
			 </td>
	    </tr>
	    <tr style="background-color:#CBD49E;">
	        <td align="center" colspan="3">
	         <asp:Label ID="lblEnv" runat="server" CssClass="WelcomeCss"></asp:Label>
	        </td>
	    </tr>
	    <tr style="background-color:#CBD49E;">
		    <td colspan="3" style="FILTER: progid:DXImageTransform.Microsoft.Shadow(direction=180,color=#37411a,strength=12);MARGIN:0px;">&nbsp;</td>
	    </tr>
	    <tr>
	        <td valign="top" width="20%" rowspan="2" style="height:500px"> 
                <asp:Menu ID="MainMenu" runat="server" BackColor="#CBD49E" DynamicHorizontalOffset="2"
                    Font-Names="Tahoma" Font-Size="Medium" ForeColor="Maroon" StaticSubMenuIndent="10px" Height="300px" Enabled="False">
                    <StaticSelectedStyle BackColor="#7C6F57" ForeColor="White" />
                    <LevelSubMenuStyles>
                        <asp:SubMenuStyle Font-Underline="False" />
                    </LevelSubMenuStyles>
                    <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                    <DynamicHoverStyle BackColor="#7C6F57" ForeColor="White" />
                    <DynamicMenuStyle BackColor="#F7F6F3" />
                    <DynamicSelectedStyle BackColor="#7C6F57" ForeColor="White" />
                    <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                    <StaticHoverStyle BackColor="#7C6F57" ForeColor="White" />
                    <Items>
                    <asp:MenuItem Text="User List" ToolTip="ROC/ROCON Users" Value="UserList" NavigateUrl="~/UserList.aspx"></asp:MenuItem>
                        <asp:MenuItem Text="Reset User" ToolTip="Reset HH User Session" Value="ResetHHUser" NavigateUrl="~/ResetHHUser.aspx"></asp:MenuItem>
                        <asp:MenuItem Text="Label Printing" ToolTip="Restart Printing Services" Value="RestartRRPS" NavigateUrl="~/RestartRRPS.aspx">
                        </asp:MenuItem>
                        <asp:MenuItem Text="Unlock Cartons" ToolTip="Page for the user to unlock the locked Carton" Value="UnlockCarton" NavigateUrl="~/UnlockCarton.aspx">
                        </asp:MenuItem>
                        <asp:MenuItem Text="Fixing Location" ToolTip="Fix Locations" Value="FixLocation" NavigateUrl="~/FixLocation.aspx">
                        </asp:MenuItem>
                        <asp:MenuItem NavigateUrl="~/FixCubeFlag.aspx" Text="Fix Cube Item Flag" ToolTip="Fix Cube Item Flag"
                            Value="FixCubeFlag"></asp:MenuItem>
                        <asp:MenuItem Text="View Archive Invoices" ToolTip="View Archive Invoices" Value="ArchiveInvoice" NavigateUrl="~/ArchiveInvoice.aspx">
                        </asp:MenuItem>
                        <asp:MenuItem Text="Tender Override" ToolTip="Temporary change tender override" Value="TenderRestrictionOverride" NavigateUrl="~/TenderRestrictionOverride.aspx">
                        </asp:MenuItem>
                        <asp:MenuItem Text="Run Eord" ToolTip="Run Eord" Value="RunEord" NavigateUrl="~/RunEord.aspx"></asp:MenuItem>
                       <asp:MenuItem Text="SPUR Reporting" ToolTip="SPUR Reporting" Value="SPURReport" NavigateUrl="~/SPURReport.aspx">
                        </asp:MenuItem>
                        <asp:MenuItem NavigateUrl="ViewConveyorLogs.aspx" Text="View Conveyor Logs" ToolTip="View Dematic Conveyor Log Files"
                            Value="ViewConveyorLogs"></asp:MenuItem>
                        <asp:MenuItem Text="Create IST" ToolTip="Create IST" NavigateUrl="~/CreateIST.aspx" Value="CreateIST"></asp:MenuItem>    
                        <asp:MenuItem Enabled="False"></asp:MenuItem>
                        <asp:MenuItem NavigateUrl="~/ChangePassword.aspx" Text="Change Password" Value="ChangePassword">
                        </asp:MenuItem>
                    </Items>
                </asp:Menu>
            </td>
            <td width="80%" align="left" style="height: 30px" valign="top"><asp:Label ID="lblWelcome" runat="server"  CssClass="WelcomeCss"></asp:Label></td>
	       </tr> 
           <tr>
              <td width="80%" valign="top" colspan="2">  &nbsp;</td>
           </tr>
    </table>
      
    </form>
</body>
   
</html>