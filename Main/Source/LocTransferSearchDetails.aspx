<%@ Page Language="vb" AutoEventWireup="false" Title="Location Transfer Search" MasterPageFile="~/Main.Master" CodeBehind="LocTransferSearchDetails.aspx.vb" Inherits="_941Support.LocTransferSearchDetails" MaintainScrollPositionOnPostback="True" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
<script language="javascript" type="text/javascript">
    function showCustomerList(txtCustID)
   { 
   //alert("OK");
        var WinSettings = "center:yes;resizable:no;dialogHeight:500px;dialogWidth:900px"
        var myCust = window.showModalDialog("LocTransferCustomer.aspx?custno=" + document.getElementById(txtCustID).value ,"",WinSettings);
      
          if (myCust == null || myCust == '')
        {
            //alert("Nothing selected");
        }
        else
        {
            
            document.getElementById(txtCustID).value = myCust;
            
        }
        return false;        
   }
  </script>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true" ShowSummary="false" />   
<table cellspacing="0" cellpadding="0" align="center" border="0" style="width: 90%;">

<tr>
    <td>&nbsp;</td>
    <td colspan="6">
    <asp:Label ID="lblMsg" Text="" runat="server" CssClass="WelcomeCss"></asp:Label>
    </td>
</tr>

<tr><td colspan="7"><br /></td></tr>

<tr>
    <td colspan="1">
    &nbsp;
    </td>
    <td colspan="2" valign="top">
        <table border="1" align="left">
        <tr>
            <td colspan="2" style="background-color:#CBD49E">
            <asp:Label ID="Label1" runat="server" Text="Original Customer Information:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl1" runat="server" Text="Customer #:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:Label ID="lblOrigCustNo" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl2" runat="server" Text="Customer Name:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:Label ID="lblOrigCustName" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl3" runat="server" Text="Address:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:Label ID="lblOrigCustAddress" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl4" runat="server" Text="City:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:Label ID="lblOrigCustCity" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl5" runat="server" Text="Province:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:Label ID="lblOrigCustProvince" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl6" runat="server" Text="Postal Code:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:Label ID="lblOrigCustPostal" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl7" runat="server" Text="Phone:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:Label ID="lblOrigCustPhone" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
            </td>
        </tr>
        </table>
    </td>
    
    <td>
    &nbsp;
    </td>
    
    <td colspan="2" valign="top">
        <table border="1" align="left">
        <tr>
            <td colspan="2" style="background-color:#CBD49E">
            <asp:Label ID="label2" runat="server" Text="New Customer Information:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl11" runat="server" Text="Customer #:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left" style="white-space:nowrap;">
            <asp:TextBox ID="txtNewCustNo" runat="server" Width="50px"></asp:TextBox>&nbsp;
            <asp:LinkButton ID="lnkLookUp"  runat="server" Text="Search" CausesValidation="False"></asp:LinkButton>&nbsp;
            <asp:Button ID="btnSetCustomer" runat="server" CssClass="BtnCss" Text="Set" Width="40px"  />&nbsp;            
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl12" runat="server" Text="Customer Name:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:TextBox ID="txtNewCustName" runat="server" Width="130px"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl13" runat="server" Text="Address:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:TextBox ID="txtNewCustAddress" runat="server" Width="130px" TextMode="MultiLine"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl14" runat="server" Text="City:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:TextBox ID="txtNewCustCity" runat="server" Width="100px"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl15" runat="server" Text="Province:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:TextBox ID="txtNewCustProvince" runat="server" Width="50"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="white-space:nowrap;">
            <asp:Label ID="lbl16" runat="server" Text="Postal Code:"  CssClass="HeadCss"></asp:Label>&nbsp;
            </td>
            <td align="left">
            <asp:TextBox ID="txtNewCustPostal" runat="server" Width="50"></asp:TextBox>&nbsp;
            </td>
        </tr>
        </table>
    </td>
      
    <td>
    &nbsp;
    </td>  
</tr>


<tr><td colspan="7"><br /></td></tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td colspan="2" align="left">
    <asp:Label ID="lbl21" runat="server" Text="Expected Transfer Date:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:TextBox ID="txtNewCustTransfer" runat="server" Width="80"></asp:TextBox>
     <a id="a1" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtNewCustTransfer, 'a1', 'yyyy-MM-dd');return false;"
	href="#" name="a1"><img id="Img1" runat="server" height="21" alt="Calendar"  src="Images/calendar.gif" width="34" align="top" border="0"/></a>
     &nbsp;
    </td>
    <td></td>
    <td colspan="2" align="left">
    <asp:Label ID="lbl22" runat="server" Text="Return Order#:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblReturnNo" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
    </td>
    <td>
    &nbsp;
    </td>
</tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td colspan="2" align="left">
    <asp:Label ID="lbl23" runat="server" Text="Contact Name:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:TextBox ID="txtNewCustContact" runat="server" Width="150px"></asp:TextBox>&nbsp;
    </td>
    <td></td>
    <td colspan="2" align="left">
    <asp:Label ID="lbl24" runat="server" Text="Transfer Order#:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblTransferNo" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
    </td>
    <td>
    &nbsp;
    </td>
</tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td colspan="2" align="left">
    <asp:Label ID="lbl25" runat="server" Text="Corporate Email:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:TextBox ID="txtNewCustEmail" runat="server" Width="150px"></asp:TextBox>&nbsp;
    </td>
    <td></td>
    <td colspan="2" align="left">
    <asp:Label ID="lbl26" runat="server" Text="Prepared By:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblPreparedBy" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
    </td>
    <td>
    &nbsp;
    </td>
</tr>


<tr>
    <td>
    &nbsp;
    </td>
    <td colspan="2" align="left">
    <asp:Label ID="lbl27" runat="server" Text="Status:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblCurrentStatus" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
    </td>
    <td colspan="3" align="right">    
    </td>
    <td>
    &nbsp;
    </td>
</tr>


<tr>
    <td colspan="7" align="right">
    <asp:Button ID="btnSave" runat="server" CssClass="BtnCss" Text="Save" />
    <asp:Button ID="btnCancel" runat="server" CssClass="BtnCss" Text="Cancel" />
    &nbsp;&nbsp;&nbsp;
    </td>
</tr>

<tr><td colspan="7">&nbsp;</td></tr>

<tr>
    <td>&nbsp;</td>
    <td colspan="6">
    <asp:Label ID="Label3" runat="server" Text="CheckList:"  CssClass="HeadCss"></asp:Label>&nbsp;
    </td>
</tr>

<tr>
    <td>&nbsp;</td>
    <td colspan="6" align="center" >
       <%-- <div style="z-index: 10; overflow: auto;height:290px">--%>
            <asp:GridView ID="grdCheckList" runat="server" AutoGenerateColumns="False"
            BackColor="White" BorderColor="#CBD49E" BorderStyle="Inset" BorderWidth="1px" 
            CellPadding="4" Font-Size="Small" Font-Names="Arial" GridLines="Vertical" ForeColor="Black" DataKeyNames="task_id"   >
            <FooterStyle BackColor="#CCCC99" />
            <RowStyle BackColor="#F7F7DE" Font-Size="Small" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="White" BorderStyle="None" />
            <HeaderStyle BackColor="#CBD49E" Font-Size="Small" ForeColor="Maroon" Font-Bold="True" />            
                <Columns>      
                    <asp:TemplateField HeaderText="Check Id" SortExpression="check_id" Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lblCheckListCheck" runat="server" Text='<%# Bind("check_id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="task_id" HeaderText="Task Id" SortExpression="task_id" />
                    <asp:TemplateField HeaderText="Task Description" SortExpression="task_description">
                        <ItemTemplate>
                        <asp:HyperLink id="hypCheckListTask" runat="server" Text='<%# Bind("task_description") %>'></asp:HyperLink>
                       </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="last_update"   HeaderText="Last Update" SortExpression="last_update" NullDisplayText=" " />
                    <asp:BoundField DataField="completed_tasks" HeaderText="Completed Tasks" SortExpression="completed_tasks" />
                    <asp:BoundField DataField="total_tasks" HeaderText="Total Tasks" SortExpression="total_tasks" />
                    <asp:BoundField DataField="progress" HeaderText="Progress" SortExpression="progress" />
                    <asp:TemplateField HeaderText="Status" SortExpression="status">
                        <ItemTemplate>
                        <asp:CheckBox ID="chkCheckListStatus" runat="server" OnCheckedChanged="chkStatus_Changed" 
                         AutoPostBack="true" Checked='<%# Bind("status") %>'  />
                        <asp:Label ID="Label2" runat="server" Visible="false" Text='<%# Bind("status") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="automated_check" Visible="False" HeaderText="Automated Check" SortExpression="automated_check" />
                </Columns>
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
       <%-- </div>  --%>     
    </td>
</tr>

<tr><td colspan="7">&nbsp;</td></tr>

<tr>
    <td>&nbsp;</td>
    <td colspan="6">
    <asp:Label ID="Label4" runat="server" Text="Uploaded Documents:"  CssClass="HeadCss"></asp:Label>&nbsp;&nbsp;
    <asp:Label ID="lblUpMsg" Text="" runat="server" CssClass="LabelCss"></asp:Label>
    </td>
</tr>

<tr>
    <td>&nbsp;</td>
    <td colspan="6" align="center" >
        <%--<div style="z-index: 10; overflow: auto;height:150px">--%>
            <asp:GridView ID="grdUploadedDocs" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            BackColor="White" BorderColor="#CBD49E" BorderStyle="Inset" BorderWidth="1px" 
            CellPadding="4" Font-Size="Small" Font-Names="Arial" GridLines="Vertical" ForeColor="Black" DataKeyNames="doc_id"   >
            <FooterStyle BackColor="#CCCC99" />
            <RowStyle BackColor="#F7F7DE" Font-Size="Small" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="White" BorderStyle="None" />
            <HeaderStyle BackColor="#CBD49E" Font-Size="Small" ForeColor="Maroon" Font-Bold="True" />            
                <Columns>                                   
                    <asp:BoundField DataField="doc_id" HeaderText="ID" SortExpression="doc_id" />
                    <asp:BoundField DataField="doc_type_id" HeaderText="Type" SortExpression="doc_type_id" />
                    <asp:TemplateField HeaderText="Document Name" SortExpression="doc_filepath">                       
                        <ItemTemplate>
                            <asp:HyperLink ID="hypDocFilePath" runat="server" NavigateURL='<%# Bind("doc_filepath") %>'></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="doc_desc" HeaderText="Document Description" SortExpression="doc_desc" />
                    <asp:BoundField DataField="upload_by" HeaderText="Upload By" SortExpression="upload_by" />
                    <asp:BoundField DataField="upload_dt" DataFormatString="{0:d}" HeaderText="Upload Date" SortExpression="upload_dt" />
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkUploadDelete" runat="server" CausesValidation="False" 
                            OnClientClick ="return confirm('Are you sure you want to delete this document?');"
                            CommandName="Delete"  Text="Delete"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                 </Columns>
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
     <%--   </div>  --%>     
    </td>
</tr>

<tr><td colspan="7">&nbsp;</td></tr>

<tr>
    <td>&nbsp;</td>
    <td colspan="6">
    <asp:Label ID="Label5" runat="server" Text="Pending Documents:"  CssClass="HeadCss"></asp:Label>&nbsp;&nbsp;
    <asp:Label ID="lblPendMsg" Text="" runat="server" CssClass="LabelCss"></asp:Label>
    </td>
</tr>

<tr>
    <td>&nbsp;</td>
    <td colspan="6" align="left" >
      <%--  <div style="z-index: 10; overflow: auto;height:140px">--%>
            <asp:GridView ID="grdPendingDocs" runat="server" AutoGenerateColumns="False"
            BackColor="White" BorderColor="#CBD49E" BorderStyle="Inset" BorderWidth="1px" 
            CellPadding="4" Font-Size="Small" Font-Names="Arial" GridLines="Vertical" ForeColor="Black" DataKeyNames="Le_Select_Id"   >
            <FooterStyle BackColor="#CCCC99" />
            <RowStyle BackColor="#F7F7DE" Font-Size="Small" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="White" BorderStyle="None" />
            <HeaderStyle BackColor="#CBD49E" Font-Size="Small" ForeColor="Maroon" Font-Bold="True" />            
                <Columns>               
                   <asp:BoundField DataField="doc_id" HeaderText="ID" SortExpression="doc_id" />                  
                   <asp:BoundField DataField="doc_desc" HeaderText="Document Description" SortExpression="doc_desc" />
                </Columns>
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        <%--</div> --%>      
    </td>
</tr>

<tr><td colspan="7">&nbsp;</td></tr>
<tr><td colspan="7">&nbsp;</td></tr>

<tr>
    <td>&nbsp;</td>
    <td colspan="6">    
    <asp:Label ID="lbl33" runat="server" Text="Replace if Exist:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:CheckBox ID="chkReplace" runat="server" Checked="true" /> &nbsp;  
   
    <asp:Label ID="lbl32" runat="server" Text="Document Description:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:DropDownList ID="drpDocDescription" runat="server"></asp:DropDownList>&nbsp;
      
    </td>
</tr>



<tr>  
    <td>&nbsp;</td>
    <td colspan="6">
    <asp:Label ID="lbl31" runat="server" Text="Filename:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:FileUpload ID="flupDocument" runat="server" EnableViewState="True" Width="499px" />    
    <asp:Button ID="btnUpload" runat="server" CssClass="BtnCss" Text="Upload" />    
    &nbsp;&nbsp;&nbsp;&nbsp;
    </td>
 </tr>


<tr><td colspan="7">&nbsp;</td></tr>
<tr><td colspan="7">&nbsp;</td></tr>

</table>

</asp:Content>

