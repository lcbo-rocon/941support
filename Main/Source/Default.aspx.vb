Imports System.Security.Principal
Imports System.Threading

Partial Public Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim id As IIdentity = Thread.CurrentPrincipal.Identity 'HttpContext.Current.User.Identity
        Dim userid As String = UCase(id.Name).Replace("LCBO\", "")

        Dim OperationUsers As String() = ConfigurationManager.AppSettings("OperationsUser").Split(",")
        For Each touser As String In OperationUsers
            If UCase("lcbo\" & userid).Equals(UCase(touser)) Then
                Response.Redirect("ResetHHUser.aspx")
            End If

        Next
        Dim TenderOverrideUsers As String() = ConfigurationManager.AppSettings("TenderOverrideUsers").Split(",")
        For Each TOUser As String In TenderOverrideUsers
            If UCase("lcbo\" & userid).Equals(UCase(TOUser)) Then
                Response.Redirect("TenderRestrictionOverride.aspx")
            End If
        Next

        Dim CreateISTUsers As String() = ConfigurationManager.AppSettings("CreateISTUsers").Split(",")
        For Each TOUser As String In CreateISTUsers
            If UCase("lcbo\" & userid).Equals(UCase(TOUser)) Then
                Response.Redirect("CreateIST.aspx")
            End If
        Next

        lblWelcome.Text = "Sorry, it appears that you do not have access to this site. Please contact RSG-HOST for assistance."
    End Sub

End Class