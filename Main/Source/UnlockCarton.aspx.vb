'----------------------------------------------------------------------------------------------
' This page displays the form for the user to unlock the locked Carton.
' The User is supposed to enter the Pick Plan Id and the Ticket Id to be unlocked.
'----------------------------------------------------------------------------------------------
' Created By: Ria Bhatnagar, LCBO-RSG
' Date: May, 2008
'----------------------------------------------------------------------------------------------
' Updated By: Shirley Lam, LCBO-RSG
' Date: June 15, 2008
' Use MainTitle.ascx instead for the Heading and dynamically change the title in Page Load
'----------------------------------------------------------------------------------------------
Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Partial Class UnlockCarton
    Inherits System.Web.UI.Page

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtTicketId.Text = ""
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strConnection As String
        Dim myConnection As OracleConnection = Nothing
        Dim myCommand As OracleCommand = Nothing
        Dim dr As OracleDataReader = Nothing

        Dim pp_no As Integer = 0
        Dim p_check As Integer = 0
        Dim rc As Integer = 0
        Dim pick_plan As Integer = 0
        Dim temp_value As Integer = 0



        If txtTicketId.Text = "" Then
            lblMessage.Text = "Please enter the Tote Id, it can not be blank."
        Else

            Dim check_tote As String = ""
            check_tote = txtTicketId.Text

            Dim currentUser As String = User.Identity.Name.Substring(User.Identity.Name.IndexOf("\") + 1)

            Dim env As String = System.Configuration.ConfigurationManager.AppSettings("env")

            strConnection = System.Configuration.ConfigurationManager.AppSettings(env)
            myConnection = New OracleConnection(strConnection)


            Try
                'Make sure that the toteid to unlocked is a valid tote id
                myCommand = New OracleCommand("SELECT COUNT(*) FROM dbo.pick_cont_group WHERE Trim(pick_cont_group_label_id) = Trim(:TicketId)", myConnection)
                myCommand.Parameters.Clear()
                myCommand.Parameters.AddWithValue("TicketId", check_tote)

                If strConnection <> "" Then
                    myConnection.Open()
                End If

                dr = myCommand.ExecuteReader()

                While dr.Read()
                    temp_value = dr(0)
                End While

                If temp_value <= 0 Then
                    rc = 5
                Else
                    'Getting the pick plan number corresponding to the tote id
                    myCommand.CommandText = "SELECT DISTINCT pick_plan_nu as PickPlan FROM dbo.pick_cont_group WHERE Trim(pick_cont_group_label_id) = Trim(:TicketId)"
                    myCommand.Parameters.Clear()
                    myCommand.Parameters.AddWithValue("TicketId", check_tote)

                    dr = myCommand.ExecuteReader()

                    While dr.Read()
                        pick_plan = dr(0)
                    End While

                    If pick_plan <= 0 Then
                        rc = 4
                    Else

                        myCommand.CommandText = "SELECT COUNT(*) As Count FROM dbo.pick_workset WHERE Trim(pick_header_label_id) = Trim(:TicketId)"

                        dr = myCommand.ExecuteReader()

                        While dr.Read()
                            pp_no = dr(0)
                        End While

                        myCommand.CommandText = "SELECT COUNT(*) As Count FROM dbo.picking_container WHERE Trim(cont_label_id) = Trim(:TicketId)"

                        dr = myCommand.ExecuteReader()

                        While dr.Read()
                            p_check = dr(0)
                        End While

                        If pp_no <= 0 And p_check <= 0 Then
                            rc = 1
                        ElseIf pp_no > 0 Then
                            pp_no = 0

                            myCommand.CommandText = "SELECT Count(*) FROM dbo.pick_workset pw, dbo.ipic_to_codtl ipcd WHERE " _
                                                    & "Trim(pw.pick_header_label_id) = Trim(:TicketId) " _
                                                    & "AND Trim(pw.pick_plan_nu) = Trim(:PickPlanId) " _
                                                    & "AND pw.pick_plan_nu = ipcd.pick_plan_nu " _
                                                    & "AND pw.workset_seq_no = ipcd.workset_seq_no " _
                                                    & "AND pw.workset_status = 'A' " _
                                                    & "AND ipcd.picked_qty != ipcd.planned_qty " _
                                                    & "AND ipcd.ipic_to_codtl_status = 'A'"

                            myCommand.Parameters.Clear()
                            myCommand.Parameters.AddWithValue("TicketId", check_tote)
                            myCommand.Parameters.AddWithValue("PickPlanId", pick_plan)

                            dr = myCommand.ExecuteReader()

                            While dr.Read()
                                pp_no = dr(0)
                            End While

                            If pp_no <= 0 Then
                                rc = 2
                            Else
                                myCommand.CommandText = "UPDATE dbo.pick_workset set workset_status='O' " _
                                                        & "WHERE Trim(pick_header_label_id) = Trim(:TicketId) " _
                                                        & "AND Trim(pick_plan_nu) = Trim(:PickPlanId) " _
                                                        & "AND workset_status ='A'"

                                myCommand.Parameters.Clear()
                                myCommand.Parameters.AddWithValue("TicketId", check_tote)
                                myCommand.Parameters.AddWithValue("PickPlanId", pick_plan)

                                myCommand.ExecuteNonQuery()

                                myCommand.CommandText = "INSERT INTO dbo.tote_log VALUES(:TicketId, :PickPlanId, currentUser, SYSDATE)"

                                myCommand.Parameters.Clear()
                                myCommand.Parameters.AddWithValue("TicketId", check_tote)
                                myCommand.Parameters.AddWithValue("PickPlanId", pick_plan)

                                myCommand.ExecuteNonQuery()

                            End If

                        ElseIf p_check > 0 Then
                            pp_no = 0

                            myCommand.CommandText = "SELECT COUNT(*) FROM dbo.pick_workset " _
                                                    & "WHERE Trim(pick_plan_nu) = Trim(:PickPlanId) " _
                                                    & "AND workset_seq_no IN (SELECT workset_seq_no FROM " _
                                                    & "dbo.picking_container WHERE Trim(cont_label_id) = Trim(:TicketId) " _
                                                    & "AND Trim(pick_plan_nu) = Trim(:PickPlanId)) " _
                                                    & "AND workset_status='A' " _
                                                    & "AND workset_seq_no IN (SELECT workset_seq_no FROM " _
                                                    & "dbo.ipic_to_codtl WHERE Trim(pick_plan_nu) = Trim(:PickPlanId) " _
                                                    & "AND planned_qty != picked_qty " _
                                                    & "AND ipic_to_codtl_status = 'A')"

                            myCommand.Parameters.Clear()
                            myCommand.Parameters.AddWithValue("TicketId", check_tote)
                            myCommand.Parameters.AddWithValue("PickPlanId", pick_plan)


                            dr = myCommand.ExecuteReader()

                            While dr.Read()
                                pp_no = dr(0)
                            End While

                            If pp_no <= 0 Then
                                rc = 3
                            Else
                                myCommand.CommandText = "UPDATE dbo.pick_workset SET workset_status = 'O' " _
                                                        & "WHERE Trim(pick_plan_nu) = Trim(:PickPlanId) " _
                                                        & "AND workset_seq_no IN (SELECT workset_seq_no FROM " _
                                                        & "dbo.picking_container WHERE Trim(cont_label_id) = Trim(:TicketId) " _
                                                        & "AND Trim(pick_plan_nu) = Trim(:PickPlanId)) " _
                                                        & "AND workset_status ='A'"

                                myCommand.Parameters.Clear()
                                myCommand.Parameters.AddWithValue("TicketId", check_tote)
                                myCommand.Parameters.AddWithValue("PickPlanId", pick_plan)
                                myCommand.ExecuteNonQuery()

                                myCommand.CommandText = "INSERT INTO dbo.tote_log VALUES(:TicketId, :PickPlanId, currentUser, SYSDATE)"

                                myCommand.Parameters.Clear()
                                myCommand.Parameters.AddWithValue("TicketId", check_tote)
                                myCommand.Parameters.AddWithValue("PickPlanId", pick_plan)

                                myCommand.ExecuteNonQuery()

                            End If
                        End If
                    End If

                End If
                dr.Close()
                myConnection.Close()
                myCommand.Dispose()


            Catch exc As Exception
                If Not dr Is Nothing Then
                    dr.Close()
                End If
                If Not myConnection Is Nothing Then
                    If myConnection.State <> Data.ConnectionState.Closed Then
                        myConnection.Close()
                    End If
                End If
                If Not myCommand Is Nothing Then
                    myCommand.Dispose()
                End If
                lblMessage.Text = ConfigurationManager.AppSettings("genericErrMsg")
            End Try

            If rc = 0 Then
                lblMessage.Text = "The Tote Id: " & txtTicketId.Text & " has been successfully unlocked."
            ElseIf rc = 1 Then
                lblMessage.Text = "Could not find the Tote Id: " & txtTicketId.Text
            ElseIf rc = 2 Then
                lblMessage.Text = "No Active picks, workset might need to close-up, please contact RSG Host Support"
            ElseIf rc = 3 Then
                lblMessage.Text = "No Active picks, workset might need to close-up, please contact RSG Host Support"
            ElseIf rc = 4 Then
                lblMessage.Text = "There is no pick plan corresponding to this Tote Id: " & txtTicketId.Text
            ElseIf rc = 5 Then
                lblMessage.Text = "The Tote Id: " & txtTicketId.Text & " is invalid, please enter a valid Tote Id"
            End If

        End If


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Text = ""
        Dim lblHeading As Label = Master.FindControl("lblMainTitle")
        If Not lblHeading Is Nothing Then
            lblHeading.Text = "Unlock Carton"
        End If
    End Sub
End Class
