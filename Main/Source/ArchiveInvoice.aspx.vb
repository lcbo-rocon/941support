Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.IO.Directory
Imports System.Web.SessionState
Imports System.Data
Partial Class ArchiveInvoice
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Text = ""
        Dim lblHeading As Label = Master.FindControl("lblMainTitle")
        If Not lblHeading Is Nothing Then
            lblHeading.Text = "View Archived Invoices"
        End If

    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtCustNo.Text = ""
        txtYear.Text = ""
        GridView1.Visible = False
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim custStr As String = ""
        Dim yearStr As String = ""

        custStr = txtCustNo.Text
        yearStr = txtYear.Text

        Dim str1 As String = ""
        str1 = Left(custStr, 1)

        Dim dt As New DataTable

        dt.Columns.Add("Customer")
        dt.Columns.Add("Year")
        dt.Columns.Add("InvoiceNum")
        dt.Columns.Add("Invoice")
        dt.Columns.Add("Link")

        Dim drow As DataRow

        Dim filesStr As String()
        Dim fileStr As String = ""
        Dim monthsStr As String()
        Dim monthStr As String = ""

        Dim temp As String()
        Dim temp1 As String()
        Dim temp2 As String()
        Dim temp3 As String()

        Dim connect As String = ConfigurationManager.AppSettings("Invoice")

        temp = System.IO.Directory.GetDirectories(connect)
        If temp.Length <> 0 And System.IO.Directory.Exists(connect & str1 & "\") Then
            temp1 = System.IO.Directory.GetDirectories(connect & str1 & "\")
            If temp1.Length <> 0 And System.IO.Directory.Exists(connect & str1 & "\" & custStr & "\") Then
                temp2 = System.IO.Directory.GetDirectories(connect & str1 & "\" & custStr & "\")
                If temp2.Length <> 0 And System.IO.Directory.Exists(connect & str1 & "\" & custStr & "\" & yearStr & "\") Then
                    temp3 = System.IO.Directory.GetDirectories(connect & str1 & "\" & custStr & "\" & yearStr & "\")
                    If temp3.Length <> 0 Then
                        Dim myStr As String = connect & str1 & "\" & custStr & "\" & yearStr & "\"
                        monthsStr = System.IO.Directory.GetDirectories(myStr)
                        If monthsStr.Length = 0 Then
                            lblMessage.Text = "There are no Invoices archived for " & custStr & " for year " & yearStr & ""
                        Else
                            For Each monthStr In monthsStr
                                filesStr = System.IO.Directory.GetFiles(monthStr)
                                For Each fileStr In filesStr
                                    drow = dt.NewRow
                                    drow.Item("Customer") = txtCustNo.Text
                                    drow.Item("Year") = txtYear.Text
                                    drow.Item("Link") = fileStr

                                    Dim startInt As Integer = fileStr.LastIndexOf("\")
                                    startInt = startInt + 1
                                    drow.Item("Invoice") = fileStr.Substring(startInt)

                                    Dim invPos As Integer
                                    Dim inv As String = ""
                                    invPos = (fileStr.Substring(startInt)).IndexOf(".")
                                    invPos = invPos + 1
                                    inv = (fileStr.Substring(startInt)).Substring(invPos, 6)
                                    drow.Item("InvoiceNum") = inv

                                    dt.Rows.Add(drow)
                                Next
                            Next

                            'Dim invoiceLink As HyperLink
                            'Dim count As Integer = GridView1.Rows.Count
                            'Dim i As Integer
                            'For i = 0 To count - 1
                            '    invoiceLink = GridView1.Rows(i).FindControl("lblInvoice")
                            '    invoiceLink.NavigateUrl = ""
                            'Next i

                            GridView1.DataSource = dt

                            GridView1.DataBind()

                            lblMessage.Text = "There are " & GridView1.Rows.Count & " invoices that match your search criteria"
                        End If
                    Else
                        lblMessage.Text = "There are no Invoices archived for " & custStr & " for year " & yearStr & ""
                    End If
                Else
                    lblMessage.Text = "There are no Invoices archived for " & custStr & " for year " & yearStr & ""
                End If
            Else
                lblMessage.Text = "There are no Invoices archived for " & custStr & " for year " & yearStr & ""
            End If
        Else
            lblMessage.Text = "There are no Invoices archived for " & custStr & " for year " & yearStr & ""
        End If



    End Sub
End Class
