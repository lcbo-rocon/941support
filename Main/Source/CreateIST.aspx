<%@ Page Language="vb" Title="Create IST" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="CreateIST.aspx.vb" Inherits="_941Support.CreateIST" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
<asp:ValidationSummary ID="ValidationSummaryReport" runat="server" ShowMessageBox="true"  ShowSummary="false" />
    <table>
        <tr>
        <td colspan="6"></td>
        </tr>
       <%-- <tr>            
            <td colspan="6" align="left">
            <asp:Label ID="lblHeading" runat="server" CssClass="HeadCss" Text="Create IST"></asp:Label> 
            </td>            
        </tr>--%>
        <tr>
            <td colspan="2" style="width: 119px" ></td>
            <td class="rprompt" > 
                Order#</td>
            <td>
                <asp:TextBox ID="txtOrderNo" runat="server" CssClass="textbox" MaxLength="6" Width="73px"></asp:TextBox></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2" style="width: 119px" ></td>
            <td class="rprompt"  >
                Store#</td>
            <td colspan="3">
            <asp:TextBox ID="txtStoreNo" runat="server" CssClass="textbox" Width="51px"></asp:TextBox>
            <asp:Label ID="lblNote" runat="server" CssClass="LabelCss" Text="(Store Number e.g. 10)"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width: 119px" ></td>
            <td class="rprompt" >Order Required Date From:</td>
            <td>
                <asp:TextBox ID="txtOrderDateFrom" runat="server" Columns="10" CssClass="textbox"
                    MaxLength="10"></asp:TextBox>
                 <a id="a1" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtOrderDateFrom, 'a1', 'yyyy-MM-dd');return false;"
								href="#" name="a1"><img id="Img1" height="21" src="Images/calendar.gif" width="34" align="top" border="0"></a>
			 <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtOrderDateFrom" ErrorMessage="Invalid Order Required Date From"
           Operator="DataTypeCheck" Type="Date" Display="None"></asp:CompareValidator>
           <asp:RequiredFieldValidator ID="reqdFromDate" runat="server" ControlToValidate="txtOrderDateFrom"
           Display="None" ErrorMessage="Please enter a valid Order Required Date From"></asp:RequiredFieldValidator>
         
			</td>
			<td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2" style="width: 119px" ></td>
            <td class="rprompt" >
                Order Required Date To:</td>
            <td>
            <asp:TextBox ID="txtOrderDateTo" runat="server" Columns="10" CssClass="textbox"
                MaxLength="10"></asp:TextBox>
            <a id="a2" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtOrderDateTo, 'a2', 'yyyy-MM-dd');return false;"
								href="#" name="a2"><img id="Img2" height="21" src="Images/calendar.gif" width="34" align="top" border="0"></a>
            </td>
            <td colspan="2"></td>
        </tr>
        <tr>
           <td colspan="4"></td>
            <td colspan="2" align="left">
                <asp:Button ID="btnSearch" runat="server" CssClass="BtnCss" Text="Search" />
                <asp:Button ID="btnClear" runat="server" CssClass="BtnCss" Text="Reset" />&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                </td>
        </tr>
        <tr>
            <td align="center" colspan="6" >
                <asp:Label ID="lblMsg" runat="server" CssClass="LblCss" ></asp:Label></td>
        </tr>    
         <tr>
            <td align="center" colspan="6" >
                <asp:Panel ID="pnlSummary" runat="server" Visible="false">
                    <table>
                        
                        
                        <tr>
                            <td>
                                <div style="z-index: 10; overflow: auto; height: 100px">
                                    <asp:GridView ID="grdSummary" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                        BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px"
                                        CellPadding="3" CellSpacing="1" Font-Names="Arial" GridLines="None" Width="496px">
                                        <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                        <RowStyle BackColor="#DEDFDE" ForeColor="Black" Font-Size="Small" />
                                        <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                                        <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="Maroon" />   
                                        <Columns>
                                           <asp:TemplateField HeaderText="Order#" SortExpression="OrderNo">
                                                <ItemTemplate>
                                                   <asp:LinkButton ID="lblOrderNo" runat="server" Text='<%# Bind("OrderNo") %>' CommandArgument='<%# Bind("OrderNo") %>' CommandName="GetOrderDetails"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Store#" SortExpression="StoreNo" >
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStoreNo" runat="server" Text='<%# Bind("StoreNo") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Order Required Date" SortExpression="OrderRequiredDate" >
                                                 <ItemTemplate>
                                                    <asp:Label ID="lblOrderRequiredDate" runat="server" Text='<%# Bind("OrderRequiredDate") %>' ></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Units Picked" SortExpression="UnitsPicked" >
                                                 <ItemTemplate>
                                                    <asp:Label ID="lblUnitsPicked" runat="server" Text='<%# Bind("UnitsPicked") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                           <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkCreateASN" runat="server" Text="Create ASN" CommandArgument='<%# Eval("OrderNo") + "," + Eval("StoreNo") %>' CommandName="GetCreateASN"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                   
                                </div></td>                                                   
                         </tr>
                         <tr>
                            <td align="right" >  
                               <asp:Button ID="btnExportSummary" runat="server" CssClass="BtnCss" Text="Export Summary" Width="145px"  />
                            
                            </td>                          
                         </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        
        <tr>
                 <td align="center" colspan="6" >
                            <asp:Panel ID="pnlDetails" runat="server" Visible="false">
                                <table cellpadding="0" cellspacing="0" >
                                    <tr>
                                        <td align="center" >
                                            <asp:Label ID="lblDetailMsg" runat="server" CssClass="LblCss" ></asp:Label>
                                        </td>
                                        
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            <div style="z-index: 10; overflow: auto;  height: 250px">
                                                <asp:GridView ID="grdDetails" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                         Font-Names="Arial" Width="531px" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CellSpacing="1" GridLines="None" >
                                                    <RowStyle Font-Size="Small" BackColor="#DEDFDE" ForeColor="Black" />
                                                    <HeaderStyle BackColor="#CBD49E" ForeColor="Maroon" Font-Size="Medium" Font-Bold="True" />
                                                    <Columns>
                                                        <asp:BoundField DataField="OrderNo" HeaderText="Order#" SortExpression="OrderNo" />
                                                        <asp:BoundField DataField="SkuNo" HeaderText="Sku#" SortExpression="SkuNo" />
                                                        <asp:BoundField DataField="OrderlineNo" HeaderText="Orderline #" SortExpression="OrderLineNo" />
                                                        <asp:BoundField DataField="OrderedQty" HeaderText="Ordered Qty" SortExpression="OrderedQty" />
                                                        <asp:BoundField DataField="PickedQty" HeaderText="Picked Qty" SortExpression="PickedQty">
                                                        <ItemStyle Width="200px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                                                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                                                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>                                               
                                            </div>
                                         </td>
                                    </tr>
                                    <tr><td></td></tr>
                                    <tr>                                       
                                       <td align="right"><asp:Button ID="btnExportDetails" runat="server" CssClass="BtnCss" Text="Export Details"  Width="144px"  />
                                    
                                    </td>
                                    </tr>
                                </table>
                          </asp:Panel>
                </td>
        </tr>
  </table>
           
</asp:Content>