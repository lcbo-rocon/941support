Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Imports System.IO.Directory
Imports System.Data
Imports System.Security.Principal
Imports System.Threading
Imports RSG_ROC.DataLayer
Imports RSG_ROC.Exceptions
Imports RSG_ROC.OrderClasses
Imports RSG_ROC.LS_TRANSFERS
Imports System.Text.RegularExpressions
Partial Public Class LocTransferCreate
    Inherits System.Web.UI.Page

    Private Sub LocTransferCreate_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Dim ex As Exception = Server.GetLastError
        Session("ExceptionObj") = ex
        Response.Redirect("~\ErrorPage.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lblheading As Label = Master.FindControl("lblMainTitle")
        If Not lblheading Is Nothing Then
            lblheading.Text = "Create Licensee Location Transfer"
        End If

        'lnkCatchAll.Attributes.Add("onclick", "popuplist();return false;")
        lnkCatchAll.Attributes.Add("onclick", "return showStoreSearch('" & txtAddItemNo.ClientID & "','" & txtAddUnitPrice.ClientID & "');")
        lnkLookUp.Attributes.Add("onclick", "showCustomerList('" & txtNewCustNo.UniqueID & "' );return false;")

        If Not IsPostBack Then
            Dim transfer_id As String = Session("LocTransferSelectId")
            If transfer_id <> "" Then
                lblMsg.Text = "Details for Transfer ID: " & transfer_id
                Dim roc_le_select_transfer As ROC_LE_SELECT_TRANSFER = New ROC_LE_SELECT_TRANSFER(transfer_id)

                lblOrigCustNo.Text = roc_le_select_transfer.orig_customer_no
                lblOrigCustName.Text = roc_le_select_transfer.orig_cust_name
                lblOrigCustAddress.Text = roc_le_select_transfer.orig_cust_address
                lblOrigCustCity.Text = roc_le_select_transfer.orig_cust_city
                lblOrigCustProvince.Text = roc_le_select_transfer.orig_cust_prov
                lblOrigCustPostal.Text = roc_le_select_transfer.orig_cust_post_code
                lblOrigCustPhone.Text = roc_le_select_transfer.orig_cust_phone

                txtNewCustNo.Text = roc_le_select_transfer.new_customer_no
                txtNewCustName.Text = roc_le_select_transfer.new_cust_name
                txtNewCustAddress.Text = roc_le_select_transfer.new_cust_address
                txtNewCustCity.Text = roc_le_select_transfer.new_cust_city
                txtNewCustProvince.Text = roc_le_select_transfer.new_cust_prov
                txtNewCustPostal.Text = roc_le_select_transfer.new_cust_post_code

                txtNewCustTransfer.Text = IIf(roc_le_select_transfer.expected_transfer_dt.Year < 2000, "", roc_le_select_transfer.expected_transfer_dt.ToString("yyyy-MM-dd"))
                txtNewCustContact.Text = roc_le_select_transfer.le_select_contact_name
                txtNewCustEmail.Text = roc_le_select_transfer.le_select_corporate_email

                lblReturnNo.Text = roc_le_select_transfer.le_select_return_no
                lblTransferNo.Text = roc_le_select_transfer.le_select_order_no
                lblPreparedBy.Text = roc_le_select_transfer.prepared_by
                'lblCurrentStatus.Text = roc_le_select_transfer.status

                Dim le_select_order As ROC_LE_SELECT_ORDER = New ROC_LE_SELECT_ORDER(transfer_id)
                Session("LSItemList") = New DataView(le_select_order.tblOrderDetails)
                grdItemList.DataSource = Session("LSItemList")
                grdItemList.DataBind()

                If DALRoc.isTransferFinalized(transfer_id) Then
                    txtNewCustNo.Enabled = False
                    txtNewCustName.Enabled = False
                    txtNewCustAddress.Enabled = False
                    txtNewCustCity.Enabled = False
                    txtNewCustProvince.Enabled = False
                    txtNewCustPostal.Enabled = False
                    txtNewCustTransfer.Enabled = False
                    txtNewCustContact.Enabled = False
                    txtNewCustEmail.Enabled = False

                    grdItemList.Enabled = False
                    lnkCatchAll.Visible = False
                    btnFinalize.Enabled = False
                    btnFinalize2.Enabled = False
                    btnAddItem.Enabled = False

                    txtAddItemNo.Enabled = False
                    txtAddUnitPrice.Enabled = False
                    txtAddQty.Enabled = False
                    txtAddCatchAll.Enabled = False
                    btnSave.Enabled = False
                    btnSave2.Enabled = False
                    btnSetCustomer.Enabled = False
                    lnkLookUp.Visible = False
                    Me.Img1.Visible = False

                End If
            ElseIf transfer_id = "" Or transfer_id = 0 Then
                Response.Redirect("~\LocTransferSearch.aspx")
            End If
        Else
            ''If the page is a postback and there are items that are added
            'Dim catchall_item As Integer = 0
            'Dim item_cmp As Integer = 0
            'If catchall_item <> 0 Then
            '    catchall_item = Session("CatchAllItem")
            'End If
            'If txtAddItemNo.Text.Trim <> "" Then
            '    item_cmp = txtAddItemNo.Text
            'End If
            'If catchall_item <> 0 And item_cmp <> catchall_item Then
            '    txtAddItemNo.Text = catchall_item
            'ElseIf catchall_item = 0 And item_cmp = 0 Then
            '    txtAddItemNo.Text = ""
            'Else
            '    txtAddItemNo.Text = item_cmp
            'End If

            'Dim catchall_price As Decimal = 0
            'Dim price_cmp As Decimal = 0
            'If catchall_price <> 0 Then
            '    catchall_price = Session("CatchAllPrice")
            'End If
            'If txtAddUnitPrice.Text.Trim <> "" Then
            '    price_cmp = txtAddUnitPrice.Text
            'End If
            'If catchall_price <> 0 And price_cmp <> catchall_price And price_cmp <> 0 Then
            '    txtAddUnitPrice.Text = price_cmp
            'ElseIf catchall_price = 0 And price_cmp = 0 Then
            '    txtAddUnitPrice.Text = ""
            'Else
            '    txtAddUnitPrice.Text = catchall_price
            'End If

            'Session("CatchAllItem") = 0
            'Session("CatchAllPrice") = 0
        End If
    End Sub

    Private Sub btnAddItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddItem.Click
        Dim item_no As Integer = 0
        Dim catchall_desc As String = ""
        Dim qty As String = ""
        Dim unit_price As Decimal = 0
        Dim catchall_status As Boolean = False
        Dim ctr As Integer = 1
        lblMessage.Text = ""

        If txtAddItemNo.Text.Trim = "" Then
            item_no = 0
        Else
            item_no = txtAddItemNo.Text.Trim
        End If

        catchall_desc = txtAddCatchAll.Text.Trim

        If txtAddQty.Text.Trim = "" Then
            qty = 0
        Else
            qty = txtAddQty.Text.Trim
        End If

        If txtAddUnitPrice.Text.Trim = "" Then
            unit_price = 0
        Else
            unit_price = txtAddUnitPrice.Text.Trim
        End If

        'check if item is valid
        Try
            DALRoc.getItemInfo(txtAddItemNo.Text.Trim)
        Catch ex As Exception
            ctr = 0
            lblMessage.Text = "Item entered is not valid!"
            txtAddItemNo.Focus()
            txtAddItemNo.Attributes.Add("onfocusin", " select();")


        End Try

        If Not (Regex.IsMatch(txtAddItemNo.Text.Trim, "^[0-9]+$") Or Regex.IsMatch(qty.Trim, "^[0-9]+$")) Then
            lblMessage.Text = "The item number, quantity and the unit price should be numeric and are mandatory fields."
            ctr = 0
            'check if the sku is a catch all sku
        ElseIf DALRoc.isCatchAllSku(item_no) = True Then
            catchall_status = True
            If unit_price = 0 Or catchall_desc = "" Then
                lblMessage.Text = "The unit price and description should be provided for a catch all sku."
                ctr = 0
                'Else
                '    ctr = 1
            End If
            'Else
            '    ctr = 1
        End If

        If ctr = 1 Then
            If item_no <> 0 Then
                Dim transfer_id As String = Session("LocTransferSelectId")
                Dim roc_le_select_transfer As ROC_LE_SELECT_TRANSFER = New ROC_LE_SELECT_TRANSFER(transfer_id)

                Dim conn As OracleConnection = Nothing
                conn = New OracleConnection(DALRoc.getConnectionstring(ConfigurationManager.AppSettings("env")))
                Dim cmd As OracleCommand = New OracleCommand
                Dim trans As OracleTransaction = Nothing

                Try
                    conn.Open()
                    trans = conn.BeginTransaction
                    cmd.Connection = conn
                    cmd.Transaction = trans

                    roc_le_select_transfer.le_select_order.AddItem(item_no, qty, unit_price, catchall_status, catchall_desc, "LS-Licensee Transfer", cmd)

                    cmd.Dispose()
                    trans.Commit()
                    conn.Close()

                Catch ex As Exception
                    trans.Rollback()
                    cmd.Dispose()
                    conn.Close()
                    Throw ex
                End Try

                Dim le_select_order As ROC_LE_SELECT_ORDER = New ROC_LE_SELECT_ORDER(transfer_id)
                grdItemList.DataSource = le_select_order.tblOrderDetails
                grdItemList.DataBind()
                Session("CatchAllItem") = 0
                Session("CatchAllPrice") = 0

                txtAddItemNo.Text = ""
                txtAddUnitPrice.Text = ""
                txtAddCatchAll.Text = ""
                txtAddQty.Text = ""

                txtAddItemNo.Focus()
            End If
        End If
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("~\LocTransferSearchDetails.aspx")
    End Sub

    Private Sub grdItemList_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdItemList.DataBound
        If grdItemList.Rows.Count = 0 Then
            'In case there are no rows that match the search criteria
            'below code will populate the Route gridview so that only the header is visible
            Dim tblEmpty As New DataTable
            Dim drow As DataRow = Nothing

            tblEmpty.Columns.Add("cod_line")
            tblEmpty.Columns.Add("item")
            tblEmpty.Columns.Add("item_desc")
            tblEmpty.Columns.Add("catchall_desc")
            tblEmpty.Columns.Add("item_category")
            tblEmpty.Columns.Add("qty")
            tblEmpty.Columns.Add("price")
            tblEmpty.Columns.Add("extd_price")

            drow = tblEmpty.NewRow
            drow.Item("cod_line") = 1
            drow.Item("item") = 1
            drow.Item("item_desc") = 1
            drow.Item("catchall_desc") = 1
            drow.Item("item_category") = 1
            drow.Item("qty") = 1
            drow.Item("price") = 1
            drow.Item("extd_price") = 1

            tblEmpty.Rows.Add(drow)

            grdItemList.DataSource = tblEmpty
            grdItemList.DataBind()
            grdItemList.Rows(0).Visible = False
        End If
    End Sub

    Public Sub txtItemQty_Changed(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtbox As TextBox = CType(sender, TextBox)
        Dim lblLine As Label = Nothing
        Dim line_no As String = ""
        Dim grow As GridViewRow = txtbox.Parent.Parent
        Dim lblCheck As Label = Nothing
        Dim qty As String = ""

        qty = txtbox.Text
        lblLine = grow.FindControl("lblItemListLine")
        line_no = lblLine.Text

        Dim transfer_id As String = Session("LocTransferSelectId")
        Dim roc_le_select_transfer As ROC_LE_SELECT_TRANSFER = New ROC_LE_SELECT_TRANSFER(transfer_id)

        Dim conn As OracleConnection = Nothing
        conn = New OracleConnection(DALRoc.getConnectionstring(ConfigurationManager.AppSettings("env")))
        Dim cmd As OracleCommand = New OracleCommand
        Dim trans As OracleTransaction = Nothing

        Try
            conn.Open()
            trans = conn.BeginTransaction
            cmd.Connection = conn
            cmd.Transaction = trans

            roc_le_select_transfer.le_select_order.UpdateQty(line_no, qty)

            cmd.Dispose()
            trans.Commit()
            conn.Close()

            txtAddItemNo.Focus()

        Catch ex As Exception
            trans.Rollback()
            cmd.Dispose()
            conn.Close()
            Throw ex
        End Try

        Dim le_select_order As ROC_LE_SELECT_ORDER = New ROC_LE_SELECT_ORDER(transfer_id)
        grdItemList.DataSource = le_select_order.tblOrderDetails
        grdItemList.DataBind()
    End Sub

    Private Sub grdItemList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdItemList.RowDeleting
        Dim currIndex As Integer = e.RowIndex

        Dim transfer_id As String = Session("LocTransferSelectId")
        Dim roc_le_select_transfer As ROC_LE_SELECT_TRANSFER = New ROC_LE_SELECT_TRANSFER(transfer_id)
        Dim cod_line As String = ""
        cod_line = grdItemList.DataKeys(currIndex).Value

        Dim conn As OracleConnection = Nothing
        conn = New OracleConnection(DALRoc.getConnectionstring(ConfigurationManager.AppSettings("env")))
        Dim cmd As OracleCommand = New OracleCommand
        Dim trans As OracleTransaction = Nothing

        Try
            conn.Open()
            trans = conn.BeginTransaction
            cmd.Connection = conn
            cmd.Transaction = trans

            roc_le_select_transfer.le_select_order.UpdateQty(cod_line, 0)

            cmd.Dispose()
            trans.Commit()
            conn.Close()

            txtAddItemNo.Focus()

        Catch ex As Exception
            trans.Rollback()
            cmd.Dispose()
            conn.Close()
            Throw ex
        End Try

        Dim le_select_order As ROC_LE_SELECT_ORDER = New ROC_LE_SELECT_ORDER(transfer_id)
        grdItemList.DataSource = le_select_order.tblOrderDetails
        grdItemList.DataBind()
    End Sub

    Private Sub btnFinalize_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalize.Click
        If txtNewCustNo.Text.Trim = "" Or txtNewCustName.Text.Trim = "" Or txtNewCustAddress.Text.Trim = "" _
            Or txtNewCustCity.Text.Trim = "" Or txtNewCustProvince.Text.Trim = "" Or txtNewCustPostal.Text.Trim = "" _
            Or txtNewCustContact.Text.Trim = "" Or txtNewCustEmail.Text.Trim = "" Then

            lblMessage.Text = "Please provide all the information for the new customer before finalizing.<br/>" & _
            "(Customer#, Customer Name, Address, City, Province, Postal_Code, <br/> Expected Transfer Date, Contact, Corporate Email)"
        Else
            If Regex.IsMatch(txtNewCustNo.Text.Trim, "^[0-9]+$") And DALRoc.isValidCustomer(txtNewCustNo.Text.Trim) And IsDate(txtNewCustTransfer.Text.ToString) Then

                Dim transfer_id As String = Session("LocTransferSelectId")
                Dim roc_le_select_transfer As ROC_LE_SELECT_TRANSFER = New ROC_LE_SELECT_TRANSFER(transfer_id)
                Dim current_user As String = Session("CurrentUserId")

                roc_le_select_transfer.new_customer_no = txtNewCustNo.Text.Trim
                roc_le_select_transfer.new_cust_name = txtNewCustName.Text.Trim
                roc_le_select_transfer.new_cust_address = txtNewCustAddress.Text.Trim
                roc_le_select_transfer.new_cust_city = txtNewCustCity.Text.Trim
                roc_le_select_transfer.new_cust_prov = txtNewCustProvince.Text.Trim
                roc_le_select_transfer.new_cust_post_code = txtNewCustPostal.Text.Trim
                roc_le_select_transfer.expected_transfer_dt = txtNewCustTransfer.Text.Trim
                roc_le_select_transfer.le_select_contact_name = txtNewCustContact.Text.Trim
                roc_le_select_transfer.le_select_corporate_email = txtNewCustEmail.Text.Trim


                DALRoc.setEnv(ConfigurationManager.AppSettings("env"))
                Dim conn As OracleConnection = Nothing
                conn = New OracleConnection(DALRoc.getConnectionstring(ConfigurationManager.AppSettings("env")))
                Dim cmd As OracleCommand = New OracleCommand
                Dim trans As OracleTransaction = Nothing

                Try
                    conn.Open()
                    trans = conn.BeginTransaction
                    cmd.Connection = conn
                    cmd.Transaction = trans

                    roc_le_select_transfer.UPDATE(cmd)
                    roc_le_select_transfer.FinalizeOrder(current_user, cmd)

                    cmd.Dispose()
                    trans.Commit()
                    conn.Close()

                Catch ex As Exception
                    trans.Rollback()
                    cmd.Dispose()
                    conn.Close()
                    Throw ex
                End Try
                grdItemList.Enabled = False
                txtAddItemNo.Enabled = False
                txtAddCatchAll.Enabled = False
                txtAddQty.Enabled = False
                txtAddUnitPrice.Enabled = False

                btnAddItem.Enabled = False
                btnFinalize.Enabled = False
                lnkCatchAll.Visible = False

                lblReturnNo.CssClass = "WelcomeCss"
                lblTransferNo.CssClass = "WelcomeCss"

                lblReturnNo.Text = roc_le_select_transfer.le_select_return_no
                lblTransferNo.Text = roc_le_select_transfer.le_select_order_no

                lblMessage.Text = "The Inventory Transfer was successfully finalized, <br/>" & _
                                "Please check the new Return Order# and the Transfer Order#."

                txtNewCustNo.Enabled = False
                txtNewCustName.Enabled = False
                txtNewCustAddress.Enabled = False
                txtNewCustCity.Enabled = False
                txtNewCustProvince.Enabled = False
                txtNewCustPostal.Enabled = False
                txtNewCustTransfer.Enabled = False
                txtNewCustContact.Enabled = False
                txtNewCustEmail.Enabled = False

                grdItemList.Enabled = False
                lnkCatchAll.Visible = False
                btnFinalize.Enabled = False
                btnAddItem.Enabled = False

                txtAddItemNo.Enabled = False
                txtAddUnitPrice.Enabled = False
                txtAddQty.Enabled = False
                txtAddCatchAll.Enabled = False
                btnSetCustomer.Enabled = False
                lnkLookUp.Enabled = False
                btnSave.Enabled = False
            Else
                lblMessage.Text = "The new customer number should be a valid customer, <br/>" & _
                                   "and the expected transfer date should be a valid date."
            End If

        End If

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim bValid As Boolean = True

        Dim transfer_id As String = Session("LocTransferSelectId")
        Dim roc_le_select_transfer As ROC_LE_SELECT_TRANSFER = New ROC_LE_SELECT_TRANSFER(transfer_id)

        If (Regex.IsMatch(txtNewCustNo.Text.Trim, "^[0-9]+$") And DALRoc.isValidCustomer(txtNewCustNo.Text.Trim)) = False Then
            bValid = False
            lblMsg.Text = "Invalid New Customer Number entered!"
        Else
            roc_le_select_transfer.new_customer_no = txtNewCustNo.Text.Trim
        End If

        If bValid AndAlso txtNewCustTransfer.Text.Trim <> "" Then
            If IsDate(txtNewCustTransfer.Text.ToString) = False Then
                bValid = False
                lblMsg.Text = "Invalid Expected Transfer Date Entered. Please enter date in YYYY-MM-DD format!"
            Else
                roc_le_select_transfer.expected_transfer_dt = txtNewCustTransfer.Text.Trim
            End If
        Else
            roc_le_select_transfer.expected_transfer_dt = New DateTime
        End If

        'Need to escape the single quotes with two single quotes
        roc_le_select_transfer.new_cust_name = txtNewCustName.Text.Trim.Replace("'", "|").Replace("|", "''")
        roc_le_select_transfer.new_cust_address = txtNewCustAddress.Text.Trim.Replace("'", "|").Replace("|", "''")
        roc_le_select_transfer.new_cust_city = txtNewCustCity.Text.Trim.Replace("'", "|").Replace("|", "''")
        roc_le_select_transfer.new_cust_prov = txtNewCustProvince.Text.Trim.Replace("'", "|").Replace("|", "''")
        roc_le_select_transfer.new_cust_post_code = txtNewCustPostal.Text.Trim

        roc_le_select_transfer.le_select_contact_name = txtNewCustContact.Text.Trim.Replace("'", "|").Replace("|", "''")
        roc_le_select_transfer.le_select_corporate_email = txtNewCustEmail.Text.Trim.Replace("'", "|").Replace("|", "''")

        If bValid = True Then

            DALRoc.setEnv(ConfigurationManager.AppSettings("env"))
            Dim conn As OracleConnection = Nothing
            conn = New OracleConnection(DALRoc.getConnectionstring(ConfigurationManager.AppSettings("env")))
            Dim cmd As OracleCommand = New OracleCommand
            Dim trans As OracleTransaction = Nothing

            Try
                conn.Open()
                trans = conn.BeginTransaction
                cmd.Connection = conn
                cmd.Transaction = trans

                roc_le_select_transfer.UPDATE(cmd)

                cmd.Dispose()
                trans.Commit()
                conn.Close()

            Catch ex As Exception
                trans.Rollback()
                cmd.Dispose()
                conn.Close()
                Me.lblMsg.Text = "Error Saving Transfer information!"
            End Try
        End If

    End Sub

    Private Sub btnSetCustomer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSetCustomer.Click
        Dim valid_cust As Boolean = False
        Dim dr As DataRow = Nothing

        dr = DALRoc.getCustInfo(txtNewCustNo.Text.Trim)

        txtNewCustName.Text = dr("CU_NAME")
        txtNewCustAddress.Text = dr("ADD1_SH") & " " & dr("ADD2_SH")
        txtNewCustCity.Text = dr("CITY_SH")
        txtNewCustProvince.Text = dr("PROV_SH")
        txtNewCustPostal.Text = dr("POST_CODE")

    End Sub

    Protected Sub btnSave2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave2.Click
        btnSave_Click(sender, e)
    End Sub


    Protected Sub btnFinalize2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinalize2.Click
        btnFinalize_Click(sender, e)
    End Sub

    Protected Sub btnBack2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack2.Click
        btnBack_Click(sender, e)
    End Sub

    Private Sub grdItemList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdItemList.Sorting
        Dim dv As DataView = Session("LSItemList")
        Dim order As String = ""
        If Not ViewState("SSOrder") Is Nothing Then
            order = ViewState("SSOrder")
        End If

        If order.StartsWith(e.SortExpression) Then
            If order.ToString.EndsWith("ASC") Then
                order = "DESC"
            Else
                order = "ASC"
            End If
        Else
            order = "ASC"
        End If

        ViewState("SSOrder") = e.SortExpression + " " & order
        dv.Sort = ViewState("SSOrder")
        grdItemList.DataSource = dv
        grdItemList.DataBind()
    End Sub

End Class