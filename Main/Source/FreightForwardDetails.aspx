<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" Title="Freight Forwarding"  CodeBehind="FreightForwardDetails.aspx.vb" Inherits="_941Support.FreightForwardDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
<table cellspacing="0" cellpadding="0" align="center" border="0" >

<tr>
    <td colspan="4" align="center">
    <asp:Label ID="lblMessage" Text="" runat="server" CssClass="MsgCss"></asp:Label>
    </td>
</tr>

<tr>
    <td colspan="4">&nbsp;</td>
</tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblOldCustomer" runat="server" Text="Original Customer#:"  CssClass="HeadCss"></asp:Label>&nbsp;
    </td>
    <td>
    <asp:Label ID="lblCuNo" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
    </td>
    <td>
    &nbsp;
    </td>
</tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblNewCustomer" runat="server" Text="New Customer#:"  CssClass="HeadCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtNewCustomer" runat="server" Width="80px"></asp:TextBox>
    <asp:Label ID="lblStar" runat="server" Text="*" CssClass="LabelCss"></asp:Label>
    </td>
    <td>
    &nbsp;
    </td>
</tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td align="right" valign="top">
    <asp:Label ID="lblComment" runat="server" Text="Comments(Max 50 characters):"  CssClass="HeadCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" MaxLength="50" Width="179px"></asp:TextBox>
    <asp:Label ID="Label1" runat="server" Text="*" CssClass="LabelCss"></asp:Label>
    </td> 
     <td>
    &nbsp;
    </td>  
</tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblOrder" runat="server" Text="Order#:"  CssClass="HeadCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:Label ID="lblOrderNo" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
    </td>
    <td>
    &nbsp;
    </td>
</tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblInv" runat="server" Text="Invoice Date:"  CssClass="HeadCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:Label ID="lblInvDt" runat="server" Text="" CssClass="ValueCss"></asp:Label>&nbsp;
    </td>
    <td>
    &nbsp;
    </td>
</tr>

<tr>    
    <td align="left" colspan="4">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="lbl1" runat="server" Text="Delivery Tax:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblDelChrg" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;&nbsp;&nbsp;
    
    <asp:Label ID="lbl2" runat="server" Text="Delivery Charge:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblDelTax" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;&nbsp;&nbsp;
    
     <asp:Label ID="lbl3" runat="server" Text="Delivery Total:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblDelTotal" runat="server" Text=""  CssClass="ValueCss"></asp:Label>&nbsp;
    </td>    
</tr>

<tr><td colspan="4">&nbsp;</td></tr>

<tr>                                                                                                                        
    <td colspan="3" align="right" >                                                                                        
        <div style="z-index: 10; overflow: auto;height:230px;">                                                              
            <asp:GridView ID="grdFreightFwdDetail" runat="server" AutoGenerateColumns="False"                 
            BackColor="White" BorderColor="#CBD49E" BorderStyle="None" BorderWidth="1px"                                    
            CellPadding="4" Font-Names="Arial" GridLines="Vertical" ForeColor="Black"   >                                   
            <FooterStyle BackColor="#CCCC99" />                                                                             
            <RowStyle BackColor="#F7F7DE" Font-Size="Small" />                                                              
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />                                    
            <SelectedRowStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="White" BorderStyle="None" />                  
            <HeaderStyle BackColor="#CBD49E" Font-Size="Small" ForeColor="Maroon" Font-Bold="True" />                       
                <Columns>                                                                                                   
                    <asp:BoundField DataField="cod_line" HeaderText="Order Line" SortExpression="cod_line" />                      
                    <asp:BoundField DataField="item" HeaderText="Item" SortExpression="item" />              
                    <asp:BoundField DataField="price" HeaderText="Price" SortExpression="price" />                     
                    <asp:BoundField DataField="qty" HeaderText="Quantity" SortExpression="qty" />
                    <asp:BoundField DataField="deposit_price" HeaderText="Deposit" SortExpression="deposit_price" />           
                    <asp:BoundField DataField="extd_price" HeaderText="Extended Amount" SortExpression="extd_price" />     
                </Columns>                                                                                                  
                <AlternatingRowStyle BackColor="White" />                                                                   
            </asp:GridView>                                                                                                 
        </div>                                                                                                              
    </td>
    <td>&nbsp;</td>                                                                                                                   
</tr> 

<tr>
    <td >
    &nbsp;
    </td>
    <td align="right" colspan="2">
    <asp:Label ID="lblSTotal" runat="server" Text="Sub Total:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblSubTotal" runat="server" Text="" CssClass="ValueCss"></asp:Label>&nbsp;
    </td>  
    <td>
    &nbsp;
    </td> 
</tr>

<tr>
    <td >
    &nbsp;
    </td>
    <td align="right" colspan="2">
    <asp:Label ID="lblDisc" runat="server" Text="Discount:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblDiscount" runat="server" Text="" CssClass="ValueCss"></asp:Label>&nbsp;
    </td>  
    <td>
    &nbsp;
    </td> 
</tr>

<tr>
    <td >
    &nbsp;
    </td>
    <td align="right" colspan="2">
    <asp:Label ID="lblMUp" runat="server" Text="Markup:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblMarkup" runat="server" Text="" CssClass="ValueCss"></asp:Label>&nbsp;
    </td>  
    <td>
    &nbsp;
    </td> 
</tr>

<tr>
    <td >
    &nbsp;
    </td>
    <td align="right" colspan="2">
    <asp:Label ID="lblLvy" runat="server" Text="Levy:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblLevy" runat="server" Text="" CssClass="ValueCss"></asp:Label>&nbsp;
    </td>  
    <td>
    &nbsp;
    </td> 
</tr>

<tr>
    <td >
    &nbsp;
    </td>
    <td align="right" colspan="2">
    <asp:Label ID="lblH" runat="server" Text="HST:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblHST" runat="server" Text="" CssClass="ValueCss"></asp:Label>&nbsp;
    </td>  
    <td>
    &nbsp;
    </td> 
</tr>

<tr>
    <td >
    &nbsp;
    </td>
    <td align="right" colspan="2">
    <asp:Label ID="lblTot" runat="server" Text="Total:"  CssClass="HeadCss"></asp:Label>&nbsp;
    <asp:Label ID="lblTotal" runat="server" Text="" CssClass="ValueCss"></asp:Label>&nbsp;
    </td>  
    <td>
    &nbsp;
    </td> 
</tr>

<tr>
    <td>
    &nbsp;
    </td>
    <td align="right"  colspan="2">
    <hr  style="width:100px;"/>
    </td>
     <td>
    &nbsp;
    </td> 
</tr>

<tr>
    <td >
    &nbsp;
    </td>
    <td align="right" colspan="2">
    <asp:Label ID="lblTPD" runat="server" Text="Total + Delivery:"  CssClass="HeadCss"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="lblTotalPlusDel" runat="server" Text="" CssClass="ValueCss"></asp:Label>&nbsp;
    </td>  
    <td>
    &nbsp;
    </td> 
</tr>

<tr>
    <td >
    &nbsp;
    </td>
    <td align="right"  colspan="2">
    <hr style="width:100px;"/>
    </td>
     <td>
    &nbsp;
    </td> 
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
    <td colspan="2">&nbsp;</td>
    <td colspan="2" align="right">
    <asp:Button ID="btnForward" runat="server" CssClass="BtnCss" Text="Freight Forward" Width="130px" />
    <asp:Button ID="btnCancel" runat="server" CssClass="BtnCss" Text="Cancel" />
    &nbsp;
    </td>
</tr>

<tr>
    <td colspan="4">&nbsp;</td>
</tr>

</table>

</asp:Content>