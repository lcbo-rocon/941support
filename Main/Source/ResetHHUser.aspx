<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="ResetHHUser.aspx.vb" Inherits="_941Support.ResetHHUser" 
    title="Reset HH Users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">

    <table cellspacing="0" cellpadding="0" align="center" border="0" style="width: 400px; height: 70px;">
         
                  
          <tr>
            <td colspan="2" align="center" style="height: 19px">
            <asp:Label ID="lblHeading" runat="server" CssClass="HeadCss" Text="Current Users: "></asp:Label>       
            </td>
          </tr>
          
          <tr> 
            <td colspan="2" align="center" style="height: 19px">&nbsp;</td>
          </tr>
            
         <tr>
            <td align="center" valign="middle" colspan="2" >                 
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" AllowSorting="True">
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <Columns>
                        <asp:BoundField DataField="USER_ID" HeaderText="USER_ID" SortExpression="USER_ID" />
                        <asp:BoundField DataField="PROGRAM" HeaderText="PROGRAM" SortExpression="PROGRAM"
                            Visible="False" />
                        <asp:TemplateField>
                        <ItemTemplate>
                        <asp:CheckBox ID="chkResetUser" runat="server" Checked="False" />
                        </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#999999" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:QA %>"
                    ProviderName="<%$ ConnectionStrings:QA.ProviderName %>" SelectCommand="select lower(username) As User_Id, program As Program from sys.v_$session where lower(program) like '%handheld%' order by 1">
                </asp:SqlDataSource>
              
           
           </td>
          </tr>
           
          <tr> 
            <td colspan="2" align="center">&nbsp;</td>
          </tr> 
           
          <tr>
            <td align="right"> 
              <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="73px" CssClass="BtnCss" TabIndex="2" />
              <asp:Button ID="btnClear" runat="server" Text="Clear" Width="73px" CssClass="BtnCss" TabIndex="3" CausesValidation="False" />&nbsp;
            </td>
          </tr>  
          
          <tr> 
            <td colspan="2" align="center">&nbsp;</td>
          </tr>
          
          <tr>
            <td colspan="2" align="center">
            <asp:Label ID="lblMessage" runat="server" CssClass="LblCss"></asp:Label>       
            </td>
          </tr>  
     </table>
     
    
     
</asp:Content>
