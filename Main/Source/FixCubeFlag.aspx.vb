Imports System.Data.OracleClient
Imports System.Data
'----------------------------------------------------------------------------------------------
' This page displays the form for the user to enter the Item when cube_item_fl does match.
' And allows the user to synchronize the cube_item_fl between LOCA_PALLET value and 
' LOCATION_QUANT value.
'----------------------------------------------------------------------------------------------
' Created By: Shirley Lam, LCBO-RSG
' Date: June 2008
'----------------------------------------------------------------------------------------------
Partial Class FixCubeFlag
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lblHeading As Label = Master.FindControl("lblMainTitle")
        If Not lblHeading Is Nothing Then
            lblHeading.Text = "Fix Location Cube Item Flag"
        End If
        lblMessage.Text = ""
        dataMismatch.ConnectionString = ConfigurationManager.ConnectionStrings(ConfigurationManager.AppSettings("env")).ToString
        dataMismatch.DataBind()
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'validate the number entered
        Dim itemnum As Long = Nothing
        Dim qry As String = "SELECT count(1) From glob_item where to_number(item) = :item"
        Dim sqlcommand As OracleCommand = Nothing

        If Long.TryParse(txtItem.Text, itemnum) = False Then
            lblMessage.Text = "Invalid item number entered!"
        Else
            'validate if it is a valid sku
            sqlcommand = New OracleCommand(qry, New OracleConnection(ConfigurationManager.AppSettings( _
                        ConfigurationManager.AppSettings("env"))))
            With sqlcommand
                .Connection.Open()
                .Parameters.AddWithValue(":item", itemnum)
            End With
            Dim retval As Integer = sqlcommand.ExecuteScalar
            If retval > 0 Then
                txtItem.Text = itemnum.ToString
                grdLocationResults.DataBind()
                pnlLocationResults.Visible = True
            Else
                lblMessage.Text = txtItem.Text & " does not exist!"
                pnlLocationResults.Visible = False
            End If
        End If

    End Sub

    Protected Sub grdLocationResults_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdLocationResults.DataBound
        If grdLocationResults.Rows.Count > 0 Then
            lblLocationResults.Text = "Found " & grdLocationResults.Rows.Count & " Location(s) with mismatches!"
        Else
            lblLocationResults.Text = "There is no mismatch found for item " & txtItem.Text & "."
        End If
    End Sub

    Protected Sub grdLocationResults_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdLocationResults.RowCommand
        If e.CommandName = "UpdateLoca" Then
            Dim grow As GridViewRow = grdLocationResults.Rows(grdLocationResults.EditIndex)
            Dim lblLoca_pallet As Label = grow.FindControl("lblLoca_pallet")
            Dim lblQuant As Label = grow.FindControl("lblQuant")
            Dim lblTicket As Label = grow.FindControl("lblTicket")
            Dim drpLoca_pallet As DropDownList = grow.FindControl("drpLoca_pallet")
            Dim drpQuant As DropDownList = grow.FindControl("drpQuant")
            Dim lblLo_loca As Label = grow.FindControl("lblLo_Loca")
            Dim lblItem As Label = grow.FindControl("lblItem")

            Dim change As Int16 = 0
            Dim rowsaffected As Int16 = 0
            Dim qry As String = ""
            Dim sqlcommand As OracleCommand = Nothing
            Dim connstring As String = dataMismatch.ConnectionString
            Try
                If drpQuant.SelectedValue <> drpLoca_pallet.SelectedValue Then
                    lblLocationResults.Text = "Selected values for LOCA_PALLET and LOCATION_QUANT is not the same!"
                Else
                    'Check for change
                    If lblLoca_pallet.Text <> drpLoca_pallet.SelectedValue Then
                        change = 1
                        qry = "UPDATE LOCA_PALLET set CUBE_ITEM_FL = :flag " & _
                              " WHERE Trim(LO_LOCA) = :LO_LOCA " & _
                              " AND Trim(TICKET_ID) = :TICKET_ID " & _
                              " AND TO_NUMBER(ITEM) = :ITEM " & _
                              " AND Trim(TICKET_TYPE) <> 'DEL' " & _
                              " AND Trim(CUBE_ITEM_FL) = :oldflag "
                    End If
                    If lblQuant.Text <> drpQuant.SelectedValue Then
                        change = 2
                        qry = "UPDATE LOCATION_QUANT set CUBE_ITEM_FL = :flag " & _
                              " WHERE Trim(LO_LOCA) = :LO_LOCA " & _
                              " AND Trim(CUBE_ITEM_FL) = :oldflag "
                    End If

                    Select Case (change)
                        Case 1
                            sqlcommand = New OracleCommand(qry, New OracleConnection(connstring))
                            Try
                                With sqlcommand
                                    .Connection.Open()
                                    .Parameters.AddWithValue(":flag", drpLoca_pallet.SelectedValue)
                                    .Parameters.AddWithValue(":LO_LOCA", lblLo_loca.Text.Trim)
                                    .Parameters.AddWithValue(":TICKET_ID", lblTicket.Text.Trim)
                                    .Parameters.AddWithValue(":ITEM", lblItem.Text.Trim)
                                    .Parameters.AddWithValue(":oldflag", lblLoca_pallet.Text.Trim)
                                End With

                                rowsaffected = sqlcommand.ExecuteNonQuery
                                If rowsaffected > 0 Then
                                    lblMessage.Text = "Location " & lblLo_loca.Text & " updated successfully."
                                End If
                                sqlcommand.Connection.Close()
                                sqlcommand.Dispose()

                            Catch ex As Exception
                                lblMessage.Text = ConfigurationManager.AppSettings("genericErrMsg")
                                If sqlcommand.Connection.State <> ConnectionState.Closed Then
                                    sqlcommand.Connection.Close()
                                End If
                                sqlcommand.Dispose()
                            End Try

                        Case 2
                            sqlcommand = New OracleCommand(qry, New OracleConnection(connstring))
                            Try
                                With sqlcommand
                                    .Connection.Open()
                                    .Parameters.AddWithValue(":flag", drpQuant.SelectedValue)
                                    .Parameters.AddWithValue(":LO_LOCA", lblLo_loca.Text.Trim)
                                    .Parameters.AddWithValue(":oldflag", lblQuant.Text.Trim)
                                End With

                                rowsaffected = sqlcommand.ExecuteNonQuery
                                If rowsaffected > 0 Then
                                    lblMessage.Text = "Location " & lblLo_loca.Text & " updated successfully."

                                End If
                                sqlcommand.Connection.Close()
                                sqlcommand.Dispose()

                            Catch ex As Exception
                                lblMessage.Text = ConfigurationManager.AppSettings("genericErrMsg")
                                If sqlcommand.Connection.State <> ConnectionState.Closed Then
                                    sqlcommand.Connection.Close()
                                End If
                                sqlcommand.Dispose()
                            End Try
                        Case Else
                            lblLocationResults.Text = "There were no changes detected!"
                    End Select
                End If
            Catch ex As Exception
                lblMessage.Text = ConfigurationManager.AppSettings("genericErrMsg")
            End Try
            If rowsaffected > 0 Then
                sqlcommand = New OracleCommand("INSERT INTO KILL_LOG (osuser,username,program,timestamp) " & _
                                               " VALUES (:osuser,:username,:program,sysdate) ", _
                                               New OracleConnection(connstring))
                Try
                    Dim userid As String = User.Identity.Name
                    With sqlcommand
                        .Connection.Open()
                        .Parameters.AddWithValue(":osuser", userid.Substring(userid.IndexOf("\") + 1))
                        .Parameters.AddWithValue(":username", userid.Substring(userid.IndexOf("\") + 1))
                        .Parameters.AddWithValue(":program", Left("CF: " & lblLo_loca.Text.Trim, 20))
                    End With
                    sqlcommand.ExecuteNonQuery()
                    grdLocationResults.EditIndex = -1
                    grdLocationResults.DataBind()

                    sqlcommand.Connection.Close()
                    sqlcommand.Dispose()

                Catch ex As Exception
                    lblMessage.Text = ConfigurationManager.AppSettings("genericErrMsg")
                    If sqlcommand.Connection.State <> ConnectionState.Closed Then
                        sqlcommand.Connection.Close()
                    End If
                    sqlcommand.Dispose()
                End Try
            End If
        End If
    End Sub

    Protected Sub grdLocationResults_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdLocationResults.RowDataBound
        Dim grow As GridViewRow = e.Row
        Dim lblLoca_pallet As Label = grow.FindControl("lblLoca_pallet")
        Dim lblQuant As Label = grow.FindControl("lblQuant")
        Dim drpLoca_pallet As DropDownList = grow.FindControl("drpLoca_pallet")
        Dim drpQuant As DropDownList = grow.FindControl("drpQuant")

        If Not lblLoca_pallet Is Nothing Then
            If Not drpLoca_pallet Is Nothing Then
                drpLoca_pallet.Items.FindByText(lblLoca_pallet.Text).Selected = True
            End If
        End If

        If Not lblQuant Is Nothing Then
            If Not drpQuant Is Nothing Then
                drpQuant.Items.FindByText(lblQuant.Text).Selected = True
            End If
        End If

    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("FixCubeFlag.aspx")
    End Sub

End Class
