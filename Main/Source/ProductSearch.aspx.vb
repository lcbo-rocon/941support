Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Imports System.IO.Directory
Imports System.Data
Imports System.Security.Principal
Imports System.Threading
Partial Public Class ProductSearch
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            grdProduct.Visible = False
            grdRestriction.Visible = False
            grdQuantity.Visible = False
            lblMsg.Text = ""
            lblMessage.Text = ""
        End If
        Dim lblheading As Label = Master.FindControl("lblMainTitle")
        If Not lblheading Is Nothing Then
            lblheading.Text = "Product Search"
        End If
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim whereclause As String = ""
        Dim conn As OracleConnection = getOraConnection()
        Dim dataProd As New DataTable
        Dim dataRes As New DataTable
        Dim dataQty As New DataTable
        Dim product As String = Trim(txtProduct.Text)
        Dim adapter As OracleDataAdapter = Nothing

        adapter = New OracleDataAdapter("Select a.ITEM AS Item, a.IM_DESC as Description, a.CASE_SZ As Case_Size, b.ITEM_CATEGORY_DESC AS Category, " & _
        "CASE a.ACTIVE_FL WHEN 'Y' THEN 'Active' ELSE 'Inactive' END As Status, a.GRP_CODE As Groupage " & _
        "from DBO.IMMAS a, DBO.ON_ITEM_CATEGORY b where trim(a.item_category)=trim(b.item_category) and trim(a.item)='" & product & "'", conn)
        Try
            adapter.Fill(dataProd)
        Catch ex As Exception
            adapter.Dispose()
        End Try
        adapter.Dispose()
        grdProduct.DataSource = dataProd
        grdProduct.DataBind()


        adapter = New OracleDataAdapter("Select a.item as Item, b.order_type as Sold_To_Order_Type from " & _
        "DBO.IMMAS a, DBO.ON_ORDER_RESTR b where Trim(a.grp_code)=Trim(b.grp_code) and a.item='" & product & "'", conn)
        Try
            adapter.Fill(dataRes)
        Catch ex As Exception
            adapter.Dispose()
        End Try
        adapter.Dispose()

        grdRestriction.DataSource = dataRes
        grdRestriction.DataBind()

        adapter = New OracleDataAdapter("select a.co_odno as Order_No, a.co_Status as Status, a.cu_no as Customer, " & _
        "a.order_dt_tm as Order_Date_Time, a.order_reqd_dt as Order_reqd_Date,a.route_code as Route_Code, b.item as Item, b.cod_qty as Quantity  " & _
        "FROM dbo.ON_COHDR a, dbo.ON_CODTL b where trim(a.co_odno)=trim(b.co_odno) " & _
        "and a.co_status not in ('D','C') and upper(b.deleted_fl)='N'  and b.item='" & product & "'", conn)
        Try
            adapter.Fill(dataQty)
        Catch ex As Exception
            adapter.Dispose()
        End Try
        adapter.Dispose()

        grdQuantity.DataSource = dataQty
        grdQuantity.DataBind()
    End Sub

    Protected Function getOraConnection() As OracleConnection
        Dim conn As OracleConnection = Nothing
        Dim env As String = ConfigurationManager.AppSettings("env")
        conn = New OracleConnection(ConfigurationManager.ConnectionStrings(env).ToString)
        Return conn
    End Function

    Private Sub grdProduct_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdProduct.DataBound
        If grdProduct.Rows.Count > 0 Then
            For Each tcell As TableCell In grdProduct.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
            grdProduct.Visible = True
            grdRestriction.Visible = True
            grdQuantity.Visible = True
        Else
            lblMessage.Text = "There were no records found for your search criteria."
        End If
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("~/ProductSearch.aspx")
    End Sub

    Private Sub grdQuantity_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdQuantity.DataBound
        If grdQuantity.Rows.Count > 0 Then
            For Each tcell As TableCell In grdQuantity.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
        End If
    End Sub

    Private Sub grdQuantity_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdQuantity.RowCommand
        Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
        Dim order_no As String = ""
        Dim product As String = Trim(txtProduct.Text)
        Dim adapter As OracleDataAdapter = Nothing
        Dim dataQty As New DataTable
        Dim user_id As String = ""
        Dim msg As String = ""
        order_no = grdQuantity.DataKeys(row.RowIndex).Values(0).ToString.Trim

        Dim conn As OracleConnection = getOraConnection()
        Dim cmd As OracleCommand = Nothing
        Dim trans As OracleTransaction = Nothing
        user_id = Session("CurrentUserId")
        msg = "W Status:" & order_no

        Dim stmt1 As String = "Update dbo.on_cohdr set co_status='D' where co_odno='" & order_no & "'"
        Dim stmt2 As String = "Update dbo.on_codtl set deleted_fl='R' where co_odno='" & order_no & "'"
        Dim stmt3 As String = "Insert into dbo.kill_log(osuser,username,program,timestamp) " & _
                              "values('" & user_id.ToLower & "','" & user_id.ToLower & "','" & msg & "',sysdate) "

        Try
            conn.Open()
            trans = conn.BeginTransaction
            cmd = New OracleCommand(stmt1, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            cmd = New OracleCommand(stmt2, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            cmd = New OracleCommand(stmt3, conn, trans)
            cmd.ExecuteNonQuery()
            cmd.Dispose()

            trans.Commit()
            conn.Close()
        Catch ex As Exception
            trans.Rollback()
            cmd.Dispose()
            conn.Close()
        End Try

        lblMsg.Text = "Order#: " & order_no & " was successfully deleted."

        adapter = New OracleDataAdapter("select a.co_odno as Order_No, a.co_Status as Status, a.cu_no as Customer, " & _
         "a.order_dt_tm as Order_Date_Time, a.order_reqd_dt as Order_reqd_Date, a.route_code as Route_Code, b.item as Item, b.cod_qty as Quantity  " & _
         "FROM dbo.ON_COHDR a, dbo.ON_CODTL b where trim(a.co_odno)=trim(b.co_odno) " & _
         "and a.co_status not in ('D','C') and upper(b.deleted_fl)='N'  and b.item='" & product & "'", conn)
        Try
            Adapter.Fill(dataQty)
        Catch ex As Exception
            Adapter.Dispose()
        End Try
        Adapter.Dispose()

        grdQuantity.DataSource = dataQty
        grdQuantity.DataBind()
    End Sub
    Private Sub grdQuantity_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdQuantity.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnkDel As LinkButton = Nothing
            Dim lblStat As Label = Nothing

            lnkDel = e.Row.FindControl("lnkDelete")
            lblStat = e.Row.FindControl("lblStatus")

            If lblStat.Text.ToUpper <> "W" Then
                lnkDel.Visible = False
            Else
                lnkDel.Visible = True
            End If

        End If
    End Sub

    Private Sub grdRestriction_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdRestriction.DataBound
        If grdRestriction.Rows.Count > 0 Then
            For Each tcell As TableCell In grdRestriction.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
        End If
    End Sub
End Class