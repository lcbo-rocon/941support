Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Imports Microsoft.VisualBasic
Imports System.Security.Principal
Imports System.Threading
Imports System.DirectoryServices
Imports System.Data
Imports system.Xml

Partial Public Class UserList

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GridView1.DataSourceID = Nothing
            GridView1.DataSource = Nothing
            GridView1.DataBind()
            btnExport.Visible = False
            lblMsg.Text = ""

            Dim lblHeading As Label = Master.FindControl("lblMainTitle")
            If Not lblHeading Is Nothing Then
                lblHeading.Text = "User List"
            End If
        End If


    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=Users.xls")
        Response.Charset = "UTF-8"
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        ' Response.ContentType = "application/vnd.xls"
        Response.ContentType = "application/vnd.ms-excel"
        Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-1")

        EnableViewState = False
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New HtmlTextWriter(stringWrite)

        Dim newReport As New GridView
        ' newReport = dgReport
        'report format
        newReport.BorderStyle = GridView1.BorderStyle
        newReport.BorderWidth = GridView1.BorderWidth
        newReport.CellPadding = GridView1.CellPadding
        newReport.HeaderStyle.ForeColor = Drawing.Color.White
        newReport.HeaderStyle.BackColor = Drawing.Color.FromArgb(617231)

        Dim dv As DataView
        dv = New DataView(Session("UserListTable"))
        newReport.DataSource = dv
        newReport.DataBind()

        newReport.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString)
        Response.End()


    End Sub

    Private Sub btnExportROCON_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportROCON.Click
        Dim selectstr As String = ""
        Dim dt As DataTable = Nothing
        selectstr = "SELECT distinct a.group_id As Access_Group, a.caption As Menu_Header, b.caption As Menu_Detail from " & _
              "ON_MENU_ACCESS a, ON_MENU_ACCESS b WHERE a.group_id=b.group_id  " & _
              "and a.menu_id=b.parent_id and a.enable='Y' and b.enable='Y' and b.caption is not null  " & _
              "and trim(a.group_id) in (select distinct menu_access_grp_desktop from on_emp_mas) "
        dt = SPURDataControl.getData(selectstr)

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=Users.xls")
        Response.Charset = "UTF-8"
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        ' Response.ContentType = "application/vnd.xls"
        Response.ContentType = "application/vnd.ms-excel"
        Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-1")

        EnableViewState = False
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New HtmlTextWriter(stringWrite)

        Dim newReport As New GridView
        ' newReport = dgReport
        'report format
        newReport.HeaderStyle.ForeColor = Drawing.Color.White
        newReport.HeaderStyle.BackColor = Drawing.Color.FromArgb(617231)

        Dim dv As DataView
        dv = New DataView(dt)
        newReport.DataSource = dv
        newReport.DataBind()

        newReport.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString)
        Response.End()


    End Sub

    Private Sub btnExportROC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportROC.Click
        Dim selectstr As String = ""
        Dim dt As DataTable = Nothing
        selectstr = "SELECT distinct a.group_id As Access_Group, a.caption As Menu_Header, b.caption As Menu_Detail from " & _
              "MENU_ACCESS a, MENU_ACCESS b WHERE a.group_id=b.group_id  " & _
              "and a.menu_id=b.parent_id and a.enable='Y' and b.enable='Y' and b.caption is not null  " & _
              "and trim(a.group_id) in (Select  MENU_ACCESS_GRP_DESKTOP from EMMAS) "
        dt = SPURDataControl.getData(selectstr)

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=Users.xls")
        Response.Charset = "UTF-8"
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        ' Response.ContentType = "application/vnd.xls"
        Response.ContentType = "application/vnd.ms-excel"
        Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-1")

        EnableViewState = False
        Dim stringWrite As New System.IO.StringWriter
        Dim htmlWrite As New HtmlTextWriter(stringWrite)

        Dim newReport As New GridView
        ' newReport = dgReport
        'report format
        newReport.HeaderStyle.ForeColor = Drawing.Color.White
        newReport.HeaderStyle.BackColor = Drawing.Color.FromArgb(617231)

        Dim dv As DataView
        dv = New DataView(dt)
        newReport.DataSource = dv
        newReport.DataBind()

        newReport.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString)
        Response.End()

    End Sub


    Private Sub GridView1_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.DataBound
        lblMsg.Text = GridView1.Rows.Count & " row(s) match your search criteria."
        If GridView1.Rows.Count = 0 Then
            btnExport.Visible = False
        Else
            pnlSummary.Visible = True
            btnExport.Visible = True
            For Each tcell As TableCell In GridView1.HeaderRow.Cells
                tcell.CssClass = "locked"
            Next
        End If
    End Sub

   

    Private Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim groupid As String = ""
        Dim lnkGroupId As LinkButton = Nothing
        Dim grow As GridViewRow = GridView1.Rows(index)

        lnkGroupId = CType(grow.FindControl("lnkGroupId"), LinkButton)
        groupid = GridView1.DataKeys.Item(index).Values(0).ToString.Trim
        Session("UserListMenuGroup") = groupid
        'ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "str", "<script language='javascript'>alert('Inserted Emails Successfully');</script>", False)

        Dim strScript As String = "<script language='javascript'>"
        strScript = strScript & "window.open('UserAccess.aspx', '_blank','height=450, center:yes, width=400');"
        strScript = strScript & "</script>"
        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "strScript", strScript, False)
        'Response.Redirect("javascript:window.open('UserAccess.aspx','_blank','height=600,width=800);")

    End Sub

    'Private Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        Dim lnkGroupId As LinkButton = Nothing
    '        lnkGroupId = e.Row.FindControl("lnkGroupId")
    '        lnkGroupId.Attributes.Add("onclick", "popuplist();return false;")
    '    End If
    'End Sub

    Private Sub GridView1_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GridView1.Sorting
        Dim d As Integer = 0
        d = GridView1.Columns.Count
        GridViewSortExpression = e.SortExpression
        Dim pageIndex As Integer = GridView1.PageIndex

        GridView1.DataSource = SortingTable(Session("UserListTable"), False)
        GridView1.DataBind()
        GridView1.PageIndex = pageIndex
    End Sub
    Private Property GridViewSortDirection() As String
        Get
            If ViewState("SortDirection") Is Nothing Then
                ViewState("SortDirection") = SortDirection.Ascending
            End If
            Return ViewState("SortDirection")
        End Get
        Set(ByVal value As String)
            ViewState("SortDirection") = value
        End Set
    End Property

    Private Property GridViewSortExpression() As String
        Get
            If ViewState("SortExpression") Is Nothing Then
                ViewState("SortExpression") = ""
            End If
            Return ViewState("SortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("SortExpression") = value
        End Set
    End Property

    Private Function GetSortDirection() As String
        Select Case GridViewSortDirection
            Case "0"
                GridViewSortDirection = "1"
            Case "1"
                GridViewSortDirection = "0"
        End Select

        Return GridViewSortDirection
    End Function

    Protected Function SortingTable(ByVal dt As DataTable, ByVal isPageIndexChanging As Boolean) As DataView
        Dim dv As DataView
        Dim order As String = ""

        dv = New DataView(dt)
        If GridViewSortExpression <> "" Then
            If isPageIndexChanging Then
                If GridViewSortDirection = "0" Then
                    order = "ASC"
                Else
                    order = "DESC"
                End If
                dv.Sort = GridViewSortExpression & " " & order

            Else
                If GetSortDirection() = "0" Then
                    order = "ASC"
                Else
                    order = "DESC"
                End If
                dv.Sort = GridViewSortExpression & " " & order

            End If

        End If

        Return dv
    End Function

    Public Shared Function GetUserInfo(ByVal inSAM As String, ByVal inType As String) As String
        Dim sPath As String = "LDAP://lcbo.com/DC=lcbo,DC=com"
        Dim SamAccount As String = Right(inSAM, Len(inSAM) - InStr(inSAM, "\"))
        Dim myDirectory As New DirectoryEntry(sPath)
        Dim mySearcher As New DirectorySearcher(myDirectory)
        Dim mySearchResultColl As SearchResultCollection
        Dim mySearchResult As SearchResult
        Dim myResultPropColl As ResultPropertyCollection
        Dim myResultPropValueColl As ResultPropertyValueCollection
        Dim retval As String = ""
        Dim group As String = ""
        Dim groups As String()
        'Build LDAP query
        Try
            mySearcher.Filter = ("(&(objectClass=user)(samaccountname=" & SamAccount & "))")
            'mySearcher.PropertiesToLoad.Add("memberOf")
            mySearchResultColl = mySearcher.FindAll()
            For Each mySearchResult In mySearchResultColl
                mySearchResult = mySearchResultColl.Item(0)
                myResultPropColl = mySearchResult.Properties
                myResultPropValueColl = myResultPropColl.Item(inType)

                For Each group In myResultPropValueColl
                    groups = group.Split(",")
                    retval = retval & group
                Next

            Next

        Catch ex As Exception
            retval = ""
        End Try

        Return retval
    End Function

    Public Shared Function GetUserList(ByVal groupname As String) As SortedList
        Dim sPath As String = "LDAP://CN=" & groupname & ",OU=Distribution Groups,DC=lcbo,DC=com"
        Dim retval As String = ""
        Dim strUser As String = ""
        Dim aryUserList As New SortedList
        Try
            Dim myUserList As New DirectoryEntry(sPath)
            For Each strUser In myUserList.Properties("member")
                Dim userDirectory As New DirectoryEntry("LDAP://" & strUser)
                If Not userDirectory.Properties("sn").Value Is Nothing Then
                    aryUserList.Add(userDirectory.Properties("samaccountname").Value, _
                    userDirectory.Properties("givenName").Value & " " & userDirectory.Properties("sn").Value)
                End If
            Next
        Catch ex As Exception
            Throw New Exception("Error retrieving userlist for group: " & groupname & ":" & ex.Message.ToString)
        End Try

        Return aryUserList
    End Function

    Private Sub drpSystem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpSystem.SelectedIndexChanged
        Dim selectstr As String = ""
        Dim firstname As String = ""
        Dim lastname As String = ""

        Dim userupdatedt As New DataTable
        Dim updatedrow As DataRow
        Dim userviewdt As New DataTable
        Dim viewdrow As DataRow
        Dim dt As DataTable = Nothing

        Session("UserListType") = drpSystem.SelectedValue

        If drpSystem.SelectedValue = 0 Then
            GridView1.DataSourceID = Nothing
            GridView1.DataSource = Nothing
            GridView1.DataBind()

            'ROCON
        ElseIf drpSystem.SelectedValue = 1 Then
            selectstr = "select em_no AS Emp_Id, initcap(em_name) As Employee, MENU_ACCESS_GRP_DESKTOP As Access_Group " & _
                        "from ON_EMP_MAS where lower(em_no) not in " & _
                        "('archive','emp1','emp2','emp3','emp4','emp5','rnet1','rnet2','rnet3','rnet4','rnet5','rocnet','test1')" & _
                        "order by 2 "
            dt = SPURDataControl.getData(selectstr)
            GridView1.DataSource = dt
            GridView1.DataBind()

            'ROC           
        ElseIf drpSystem.SelectedValue = 2 Then
            selectstr = "Select em_no as Emp_Id, initcap(em_name) as Employee, MENU_ACCESS_GRP_DESKTOP As Access_Group   " & _
                        "from dbo.EMMAS where upper(initials)='Y' and lower(job_descr) not like 'admin' order by 2"
            dt = SPURDataControl.getData(selectstr)
            GridView1.DataSource = dt
            GridView1.DataBind()

        ElseIf (drpSystem.SelectedValue = 3) Or (drpSystem.SelectedValue = 4) Then
            Dim glEntryUsers As String = ""
            Dim usr As String = ""
            Dim userIds As String() = Nothing
            Dim userId As String = ""
            Dim usergrp As String = ""
            Dim tempFile As String = ""

            tempFile = Server.MapPath(".\Work") & "\webSCSGL.xml"

            Try
                Dim m_xmld As XmlDocument
                Dim m_nodelist As XmlNodeList
                Dim m_node As XmlNode

                userupdatedt.Columns.Add("Emp_Id")
                userupdatedt.Columns.Add("Employee")
                userupdatedt.Columns.Add("Access_Group")


                userviewdt.Columns.Add("Emp_Id")
                userviewdt.Columns.Add("Employee")
                userviewdt.Columns.Add("Access_Group")



                'Create the XML Document
                m_xmld = New XmlDocument()
                'Load the Xml file
                m_xmld.Load(tempFile)
                'Get the list of add nodes 
                m_nodelist = m_xmld.SelectNodes("/configuration/appSettings/add")
                'Loop through the nodes
                For Each m_node In m_nodelist
                    Dim keyAttribute = m_node.Attributes.GetNamedItem("key").Value

                    If keyAttribute = "GLEntryUsers" Then
                        Dim keyValue = m_node.Attributes.GetNamedItem("value").Value
                        userIds = keyValue.Split(",")
                        For Each usr In userIds
                            userId = usr.Substring(usr.IndexOf("\") + 1)
                            firstname = GetUserInfo(userId, "givenName")
                            lastname = GetUserInfo(userId, "sn")
                            updatedrow = userupdatedt.NewRow
                            updatedrow.Item(0) = userId.ToString.ToLower
                            updatedrow.Item(1) = firstname & " " & lastname
                            updatedrow.Item(2) = "SCSGL Update"
                            userupdatedt.Rows.Add(updatedrow.ItemArray)

                            'Update the view users also
                            viewdrow = userviewdt.NewRow
                            viewdrow.Item(0) = userId.ToString.ToLower
                            viewdrow.Item(1) = firstname & " " & lastname
                            viewdrow.Item(2) = "SCSGL View"
                            userviewdt.Rows.Add(viewdrow.ItemArray)
                        Next

                    ElseIf keyAttribute = "ViewOnlyUsers" Then
                        Dim keyValue = m_node.Attributes.GetNamedItem("value").Value
                        userIds = keyValue.Split(",")
                        For Each usr In userIds
                            userId = usr.Substring(usr.IndexOf("\") + 1)
                            firstname = GetUserInfo(userId, "givenName")
                            lastname = GetUserInfo(userId, "sn")
                            viewdrow = userviewdt.NewRow
                            viewdrow.Item(0) = userId.ToString.ToLower
                            viewdrow.Item(1) = firstname & " " & lastname
                            viewdrow.Item(2) = "SCSGL View"
                            userviewdt.Rows.Add(viewdrow.ItemArray)
                        Next

                    ElseIf keyAttribute = "ViewOnly" Then
                        Dim keyValue = m_node.Attributes.GetNamedItem("value").Value
                        userIds = keyValue.Split(",")
                        For Each usr In userIds
                            usergrp = usr.Substring(usr.IndexOf("\") + 1)

                            Dim lst As SortedList = GetUserList(usergrp)
                            Dim dUser As DictionaryEntry
                            For Each dUser In lst
                                viewdrow = userviewdt.NewRow
                                viewdrow.Item(0) = (dUser.Key).ToString.ToLower
                                viewdrow.Item(1) = dUser.Value
                                viewdrow.Item(2) = "SCSGL View"
                                userviewdt.Rows.Add(viewdrow.ItemArray)

                            Next
                        Next
                    End If
                Next

            Catch ex As Exception
                lblMsg.Text = ConfigurationManager.AppSettings("genericErrMsg")
            End Try
        End If


        If drpSystem.SelectedValue < 3 Then
            Session("UserListTable") = dt
        ElseIf drpSystem.SelectedValue = 3 Then
            GridView1.DataSource = userviewdt
            GridView1.DataBind()
            Session("UserListTable") = userviewdt
        ElseIf drpSystem.SelectedValue = 4 Then
            GridView1.DataSource = userupdatedt
            GridView1.DataBind()
            Session("UserListTable") = userupdatedt
        End If
    End Sub
End Class