Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Net
Imports System.Data.OracleClient
Imports System.IO


Public Class SPURDataControl

    Shared Function getSQLConnection() As SqlConnection
        Dim conn As SqlConnection = Nothing
        conn = New SqlConnection(ConfigurationManager.ConnectionStrings("SPUR").ToString)
        Return conn
    End Function
    Shared Function getOraConnection() As OracleConnection
        Dim conn As OracleConnection = Nothing
        Dim env As String = ConfigurationManager.AppSettings("env")
        conn = New OracleConnection(ConfigurationManager.ConnectionStrings(env).ToString)
        Return conn
    End Function
    Shared Function getData(ByVal sql As String) As DataTable
        Dim dt As New DataTable
        Dim adp As New OracleDataAdapter(sql, getOraConnection)
        Try
            adp.Fill(dt)
            adp.Dispose()
        Catch ex As Exception
            adp.Dispose()
        End Try

        Return dt
    End Function
    Shared Function changeROCONPassword(ByVal currentUser As String, ByVal userid As String, ByVal newPassword As String) As Boolean
        Dim conn As OracleConnection = getOraConnection()
        Dim cmd As New OracleCommand
        Dim rowsaffected As Integer = 0

        cmd.Connection = conn
        cmd.CommandText = "Alter User " & userid & " identified by " & newPassword

        Try
            cmd.Connection.Open()
            rowsaffected = cmd.ExecuteNonQuery
            cmd.Connection.Close()
            cmd.Dispose()
            If WriteLog(currentUser, userid, "Changed Password") = True Then
                rowsaffected = 1
            End If
        Catch ex As Exception
            If cmd.Connection.State <> ConnectionState.Closed Then
                cmd.Connection.Close()
            End If
            cmd.Dispose()
        End Try

        Return rowsaffected > 0
    End Function
    Shared Function WriteLog(ByVal ouser As String, ByVal username As String, ByVal program As String) As Boolean
        Dim conn As OracleConnection = getOraConnection()
        Dim cmd As New OracleCommand
        Dim rowsaffected As Integer = 0

        cmd.Connection = conn
        cmd.CommandText = "INSERT INTO KILL_LOG (OSUSER,USERNAME,PROGRAM,TIMESTAMP) " & _
                    " VALUES (:OUSER, :USERNAME, :PROGRAM, SYSDATE) "

        Try
            cmd.Connection.Open()
            cmd.Parameters.AddWithValue(":OUSER", ouser)
            cmd.Parameters.AddWithValue(":USERNAME", username)
            cmd.Parameters.AddWithValue(":PROGRAM", program)

            rowsaffected = cmd.ExecuteNonQuery
            cmd.Connection.Close()
            cmd.Dispose()

        Catch ex As Exception
            If cmd.Connection.State <> ConnectionState.Closed Then
                cmd.Connection.Close()
            End If
            cmd.Dispose()
        End Try

        Return rowsaffected > 0
    End Function
    Shared Function getSPURInfo(ByVal where As String, ByVal sessionid As String) As DataTable
        Dim dt As New DataTable

        Dim adp As New SqlDataAdapter("dbo.CreateSPURSummary", getSQLConnection)
        Try
            adp.SelectCommand.Parameters.AddWithValue("@where", where)
            adp.SelectCommand.Parameters.AddWithValue("@tempTableName", sessionid)
            adp.SelectCommand.CommandType = CommandType.StoredProcedure

            adp.Fill(dt)
            adp.Dispose()
        Catch ex As Exception
            adp.Dispose()
        End Try

        Return dt
        Return dt
    End Function
    Shared Function getCartonList() As String
        Dim str As String = ""
        Dim dt As New DataTable
        Dim ary As New ArrayList
        Dim qry As String = "SELECT RIGHT('00000000' + LTRIM(RTRIM(CARTON)),8) as CARTON FROM v_spur "
        Dim adp As New SqlDataAdapter(qry, getSQLConnection)
        Try
            adp.Fill(dt)
            adp.Dispose()

            For Each drow As DataRow In dt.Rows
                ary.Add("'" & drow("CARTON") & "'")
            Next
            str = Join(ary.ToArray, ",")

        Catch ex As Exception
            adp.Dispose()
        End Try
        Return str
    End Function

    Shared Function getSPURDetails(ByVal pick_plan As String, _
                                ByVal lane_no As String, _
                                ByVal sessionid As String) As DataTable
        Dim dt As New DataTable
        Dim adp As New SqlDataAdapter("dbo.CreateSPURDetails", getSQLConnection)
        adp.SelectCommand.CommandTimeout = 0

        Try
            adp.SelectCommand.Parameters.AddWithValue("@pick_plan", pick_plan)
            adp.SelectCommand.Parameters.AddWithValue("@lane_no", lane_no)
            adp.SelectCommand.Parameters.AddWithValue("@tempTableName", sessionid)
            adp.SelectCommand.CommandType = CommandType.StoredProcedure

            adp.Fill(dt)
            adp.Dispose()
        Catch ex As Exception
            adp.Dispose()
        End Try

        Return dt
    End Function
    Public Shared Function getOrderProductRestrictionItems(ByVal sOrderNumber As String, ByVal sOrderType As String, ByVal sOrderStatus As String) As DataTable
        Dim dt As New DataTable
        Dim sql As String = "SELECT T1.ITEM, T2.GRP_CODE FROM DBO.ON_INV_CODTL T1 " & vbNewLine & _
                            " INNER JOIN DBO.IMMAS T2 ON T1.ITEM = T2.ITEM " & vbNewLine & _
                            " WHERE CO_ODNO = :ORDERNUM " & vbNewLine & _
                            " AND T1.CO_INV_TYPE = :ORDERSTATUS " & vbNewLine & _
                            " AND NOT EXISTS (SELECT T3.GRP_CODE FROM DBO.ON_ORDER_RESTR T3 " & vbNewLine & _
                            " WHERE T3.ORDER_TYPE = :ORDER_TYPE AND T2.GRP_CODE = T3.GRP_CODE)  "

        Dim adp As New OracleDataAdapter(sql, getOraConnection)
        Try
            adp.SelectCommand.Parameters.Clear()
            adp.SelectCommand.Parameters.AddWithValue(":ORDERNUM", Left(sOrderNumber & "                ", 16))
            adp.SelectCommand.Parameters.AddWithValue(":ORDERSTATUS", sOrderStatus)
            adp.SelectCommand.Parameters.AddWithValue(":ORDER_TYPE", sOrderType)

            adp.Fill(dt)

            adp.Dispose()
        Catch ex As Exception
            adp.Dispose()
        End Try

        Return dt
    End Function
    Public Shared Function validateOrderProductRestriction(ByVal sOrderNumber As String, ByVal sOrderType As String, ByVal sOrderStatus As String) As Boolean
        Dim bValid As Boolean = False
        Dim dt As DataTable = Nothing

        dt = getOrderProductRestrictionItems(sOrderNumber, sOrderType, sOrderStatus)

        bValid = (dt.Rows.Count = 0)

        Return bValid

    End Function
    ''' <summary>
    ''' We first get a list of Payments against the order.
    ''' Then if order is an AGY we need to ensure that there is no credit/debit payment
    ''' 
    ''' </summary>
    ''' <param name="sInvNo"></param>
    ''' <param name="sOrderType"></param>
    ''' <param name="sCustomerNumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getTenderRestriction(ByVal sOrderNumber As String, ByVal sOrderType As String, ByVal sCustomerNumber As String) As DataTable
        Dim dt As New DataTable
        Dim sValidTenders As String = "0,6,15"
        Dim sql As String = "select T2.CO_ODNO AS ORDERNUM " & vbNewLine & _
                            ",T1.INV_NO                    " & vbNewLine & _
                            ",T1.REF_SEQ                   " & vbNewLine & _
                            ",T1.TIMESTAMP                 " & vbNewLine & _
                            ",T1.EMP_NO                    " & vbNewLine & _
                            ",T1.TENDER_CD                 " & vbNewLine & _
                            ",T1.TENDER_TYPE               " & vbNewLine & _
                            ",T1.PAY_AMT                   " & vbNewLine & _
                            ",T1.CREDIT_AUTH               " & vbNewLine & _
                            ",T1.PAY_COMMENT               " & vbNewLine & _
                            ",T1.PAY_DT                    " & vbNewLine & _
                            " from on_inv_pay T1 INNER JOIN ON_INV_CO T2 " & vbNewLine & _
                            " ON T1.INV_NO = T2.INV_NO " & vbNewLine & _
                            " WHERE CO_INV_TYPE = 'E' " & vbNewLine & _
                            " AND T2.CO_ODNO = :CO_ODNO " & vbNewLine & _
                            " AND TENDER_CD NOT IN (" & sValidTenders & ") "

        Dim adp As New OracleDataAdapter(sql, getOraConnection)
        Try
            adp.SelectCommand.Parameters.Clear()
            adp.SelectCommand.Parameters.AddWithValue(":CO_ODNO", Left(sOrderNumber & "                ", 16))

            adp.Fill(dt)

            adp.Dispose()
        Catch ex As Exception
            adp.Dispose()
        End Try

        Return dt
    End Function

    Public Shared Function validateTenderRestriction(ByVal sInvNo As String, ByVal sOrderType As String, ByVal sCustomerNumber As String) As Boolean
        Dim bValid As Boolean = False
        Dim dt As DataTable = Nothing

        dt = getTenderRestriction(sInvNo, sOrderType, sCustomerNumber)

        bValid = (dt.Rows.Count = 0)

        Return bValid

    End Function
    Public Shared Function makeCell(ByVal data As String, ByVal font As com.lowagie.text.Font, ByVal border As com.lowagie.text.Rectangle, _
                   Optional ByVal setHeader As Boolean = False, _
                   Optional ByVal colspan As Integer = 1, _
                   Optional ByVal horizonalalign As Integer = com.lowagie.text.Element.ALIGN_CENTER) As com.lowagie.text.Cell
        Dim cell As New com.lowagie.text.Cell(New com.lowagie.text.Phrase(data, font))
        cell.cloneNonPositionParameters(border)
        cell.setColspan(colspan)
        cell.setHeader(setHeader)
        cell.setHorizontalAlignment(horizonalalign)
        cell.setVerticalAlignment(com.lowagie.text.Element.ALIGN_MIDDLE)
        Return cell
    End Function

    Public Shared Sub ExportPDF(ByVal myreport As String, ByVal reportTitle As String, ByVal page As Page)
        'need to open the file again
        Dim tempFile As FileStream = New FileStream(myreport, FileMode.Open)
        Dim myBuffer(tempFile.Length) As Byte
        tempFile.Read(myBuffer, 0, tempFile.Length)
        tempFile.Close()
        'delete temporary file
        File.Delete(myreport)

        page.Response.Clear()
        page.Response.AddHeader("content-disposition", "attachment;filename=" & reportTitle & ".pdf")
        page.Response.ContentType = "application/pdf"
        page.Response.BinaryWrite(myBuffer)
        page.Response.End()
    End Sub

    Public Shared Function calcRetailPrice(ByVal basic As Decimal, ByVal deposit As Decimal, ByVal item_category As String) As Decimal
        Dim dRetailPrice As Decimal = 0
        Dim dBasic_less_dep As Decimal = basic - deposit
        Dim dHst_amount As Decimal = Math.Round(dbasic_less_dep * 0.13, 2, MidpointRounding.AwayFromZero)
        Dim iRounding As Decimal = 0

        dRetailPrice = Math.Round(basic + dHst_amount, 2, MidpointRounding.AwayFromZero)

        'need to check if rounding is necessary 
        Dim sRetailPrice As String = Convert.ToString(dRetailPrice)
        Dim iDigit As Integer = Right(sRetailPrice, 1) Mod 5

        If iDigit >= 3 Then 'round up
            iRounding = (5 - iDigit) * 0.01
        Else
            iRounding = iDigit * -0.01
        End If

        dRetailPrice = Math.Round(dRetailPrice + iRounding, 2, MidpointRounding.AwayFromZero)

        Return dRetailPrice

    End Function

End Class
