<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="ArchiveInvoice.aspx.vb" Inherits="_941Support.ArchiveInvoice" 
    title="View Archive Invoice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
<table cellspacing="0" cellpadding="6" align="center" bordercolor="#cbd39f" border="1" style="width: 180px; height: 100px;">
    <tr>
        <td valign="top">
            <table cellspacing="0" cellpadding="0" align="center" border="0" style="width: 400px; height: 70px;">
            <tr>
                <td align="right" valign="top" colspan="1">
                <asp:Label ID="lblReader" runat="server" Text="Please enter the Customer Number:"
                        Width="239px" ForeColor="Maroon"></asp:Label>
                </td>
                
                <td align="left" valign="top" colspan="2">
                <asp:TextBox ID="txtCustNo" runat="server" Width="212px" Height="20px" TabIndex="1" MaxLength="8"></asp:TextBox>
                </td>
                
               
            </tr>
    
            <tr>
                <td align="right" valign="top" colspan="1">
                <asp:Label ID="lblYear" runat="server" Text="Year(YYYY):"
                        Width="239px" ForeColor="Maroon"></asp:Label>
    
                </td>
                
                <td align="left" valign="top" colspan="2" >
                <asp:TextBox ID="txtYear" runat="server" Width="44px" Height="20px" TabIndex="1" MaxLength="4"></asp:TextBox>
                
                </td>
                
                
                
             </tr> 
             
             <tr>
                <td colspan="3" align="right">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="73px" CssClass="BtnCss" TabIndex="2" />
                <asp:Button ID="btnClear" runat="server" Text="Clear" Width="73px" CssClass="BtnCss" TabIndex="3" CausesValidation="False" />&nbsp;
                </td>
               
                
             </tr>   
             </table>
        </td>
   </tr>
   </table>
    <br />
   <table cellspacing="0" cellpadding="0" align="center" border="0" style="width: 646px; height: 70px;">
   <tr>
   <td align="center" >
   <asp:Label ID="lblMessage" runat="server" CssClass="LblCss" Width="643px"></asp:Label>
   </td>
   </tr>
   
   <tr>
   <td align="center" >
    <div style="width:500px;height:300px;overflow:auto">
       <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Width="380px" EnableViewState="False">
           <Columns>
               <asp:TemplateField HeaderText="Customer">
               <ItemTemplate>
                        <asp:Label ID="lblCust" runat="server" Text='<%# Eval("Customer") %>'></asp:Label>
               </ItemTemplate>
                   <ControlStyle Width="100px" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Year">
               <ItemTemplate>
                        <asp:Label ID="lblYear" runat="server" Text='<%# Eval("Year") %>'></asp:Label>
               </ItemTemplate>
                   <ControlStyle Width="50px" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Invoice">
               <ItemTemplate>
                        <asp:Label ID="lblCust" runat="server" Text='<%# Eval("InvoiceNum") %>'></asp:Label>
               </ItemTemplate>
                   <ControlStyle Width="100px" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Invoice Link">
               <ItemTemplate>
                        <asp:HyperLink ID="lblInvoice" NavigateUrl='<%# Eval("Link") %>'  runat="server" Text='<%# Eval("Invoice") %>'></asp:HyperLink>
               </ItemTemplate>
                   <ControlStyle Width="150px" />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Link" Visible="False">
               <ItemTemplate>
                        <asp:Label ID="lblLink" runat="server" Text='<%# Eval("Link") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
           </Columns>
           <FooterStyle BackColor="White" ForeColor="#000066" />
           <RowStyle ForeColor="#000066" />
           <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
           <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
           <HeaderStyle BackColor="#617231" Font-Bold="True" ForeColor="White" CssClass="FixedHeader" />
       </asp:GridView>
   </div>
   </td>
   </tr>
   </table>  
</asp:Content>
