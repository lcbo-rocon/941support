'Class RebootConveyor
'----------------------------------------------------------------------------------------------
' This page displays the interface for user to reboot conveyor server
'----------------------------------------------------------------------------------------------
' Created By: Ria Bhatnagar, LCBO-RSG
' Date: Jan, 2011
'----------------------------------------------------------------------------------------------

Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Partial Public Class RebootConveyor
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Text = ""
        Dim lblHeading As Label = Master.FindControl("lblMainTitle")
        If Not lblHeading Is Nothing Then
            lblHeading.Text = "Reboot Conveyor Server"
        End If
    End Sub

    Private Sub btnRestart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRestart.Click
        Dim currentUser As String = User.Identity.Name.Substring(User.Identity.Name.IndexOf("\") + 1)
        Dim env As String = System.Configuration.ConfigurationManager.AppSettings("env")

        Dim fp As StreamWriter
        'Create a file in the \Data folder with the format Conveyor_userid.rs
        'This file is checked by the perl script at whm001 every 2 mins and if this file exists the
        'conveyor server is restarted.
        Try
            fp = File.CreateText(Server.MapPath(".\Data\Conveyor_" & currentUser.ToLower & ".rs"))
            fp.Close()
            Dim minutes As String = ConfigurationManager.AppSettings("JobTime")
            lblMessage.Text = "Conveyor server rebooting, please try in 5 minute(s)."
        Catch err As Exception
            lblMessage.Text = ConfigurationManager.AppSettings("GenericErrMsg")
        End Try
    End Sub
End Class