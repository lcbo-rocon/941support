'----------------------------------------------------------------------------------------------------
' Created by Shirley Lam
' January 06, 2009
' Tender Restriction Override - allows users to override the customer's tender restriction for the day
'  A log is created for any updates done on this page.
'-----------------------------------------------------------------------------------------------------
Imports System.Security.Principal
Imports System.Data.OracleClient
Imports System.Data
Imports System.IO
Imports System.Threading

Partial Class TenderRestrictionOverride
    Inherits System.Web.UI.Page

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim env As String = ConfigurationManager.AppSettings("env")
        Dim cu_no As String = txtCustomerNumber.Text.Trim
        If cu_no = "" Then
            lblMessage.Text = "Customer Number is required!!!"
        Else
            Dim qry As String = "SELECT t1.CU_NO as CU_NO, " & _
                                " t1.CU_NAME as CU_NAME, " & _
                                " t1.Active_fl as ACTIVE_FL, " & _
                                " t2.ATTR_VALUE, " & _
                                " nvl((SELECT t3.ATTR_VALUE FROM dbo.ON_CUST_ATTR t3 " & _
                                "  WHERE t3.cu_no = t1.cu_no AND t3.ATTR_ID='CU_ATTR5'),'') as ACTIVITY " & _
                                " FROM DBO.ON_CUST_MAS t1 INNER JOIN " & _
                                " DBO.ON_CUST_ATTR t2 ON t1.cu_no = t2.cu_no " & _
                                " WHERE t2.ATTR_ID = 'CU_ATTR6' " & _
                                " AND trim(t1.CU_NO) = :CU_NO"

            Dim dt As New DataTable
            Dim sqladp As New OracleDataAdapter(qry, ConfigurationManager.ConnectionStrings(env).ToString)

            Try
                sqladp.SelectCommand.Parameters.AddWithValue(":CU_NO", cu_no)
                sqladp.Fill(dt)
                sqladp.Dispose()

                If dt.Rows.Count > 0 Then

                    lblCustomerNumber.Text = dt.Rows(0).Item("CU_NO")
                    lblCustomerName.Text = dt.Rows(0).Item("CU_NAME")
                    Dim lst As ListItem = Nothing
                    lst = Me.drpTenderRestrict.Items.FindByValue(dt.Rows(0).Item("ATTR_VALUE"))
                    If Not lst Is Nothing Then
                        drpTenderRestrict.SelectedIndex = -1
                        lst.Selected = True
                        lblOldTender.Text = dt.Rows(0).Item("ATTR_VALUE")
                    End If
                    ckActive.Checked = (dt.Rows(0).Item("ACTIVE_FL") = "Y")
                    lblOldStatus.Text = ckActive.Checked.ToString
                    lblOldActivity.Text = dt.Rows(0).Item("ACTIVITY")
                    pnlCustomerInfo.Visible = True
                Else
                    lblMessage.Text = "Customer " & txtCustomerNumber.Text & " doesn't exist!"
                    pnlCustomerInfo.Visible = False
                End If
            Catch ex As Exception
                sqladp.Dispose()
            End Try
        End If
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        'Check if comment field is filled in
        Dim valid As Boolean = True
        Dim valid1 As Boolean = True
        Dim valid2 As Boolean = True

        If valid = True Then
            If txtComments.Text.Trim = "" Then
                valid = False
                lblMessage.Text = "Please enter a comment for the update in the Comments Textbox!"
            End If
        End If
        'Check if there is a change in the 
        If lblOldTender.Text = drpTenderRestrict.SelectedValue Then
            valid1 = False
        End If
        If lblOldStatus.Text = ckActive.Checked.ToString Then
            valid2 = False
        End If
        If valid1 = False And valid2 = False Then
            valid = False
            lblMessage.Text = "There were no changes detected in the Status or Tender Restriction!"
        End If
        If valid = True Then
            Dim env As String = ConfigurationManager.AppSettings("env")
            Dim qry1 As String = "UPDATE dbo.ON_CUST_ATTR SET ATTR_VALUE = :NEWTENDER " & _
                                " WHERE TRIM(CU_NO) = :CU_NO " & _
                                " AND ATTR_ID = 'CU_ATTR6' "

            Dim qry2 As String = "INSERT INTO dbo.KILL_LOG (osuser,username,program,timestamp) " & _
                                 " VALUES (:osuser, :username, :program, sysdate) "

            Dim qry3 As String = "UPDATE dbo.ON_CUST_ATTR SET ATTR_VALUE = '6 = 941 Override' " & _
                                " WHERE TRIM(CU_NO) = :CU_NO " & _
                                " AND ATTR_ID = 'CU_ATTR5' "

            Dim qry4 As String = "UPDATE dbo.ON_CUST_MAS SET Active_fl = :STATUS " & _
                                 " WHERE TRIM(CU_NO) = :CU_NO "

            Dim qry5 As String = "UPDATE dbo.GLOB_CUST SET Active_fl = :STATUS " & _
                                 " WHERE TRIM(CU_NO) = :CU_NO "


            Dim conn As New OracleConnection(ConfigurationManager.ConnectionStrings(env).ToString)
            Dim cmd As New OracleCommand
            Dim trans As OracleTransaction = Nothing

            Try
                conn.Open()
                trans = conn.BeginTransaction
                cmd = conn.CreateCommand
                cmd.Transaction = trans

                Dim fstream As StreamWriter = Nothing
                Dim logPath As String = ConfigurationManager.AppSettings("LOGDIR")

                If valid1 = True Then
                    cmd.CommandText = qry1
                    cmd.Parameters.AddWithValue(":NEWTENDER", drpTenderRestrict.SelectedValue.Trim)
                    cmd.Parameters.AddWithValue(":CU_NO", lblCustomerNumber.Text.Trim)

                    If cmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error Updating new Tender Restriction for Customer: " & lblCustomerNumber.Text.Trim)
                    End If

                    'Log issue
                    cmd.CommandText = qry2
                    Dim userid As String = User.Identity.Name
                    With cmd.Parameters
                        .Clear()
                        .AddWithValue(":osuser", userid.Substring(userid.IndexOf("\") + 1))
                        .AddWithValue(":username", userid.Substring(userid.IndexOf("\") + 1))
                        .AddWithValue(":program", "TenderOverride")
                    End With

                    fstream = File.AppendText(logPath & "\TenderRestrictionOverride." & Now.ToString("yyyyMMdd") & ".log")
                    fstream.WriteLine(Now.ToLongTimeString & " " & userid.Substring(userid.IndexOf("\") + 1) & "," & lblCustomerNumber.Text.Trim & _
                         "," & lblOldTender.Text.Trim & "," & drpTenderRestrict.SelectedValue & "," & txtComments.Text.Trim)
                    fstream.Close()
                End If
                If valid2 = True Then
                    cmd.CommandText = qry3
                    cmd.Parameters.Clear()
                    cmd.Parameters.AddWithValue(":CU_NO", lblCustomerNumber.Text.Trim)

                    If cmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error Updating Override Status for Customer: " & lblCustomerNumber.Text.Trim)
                    End If

                    cmd.CommandText = qry4
                    cmd.Parameters.Clear()
                    cmd.Parameters.AddWithValue(":CU_NO", lblCustomerNumber.Text.Trim)
                    cmd.Parameters.AddWithValue(":STATUS", IIf(ckActive.Checked = True, "Y", "N"))

                    If cmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error Updating Override Status to table ON_CUST_MAS for Customer: " & lblCustomerNumber.Text.Trim)
                    End If

                    cmd.CommandText = qry5
                    cmd.Parameters.Clear()
                    cmd.Parameters.AddWithValue(":CU_NO", lblCustomerNumber.Text.Trim)
                    cmd.Parameters.AddWithValue(":STATUS", IIf(ckActive.Checked = True, "Y", "N"))

                    If cmd.ExecuteNonQuery <= 0 Then
                        Throw New Exception("Error Updating Override Status to table GLOB_CUST for Customer: " & lblCustomerNumber.Text.Trim)
                    End If

                    'Log issue
                    cmd.CommandText = qry2
                    Dim userid As String = User.Identity.Name
                    With cmd.Parameters
                        .Clear()
                        .AddWithValue(":osuser", userid.Substring(userid.IndexOf("\") + 1))
                        .AddWithValue(":username", userid.Substring(userid.IndexOf("\") + 1))
                        .AddWithValue(":program", "941Override")
                    End With

                    fstream = File.AppendText(logPath & "\TenderRestrictionOverride." & Now.ToString("yyyyMMdd") & ".log")
                    fstream.WriteLine(Now.ToLongTimeString & " " & userid.Substring(userid.IndexOf("\") + 1) & "," & lblCustomerNumber.Text.Trim & _
                         "," & lblOldStatus.Text.Trim & "," & ckActive.Checked.ToString & "," & txtComments.Text.Trim)
                    fstream.Close()
                End If

                trans.Commit()
                conn.Close()
                cmd.Dispose()
                lblMessage.Text = "Customer " & lblCustomerNumber.Text & " tender restriction updated to " & drpTenderRestrict.SelectedValue.Trim & " successfully!<br>" & _
                "NOTE: The status will be reset tommorrow if restriction is not changed at Head Office!"

                lblCustomerName.Text = ""
                lblOldTender.Text = ""
                lblCustomerNumber.Text = ""
                txtComments.Text = ""
                pnlCustomerInfo.Visible = False

            Catch ex As Exception

                trans.Rollback()
                If conn.State = ConnectionState.Open Then
                    conn.Close()
                End If

                cmd.Dispose()
                conn.Dispose()
                lblMessage.Text = "An error occured!!!! " & ex.Message.ToString

            End Try
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lblHeading As Label = Master.FindControl("lblMainTitle")
        If Not lblHeading Is Nothing Then
            lblHeading.Text = "Tender Restriction Override"
        End If
        lblMessage.Text = ""

        ''MOD: 2010-08-24 - request to limit access to TenderOverride User group to this page.
        'Dim id As IIdentity = Thread.CurrentPrincipal.Identity 'HttpContext.Current.User.Identity
        'Dim userid As String = UCase(id.Name).Replace("LCBO\", "")
        'Dim access As Boolean = False
        'Dim TenderOverrideUsers As String() = ConfigurationManager.AppSettings("TenderOverrideUsers").Split(",")


        'For Each TOUser As String In TenderOverrideUsers
        '    If UCase("LCBO\" & userid).Equals(UCase(TOUser)) Then
        '        'show menu item
        '        access = True
        '    End If
        'Next

        'If access = False Then
        '    Response.Redirect("Default.aspx")
        'End If
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("TenderRestrictionOverride.aspx")
    End Sub

   
End Class
