Imports System
Imports System.Data.OracleClient
Imports System.IO
Imports System.Web.SessionState
Imports System.IO.Directory
Imports System.Data
Imports System.Security.Principal
Imports System.Threading
Imports RSG_ROC.DataLayer
Imports RSG_ROC.Exceptions
Imports RSG_ROC.OrderClasses
Imports RSG_ROC.LS_TRANSFERS
Imports System.Text.RegularExpressions
Partial Public Class LocTransferNew
    Inherits System.Web.UI.Page
    Private Sub LocTransferNew_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Dim ex As Exception = Server.GetLastError
        Session("ExceptionObj") = ex
        Response.Redirect("~\ErrorPage.aspx")
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim lblheading As Label = Master.FindControl("lblMainTitle")
        If Not lblheading Is Nothing Then
            lblheading.Text = "New Licensee Location Transfer"
        End If

        If Not IsPostBack Then
            lnkLookUp.Attributes.Add("onclick", "showCustomerList('" & txtOrigCustNo.UniqueID & "' );return false;")
        End If

        lblMessage.Text = ""

    End Sub
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("~\LocTransferSearch.aspx")
    End Sub

    Private Sub lnkSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSearch.Click
        Response.Redirect("~\LocTransferSearch.aspx")
    End Sub
    Private Sub lnkNewTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNewTransfer.Click
        Response.Redirect("~\LocTransferNew.aspx")
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim valid_old As Boolean = False
        lblMessage.Text = ""

        Dim valid_cust As Boolean = False
        Dim valid_transfer As Boolean = False

        valid_cust = DALRoc.isValidOrigCust(txtOrigCustNo.Text.Trim)
        If valid_cust Then
            pnlNewCustomer.Visible = True
            pnlNewInfo.Visible = True
            valid_transfer = True
        Else
            lblMessage.Text = "Please enter a valid customer number."
            valid_transfer = False
        End If

        If IsDate(txtNewCustTransfer.Text.Trim) Then
            If Date.Compare(txtNewCustTransfer.Text.Trim, Date.Today) > 0 Then
                valid_transfer = True
            Else
                valid_transfer = False
                lblMessage.Text = "Please enter a date in the future."
            End If
        Else
            valid_transfer = False
            lblMessage.Text = "Please enter a valid transfer date."
        End If

        If txtNewCustContact.Text.Trim = "" Or txtNewCustEmail.Text.Trim = "" Then
            valid_transfer = False
            lblMessage.Text = "Please enter the Contact name and the corporate email."
        Else
            valid_transfer = True
        End If

        If valid_transfer Then
            Dim conn As OracleConnection = Nothing
            conn = New OracleConnection(DALRoc.getConnectionstring(ConfigurationManager.AppSettings("env")))
            Dim cmd As OracleCommand = New OracleCommand
            Dim trans As OracleTransaction = Nothing
            Dim rdr As OracleDataReader = Nothing
            Dim current_user As String = Session("CurrentUserId")

            Dim cust_no As String = ""
            Dim cust_name As String = ""
            Dim cust_add As String = ""
            Dim cust_city As String = ""
            Dim cust_prov As String = ""
            Dim cust_post As String = ""

            cust_no = txtNewCustNo.Text.Trim
            cust_name = txtNewCustName.Text.Trim
            cust_add = txtNewCustAddress.Text.Trim
            cust_city = txtNewCustCity.Text.Trim
            cust_prov = txtNewCustProvince.Text.Trim
            cust_post = txtNewCustPostal.Text.Trim

            Try
                Dim ls As New ROC_LE_SELECT_TRANSFER(txtOrigCustNo.Text.Trim, txtNewCustContact.Text.Trim, txtNewCustEmail.Text.Trim, current_user)

                conn.Open()
                trans = conn.BeginTransaction
                cmd.Connection = conn
                cmd.Transaction = trans

                ls.Insert(cmd)

                ls.new_customer_no = cust_no
                ls.new_cust_name = cust_name
                ls.new_cust_address = cust_add
                ls.new_cust_city = cust_city
                ls.new_cust_prov = cust_prov
                ls.new_cust_post_code = cust_post

                ls.UPDATE(cmd)

                cmd.Dispose()
                trans.Commit()
                conn.Close()
                Session("LocTransferSelectId") = ls.le_select_id

            Catch ex As Exception
                trans.Rollback()
                cmd.Dispose()
                conn.Close()
                Throw ex
            End Try

            Response.Redirect("~/LocTransferSearchDetails.aspx")
        End If

    End Sub
    Private Sub btnSetCustomer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSetCustomer.Click
        Dim valid_cust As Boolean = False
        Dim dr As DataRow = Nothing

        dr = DALRoc.getCustInfo(txtOrigCustNo.Text.Trim)

        lblOrigCustName.Text = dr("CU_NAME")
        lblOrigCustAddress.Text = dr("ADD1_SH") & " " & dr("ADD2_SH")
        lblOrigCustCity.Text = dr("CITY_SH")
        lblOrigCustProvince.Text = dr("PROV_SH")
        lblOrigCustPostal.Text = dr("POST_CODE")
        lblOrigCustPhone.Text = dr("PHONE_NU")
        lblStatus.Text = dr.Item("STATUS")

        valid_cust = DALRoc.isValidOrigCust(txtOrigCustNo.Text.Trim)

        If valid_cust Then
            pnlNewCustomer.Visible = True
            pnlNewInfo.Visible = True
            btnSave.Visible = True
        Else
            Dim dt As DataTable = DALRoc.getLeSelectTransfer("", dr("CU_NO"), dr("CU_NAME"), "", "", "", "", "", "", "", "", "", "", "", "")
            If dt.Rows.Count > 0 Then
                lblMessage.Text = "Licensee Location Transfer for this customer already exist id: " & dt.Rows(0).Item("LE_SELECT_ID")
            Else
                lblMessage.Text = "Please enter a valid customer number. Please check the status of this customer"
            End If

        End If
    End Sub
    'Private Sub txtOrigCustNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOrigCustNo.TextChanged
    '    Session("LocTransferNewCuNo") = txtOrigCustNo.Text
    'End Sub

    'Private Sub lnkLookUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLookUp.Click
    '    txtOrigCustNo.Text = Session("LocTransferCustomerNo")
    '    lblOrigCustName.Text = Session("LocTransferCustomerName")
    '    lblOrigCustAddress.Text = Session("LocTransferCustomerAdd")
    '    lblOrigCustCity.Text = Session("LocTransferCustomerCity")
    '    lblOrigCustProvince.Text = Session("LocTransferCustomerProv")
    '    lblOrigCustPostal.Text = Session("LocTransferCustomerPost")
    '    lblOrigCustPhone.Text = Session("LocTransferCustomerPhn")
    'End Sub
End Class