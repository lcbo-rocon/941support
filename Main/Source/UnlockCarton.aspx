<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Main.Master" CodeBehind="UnlockCarton.aspx.vb" Inherits="_941Support.UnlockCarton" 
    title="Unlock Carton" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
<table cellSpacing="0" cellPadding="6" align="center" bordercolor="#cbd39f" border="1" style="width: 460px; height: 100px;">
      <tr>
      <td valign="top" style="height: 78px">
      <table cellSpacing="0" cellPadding="1" align="center" style="width: 450px; height: 70px;">
            
      <tr>
            <td valign="top" align="left" style="height: 13px; width: 255px;" >                    
                    <table cellSpacing="0" cellPadding="0" align="left">
                    <tr>
                    <td align="left" valign="top" style="width: 256px;" >
                        <asp:Label ID="lblTicketId" runat="server" Text="Please enter the Tote Id to be unlocked:        (Format: 01234567)"  
                        ForeColor="Maroon" Width="245px" ></asp:Label> 
                    </td>
                    </tr>
                    </table>
            </td>
            
            <td valign="top" align="left" style="height: 13px; width: 195px;" >         
                    <table cellspacing="0" cellpadding="0" align="left">
                    <tr>  
                    <td align="left" valign="top" style="width: 196px" >
                        <asp:TextBox ID="txtTicketId" runat="server" Height="20px" Width="184px" TabIndex="1" MaxLength="20"></asp:TextBox></td>
                    </tr>
                    </table>
            </td>                                             
      </tr>    
                 
      <tr>          
            <td style="width: 255px"></td>
            <td align="right" style="width: 195px"> 
              <asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="72px" CssClass="BtnCss" TabIndex="2" />
              <asp:Button ID="btnClear" runat="server" Text="Clear" Width="72px" CssClass="BtnCss" TabIndex="3" CausesValidation="False" />&nbsp;
            </td>
      </tr>         
  </table>
</td>
</tr>
</table> 
          
     <br />
     <br />
     <br />     
     
<table cellspacing="0" cellpadding="0" align="center" border="0" style="width: 100%">
    <tr>
    <td align=center>
    <asp:Label ID="lblMessage" runat="server" CssClass="LblCss"></asp:Label>
    </td>
    </tr>
</table>
</asp:Content>
