<%@ Page Language="vb" AutoEventWireup="false" Title="Location Transfer Search" CodeBehind="LocTransferSearch.aspx.vb" MasterPageFile="~/Main.Master" Inherits="_941Support.LocTransferSearch" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Contents" runat="server">
<asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true" ShowSummary="false" />   
<table cellspacing="0" cellpadding="0" align="center" border="0" style="width: 90%;">

<tr>
    <td colspan="6" align="left">
    <asp:LinkButton ID="lnkSearch" runat="server" Text="Search" CssClass="HeadCss"></asp:LinkButton>
    &nbsp;
    <asp:LinkButton ID="lnkNewTransfer" runat="server" Text="New Transfer" CssClass="HeadCss"></asp:LinkButton>
    </td>
</tr>

<tr><td colspan="6"><br /></td></tr>

<tr>
    <td colspan="6">
    <asp:Label ID="lbl1" Text="Search for Licensee Location Transfers" runat="server" CssClass="WelcomeCss"></asp:Label>
    </td>
</tr>

<tr><td colspan="6"><br /></td></tr>

<tr>
    <td colspan="2">
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblTransferId" runat="server" Text="Location Transfer ID:"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtTransferId" runat="server" Width="100px"></asp:TextBox>
    <asp:CompareValidator ID="TransferValidator" runat="server" ControlToValidate="txtTransferId"
    Operator="DataTypeCheck" Type="Integer" ErrorMessage="Transfer ID should be numerical" Display="None"></asp:CompareValidator>
    </td>
    <td colspan="2">
    &nbsp;
    </td>    
</tr>

<tr>
    <td colspan="2">
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblOrigCustomerNo" runat="server" Text="Original Customer#:"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtOrigCustomerNo" runat="server" Width="100px"></asp:TextBox>
    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtOrigCustomerNo"
    Operator="DataTypeCheck" Type="Integer" ErrorMessage="Original Customer# should be numerical" Display="None"></asp:CompareValidator>
    </td>
     <td align="right" style="width: 157px">
    <asp:Label ID="lblNewCustomerNo" runat="server" Text="New Customer#:"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtNewCustomerNo" runat="server" Width="100px"></asp:TextBox>
    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtNewCustomerNo"
    Operator="DataTypeCheck" Type="Integer" ErrorMessage="New Customer# should be numerical" Display="None"></asp:CompareValidator>
    </td>    
</tr>

<tr>
    <td colspan="2">
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblOrigCustomerName" runat="server" Text="Original Customer Name:"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtOrigCustomerName" runat="server" Width="150px"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtOrigCustomerName"     
    ErrorMessage="Please enter minimum 3 characters for Original Customer Name"  Display="None" ValidationExpression="^[\w]{3,100}$" />
    </td>
     <td align="right" style="width: 157px">
    <asp:Label ID="lblNewCustomerName" runat="server" Text="New Customer Name:"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtNewCustomerName" runat="server" Width="150px"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtNewCustomerName"     
    ErrorMessage="Please enter minimum 3 characters for the New Customer Name"  Display="None" ValidationExpression="^[\w]{3,100}$" />
    </td>    
</tr>

<tr>
    <td colspan="2">
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblContactName" runat="server" Text="Contact Name:"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtContactName" runat="server" Width="150px"></asp:TextBox>
    <asp:RegularExpressionValidator ID="regexpName" runat="server" ControlToValidate="txtContactName"     
    ErrorMessage="Please enter minimum 3 characters for Contact Name"  Display="None" ValidationExpression="^[\w]{3,100}$" />
    </td>
    <td colspan="2">
    &nbsp;
    </td>    
</tr>

<tr>
    <td colspan="2">
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblCorpEmail" runat="server" Text="Corporate Email:"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtCorpEmail" runat="server" Width="150px"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtCorpEmail"     
    ErrorMessage="Please enter minimum 3 characters for Corporate Email"  Display="None" ValidationExpression="^[\w]{3,100}$" />
    </td>
    <td colspan="2">
    &nbsp;
    </td>    
</tr>

<tr><td colspan="6"><br /></td></tr>

<tr>
    <td colspan="2">
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblCreationFrom" runat="server" Text="Creation Date From(YYYY-MM-DD):"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtCreationFrom" runat="server" Width="100px"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegValidator1" runat="server" ControlToValidate="txtCreationFrom" 
    ErrorMessage="Invalid Creation Date From" Display="None" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator>
    <asp:CompareValidator ID="CmpValidator1" runat="server" ControlToValidate="txtCreationFrom"
    Operator="DataTypeCheck" Type="Date" ErrorMessage="Invalid Creation Date From" Display="None"></asp:CompareValidator>
    <a id="a1" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtCreationFrom, 'a1', 'yyyy-MM-dd');return false;"
	href="#" name="a1"><img id="Img1" alt="calendar" height="21" src="Images/calendar.gif" width="34" align="top" border="0" /></a>
    </td>
    <td align="right" style="width: 157px">
    <asp:Label ID="lblCreationTo" runat="server" Text="Creation Date To:" CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtCreationTo" runat="server" Width="100px"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegValidator2" runat="server" ControlToValidate="txtCreationTo" 
    ErrorMessage="Invalid Creation Date To" Display="None" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator>
    <asp:CompareValidator ID="CmpValidator2" runat="server" ControlToValidate="txtCreationTo"
    Operator="DataTypeCheck" Type="Date" ErrorMessage="Invalid Creation Date To" Display="None"></asp:CompareValidator>    
    <a id="a2" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtCreationTo, 'a1', 'yyyy-MM-dd');return false;"
	href="#" name="a2"><img id="Img2" height="21" src="Images/calendar.gif" width="34" align="top" border="0"></a>
    </td>  
</tr>

<tr>
    <td colspan="2">
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblTransferFrom" runat="server" Text="Expected Transfer Date(YYYY-MM-DD):"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtTransferFrom" runat="server" Width="100px"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegValidator3" runat="server" ControlToValidate="txtTransferFrom" 
    ErrorMessage="Invalid Creation Date From" Display="None" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator>
    <asp:CompareValidator ID="CmpValidator3" runat="server" ControlToValidate="txtTransferFrom"
    Operator="DataTypeCheck" Type="Date" ErrorMessage="Invalid Creation Date To" Display="None"></asp:CompareValidator>    
    <a id="a3" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtTransferFrom, 'a1', 'yyyy-MM-dd');return false;"
	href="#" name="a3"><img id="Img3" height="21" src="Images/calendar.gif" width="34" align="top" border="0"></a>
    </td>
    <td align="right" style="width: 157px">
    <asp:Label ID="lblTransferTo" runat="server" Text="Expected Transfer To:" CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtTransferTo" runat="server" Width="100px"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegValidator4" runat="server" ControlToValidate="txtTransferTo" 
    ErrorMessage="Invalid Transfer Date From" Display="None" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator>
    <asp:CompareValidator ID="CmpValidator4" runat="server" ControlToValidate="txtTransferTo"
    Operator="DataTypeCheck" Type="Date" ErrorMessage="Invalid Transfer Date To" Display="None"></asp:CompareValidator> 
    <a id="a4" onclick="objCal.select(document.forms['aspnetForm'].ctl00$Contents$txtTransferTo, 'a1', 'yyyy-MM-dd');return false;"
	href="#" name="a4"><img id="Img4" height="21" src="Images/calendar.gif" width="34" align="top" border="0"></a>
    </td>  
</tr>

<tr><td colspan="6"><br /></td></tr>

<tr>
    <td colspan="2">
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblReturnOrder" runat="server" Text="Return Order#:"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtReturnOrder" runat="server" Width="100px"></asp:TextBox>
    <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtReturnOrder"
    Operator="DataTypeCheck" Type="Integer" ErrorMessage="Return Order# should be numerical" Display="None"></asp:CompareValidator>
    </td>
    <td colspan="2">
    &nbsp;
    </td>    
</tr>

<tr>
    <td colspan="2">
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblTransferOrder" runat="server" Text="Transfer Order#:"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtTransferOrder" runat="server" Width="100px"></asp:TextBox>
    <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtTransferOrder"
    Operator="DataTypeCheck" Type="Integer" ErrorMessage="Transfer Order# should be numerical" Display="None"></asp:CompareValidator>
    </td>
    <td colspan="2">
    &nbsp;
    </td>    
</tr>

<tr>
    <td colspan="2">
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblPreparedBy" runat="server" Text="Prepared By:"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:TextBox ID="txtPreparedBy" runat="server" Width="100px"></asp:TextBox>
    </td>
    <td colspan="2">
    &nbsp;
    </td>    
</tr>

<tr>
    <td colspan="2">
    &nbsp;
    </td>
    <td align="right">
    <asp:Label ID="lblStatus" runat="server" Text="Status:"  CssClass="LabelCss"></asp:Label>&nbsp;
    </td>
    <td align="left">
    <asp:DropDownList ID="drpStatus" runat="server">
        <asp:ListItem Value="0">All</asp:ListItem>
        <asp:ListItem Value="In Progress">In Progress</asp:ListItem>
        <asp:ListItem Value="Complete">Complete</asp:ListItem>
    </asp:DropDownList>
    </td>
    <td colspan="2">
    &nbsp;
    </td>    
</tr>

<tr>
    <td colspan="3">&nbsp;</td>
    <td colspan="3" align="right">
    <asp:Button ID="btnSearch" runat="server" CssClass="BtnCss" Text="Search" />
    <asp:Button ID="btnReset" runat="server" CssClass="BtnCss" Text="Reset" />
    &nbsp;
    </td>
</tr>

<tr><td colspan="6">&nbsp;</td></tr>

<tr>
    <td colspan="6" align="center">
    <asp:Label ID="lblMessage" Text="" runat="server" CssClass="LabelCss"></asp:Label>
    </td>
</tr>

<tr>
    <td colspan="6" align="center" >
        <div style="z-index: 10; overflow: auto;height:250px">
            <asp:GridView ID="grdForwarding" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            BackColor="White" BorderColor="#CBD49E" BorderStyle="None" BorderWidth="1px" 
            CellPadding="4" Font-Size="Small" Font-Names="Arial" GridLines="Vertical" ForeColor="Black" DataKeyNames="Le_Select_Id"   >
            <FooterStyle BackColor="#CCCC99" />
            <RowStyle BackColor="#F7F7DE" Font-Size="Small" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CBD49E" Font-Bold="True" ForeColor="White" BorderStyle="None" />
            <HeaderStyle BackColor="#CBD49E" Font-Size="Small" ForeColor="Maroon" Font-Bold="True" />            
                <Columns>                                   
                    <asp:TemplateField HeaderText="Transfer Id" SortExpression="Le_Select_Id">
                        <ItemTemplate>
                        <asp:LinkButton ID="lnkForward" runat="server" Text='<%# Bind("Le_Select_Id") %>'
                        CommandArgument='<%# Bind("Le_Select_Id") %>' CommandName="SelectLS"></asp:LinkButton>                        
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Orig_Customer_No" HeaderText="Orig Cust#" SortExpression="Orig_Customer_No" />
                    <asp:BoundField DataField="Orig_Cust_Name" HeaderText="Orig Cust Name" SortExpression="Orig_Cust_Name" />
                    <asp:BoundField DataField="New_Customer_No" HeaderText="New Cust#" SortExpression="New_Customer_No" />
                    <asp:BoundField DataField="New_Cust_Name" HeaderText="New Cust Name" SortExpression="New_Cust_Name" />
                    <asp:BoundField DataField="Creation_Dt" DataFormatString="{0:d}" HeaderText="Creation Date" SortExpression="Creation_Dt" />
                    <asp:BoundField DataField="Expected_Transfer_Dt" DataFormatString="{0:d}" HeaderText="Expected Transfer Date" SortExpression="Expected_Transfer_Dt" />
                    <asp:BoundField DataField="Le_Select_Contact_Name" HeaderText="Contact Name" SortExpression="Le_Select_Contact_Name" />
                    <asp:BoundField DataField="Le_Select_Corporate_Email" HeaderText="Corporate Email" SortExpression="Le_Select_Corporate_Email" />
                    <asp:BoundField DataField="Le_Select_Return_No" HeaderText="Return Ord#" SortExpression="Le_Select_Return_No" />
                    <asp:BoundField DataField="Le_Select_Order_No" HeaderText="Transfer Ord#" SortExpression="Le_Select_Order_No" />
                    <asp:BoundField DataField="Prepared_By" HeaderText="Prepared By" SortExpression="Prepared_By" />
                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                </Columns>
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        </div>       
    </td>
</tr>
<tr><td colspan="6">&nbsp;</td></tr>


</table>

</asp:Content>

